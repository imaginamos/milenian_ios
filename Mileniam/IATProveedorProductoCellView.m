//
//  IATProveedorProductoCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 15/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATProveedorProductoCellView.h"
#import "IATProducto.h"

@implementation IATProveedorProductoCellView
+(NSString *) cellId{
    return NSStringFromClass(self);
}
+(CGFloat)cellHeight{
    return 60.0f;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    
    self.logoProveedor.image = nil;
    
}

-(void)imageWithUrl:(NSURL *)urlImage producto:(IATProducto *)producto block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        
        if (self.producto == producto) {
            NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.producto == producto) {
                    
                    UIImage *image = [UIImage imageWithData:imageData];
                    completionBlock(image);
                }
                else{
                    completionBlock(nil);
                }
                
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil);
            });
        }
        
    });
}

-(void)imageWithUrl:(NSURL *)urlImage producto:(IATProducto *)producto cache:(NSCache *)cache block:(void (^)(UIImage *image))completionBlock{
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        
        if (self.producto == producto) {
            NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
            UIImage *image = [UIImage imageWithData:imageData];
            if (image) {
                [cache setObject:image forKey:urlImage];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.producto == producto) {
                    completionBlock(image);
                }
                else{
                    completionBlock(nil);
                }
                
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil);
            });
        }
        
    });
}

@end

#import "IATPromoProveedor.h"

@interface IATPromoProveedor ()

// Private interface goes here.

@end

@implementation IATPromoProveedor

+(instancetype) promoProveedorWithPrecio:(NSDecimalNumber *)precio
                               promocion:(IATPromocion *)promocion
                               proveedor:(IATProveedor *)proveedor
                                 context:(NSManagedObjectContext *)context{
    
    IATPromoProveedor *nPromoProveedor = [NSEntityDescription insertNewObjectForEntityForName:[IATPromoProveedor entityName]
                                                               inManagedObjectContext:context];
    
    nPromoProveedor.precio = precio;
    nPromoProveedor.promocion = promocion;
    nPromoProveedor.proveedor = proveedor;
    
    return nPromoProveedor;
}

+(instancetype) obtenerPromoProvedorWithPromo:(IATPromocion *)promocion
                                    proveedor:(IATProveedor *)proveedor
                                      context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPromoProveedor entityName]];
    
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"promocion == %@ AND proveedor == %@",promocion, proveedor];
    
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

+(NSArray *) obtenerPreciosOrdenadoPorMenor:(IATPromocion *)promocion
                                    context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPromoProveedor entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promocion = %@", promocion];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"precio" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array.count > 0)
    {
        return array;
    }
    
    return nil;
    
}

@end

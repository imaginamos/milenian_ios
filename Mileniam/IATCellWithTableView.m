//
//  IATCellWithTableView.m
//  Milenian
//
//  Created by Desarrollador IOS on 27/07/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATCellWithTableView.h"
#import "IATPedidoCellView.h"
#import "IATItemPedido.h"
#import "IATProducto.h"
#import "IATPromocion.h"
#import "IATProveedor.h"
#import "ItemSolicitudVC.h"
#import "ItemSolicitudPromoVC.h"
#import "IATCantidadProducto.h"

@implementation IATCellWithTableView


-(void)asignarDelegado{
    
    //Registrando celda personalizada
    UINib *cellNib = [UINib nibWithNibName:@"IATPedidoCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATPedidoCellView cellId]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    
}

+(CGFloat)cellHeight{
    return 70.0f;
}

-(CGFloat)cellH{
    if (self.productos.count > 0) {
        return 80.0f * self.productos.count;
    }
    return 0;
}

+(NSString *) cellId{
    return NSStringFromClass(self);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


#pragma mark <Delegate & Datasource UITableView)
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productos.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IATItemPedido *item = [self.productos objectAtIndex:indexPath.row];
    IATPedidoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATPedidoCellView cellId]];
    
    if (item.producto != nil) {
        cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.producto.nombre];
        cell.lblMarca.text = item.producto.marca;
    }
    else{
        cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.promocion.nombre];
        
        //cell.lblMarca.text = item.producto.marca;
    }
    
    cell.lblUnidad.text = [NSString stringWithFormat:@"Cantidad: %@", item.cantidad];
    cell.lblProveedor.text = [NSString stringWithFormat:@"%@", item.proveedor.nombre];
    cell.lblPrecioItem.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[item.precioTotal doubleValue]]];
    
    if (cell.imgProducto.image == nil) {
        
        NSURL *urlImg;
        if (item.producto != nil) {
            IATProducto *producto = item.producto;
            urlImg = [NSURL URLWithString:producto.urlImagen];
        }
        else{
            IATPromocion *promocion = item.promocion;
            urlImg = [NSURL URLWithString:promocion.urlImagen];
        }
        
        UIImage *imgProducto = [self.cache objectForKey:urlImg];
        if (imgProducto) {
            cell.imgProducto.image = imgProducto;
            [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
            [cell.activityLoad startAnimating];
            
            cell.imgProducto.image = [UIImage imageNamed:@"page"];
            [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFill];
            [self imageWithUrl:urlImg block:^(UIImage *image) {
                if (image != nil) {
                    cell.imgProducto.image = image;
                    [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
                }
                [cell.activityLoad stopAnimating];
            }];
        }
        
    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
        IATItemPedido *itemPedido = [self.productos objectAtIndex:indexPath.row];
        if (itemPedido.promocion == nil) {
            ItemSolicitudVC *itemPedidoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"itemSolicitudVC"];
    
            itemPedidoVC.itemPedido = itemPedido;
            [self.navController pushViewController:itemPedidoVC animated:YES];
            //[self.navigationController presentViewController:itemPedidoVC animated:YES completion:nil];
        }
        else{
            ItemSolicitudPromoVC *itemPedidoPromoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"itemSolicitudPromoVC"];
    
            itemPedidoPromoVC.itemPedido = itemPedido;
            [self.navController pushViewController:itemPedidoPromoVC animated:YES];
            //[self.navigationController presentViewController:itemPedidoPromoVC animated:YES completion:nil];
            
        }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATPedidoCellView cellHeight];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //90 - 203 - 227
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 25)];
    headerView.backgroundColor = [UIColor colorWithRed:90/255.0f green:203/255.0f blue:227/255.0f alpha:1.0f];
    
    UILabel *nombreCategoria =
    [[UILabel alloc] initWithFrame:CGRectMake(10, 3, self.tableView.frame.size.width-20, 25)];
    [nombreCategoria setTextColor:[UIColor whiteColor]];
    [nombreCategoria setText:[NSString stringWithFormat:@"%@", self.mayorista]];
    [nombreCategoria setFont:[UIFont fontWithName:@"Arial" size:16]];
    [nombreCategoria setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:nombreCategoria];
    
    return headerView;
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //Tomo el elemento a elimar antes de que sea quitado
        IATItemPedido *deleteItem = [self.productos objectAtIndex:indexPath.row];
        
        //Lo remuevo de la tabla
        [self.productos removeObjectAtIndex:indexPath.row];
        
        //Reseteo el item de Cantidad por producto
        deleteItem.producto.cantidadPedido.cantidad = 0;
        
        //Lo elimino de base de datos
        [self.context deleteObject:deleteItem];
        if([deleteItem isDeleted]) {
            
            NSError *errorInfo = nil;
            
            if([self.context save:&errorInfo]){
                
                NSLog(@"Actualizado el contexto;");
            }
            else{
                NSLog(@"Error al eliminar: %@", errorInfo);
            }
        }
        
        //Refresco la tabla
        [tableView reloadData];
        
        //if (self.productos.count == 0) {
            NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_TABLA_PADRE
                                                              object:self
                                                            userInfo:nil];
            
            [[NSNotificationCenter defaultCenter] postNotification:n];
        //}
        
        
    }
    
    
    
}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}


#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}

-(void)prepareForReuse{
    
    self.productos = nil;
    
}



@end

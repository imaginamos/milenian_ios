//
//  PromocionVC.h
//  Milenian
//
//  Created by Carlos Obregón on 18/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVFoundation;
@class IATPromocion;

@interface PromocionVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IATPromocion *promocionSeleccionada;

@property (strong, nonatomic) AVAudioPlayer* soundAdd;
@property (strong, nonatomic) AVAudioPlayer* soundRemove;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;
@property (weak, nonatomic) IBOutlet UIImageView *imgPromo;
@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblMarca;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaInicio;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaFinal;
@property (weak, nonatomic) IBOutlet UILabel *lblUnidad;

@property (weak, nonatomic) IBOutlet UILabel *lblPrecioTotal;
@property (weak, nonatomic) IBOutlet UITextField *tfCantidad;
@property (weak, nonatomic) IBOutlet UITableView *listaProveedores;


- (IBAction)sumarUnaPromo:(id)sender;
- (IBAction)restarUnaPromo:(id)sender;
- (IBAction)agregarAlPedido:(id)sender;


@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATItemAcuerdo.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATAcuerdo;
@class IATProducto;

@interface IATItemAcuerdoID : NSManagedObjectID {}
@end

@interface _IATItemAcuerdo : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATItemAcuerdoID*objectID;

@property (nonatomic, strong, nullable) NSNumber* cantidad;

@property (atomic) int32_t cantidadValue;
- (int32_t)cantidadValue;
- (void)setCantidadValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* consumido;

@property (atomic) int32_t consumidoValue;
- (int32_t)consumidoValue;
- (void)setConsumidoValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* porcentaje;

@property (nonatomic, strong) IATAcuerdo *acuerdo;

@property (nonatomic, strong) IATProducto *producto;

@end

@interface _IATItemAcuerdo (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCantidad;
- (void)setPrimitiveCantidad:(NSNumber*)value;

- (int32_t)primitiveCantidadValue;
- (void)setPrimitiveCantidadValue:(int32_t)value_;

- (NSNumber*)primitiveConsumido;
- (void)setPrimitiveConsumido:(NSNumber*)value;

- (int32_t)primitiveConsumidoValue;
- (void)setPrimitiveConsumidoValue:(int32_t)value_;

- (NSString*)primitivePorcentaje;
- (void)setPrimitivePorcentaje:(NSString*)value;

- (IATAcuerdo*)primitiveAcuerdo;
- (void)setPrimitiveAcuerdo:(IATAcuerdo*)value;

- (IATProducto*)primitiveProducto;
- (void)setPrimitiveProducto:(IATProducto*)value;

@end

@interface IATItemAcuerdoAttributes: NSObject 
+ (NSString *)cantidad;
+ (NSString *)consumido;
+ (NSString *)porcentaje;
@end

@interface IATItemAcuerdoRelationships: NSObject
+ (NSString *)acuerdo;
+ (NSString *)producto;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPreItemAcuerdo.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATProducto;
@class IATVariedad;

@interface IATPreItemAcuerdoID : NSManagedObjectID {}
@end

@interface _IATPreItemAcuerdo : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATPreItemAcuerdoID*objectID;

@property (nonatomic, strong, nullable) NSNumber* cantidad;

@property (atomic) int32_t cantidadValue;
- (int32_t)cantidadValue;
- (void)setCantidadValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSDecimalNumber* precio;

@property (nonatomic, strong) IATProducto *producto;

@property (nonatomic, strong) IATVariedad *variedad;

@end

@interface _IATPreItemAcuerdo (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCantidad;
- (void)setPrimitiveCantidad:(NSNumber*)value;

- (int32_t)primitiveCantidadValue;
- (void)setPrimitiveCantidadValue:(int32_t)value_;

- (NSDecimalNumber*)primitivePrecio;
- (void)setPrimitivePrecio:(NSDecimalNumber*)value;

- (IATProducto*)primitiveProducto;
- (void)setPrimitiveProducto:(IATProducto*)value;

- (IATVariedad*)primitiveVariedad;
- (void)setPrimitiveVariedad:(IATVariedad*)value;

@end

@interface IATPreItemAcuerdoAttributes: NSObject 
+ (NSString *)cantidad;
+ (NSString *)precio;
@end

@interface IATPreItemAcuerdoRelationships: NSObject
+ (NSString *)producto;
+ (NSString *)variedad;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATAcuerdo.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATItemAcuerdo;
@class IATProveedor;

@interface IATAcuerdoID : NSManagedObjectID {}
@end

@interface _IATAcuerdo : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATAcuerdoID*objectID;

@property (nonatomic, strong, nullable) NSString* comentarios;

@property (nonatomic, strong, nullable) NSString* estado;

@property (nonatomic, strong, nullable) NSDate* fechaFinal;

@property (nonatomic, strong, nullable) NSDate* fechaInicio;

@property (nonatomic, strong, nullable) NSString* idAcuerdo;

@property (nonatomic, strong, nullable) NSString* porcentaje;

@property (nonatomic, strong, nullable) NSSet<IATItemAcuerdo*> *itemsAcuerdo;
- (nullable NSMutableSet<IATItemAcuerdo*>*)itemsAcuerdoSet;

@property (nonatomic, strong) IATProveedor *provedor;

@end

@interface _IATAcuerdo (ItemsAcuerdoCoreDataGeneratedAccessors)
- (void)addItemsAcuerdo:(NSSet<IATItemAcuerdo*>*)value_;
- (void)removeItemsAcuerdo:(NSSet<IATItemAcuerdo*>*)value_;
- (void)addItemsAcuerdoObject:(IATItemAcuerdo*)value_;
- (void)removeItemsAcuerdoObject:(IATItemAcuerdo*)value_;

@end

@interface _IATAcuerdo (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveComentarios;
- (void)setPrimitiveComentarios:(NSString*)value;

- (NSString*)primitiveEstado;
- (void)setPrimitiveEstado:(NSString*)value;

- (NSDate*)primitiveFechaFinal;
- (void)setPrimitiveFechaFinal:(NSDate*)value;

- (NSDate*)primitiveFechaInicio;
- (void)setPrimitiveFechaInicio:(NSDate*)value;

- (NSString*)primitiveIdAcuerdo;
- (void)setPrimitiveIdAcuerdo:(NSString*)value;

- (NSString*)primitivePorcentaje;
- (void)setPrimitivePorcentaje:(NSString*)value;

- (NSMutableSet<IATItemAcuerdo*>*)primitiveItemsAcuerdo;
- (void)setPrimitiveItemsAcuerdo:(NSMutableSet<IATItemAcuerdo*>*)value;

- (IATProveedor*)primitiveProvedor;
- (void)setPrimitiveProvedor:(IATProveedor*)value;

@end

@interface IATAcuerdoAttributes: NSObject 
+ (NSString *)comentarios;
+ (NSString *)estado;
+ (NSString *)fechaFinal;
+ (NSString *)fechaInicio;
+ (NSString *)idAcuerdo;
+ (NSString *)porcentaje;
@end

@interface IATAcuerdoRelationships: NSObject
+ (NSString *)itemsAcuerdo;
+ (NSString *)provedor;
@end

NS_ASSUME_NONNULL_END

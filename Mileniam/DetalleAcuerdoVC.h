//
//  DetalleAcuerdoVC.h
//  Milenian
//
//  Created by Carlos Obregón on 18/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IATAcuerdo;

@interface DetalleAcuerdoVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IATAcuerdo *acuerdo;

@property (weak, nonatomic) IBOutlet UITableView *listAcuerdo;
@property (weak, nonatomic) IBOutlet UIImageView *ivLogoProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblNombreProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblTiempoAcuerdo;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidadProductos;
@property (weak, nonatomic) IBOutlet UITextView *tvComentarioAcuerdo;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;


@end

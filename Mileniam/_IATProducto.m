// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATProducto.m instead.

#import "_IATProducto.h"

@implementation IATProductoID
@end

@implementation _IATProducto

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Producto" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Producto";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Producto" inManagedObjectContext:moc_];
}

- (IATProductoID*)objectID {
	return (IATProductoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"esFavoritoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"esFavorito"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"esRecomendadoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"esRecomendado"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic esFavorito;

- (BOOL)esFavoritoValue {
	NSNumber *result = [self esFavorito];
	return [result boolValue];
}

- (void)setEsFavoritoValue:(BOOL)value_ {
	[self setEsFavorito:@(value_)];
}

- (BOOL)primitiveEsFavoritoValue {
	NSNumber *result = [self primitiveEsFavorito];
	return [result boolValue];
}

- (void)setPrimitiveEsFavoritoValue:(BOOL)value_ {
	[self setPrimitiveEsFavorito:@(value_)];
}

@dynamic esRecomendado;

- (BOOL)esRecomendadoValue {
	NSNumber *result = [self esRecomendado];
	return [result boolValue];
}

- (void)setEsRecomendadoValue:(BOOL)value_ {
	[self setEsRecomendado:@(value_)];
}

- (BOOL)primitiveEsRecomendadoValue {
	NSNumber *result = [self primitiveEsRecomendado];
	return [result boolValue];
}

- (void)setPrimitiveEsRecomendadoValue:(BOOL)value_ {
	[self setPrimitiveEsRecomendado:@(value_)];
}

@dynamic marca;

@dynamic unidad;

@dynamic urlImagen;

@dynamic cantidadPedido;

@dynamic itemsAcuerdo;

- (NSMutableSet<IATItemAcuerdo*>*)itemsAcuerdoSet {
	[self willAccessValueForKey:@"itemsAcuerdo"];

	NSMutableSet<IATItemAcuerdo*> *result = (NSMutableSet<IATItemAcuerdo*>*)[self mutableSetValueForKey:@"itemsAcuerdo"];

	[self didAccessValueForKey:@"itemsAcuerdo"];
	return result;
}

@dynamic itemsLista;

- (NSMutableSet<IATItemLista*>*)itemsListaSet {
	[self willAccessValueForKey:@"itemsLista"];

	NSMutableSet<IATItemLista*> *result = (NSMutableSet<IATItemLista*>*)[self mutableSetValueForKey:@"itemsLista"];

	[self didAccessValueForKey:@"itemsLista"];
	return result;
}

@dynamic itemsPedido;

- (NSMutableSet<IATItemPedido*>*)itemsPedidoSet {
	[self willAccessValueForKey:@"itemsPedido"];

	NSMutableSet<IATItemPedido*> *result = (NSMutableSet<IATItemPedido*>*)[self mutableSetValueForKey:@"itemsPedido"];

	[self didAccessValueForKey:@"itemsPedido"];
	return result;
}

@dynamic preItemsAcuerdo;

@dynamic precioProvedor;

- (NSMutableSet<IATProductoProveedor*>*)precioProvedorSet {
	[self willAccessValueForKey:@"precioProvedor"];

	NSMutableSet<IATProductoProveedor*> *result = (NSMutableSet<IATProductoProveedor*>*)[self mutableSetValueForKey:@"precioProvedor"];

	[self didAccessValueForKey:@"precioProvedor"];
	return result;
}

@dynamic variedad;

@end

@implementation IATProductoAttributes 
+ (NSString *)esFavorito {
	return @"esFavorito";
}
+ (NSString *)esRecomendado {
	return @"esRecomendado";
}
+ (NSString *)marca {
	return @"marca";
}
+ (NSString *)unidad {
	return @"unidad";
}
+ (NSString *)urlImagen {
	return @"urlImagen";
}
@end

@implementation IATProductoRelationships 
+ (NSString *)cantidadPedido {
	return @"cantidadPedido";
}
+ (NSString *)itemsAcuerdo {
	return @"itemsAcuerdo";
}
+ (NSString *)itemsLista {
	return @"itemsLista";
}
+ (NSString *)itemsPedido {
	return @"itemsPedido";
}
+ (NSString *)preItemsAcuerdo {
	return @"preItemsAcuerdo";
}
+ (NSString *)precioProvedor {
	return @"precioProvedor";
}
+ (NSString *)variedad {
	return @"variedad";
}
@end


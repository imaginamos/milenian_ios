//
//  TerminosVC.h
//  Milenian
//
//  Created by MacBook Pro on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TerminosVC : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITextView *tvTerminosCondiciones;

@end

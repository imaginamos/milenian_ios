#import "_IATPreItemAcuerdo.h"
@class IATProveedor;

@interface IATPreItemAcuerdo : _IATPreItemAcuerdo

+(instancetype) preItemAcuerdoWithCantidad:(NSNumber *)cantidad
                                  producto:(IATProducto *)producto proveedor:(IATProveedor *)proveedor
                                  variedad:(IATVariedad *)variedad
                                    precio:(NSDecimalNumber *)precio
                                   context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerPreItemsAcuerdoWithContext:(NSManagedObjectContext *)context;
+(void) deleteAllWithContext:(NSManagedObjectContext *)context;


@end

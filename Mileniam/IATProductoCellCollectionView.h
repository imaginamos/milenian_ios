//
//  IATProductoCellCollectionView.h
//  Milenian
//
//  Created by Carlos Obregón on 11/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@import AVFoundation;
@class IATCantidadProducto;

#define ACTUALIZAR_SUBTOTAL @"ActualizarSubtotal"
#define ACTUALIZAR_SUBTOTAL_REMOVE @"ActualizarSubtotalDisminuir"

@interface IATProductoCellCollectionView : UICollectionViewCell<UIGestureRecognizerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property NSCache *cache;

@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorito;
@property (weak, nonatomic) IBOutlet UIImageView *imgAcuerdo;
@property (weak, nonatomic) IBOutlet UIImageView *imgCarrito;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidad;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;
@property (strong, nonatomic) IATCantidadProducto *cantProducto;
@property (strong, nonatomic) NSDecimalNumber *subTotal;
@property (retain, nonatomic) UILongPressGestureRecognizer *pressLong;
@property (retain, nonatomic) UITapGestureRecognizer *tap;

@property (strong, nonatomic) AVAudioPlayer* soundAdd;
@property (strong, nonatomic) AVAudioPlayer* soundRemove;

+(NSString *)cellId;

- (IBAction)addOne:(id)sender;
- (IBAction)removeOne:(id)sender;
- (IBAction)marcarFavorito:(id)sender;

-(void) observeCantidadProducto:(IATCantidadProducto *) cantProducto;
@end

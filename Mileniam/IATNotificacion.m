#import "IATNotificacion.h"

@interface IATNotificacion ()

// Private interface goes here.

@end

@implementation IATNotificacion

+(instancetype) notificacionWithId:(NSString *)idNotificacion
                            nombre:(NSString *)nombre
                           mensaje:(NSString *)mensaje
                             fecha:(NSString *)fecha
                           context:(NSManagedObjectContext *)context{
    
    IATNotificacion *noti =
    [NSEntityDescription insertNewObjectForEntityForName:[IATNotificacion entityName]
                                  inManagedObjectContext:context];
    
    
    noti.idNotificacion = idNotificacion;
    noti.nombre = nombre;
    noti.message = mensaje;
    noti.date = fecha;
    
    return noti;
    
}


+(instancetype) obtenerNotificacionWithId:(NSString *)idNoti
                                  context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATNotificacion entityName]];
    
    NSPredicate *filtrarPorId = [NSPredicate predicateWithFormat:@"idNotificacion == %@",idNoti];
    [fetch setPredicate:filtrarPorId];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) obtenerNotificacionWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATNotificacion entityName]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
    
}

@end

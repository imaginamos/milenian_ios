// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATProductoProveedor.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATProducto;
@class IATProveedor;
@class IATSubcategoria;
@class IATVariedad;

@interface IATProductoProveedorID : NSManagedObjectID {}
@end

@interface _IATProductoProveedor : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATProductoProveedorID*objectID;

@property (nonatomic, strong, nullable) NSDecimalNumber* precio;

@property (nonatomic, strong) IATProducto *producto;

@property (nonatomic, strong) IATProveedor *provedor;

@property (nonatomic, strong) IATSubcategoria *subcategoria;

@property (nonatomic, strong, nullable) IATVariedad *variedad;

@end

@interface _IATProductoProveedor (CoreDataGeneratedPrimitiveAccessors)

- (NSDecimalNumber*)primitivePrecio;
- (void)setPrimitivePrecio:(NSDecimalNumber*)value;

- (IATProducto*)primitiveProducto;
- (void)setPrimitiveProducto:(IATProducto*)value;

- (IATProveedor*)primitiveProvedor;
- (void)setPrimitiveProvedor:(IATProveedor*)value;

- (IATSubcategoria*)primitiveSubcategoria;
- (void)setPrimitiveSubcategoria:(IATSubcategoria*)value;

- (IATVariedad*)primitiveVariedad;
- (void)setPrimitiveVariedad:(IATVariedad*)value;

@end

@interface IATProductoProveedorAttributes: NSObject 
+ (NSString *)precio;
@end

@interface IATProductoProveedorRelationships: NSObject
+ (NSString *)producto;
+ (NSString *)provedor;
+ (NSString *)subcategoria;
+ (NSString *)variedad;
@end

NS_ASSUME_NONNULL_END

//
//  SubCategoriasVC.h
//  Milenian
//
//  Created by MacBook Pro on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@class IATCategoria;
#import "iCarousel.h"


@interface SubCategoriasVC : UIViewController<iCarouselDataSource, iCarouselDelegate, UITableViewDelegate, UITableViewDataSource, UIPopoverControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblSubcategorias;
@property (strong, nonatomic) IATCategoria *categoriaSeleccionada;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UITableView *listSubcategories;


@end

#import "IATLista.h"

@interface IATLista ()

// Private interface goes here.

@end

@implementation IATLista

+(instancetype) ListaWithNombre:(NSString *)nombre
                        context:(NSManagedObjectContext *)context{
    
    IATLista *nLista = [NSEntityDescription insertNewObjectForEntityForName:[IATLista entityName]
                                                         inManagedObjectContext:context];
    
    nLista.nombre = nombre;
    
    
    return nLista;
    
}

+(instancetype) obtenerAcuerdoWithNombre:(NSString *)nombre
                                 context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATLista entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nombre == %@",nombre];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) obtenerListasWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATLista entityName]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}

@end

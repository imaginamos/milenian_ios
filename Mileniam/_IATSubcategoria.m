// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATSubcategoria.m instead.

#import "_IATSubcategoria.h"

@implementation IATSubcategoriaID
@end

@implementation _IATSubcategoria

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Subcategoria" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Subcategoria";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Subcategoria" inManagedObjectContext:moc_];
}

- (IATSubcategoriaID*)objectID {
	return (IATSubcategoriaID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic urlImagen;

@dynamic categoria;

@dynamic productosProvedores;

- (NSMutableSet<IATProductoProveedor*>*)productosProvedoresSet {
	[self willAccessValueForKey:@"productosProvedores"];

	NSMutableSet<IATProductoProveedor*> *result = (NSMutableSet<IATProductoProveedor*>*)[self mutableSetValueForKey:@"productosProvedores"];

	[self didAccessValueForKey:@"productosProvedores"];
	return result;
}

@dynamic variedades;

- (NSMutableSet<IATVariedad*>*)variedadesSet {
	[self willAccessValueForKey:@"variedades"];

	NSMutableSet<IATVariedad*> *result = (NSMutableSet<IATVariedad*>*)[self mutableSetValueForKey:@"variedades"];

	[self didAccessValueForKey:@"variedades"];
	return result;
}

@end

@implementation IATSubcategoriaAttributes 
+ (NSString *)urlImagen {
	return @"urlImagen";
}
@end

@implementation IATSubcategoriaRelationships 
+ (NSString *)categoria {
	return @"categoria";
}
+ (NSString *)productosProvedores {
	return @"productosProvedores";
}
+ (NSString *)variedades {
	return @"variedades";
}
@end


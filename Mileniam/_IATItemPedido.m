// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATItemPedido.m instead.

#import "_IATItemPedido.h"

@implementation IATItemPedidoID
@end

@implementation _IATItemPedido

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ItemPedido" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ItemPedido";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ItemPedido" inManagedObjectContext:moc_];
}

- (IATItemPedidoID*)objectID {
	return (IATItemPedidoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cantidadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cantidad"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cantidad;

- (int32_t)cantidadValue {
	NSNumber *result = [self cantidad];
	return [result intValue];
}

- (void)setCantidadValue:(int32_t)value_ {
	[self setCantidad:@(value_)];
}

- (int32_t)primitiveCantidadValue {
	NSNumber *result = [self primitiveCantidad];
	return [result intValue];
}

- (void)setPrimitiveCantidadValue:(int32_t)value_ {
	[self setPrimitiveCantidad:@(value_)];
}

@dynamic precioTotal;

@dynamic pedido;

@dynamic producto;

@dynamic promocion;

@dynamic proveedor;

@end

@implementation IATItemPedidoAttributes 
+ (NSString *)cantidad {
	return @"cantidad";
}
+ (NSString *)precioTotal {
	return @"precioTotal";
}
@end

@implementation IATItemPedidoRelationships 
+ (NSString *)pedido {
	return @"pedido";
}
+ (NSString *)producto {
	return @"producto";
}
+ (NSString *)promocion {
	return @"promocion";
}
+ (NSString *)proveedor {
	return @"proveedor";
}
@end


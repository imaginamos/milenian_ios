#import "IATSubcategoria.h"

@interface IATSubcategoria ()

// Private interface goes here.

@end

@implementation IATSubcategoria


+(instancetype) subcategoriaWithId:(NSString *)idSubcategoria
                            nombre:(NSString *)nombre
                       descripcion:(NSString *)descripcion
                          urlImage:(NSString *)urlImage
                         categoria:(IATCategoria *)categoria
                           context:(NSManagedObjectContext *)context{
    
    IATSubcategoria *nSubcategoria = [NSEntityDescription insertNewObjectForEntityForName:[IATSubcategoria entityName]
                                                             inManagedObjectContext:context];
    
    nSubcategoria.idEntity = idSubcategoria;
    nSubcategoria.nombre = nombre;
    nSubcategoria.descripcion = descripcion;
    nSubcategoria.urlImagen = urlImage;
    nSubcategoria.categoria = categoria;
    
    
    return nSubcategoria;
    
}

+(NSArray *) obtenerSubcategoriasWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATSubcategoria entityName]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array > 0)
    {
        return array;
    }
    
    return nil;
    
}

+(instancetype) obtenerSubCategoriaWithId:(NSString *)idSubCategoria
                                  context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATSubcategoria entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idEntity == %@",idSubCategoria];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

@end

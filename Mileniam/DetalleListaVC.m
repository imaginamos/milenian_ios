//
//  DetalleListaVC.m
//  Milenian
//
//  Created by Carlos Obregón on 31/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "DetalleListaVC.h"
#import "IATLista.h"
#import "IATPedido.h"
#import "IATItemPedido.h"
#import "IATItemLista.h"
#import "ProductosListasTVC.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATPedidoCellView.h"
#import "IATProveedor.h"
#import "IATProducto.h"
#import "DetalleItemListaVC.h"
#import "CacheImgs.h"

@interface DetalleListaVC ()
@property (nonatomic, strong) NSArray *productos;
@property (nonatomic, strong) NSMutableArray *itemsList;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation DetalleListaVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = self.lista.nombre;
    self.cache = [CacheImgs sharedInstance];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    self.precioLista.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.lista.precioTotal doubleValue]]];
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(agregarProducto:)];
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    self.itemsLista.delegate = self;
    self.itemsLista.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATPedidoCellView" bundle:nil];
    [self.itemsLista registerNib:cellNib forCellReuseIdentifier:[IATPedidoCellView cellId]];
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.itemsList = [NSMutableArray arrayWithArray:[self.lista.items allObjects]];
    [self.itemsLista reloadData];
    
    //Calcular valor de la lista
    self.lista.precioTotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    for (IATItemLista *item in self.itemsList) {
        self.lista.precioTotal = [self.lista.precioTotal decimalNumberByAdding:item.precio];
        self.precioLista.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.lista.precioTotal doubleValue]]];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATItemLista *item = [self.itemsList objectAtIndex:indexPath.row];
    IATPedidoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATPedidoCellView cellId]];
    
    if (item.producto != nil) {
        cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.producto.nombre];
        cell.lblMarca.text = item.producto.marca;
    }

    
    cell.lblUnidad.text = [NSString stringWithFormat:@"Cantidad: %@", item.cantidad];
    cell.lblProveedor.text = [NSString stringWithFormat:@"%@", item.proveedor.nombre];
    cell.lblPrecioItem.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[item.precio doubleValue]]];
  
    if (cell.imgProducto.image == nil){
        cell.imgProducto.image = [UIImage imageNamed:@"page"];
        [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFill];
        
        NSURL *urlImg = [NSURL URLWithString:item.producto.urlImagen];
        UIImage *imgProducto = [self.cache objectForKey:urlImg];
        if (imgProducto) {
            cell.imgProducto.image = imgProducto;
            [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
            [cell.activityLoad startAnimating];
            [self imageWithUrl:urlImg block:^(UIImage *image) {
                if (image != nil) {
                    cell.imgProducto.image = image;
                    [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
                }
                [cell.activityLoad stopAnimating];
            }];
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATPedidoCellView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //Tomo el elemento a elimar antes de que sea quitado
        IATLista *deleteItemLista = [self.itemsList objectAtIndex:indexPath.row];
        
        //Lo remuevo de la tabla
        [self.itemsList removeObjectAtIndex:indexPath.row];
        
        //Lo elimino de base de datos
        [self.context deleteObject:deleteItemLista];
        if([deleteItemLista isDeleted]) {
            
            NSError *errorInfo = nil;
            
            if([self.context save:&errorInfo]){
                
                NSLog(@"Actualizado el contexto;");
            }
            else{
                NSLog(@"Error al eliminar: %@", errorInfo);
            }
        }
        
        //Refresco la tabla
        [tableView reloadData];
        [self calcularTotal];
        
        if (self.itemsList.count == 0) {
            self.lista = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    }
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DetalleItemListaVC *itemListaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detalleItemListaVC"];
    
    IATItemLista *itemLista = [self.itemsList objectAtIndex:indexPath.row];
    itemListaVC.itemLista = itemLista;
    
    [self.navigationController presentViewController:itemListaVC animated:YES completion:nil];
}

-(void)agregarProducto:(id)sender{
    
    ProductosListasTVC *busquedaProductos = [self.storyboard instantiateViewControllerWithIdentifier:@"productosListasTVC"];
    
    busquedaProductos.lista = self.lista;
    
    [self.navigationController pushViewController:busquedaProductos animated:YES];
    
}

- (IBAction)agregarAlPedido:(id)sender {
    //Preguntar si existe algun pedido en proceso
    IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
    if (pedido == nil) {
        pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
    }
    
    
    //Ingresar items de pedido
    for (IATItemLista *item in self.itemsList){
        
        
        NSDecimalNumber *cantidad = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item.cantidad]];
        
        NSDecimalNumber *precioItemPedido = item.precio;
        
        IATItemPedido *itemPedido =
        [IATItemPedido obtenerItemPedidoWithProducto:item.producto
                                             context:self.context];
        if (itemPedido == nil) {
            itemPedido = [IATItemPedido itemPedidoWithCantidad:item.cantidad producto:item.producto pedido:pedido precio:precioItemPedido context:self.context];
        }
        else{
            itemPedido.cantidad = cantidad;
            itemPedido.precioTotal = precioItemPedido;
            itemPedido.pedido = pedido;
        }
        
        itemPedido.proveedor = item.proveedor;
        
        
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Lista Agregada"
                                                                   message:@"Lista agregada al pedido"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *accion =
    [UIAlertAction actionWithTitle:@"Aceptar"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               [alert dismissViewControllerAnimated:YES completion:nil];
                               [self.navigationController popViewControllerAnimated:YES];
                           }];
    
    [alert addAction:accion];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
    });
}

-(void)calcularTotal{
    
    self.lista.precioTotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    for (IATItemLista *item in self.itemsList) {
        self.lista.precioTotal = [self.lista.precioTotal decimalNumberByAdding:item.precio];
        self.precioLista.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.lista.precioTotal doubleValue]]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

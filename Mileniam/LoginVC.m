//
//  ViewController.m
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "LoginVC.h"
#import "MenuVC.h"
#import "SWRevealViewController.h"
#import "CategoriesVC.h"
#import "NSString+MD5.h"
#import "MBProgressHUD.h"
#import "Api.h"
#import "AppDelegate.h"
#import "IATCategoria.h"
#import "IATSubcategoria.h"
#import "IATVariedad.h"
#import "IATProducto.h"
#import "IATProveedor.h"
#import "IATPromocion.h"
#import "IATProductoProveedor.h"
#import "IATPromoProveedor.h"
#import "IATAcuerdo.h"
#import "IATItemAcuerdo.h"
#import "IATPedido.h"
#import "IATItemPedido.h"
#import "IATNotificacion.h"
#import "IATSectorEconomico.h"
#import "AGTSimpleCoreDataStack.h"


@interface LoginVC ()
@property(nonatomic) int movementOfView;
@property(nonatomic, strong) MBProgressHUD *hud;
@property(nonatomic, strong) AGTSimpleCoreDataStack *model;
@property(nonatomic, strong) NSDictionary *parameters;
@property(nonatomic) int typeUser;
@end

@implementation LoginVC

#pragma mark - Ciclo de Vida
-(void)viewDidLoad{
    [super viewDidLoad];
    
    //Gesto para ocultar el teclado
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ocultaTeclado:)];
    [tapGesture setNumberOfTouchesRequired:1];
    [[self view] addGestureRecognizer:tapGesture];
    
    
    //Cargando datos de inicio del ultimo usuario
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastEmail = [defaults stringForKey:@"kLastEmail"];
    if (lastEmail != nil && ![lastEmail isEqualToString:@""]) {
        self.txtEmail.text = lastEmail;
        NSString *lastPass = [defaults stringForKey:@"kLastPass"];
        self.txtPassword.text = lastPass;
    }
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.model = localVar.model;
    
}

#pragma mark - Login
-(IBAction)login:(id)sender{
    
    if (self.txtEmail.text.length > 0 && self.txtPassword.text.length > 0 )
    {
        //Pop up de carga
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.label.text = @"Iniciando...";
        [self enableControls:NO];
        
        //secondTime: Conocer si es primera vez que se ejecuta la app
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL secondTime = [defaults boolForKey:@"kSecondTime"];
        
        //token para enviar al Cms y poder recibir notificaciones
        NSString *token = [defaults objectForKey:@"uuid"];
        
        //lastUser: Conocer si es el ultimo usuario que ejecuto la app
        NSString *lastEmail = [defaults stringForKey:@"kLastEmail"];
        BOOL lastUser = NO;
        if ([lastEmail isEqualToString:self.txtEmail.text]){
            lastUser = YES;
        }
        
        //Comprobar fecha de ingreso, para actualizar contenidos
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd/MM/YYYY"];
        NSString *dateNow =[dateformate stringFromDate:[NSDate date]];
        NSString *lastDateLogin = [defaults stringForKey:@"kDateLastLogin"];
        BOOL isSameDate = NO;
        if ([dateNow isEqualToString:lastDateLogin]){
            isSameDate = YES;
        }
        
        //Capturando email
        NSString *loginEmail = self.txtEmail.text;
        
        //Capturando y generando Hash para el password
        NSString *passwithcookie = [NSString stringWithFormat:@"%@%@", COOKIE_KEY, self.txtPassword.text];
        NSString *hash = [[NSString alloc] generateMD5:passwithcookie];
        
        //Armar parametros de petición de logueo
        NSDictionary *params = @{@"email":loginEmail, @"passwd":hash, @"firstTime": @"1", @"typeMobile":@"1", @"idMobile":token};
        /*if (secondTime && lastUser){
            NSString *lastEmail = [defaults stringForKey:@"kEmail"];
            if ([lastEmail isEqualToString:self.txtEmail.text]){
                params = @{@"email":loginEmail, @"passwd": hash, @"firstTime": @"0"};
            }
        }*/
        
        
        Api *api = [Api sharedInstance];

        NSLog(@"Enviando peticion de logueo...");
        [api userLogin:params success:^(BOOL success, id result) {
            if(success){
                NSDictionary *dictionary = (NSDictionary *)result;
                if([dictionary[@"Acceso"] isEqualToString:@"1"]) {
                    NSLog(@"Usuario logueado...");
                    NSLog(@"Login response: %@", dictionary);
                    if (secondTime && isSameDate && lastUser){
                        
                        //Actualizar datos de acceso
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
                        [dateformate setDateFormat:@"dd/MM/YYYY"];
                        NSString *dateLogin =[dateformate stringFromDate:[NSDate date]];
                        [defaults setObject:dateLogin forKey:@"kDateLastLogin"];
                        [defaults synchronize];
                        
                        //Pasando al Menu principal
                        SWRevealViewController *revealVC = [self.storyboard instantiateViewControllerWithIdentifier:@"revealVC"];
                        [self presentViewController:revealVC animated:YES completion:^{
                            NSLog(@"Pasando al menu");
                        }];
                    }
                    else{
                        if (!lastUser) {
                            //Limpiar BD
                            [self.model zapAllData];
                            //self.model = [AGTSimpleCoreDataStack coreDataStackWithModelName:@"Model"];
                        }
                        
                        //Averiguar tipo de usuario
                        if ([[dictionary objectForKey:@"typeUser"] intValue]) {
                            self.typeUser = [[dictionary objectForKey:@"typeUser"] intValue];
                            
                            if (self.typeUser == USER_CLIENT) {
                                NSLog(@"El usuario es un cliente");
                            }
                            else if (self.typeUser == USER_MILENIAN){
                                NSLog(@"El usuario es un empleado de Milenian");
                            }
                            else if (self.typeUser == USER_PROVIDER){
                                NSLog(@"El usuario es un empleado de un mayorista");
                            }
                            else{
                                NSLog(@"Fracasamos, tipo de usuario no identificado o no enviado");
                            }
                            
                        }
                        
                        //Pop up, informar proceso al usuario
                        self.hud.label.text = @"Guardando datos de usuario...";
                        
                        //Guardar datos del usuario
                        [self saveUser:dictionary];
                        
                        //Descarga de datos del Cms y almacenamiento local
                        [self loadData];
                        
                    }
                }
                else{
                    UIAlertController *alert =
                    [UIAlertController alertControllerWithTitle:@"Error"
                                                        message:@"Datos invalidos, por favor verifique su email y contraseña"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *accion =
                    [UIAlertAction actionWithTitle:@"Aceptar"
                                             style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action) {
                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                           }];
                    
                    [alert addAction:accion];
                    
                    self.hud.hidden = YES;
                    [self enableControls:YES];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                               message:@"Conexión fallida"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                               }];
                [alert addAction:accion];
                
                self.hud.hidden = YES;
                [self enableControls:YES];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
        
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Datos incompletos"
                                                                       message:@"Debe completar cada campo requerido"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Cerrar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action){
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
   
}

#pragma mark - Cargar modelo BD
-(void)loadData{
    
    Api *api =[Api sharedInstance];
    
    //Construccion de diccionario con parametros para peticiones al CMS
    
    //Obtener KeySesion, IDCustomer y fecha actual.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *keySesion = [defaults stringForKey:@"kKey"];
    NSString *idCustomer = [defaults stringForKey:@"kIdCustomer"];
    NSString *typeUser = [defaults stringForKey:@"kTypeUser"]; 
    NSString *idEmployee = [defaults stringForKey:@"kIdEmployee"]; 

    NSDate* now = [NSDate date];
    
    //Opcional
    //NSDictionary *parametersConten = @{@"keys":keySesion, @"idCustomer":idCustomer};
    
    //Advertencia: Al implementar las actualizaciones,
    //en el parametro "firstime" debe asignarse la constante comentada a continuacion(@"ksecondTime")
    //BOOL secondTime = [defaults boolForKey:@"kSecondTime"];    
    
    if ([idEmployee length] == 0) {
        idEmployee = @"";
    }
//    idEmployee = @"";
    NSDictionary *parameters =
    @{@"firstTime":@"1", @"dateRequest":now ,@"keys":keySesion , @"idCustomer":idCustomer, @"typeUser":typeUser, @"idEmployee":idEmployee };
//    @{@"firstTime":@"1", @"dateRequest":now ,@"keys":keySesion , @"idCustomer":idCustomer};

    NSLog(@"parameters %@",parameters);
    
    //Hacer estos parametros globales
    self.parameters = parameters;
    
    //Inicio descargas de datos del CMS
    if (self.typeUser != USER_CLIENT) {
        [api obtenerClientes:parameters success:^(BOOL success, id response) {
            if(success){
                NSLog(@"Guardando clientes");
                [self saveCustomers:response];
            }
            else{
                NSLog(@"Fallo descarga JSON de clientes");
            }
        }];
    }
    
    
    self.hud.label.text = @"Descargando contenidos...";
    NSLog(@"Descargando Terminos y condiciones");
    [api obtenerContent:parameters success:^(BOOL success, id response) {
        if(success){
            NSLog(@"Guardando Terminos y condiciones");
            [self saveContent:response];
        }
        else{
            NSLog(@"Fallo descarga JSON de Terminos y condiciones");
        }
    }];
    
    NSLog(@"Descargando notificaciones");
    [api obtenerNotificaciones:parameters success:^(BOOL success, id response) {
        if(success){
            NSLog(@"Guardando notificaciones");
            [self saveNotificaciones:response];
        }
        else{
            NSLog(@"Fallo descarga JSON de Notificaciones");
        }
    }];
    
    NSLog(@"Descargando tipologia");
    // [api obtenerTipologia:parameters success:^(BOOL success, id response) {
    //     if(success){
    //         NSLog(@"Guardando tipologia");
    //         [self saveTipologias:response];
    //     }
    //     else{
    //         NSLog(@"Fallo cargar JSON de Tipologias");
    //     }
    // }];
    
    NSLog(@"Descargando categorias, subcategorias y variedades...");
    [api loadCategories:parameters success:^(BOOL success, id response){
        if(success){
            NSLog(@"Guardando categorias, subcategorias y variedades...");
            [self saveCategorias:response];
        }
        else{
            NSLog(@"No se descargaron categorias, subcategorias y variedades");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Conexión fallida"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];


    NSLog(@"Descargando tipologia");
    [api obtenerTipologia:parameters success:^(BOOL success, id response) {
        if(success){
            NSLog(@"Guardando tipologia");
            [self saveTipologias:response];
        }
        else{
            NSLog(@"Fallo cargar JSON de Tipologias");
        }
    }];
}
-(void)loadProveedores{
    
    Api *api =[Api sharedInstance];
    
    NSLog(@"Descargando proveedores...");
    [api loadProveedores:self.parameters success:^(BOOL success, id response){
        if(success){
            NSLog(@"Guardando proveedores...");
            [self saveProveedores:response];
        }
        else{
            NSLog(@"No se descargaron los proveedores");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Conexión fallida"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}
-(void)loadProductos{
    
    Api *api =[Api sharedInstance];
    
    NSLog(@"Descargando productos...");
    [api loadProductos:self.parameters success:^(BOOL success, id response) {
        if(success){
            NSLog(@"Guardando productos...");
            [self saveProductos:response];
        }
        else{
            NSLog(@"No se descargaron los productos");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Conexión fallida"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}
-(void)loadPromociones{
    
    Api *api =[Api sharedInstance];
    
    NSLog(@"Descargando promociones...");
    [api loadPromociones:self.parameters success:^(BOOL success, id response){
        if(success){
            NSLog(@"Guardando promociones...");
            [self savePromociones:response];
        }
        else{
            NSLog(@"No se descargaron los promociones");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Conexión fallida"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}
-(void)loadAcuerdos{
    
    Api *api =[Api sharedInstance];
    
    NSLog(@"Descargando acuerdos...");
    [api obtenerAcuerdos:self.parameters success:^(BOOL success, id response){
        if(success){
            NSLog(@"Guardando acuerdos...");
            [self saveAcuerdos:response];
        }
        else{
            NSLog(@"No se descargaron los acuerdos");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Conexión fallida"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}
-(void)loadPedidos{
    
    Api *api =[Api sharedInstance];
    
    NSLog(@"Descargando pedidos...");
    [api obtenerPedidos:self.parameters success:^(BOOL success, id response){
        if(success){
            NSLog(@"Guardando pedidos...");
            [self savePedidos:response];
        }
        else{
            NSLog(@"No se descargaron los pedidos");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Conexión fallida"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}

#pragma mark - Guardar Data
-(void)saveCategorias:(NSDictionary *)data{

    NSArray *Categorias = [data objectForKey:@"Categories"];
    __block NSString *idCategoria, *nameCategoria, *imgCategoria;

    NSArray *Subcategorias = [data objectForKey:@"SubCategories"];
    __block NSString *idSubCat, *nameSubCat, *imgSubCat, *idParentSubCat;
    
    NSArray *Variedades = [data objectForKey:@"Varieties"];
    __block NSString *idVariedad, *nameVariedad, *idParentVariedad;
    
    // crear un cola
    dispatch_queue_t saveCategories = dispatch_queue_create("categories", 0);
    
    dispatch_async(saveCategories, ^{
        
        [Categorias enumerateObjectsUsingBlock: ^(id objeto, NSUInteger indice, BOOL *stop) {
            
            idCategoria = [objeto objectForKey:@"IdCategory"];
            nameCategoria = [objeto objectForKey:@"Name"];
            imgCategoria = [objeto objectForKey:@"Imagen"];
            
            if ([IATCategoria obtenerCategoriaWithId:idCategoria context:self.model.context] == nil) {
                [IATCategoria categoriaWithId:idCategoria nombre:nameCategoria descripcion:nil urlImage:imgCategoria context:self.model.context];
            }
        }];
        
        [Subcategorias enumerateObjectsUsingBlock: ^(id objeto, NSUInteger indice, BOOL *stop) {
            idSubCat = [objeto objectForKey:@"IdCategory"];
            nameSubCat = [objeto objectForKey:@"Name"];
            imgSubCat = [objeto objectForKey:@"Imagen"];
            idParentSubCat = [objeto objectForKey:@"IdParent"];
            
            IATCategoria *categoriaPadre = [IATCategoria obtenerCategoriaWithId:idParentSubCat
                                                                        context:self.model.context];
            
            if ([IATSubcategoria obtenerSubCategoriaWithId:idSubCat context:self.model.context] == nil) {
                [IATSubcategoria subcategoriaWithId:idSubCat
                                             nombre:nameSubCat
                                        descripcion:nil
                                           urlImage:imgSubCat
                                          categoria:categoriaPadre
                                            context:self.model.context];
            }
        }];
        
        [Variedades enumerateObjectsUsingBlock: ^(id objeto, NSUInteger indice, BOOL *stop) {
            idVariedad = [objeto objectForKey:@"IdCategory"];
            nameVariedad = [objeto objectForKey:@"Name"];
            idParentVariedad = [objeto objectForKey:@"IdParent"];
            
            IATSubcategoria *subCategoriaPadre = [IATSubcategoria obtenerSubCategoriaWithId:idParentVariedad context:self.model.context];
            if ([IATVariedad obtenerVariedadWithId:idVariedad context:self.model.context] == nil) {
                [IATVariedad variedadWithId:idVariedad nombre:nameVariedad descripcion:nil subcategoria:subCategoriaPadre context:self.model.context];
            }
        }];
        
        // Volvemos a cola principal
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadProveedores];
        });
    });
    
}

-(void)saveCustomers:(NSDictionary *)data {
    
    NSLog(@"saveCustomers +");

    NSLog(@"saveCustomers %@",data);
   
    NSLog(@"saveCustomer -");

}

-(void)saveProveedores:(NSDictionary *)data{
    
    // crear un cola
    dispatch_queue_t saveProvedores = dispatch_queue_create("proveedores", 0);
    
    NSArray *Proveedores = [data objectForKey:@"Provider"];
    __block NSString *idProveedor, *nombre, *direccion, *telefono, *urllogo;
    
    dispatch_async(saveProvedores, ^{
        [Proveedores enumerateObjectsUsingBlock: ^(id objeto, NSUInteger indice, BOOL *stop) {
            idProveedor = [self changeNullToString:[objeto objectForKey:@"IdShop"]];
            direccion = [self changeNullToString:[objeto objectForKey:@"Address"]];
            nombre = [self changeNullToString:[objeto objectForKey:@"Name"]];
            telefono = [self changeNullToString:[objeto objectForKey:@"Phone"]];
            urllogo = [self changeNullToString:[objeto objectForKey:@"Image"]];
            //descripcion = [self changeNullToString:[objeto objectForKey:@"Description"]];
            
            if ([IATProveedor obtenerProveedorWithId:idProveedor context:self.model.context] == nil) {
                [IATProveedor proveedorWithId:idProveedor
                                       nombre:nombre
                                  descripcion:nil
                                    direccion:direccion
                                     telefono:telefono
                                      urlLogo:urllogo
                                      context:self.model.context];
            }
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadProductos];
        });
    });
    
}
-(void)saveProductos:(NSDictionary *)data{
    
    self.hud.label.text = @"Guardando productos...";
    dispatch_queue_t saveProductos = dispatch_queue_create("productos", 0);
    
    NSArray *dProductos = [data objectForKey:@"Products"];
    __block NSDictionary *definicion = [NSDictionary dictionary];
    __block NSString *idProduct, *descripcion, *nombre, *fabricante, *urlProducto, *idParent;
    __block NSNumber *esRecomendado = [NSNumber new];
    __block NSArray *parents = [NSArray array];
    __block NSDictionary *idParents = [NSDictionary dictionary];
    __block IATVariedad *variedadPadre;
    __block IATSubcategoria *subcategoria;
    __block NSArray *precios = [NSArray array];
    __block IATProducto *producto;
    
    dispatch_async(saveProductos, ^{
        [dProductos enumerateObjectsUsingBlock: ^(id objeto, NSUInteger indice, BOOL *stop) {
            definicion = [objeto objectForKey:@"Definition"];
            
            idProduct = [definicion objectForKey:@"IdProduct"];
            descripcion = [definicion objectForKey:@"Description"];
            nombre = [definicion objectForKey:@"Name"];
            fabricante = [definicion objectForKey:@"Manufacter"];
            urlProducto = [objeto objectForKey:@"Images"];
            
            esRecomendado = [objeto objectForKey:@"Recommend"];
            
            parents = [objeto objectForKey:@"Category"];
            
            if(parents.count > 0){
                
                if ([parents objectAtIndex:0]) {
                    //Averiguar Padre del producto
                    idParents = [parents objectAtIndex:0];
                    idParent = [idParents objectForKey:@"IdCategory"];
                    variedadPadre = [IATVariedad obtenerVariedadWithId:idParent context:self.model.context];
                    
                    subcategoria = variedadPadre.subcategoria;
                    
                    if (variedadPadre != nil  && idProduct != nil && ![idProduct isEqualToString:@""] && nombre != nil && ![nombre isEqualToString:@""]) {
                        
                        //Verificar si hay precios asignados y proveedores validos
                        precios = [objeto objectForKey:@"ShopPrice"];
                        
                        if (precios.count > 0) {
                            
                            if ([self validarPrecioPorProveedor:precios]) {
                                
                                producto =[IATProducto obtenerProductoWithId:idProduct context:self.model.context];
                                if (producto == nil) {
                                    producto = [IATProducto productoWithId:idProduct nombre:nombre descripcion:descripcion unidad:nil marca:fabricante urlImage:urlProducto variedad:variedadPadre  esRecomendado:esRecomendado context:self.model.context];
                                    
                                }
                                
                                [self saveProductoPorProveedor:producto variedad:variedadPadre subcategoria:subcategoria precios:precios];
                                
                            }
                            
                        }
                        
                    }
                }
                
            }
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadPromociones];
        });
    });
    
}
-(void)saveProductoPorProveedor:(IATProducto *)producto
                       variedad:(IATVariedad *)variedad
                   subcategoria:(IATSubcategoria *)subcategoria
                        precios:(NSArray *)precios{

    __block IATProveedor *proveedor;
    __block NSDecimalNumber *precio;
    __block IATProductoProveedor *productoProveedor;
    [precios enumerateObjectsUsingBlock: ^(id objeto, NSUInteger indice, BOOL *stop) {
        proveedor = [IATProveedor obtenerProveedorWithId:[objeto objectForKey:@"IdShop"] context:self.model.context];
        
        if (proveedor != nil) {
            precio = [NSDecimalNumber decimalNumberWithString:[objeto objectForKey:@"Price"]];
            
            
            productoProveedor =
            [IATProductoProveedor obtenerProductoProvedorWithProducto:producto proveedor:proveedor context:self.model.context];
            if (productoProveedor == nil) {
                [IATProductoProveedor productoProveedorWithPrecio:precio producto:producto proveedor:proveedor variedad:variedad subcategoria:subcategoria context:self.model.context];
            }
            else{
                if (![precio isEqual:productoProveedor.precio]) {
                    productoProveedor.precio = precio;
                }
            }
            
        }
    }];
    
}
-(void)savePromociones:(NSDictionary *)data{
    
    NSDictionary *promociones = [data objectForKey:@"Promotions"];
    
    dispatch_queue_t savePromos = dispatch_queue_create("promos", 0);
    
    dispatch_async(savePromos, ^{
        
        for (id objeto in promociones) {
            
            NSDictionary *definition = [objeto objectForKey:@"Definition"];
            NSArray *parents = [objeto objectForKey:@"Category"];
            NSDictionary *parent = [parents objectAtIndex:0];
            NSString *idParent = [parent objectForKey:@"IdCategory"];
            IATCategoria *categoria = [IATCategoria obtenerCategoriaWithId:idParent context:self.model.context];
            
            if (categoria != nil) {
                NSString *idPromocion, *nombre, *urlImage, *descripcion, *unidad, *idProduct, *fechaInicio, *fechaFinal;
                
                
                idPromocion = [self changeNullToString:[definition objectForKey:@"IdPromotion"]];
                nombre = [self changeNullToString:[definition objectForKey:@"Name"]];
                descripcion = [self changeNullToString:[definition objectForKey:@"Description"]];
                urlImage = [self changeNullToString:[definition objectForKey:@"ImgUrl"]];
                unidad = [self changeNullToString:[definition objectForKey:@"Unity"]];
                idProduct = [self changeNullToString:[definition objectForKey:@"IdProduct"]];
                fechaInicio = [self changeNullToString:[definition objectForKey:@"DateStart"]];
                fechaFinal = [self changeNullToString:[definition objectForKey:@"DateEnd"]];
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *inicioFecha = [dateFormat dateFromString:fechaInicio];
                NSDate *finalFecha = [dateFormat dateFromString:fechaFinal];
                
                NSArray *preciosxproveedor = [objeto objectForKey:@"ShopPrice"];
                
                if (preciosxproveedor.count > 0) {
                    
                    if ([self validarPrecioPorProveedor:preciosxproveedor]) {
                        IATPromocion * promocion = [IATPromocion obtenerPromocionWithId:idPromocion context:self.model.context];
                        
                        if (promocion == nil) {
                            promocion = [IATPromocion promocionWithId:idPromocion
                                                               nombre:nombre
                                                          descripcion:descripcion
                                                         fechaInicial:inicioFecha
                                                           fechaFinal:finalFecha
                                                               unidad:unidad
                                                             urlImage:urlImage
                                                              context:self.model.context];
                            promocion.idProducto = idProduct;
                            promocion.categoria = categoria;
                        }
                        
                        [self savePromoPorProveedor:promocion precios:preciosxproveedor];
                        
                    }
                }
            }
            
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadAcuerdos];
        });
    });
}
-(void)savePromoPorProveedor:(IATPromocion *)promocion
                     precios:(NSArray *)precios{
    
    for (id objeto in precios) {
        
        IATProveedor *proveedor = [IATProveedor obtenerProveedorWithId:[objeto objectForKey:@"IdShop"] context:self.model.context];
        
        if (proveedor != nil) {
            NSDecimalNumber *precio = [NSDecimalNumber decimalNumberWithString:[objeto objectForKey:@"Price"]];
            
            IATPromoProveedor *promoProveedor = [IATPromoProveedor obtenerPromoProvedorWithPromo:promocion proveedor:proveedor context:self.model.context];
            if (promoProveedor == nil) {
                [IATPromoProveedor promoProveedorWithPrecio:precio promocion:promocion proveedor:proveedor context:self.model.context];
            }
            else{
                if (![precio isEqual:promoProveedor.precio]) {
                    promoProveedor.precio = precio;
                }
            }
        }
        
    }
    
}
-(void)saveAcuerdos:(NSDictionary *)data{
    
    NSDictionary *acuerdos = [data objectForKey:@"Agreements"];
    
    dispatch_queue_t saveAcuerdos = dispatch_queue_create("acuerdos", 0);
    
    dispatch_async(saveAcuerdos, ^{
        
        for (id objeto in acuerdos){
            
            NSDictionary *definicion = [objeto objectForKey:@"Definition"];
            NSString *comentario = [definicion objectForKey:@"Comment"];
            NSString *idAgreement = [definicion objectForKey:@"IdAgreement"];
            NSString *estado = [definicion objectForKey:@"Status"];
            NSString *idShop = [definicion objectForKey:@"IdShop"];
            
            IATProveedor *proveedor = [IATProveedor obtenerProveedorWithId:idShop context:self.model.context];
            if (proveedor == nil) continue;
            
            NSString *porcentaje = [objeto objectForKey:@"Percent"];
            
            NSString *fechaInicio = [self changeNullToString:[definicion objectForKey:@"DateStart"]];
            NSString *fechaFinal = [self changeNullToString:[definicion objectForKey:@"DateEnd"]];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            //[dateFormat setDateFormat:@"yyyy-MM-dd"];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *inicioFecha = [dateFormat dateFromString:fechaInicio];
            NSDate *finalFecha = [dateFormat dateFromString:fechaFinal];
    
            
            IATAcuerdo *newAcuerdo = [IATAcuerdo obtenerAcuerdoWithId:idAgreement context:self.model.context];
            if (newAcuerdo == nil) {
                newAcuerdo =
                [IATAcuerdo acuerdoWithId:idAgreement comentarios:comentario fechaInicio:inicioFecha fechaFinal:finalFecha context:self.model.context];
                
                //IATProveedor *proveedor = [IATProveedor obtenerProveedorWithId:idShop context:self.model.context];
                newAcuerdo.provedor = proveedor;
                newAcuerdo.porcentaje = [NSString stringWithFormat:@"%@", porcentaje];
                
                
            }
            else{
                if (![porcentaje isEqual:newAcuerdo.porcentaje]) {
                    newAcuerdo.porcentaje = [NSString stringWithFormat:@"%@", porcentaje];
                }
            }
            
            //0 denegado
            //1 es aprobado
            //2 por aprobar
            
            newAcuerdo.estado = estado;
            
            NSDictionary *products = [objeto objectForKey:@"ProductQuantity"];
            for (id item in products) {
                
                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                f.numberStyle = NSNumberFormatterDecimalStyle;
                
                int cantN = [[item objectForKey:@"Quantity"] intValue];
                int cantC = [[item objectForKey:@"Consumed"] intValue];
                
                NSNumber *cantidadN = [NSNumber numberWithInt:cantN];
                NSNumber *cantidadC = [NSNumber numberWithInt:cantC];
                
                
                NSString *porcentajeP = [item objectForKey:@"Percent"];
                
                IATProducto *newProduct = [IATProducto obtenerProductoWithId:[item objectForKey:@"IdProduct"] context:self.model.context];
                
                if (newProduct != nil) {
                    IATItemAcuerdo *newItem = [IATItemAcuerdo obtenerItemAcuerdoWithAcuerdo:newAcuerdo producto:newProduct context:self.model.context];
                    
                    if (newItem == nil) {
                        newItem = [IATItemAcuerdo itemAcuerdoWithCantidad:cantidadN acuerdo:newAcuerdo context:self.model.context];
                        newItem.producto = newProduct;
                        newItem.porcentaje = [NSString stringWithFormat:@"%@", porcentajeP];
                        
                    }
                    else{
                        if (![porcentajeP isEqual:newItem.porcentaje]) {
                            newItem.porcentaje = [NSString stringWithFormat:@"%@", porcentajeP];
                        }
                    }
                    
                    newItem.consumido = cantidadC;
                }
            }
            
            // check if total > 0
            NSLog(@"items para este acuerdo %ld",[newAcuerdo.itemsAcuerdo count]);
            
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadPedidos];
        });
    });
    
}
-(void)savePedidos:(NSDictionary *)data{
    
    NSDictionary *pedidos = [data objectForKey:@"Orders"];
    
    dispatch_queue_t savePedidos = dispatch_queue_create("pedidos", 0);
    
    dispatch_async(savePedidos, ^{
        
        for (id objeto in pedidos) {
            
            NSDictionary *definicion = [objeto objectForKey:@"Definition"];
            NSString *idPedido = [definicion objectForKey:@"idOrder"];
            NSString *referencia = [definicion objectForKey:@"reference"];
            NSString *estado = [definicion objectForKey:@"currentState"];
            
            NSString *fechaPedidoString = [NSString stringWithFormat:@"%@", [definicion objectForKey:@"dateAdd"]];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *fechaPedido = [dateFormat dateFromString:fechaPedidoString];
            
            //NSString *invoiceDate = [definicion objectForKey:@"invoiceDate"];
            NSString *precioPedido = [definicion objectForKey:@"totalPaid"];
            
            
            IATPedido *newPedido = [IATPedido obtenerPedidoWithId:idPedido context:self.model.context];
            if (newPedido == nil) {
                newPedido =
                [IATPedido pedidoWithId:idPedido estado:estado fecha:fechaPedido context:self.model.context];
                
                newPedido.nombre = referencia;
                newPedido.precioTotal = [NSDecimalNumber decimalNumberWithString:precioPedido];
                
                NSDictionary *items = [objeto objectForKey:@"Products"];
                for (id item in items) {
                    
                    IATItemPedido *newItemPedido;
                    IATProducto *producto = [IATProducto obtenerProductoWithId:[item objectForKey:@"productId"] context:self.model.context];
                    
                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                    f.numberStyle = NSNumberFormatterDecimalStyle;
                    NSNumber *cantidadN = [f numberFromString:[item objectForKey:@"quantity"]];
                    
                    if (producto != nil) {
                        newItemPedido =
                        [IATItemPedido itemPedidoWithCantidad:cantidadN
                                                     producto:producto
                                                       pedido:newPedido
                                                       precio:[NSDecimalNumber decimalNumberWithString:
                                                               [item objectForKey:@"price"]]
                                                      context:self.model.context];
                    }
                    else{
                        IATPromocion *promo =
                        [IATPromocion obtenerPromocionWithId:[item objectForKey:@"productId"] context:self.model.context];
                        newItemPedido = [IATItemPedido itemPedidoWithCantidad:cantidadN
                                                                    promocion:promo
                                                                       pedido:newPedido
                                                                       precio:[NSDecimalNumber decimalNumberWithString:
                                                                               [item objectForKey:@"price"]]
                                                                      context:self.model.context];
                    }
                    
                    
                    IATProveedor *proveedor =
                    [IATProveedor obtenerProveedorWithId:[item objectForKey:@"IdShop"]
                                                 context:self.model.context];
                    
                    newItemPedido.pedido = newPedido;
                    newItemPedido.proveedor = proveedor;
                    
                }
                
            }
            else{
                newPedido.estado = estado;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self saveDataToLogin];
            
            SWRevealViewController *revealVC = [self.storyboard instantiateViewControllerWithIdentifier:@"revealVC"];
            [self presentViewController:revealVC animated:YES completion:^{
                NSLog(@"Pasando al menu");
            }];
        });
    });
    
}
-(void)saveNotificaciones:(NSDictionary *)data{
    
    NSDictionary *notificaciones = [data objectForKey:@"Notifications"];
    
    if (![notificaciones isEqual:@""]){
        NSString *idNoti, *nombre, *mensaje, *fecha, *hora, *fechaHora;
        
        for (id objeto in notificaciones){
            
            idNoti = [self changeNullToString:[objeto objectForKey:@"IdNotification"]];
            mensaje = [self changeNullToString:[objeto objectForKey:@"Message"]];
            nombre = [self changeNullToString:[objeto objectForKey:@"Name"]];
            fecha = [self changeNullToString:[objeto objectForKey:@"Date"]];
            hora = [self changeNullToString:[objeto objectForKey:@"Hour"]];
            
            fechaHora = [NSString stringWithFormat:@"%@ - %@", fecha, hora];
            
            nombre = [self decodeWithString:nombre];
            mensaje = [self decodeWithString:mensaje];
            
            if ([IATNotificacion obtenerNotificacionWithId:idNoti context:self.model.context] == nil) {
                [IATNotificacion notificacionWithId:idNoti nombre:nombre mensaje:mensaje fecha:fechaHora context:self.model.context];
            }
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString *dateRequestNotification =[NSString stringWithFormat:@"%@", [NSDate date]];
        [defaults setObject:dateRequestNotification forKey:@"kLastDateRequestNotification"];
        
        [defaults synchronize];
    }
    else{
         NSLog(@"JSON de Notificaciones vacio");
    }
}
-(void)saveTipologias:(NSDictionary *)data{
    
    NSArray *tipologias = [data objectForKey:@"SectorEconomic"];
    
    if (tipologias != nil) {
        
        NSString *idTipologia, *nombre;
        for (id objeto in tipologias) {
            idTipologia = [self changeNullToString:[objeto objectForKey:@"IdSector"]];
            nombre = [self changeNullToString:[objeto objectForKey:@"Name"]];
            
            NSLog(@"->  %@ %@",idTipologia, nombre);
            if ([IATSectorEconomico obtenerTipologiaWithId:idTipologia context:self.model.context] == nil) {
                [IATSectorEconomico tipologiaWithId:idTipologia nombre:nombre context:self.model.context];
            }
        }
    }
}
-(void)saveContent:(NSDictionary *)dataContent{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *content = [dataContent objectForKey:@"cmsContent"];
    
    NSDictionary *dicTerminos = [[content objectAtIndex:0] objectForKey:@"TermsAndConditions"];
    NSDictionary *dicAcerca = [[content objectAtIndex:1] objectForKey:@"AboutUs"];
    
    NSString *terminos = [dicTerminos objectForKey:@"Content"];
    [defaults setObject:terminos forKey:@"kTerminos"];
    
    NSString *acerca = [self decodeWithString:[dicAcerca objectForKey:@"Content"]];
    [defaults setObject:acerca forKey:@"kAcerca"];
    
    [defaults synchronize];
    
}


#pragma mark - Guardar datos de usuario
-(void)saveUser:(NSDictionary *)dataUser{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [dataUser objectForKey:@"email"];
    [defaults setObject:email forKey:@"kEmail"];
    
    NSString *lastPass = self.txtPassword.text;
    [defaults setObject:lastPass forKey:@"kPass"];
    
    NSString *idCustomer = [dataUser objectForKey:@"IdCustomer"];
    [defaults setObject:idCustomer forKey:@"kIdCustomer"];
    
    NSString *typeUser = [dataUser objectForKey:@"typeUser"];//Revisar
    [defaults setObject:typeUser forKey:@"kTypeUser"];
    
    NSString *phone = [dataUser objectForKey:@"Phone"];
    [defaults setObject:phone forKey:@"kPhone"];
    
    NSString *cif = [dataUser objectForKey:@"cif"];
    [defaults setObject:cif forKey:@"kCif"];
    
    NSString *company = [self decodeWithString:[dataUser objectForKey:@"company"]];
    [defaults setObject:company forKey:@"kCompany"];
    
    NSString *address = [self decodeWithString:[dataUser objectForKey:@"Address"]];
    [defaults setObject:address forKey:@"kAddress"];
    
    NSString *tipologia = [dataUser objectForKey:@"sectorEconomic"];
    if ([tipologia isEqual:[NSNull null]])
        [defaults setObject:@"" forKey:@"kSector"];
    else
        [defaults setObject:tipologia forKey:@"kSector"];
    
    NSString *keys = [dataUser objectForKey:@"Keys"];
    [defaults setObject:keys forKey:@"kKey"];
    
    [defaults synchronize];
    
}
-(void)saveDataToLogin{
    
    NSLog(@"Guardando datos de logueo");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"kSecondTime"];
    
    NSString *lastEmail = self.txtEmail.text;
    [defaults setObject:lastEmail forKey:@"kLastEmail"];
    
    NSString *lastPass = self.txtPassword.text;
    [defaults setObject:lastPass forKey:@"kLastPass"];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd/MM/YYYY"];
    NSString *dateLogin =[dateformate stringFromDate:[NSDate date]];
    [defaults setObject:dateLogin forKey:@"kDateLastLogin"];
    
    [defaults setBool:YES forKey:@"kSuccessfulLoad"];
    [defaults synchronize];
    
    NSLog(@"Se guardaron los datos exitosamente");
    
    
    [self.model saveWithErrorBlock:^(NSError *error) {
        if (error != nil){
            NSLog(@"No se guardaron los datos - Error:%@", [error description]);
            [defaults setObject:nil forKey:@"kLastEmail"];
            [defaults setObject:nil forKey:@"kLastPass"];
            [defaults setBool:NO forKey:@"kSuccessfulLoad"];
            
        }
    }];
    

}


#pragma mark - Utilidades
-(NSString *)decodeWithString:(NSString *)string{
    
    NSString *newString;
    
    if (string != nil) {
        const char *c = [string cStringUsingEncoding:NSISOLatin1StringEncoding];
        newString = [[NSString alloc]initWithCString:c encoding:NSUTF8StringEncoding];
    }
    else{
        newString = @"";
    }
    
    return newString;
}
-(NSString *)changeNullToString:(NSString *)string{
    
    if ( string == (NSString *)[NSNull null] )
    {
        string = @"";
    }
    
    return string;
}
-(BOOL)validarPrecioPorProveedor:(NSArray *)precioProveedor{
    
    for (id objeto in precioProveedor) {
        if ([IATProveedor obtenerProveedorWithId:[objeto objectForKey:@"IdShop"] context:self.model.context] != nil) {
            return YES;
        }
        else{
            NSLog(@"Proveedor no existe en BD local %@", [objeto objectForKey:@"IdShop"]);
        }
    }
    
    return NO;
}
-(NSDateFormatter *)darFormatoFecha{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    return dateFormatter;
    
}

#pragma mark - Ocultar teclado
-(void)ocultaTeclado:(UITapGestureRecognizer *)sender{
    [self.txtEmail resignFirstResponder];
    [self.txtPassword resignFirstResponder];
}

#pragma mark - Habilitar y deshabilitar controles
-(void)enableControls:(BOOL)enable{
    self.txtEmail.enabled = enable;
    self.txtPassword.enabled = enable;
    self.btnForgetPass.enabled = enable;
    self.btnLogin.enabled = enable;
    self.btnRegister.enabled = enable;
}

#pragma mark - Movimiento de la Vista
-(void)setViewMovedUp:(BOOL)movedUp{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= self.movementOfView;
        rect.size.height += self.movementOfView;
    }
    else
    {
        // volver al estado normal.
        rect.origin.y += self.movementOfView;
        rect.size.height -= self.movementOfView;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
-(IBAction)txtEmailDidBeginEditing:(id)sender{
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
    self.txtEmail.leftView = paddingView;
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    //mueve la vista hacia arriba, si no ha sido movida ya.
    if  (self.view.frame.origin.y >= 0)
    {
        self.movementOfView = 100;
        [self setViewMovedUp:YES];
    }
}
-(IBAction)txtEmailDidEndEditing:(id)sender{
    
    if (!(self.txtEmail.text.length > 0)) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.txtEmail.leftView = paddingView;
        self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
        [self.txtEmail.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
    }
    
    self.movementOfView = 100;
    [self setViewMovedUp:NO];
}
-(IBAction)txtPasswordDidBeginEditing:(id)sender {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
    self.txtPassword.leftView = paddingView;
    self.txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y >= 0)
    {
        self.movementOfView = 200;
        [self setViewMovedUp:YES];
        
    }
    
}
-(IBAction)txtPassDidEndEditing:(id)sender {
    
    if ((!self.txtPassword.text.length) > 0) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.txtPassword.leftView = paddingView;
        self.txtPassword.leftViewMode = UITextFieldViewModeAlways;
        [self.txtPassword.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
    }
    
    self.movementOfView = 200;
    [self setViewMovedUp:NO];
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATAcuerdo.m instead.

#import "_IATAcuerdo.h"

@implementation IATAcuerdoID
@end

@implementation _IATAcuerdo

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Acuerdo" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Acuerdo";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Acuerdo" inManagedObjectContext:moc_];
}

- (IATAcuerdoID*)objectID {
	return (IATAcuerdoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic comentarios;

@dynamic estado;

@dynamic fechaFinal;

@dynamic fechaInicio;

@dynamic idAcuerdo;

@dynamic porcentaje;

@dynamic itemsAcuerdo;

- (NSMutableSet<IATItemAcuerdo*>*)itemsAcuerdoSet {
	[self willAccessValueForKey:@"itemsAcuerdo"];

	NSMutableSet<IATItemAcuerdo*> *result = (NSMutableSet<IATItemAcuerdo*>*)[self mutableSetValueForKey:@"itemsAcuerdo"];

	[self didAccessValueForKey:@"itemsAcuerdo"];
	return result;
}

@dynamic provedor;

@end

@implementation IATAcuerdoAttributes 
+ (NSString *)comentarios {
	return @"comentarios";
}
+ (NSString *)estado {
	return @"estado";
}
+ (NSString *)fechaFinal {
	return @"fechaFinal";
}
+ (NSString *)fechaInicio {
	return @"fechaInicio";
}
+ (NSString *)idAcuerdo {
	return @"idAcuerdo";
}
+ (NSString *)porcentaje {
	return @"porcentaje";
}
@end

@implementation IATAcuerdoRelationships 
+ (NSString *)itemsAcuerdo {
	return @"itemsAcuerdo";
}
+ (NSString *)provedor {
	return @"provedor";
}
@end


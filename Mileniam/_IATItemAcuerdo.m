// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATItemAcuerdo.m instead.

#import "_IATItemAcuerdo.h"

@implementation IATItemAcuerdoID
@end

@implementation _IATItemAcuerdo

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ItemAcuerdo" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ItemAcuerdo";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ItemAcuerdo" inManagedObjectContext:moc_];
}

- (IATItemAcuerdoID*)objectID {
	return (IATItemAcuerdoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cantidadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cantidad"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"consumidoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"consumido"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cantidad;

- (int32_t)cantidadValue {
	NSNumber *result = [self cantidad];
	return [result intValue];
}

- (void)setCantidadValue:(int32_t)value_ {
	[self setCantidad:@(value_)];
}

- (int32_t)primitiveCantidadValue {
	NSNumber *result = [self primitiveCantidad];
	return [result intValue];
}

- (void)setPrimitiveCantidadValue:(int32_t)value_ {
	[self setPrimitiveCantidad:@(value_)];
}

@dynamic consumido;

- (int32_t)consumidoValue {
	NSNumber *result = [self consumido];
	return [result intValue];
}

- (void)setConsumidoValue:(int32_t)value_ {
	[self setConsumido:@(value_)];
}

- (int32_t)primitiveConsumidoValue {
	NSNumber *result = [self primitiveConsumido];
	return [result intValue];
}

- (void)setPrimitiveConsumidoValue:(int32_t)value_ {
	[self setPrimitiveConsumido:@(value_)];
}

@dynamic porcentaje;

@dynamic acuerdo;

@dynamic producto;

@end

@implementation IATItemAcuerdoAttributes 
+ (NSString *)cantidad {
	return @"cantidad";
}
+ (NSString *)consumido {
	return @"consumido";
}
+ (NSString *)porcentaje {
	return @"porcentaje";
}
@end

@implementation IATItemAcuerdoRelationships 
+ (NSString *)acuerdo {
	return @"acuerdo";
}
+ (NSString *)producto {
	return @"producto";
}
@end


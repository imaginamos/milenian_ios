#import "IATItemPedido.h"
#import "IATCategoria.h"
#import "IATProducto.h"
#import "IATPromocion.h"
#import "IATVariedad.h"
#import "IATSubcategoria.h"

@interface IATItemPedido ()
// Private interface goes here.
@end

@implementation IATItemPedido

+(instancetype) itemPedidoWithCantidad:(NSNumber *)cantidad
                              producto:(IATProducto *)producto
                                pedido:(IATPedido *)pedido
                                precio:(NSDecimalNumber *)precio
                               context:(NSManagedObjectContext *)context{
    
    
    IATItemPedido *nItemPedido = [NSEntityDescription insertNewObjectForEntityForName:[IATItemPedido entityName]
                                                                 inManagedObjectContext:context];
    
    nItemPedido.cantidad = cantidad;
    nItemPedido.producto = producto;
    nItemPedido.pedido = pedido;
    nItemPedido.precioTotal = precio;
    
    return nItemPedido;
    
}

+(instancetype) obtenerItemPedidoWithProducto:(IATProducto *)producto
                                      context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto == %@",producto];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}
+(BOOL) existeItemPedidoWithProducto:(IATProducto *)producto
                             context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto == %@ AND pedido.idPedido == %@",producto, @"1"];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return YES;
    }
    
    return NO;
    
}

+(instancetype) itemPedidoWithCantidad:(NSNumber *)cantidad
                             promocion:(IATPromocion *)promocion
                                pedido:(IATPedido *)pedido
                                precio:(NSDecimalNumber *)precio
                               context:(NSManagedObjectContext *)context{
    
    IATItemPedido *nItemPedido =
    [NSEntityDescription insertNewObjectForEntityForName:[IATItemPedido entityName]
                                  inManagedObjectContext:context];
    
    nItemPedido.cantidad = cantidad;
    nItemPedido.promocion = promocion;
    nItemPedido.pedido = pedido;
    nItemPedido.precioTotal = precio;
    
    return nItemPedido;
    
}


+(instancetype) obtenerItemPedidoWithPromocion:(IATPromocion *)promocion
                                       context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promocion == %@",promocion];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"promocion.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) obtenerMayoristasPorCategoria:(IATCategoria *)categoria conPedido:(IATPedido *)pedido context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pedido == %@ AND producto.variedad.subcategoria.categoria == %@", pedido,categoria];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.variedad.subcategoria.categoria" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        NSMutableArray *mayoristas = [NSMutableArray array];
        for (IATItemPedido *item in array) {
            if(![mayoristas containsObject:item.proveedor]){
                [mayoristas addObject:item.proveedor];
            }
        }
        return mayoristas;
    }
    
    return nil;
}

+(NSArray *) obtenerCategoriaPorMayorista:(IATProveedor *)proveedor conPedido:(IATPedido *)pedido context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pedido == %@ AND proveedor == %@", pedido,proveedor];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"proveedor" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        NSMutableArray *categorias = [NSMutableArray array];
        for (IATItemPedido *item in array) {
            if (item.producto != nil) {
                if(![categorias containsObject:item.producto.variedad.subcategoria.categoria]){
                    [categorias addObject:item.producto.variedad.subcategoria.categoria];
                }
            }
            else{
                if(![categorias containsObject:item.promocion.categoria]){
                    [categorias addObject:item.promocion.categoria];
                }
            }
            
        }
        return categorias;
    }
    
    return nil;
}

+(NSArray *) obtenerProductosPorCategoria:(IATCategoria *)categoria yMayorista:(IATProveedor *)proveedor conPedido:(IATPedido *)pedido context:(NSManagedObjectContext *)context{
    
    NSMutableArray *items = [NSMutableArray array];
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pedido == %@ AND producto.variedad.subcategoria.categoria == %@ AND proveedor == %@", pedido,categoria, proveedor];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.variedad.subcategoria.categoria" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        [items addObjectsFromArray:array];
        //return array;
    }
    
    NSPredicate *predicatePromo = [NSPredicate predicateWithFormat:@"pedido == %@ AND promocion.categoria == %@ AND proveedor == %@", pedido,categoria, proveedor];
    
    NSLog(@"pedido == %@ AND promocion.categoria == %@ AND proveedor == %@",pedido,categoria, proveedor );

    
    NSLog(@"pedido == %@ AND promocion.categoria == %@ AND proveedor == %@",pedido,categoria, proveedor );

    
    [fetch setPredicate:predicatePromo];
    
    NSSortDescriptor *sortDescriptorPromo = [[NSSortDescriptor alloc] initWithKey:@"producto.categoria" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptorPromo]];
    
    NSError *errorPromo;
    NSArray *arrayPromo = [context executeFetchRequest:fetch error:&errorPromo];
    if ([arrayPromo count] > 0){
        [items addObjectsFromArray:arrayPromo];
    }
    
    return items;
}

+(NSArray *) obtenerProductosPorPromociones:(IATCategoria *)categoria yMayorista:(IATProveedor *)proveedor conPedido:(IATPedido *)pedido context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pedido == %@ AND producto.variedad.subcategoria.categoria == %@ AND proveedor == %@", pedido,categoria, proveedor];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.variedad.subcategoria.categoria" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}

@end

#import "_IATLista.h"

@interface IATLista : _IATLista

+(instancetype) ListaWithNombre:(NSString *)nombre
                        context:(NSManagedObjectContext *)context;

+(instancetype) obtenerAcuerdoWithNombre:(NSString *)nombre
                                 context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerListasWithContext:(NSManagedObjectContext *)context;

@end

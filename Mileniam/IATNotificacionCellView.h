//
//  IATNotificacionCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 16/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATNotificacionCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNombreNoti;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaNoti;
@property (weak, nonatomic) IBOutlet UILabel *lblMensajeNoti;


+(NSString *) cellId;
+(CGFloat) cellHeight;
@end

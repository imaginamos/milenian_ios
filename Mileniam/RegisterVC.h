//
//  RegisterVC.h
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define COOKIE_KEY @"sVVP2U7FwaoyjFj5IYQapn0O10oDWDYpNad12fVCb5wsud2Ac35QuVj4"
//#define COOKIE_KEY @"hWnOpFgi8oCdSKELrySox7OGYUvsMuygshqSNEZK1OtVYKK0a8FCMDhi"

@interface RegisterVC : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtCif;
@property (weak, nonatomic) IBOutlet UITextField *txtComercialName;
@property (weak, nonatomic) IBOutlet UITextField *txtNumberPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtSectorEconomico;
@property (weak, nonatomic) IBOutlet UITextField *txtPasswd;
@property (weak, nonatomic) IBOutlet UIButton *btnAddImg;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerTipologia;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


- (IBAction)mostrarTipologias:(id)sender;

@end

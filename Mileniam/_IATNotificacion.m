// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATNotificacion.m instead.

#import "_IATNotificacion.h"

@implementation IATNotificacionID
@end

@implementation _IATNotificacion

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Notificacion" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Notificacion";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Notificacion" inManagedObjectContext:moc_];
}

- (IATNotificacionID*)objectID {
	return (IATNotificacionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic date;

@dynamic idNotificacion;

@dynamic message;

@dynamic nombre;

@end

@implementation IATNotificacionAttributes 
+ (NSString *)date {
	return @"date";
}
+ (NSString *)idNotificacion {
	return @"idNotificacion";
}
+ (NSString *)message {
	return @"message";
}
+ (NSString *)nombre {
	return @"nombre";
}
@end


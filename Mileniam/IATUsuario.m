#import "IATUsuario.h"

@interface IATUsuario ()

// Private interface goes here.

@end

@implementation IATUsuario

+(instancetype) usuarioWithId:(NSString *)idUsuario
                       nombre:(NSString *)nombre
                  descripcion:(NSString *)descripcion
                     telefono:(NSString *)telefono
                  tipoUsuario:(NSString *)tipoUsuario
                     urlImage:(NSString *)urlImage
                      context:(NSManagedObjectContext *)context{
    
    IATUsuario *usuario = [NSEntityDescription insertNewObjectForEntityForName:[IATUsuario entityName]
                                                                   inManagedObjectContext:context];
    
    usuario.idEntity = idUsuario;
    usuario.nombre = nombre;
    usuario.descripcion = descripcion;
    usuario.urlLogo = urlImage;
    usuario.telefono = telefono;
    usuario.tipoUsuario = tipoUsuario;
    
    return usuario;
    
}

@end

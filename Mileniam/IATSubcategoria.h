#import "_IATSubcategoria.h"

@interface IATSubcategoria : _IATSubcategoria

+(instancetype) subcategoriaWithId:(NSString *)idSubcategoria
                            nombre:(NSString *)nombre
                       descripcion:(NSString *)descripcion
                          urlImage:(NSString *)urlImage
                         categoria:(IATCategoria *)categoria
                           context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerSubcategoriasWithContext:(NSManagedObjectContext *)context;
+(instancetype) obtenerSubCategoriaWithId:(NSString *)idSubCategoria
                                  context:(NSManagedObjectContext *)context;

@end

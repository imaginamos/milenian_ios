//
//  PromocionesTVC.m
//  Milenian
//
//  Created by Desarrollador IOS on 15/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "PromocionesTVC.h"
#import "SWRevealViewController.h"
#import "PromocionVC.h"
#import "IATPromocion.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATPromocionCellView.h"
#import "CacheImgs.h"

@interface PromocionesTVC ()
@property (strong, nonatomic) NSArray *promociones;
@property (strong, nonatomic) NSCache *cache;
@end

@implementation PromocionesTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cache = [CacheImgs sharedInstance];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.promociones = [IATPromocion obtenerPromocionesWithContext:localVar.model.context];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATPromocionCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATPromocionCellView cellId]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.promociones.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATPromocionCellView *cell =
    [tableView dequeueReusableCellWithIdentifier:[IATPromocionCellView cellId] forIndexPath:indexPath];
    
    IATPromocion *promo = [self.promociones objectAtIndex:indexPath.row];
    
    if (promo.nombre != nil && ![promo.nombre isEqualToString:@""]) {
        cell.nombrePromocion.text = promo.nombre;
    }
    else{
        cell.nombrePromocion.text = @"Nombre de la promoción";
    }
    
    
    if (promo.descripcion != nil && ![promo.descripcion isEqualToString:@""]){
        cell.descripcionPromocion.text = promo.descripcion;
    }
    else{
        cell.descripcionPromocion.text = @"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  ";
    }
    
    UIImage *imgPromo = [self.cache objectForKey:[NSURL URLWithString:promo.urlImagen]];
    if (imgPromo) {
        cell.imgPromocion.image = imgPromo;
    }
    else{
        [cell.activityLoad startAnimating];
        [self imageWithUrl:[NSURL URLWithString:promo.urlImagen] block:^(UIImage *image) {
            if (image != nil) {
                cell.imgPromocion.image = image;
            }
            [cell.activityLoad stopAnimating];
        }];
    }
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringDateStart = [dateFormatter stringFromDate:promo.fechaInicio];
    NSString *stringDateEnd = [dateFormatter stringFromDate:promo.fechaFinal];
    
    if (stringDateStart != nil){
        cell.lblFechaInicial.text = [NSString stringWithFormat:@"%@", stringDateStart];
    }
    if (stringDateEnd != nil){
        cell.lblFechaFinal.text = [NSString stringWithFormat:@"%@", stringDateEnd];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    IATPromocion *promocion = [self.promociones objectAtIndex:indexPath.row];
    
    PromocionVC *promo = [self.storyboard instantiateViewControllerWithIdentifier:@"promocionVC"];
    promo.promocionSeleccionada = promocion;
    
    [self.navigationController pushViewController:promo animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [IATPromocionCellView cellHeight];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

#pragma mark - Utility Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

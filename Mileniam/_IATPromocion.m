// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPromocion.m instead.

#import "_IATPromocion.h"

@implementation IATPromocionID
@end

@implementation _IATPromocion

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Promocion" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Promocion";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Promocion" inManagedObjectContext:moc_];
}

- (IATPromocionID*)objectID {
	return (IATPromocionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic fechaFinal;

@dynamic fechaInicio;

@dynamic idProducto;

@dynamic unidad;

@dynamic urlImagen;

@dynamic categoria;

@dynamic itemPedido;

- (NSMutableSet<IATItemPedido*>*)itemPedidoSet {
	[self willAccessValueForKey:@"itemPedido"];

	NSMutableSet<IATItemPedido*> *result = (NSMutableSet<IATItemPedido*>*)[self mutableSetValueForKey:@"itemPedido"];

	[self didAccessValueForKey:@"itemPedido"];
	return result;
}

@dynamic promociones;

- (NSMutableSet<IATPromoProveedor*>*)promocionesSet {
	[self willAccessValueForKey:@"promociones"];

	NSMutableSet<IATPromoProveedor*> *result = (NSMutableSet<IATPromoProveedor*>*)[self mutableSetValueForKey:@"promociones"];

	[self didAccessValueForKey:@"promociones"];
	return result;
}

@end

@implementation IATPromocionAttributes 
+ (NSString *)fechaFinal {
	return @"fechaFinal";
}
+ (NSString *)fechaInicio {
	return @"fechaInicio";
}
+ (NSString *)idProducto {
	return @"idProducto";
}
+ (NSString *)unidad {
	return @"unidad";
}
+ (NSString *)urlImagen {
	return @"urlImagen";
}
@end

@implementation IATPromocionRelationships 
+ (NSString *)categoria {
	return @"categoria";
}
+ (NSString *)itemPedido {
	return @"itemPedido";
}
+ (NSString *)promociones {
	return @"promociones";
}
@end


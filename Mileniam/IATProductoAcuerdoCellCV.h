//
//  IATProductoAcuerdoCellCV.h
//  Milenian
//
//  Created by Carlos Obregón on 2/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVFoundation;
@class IATPreItemAcuerdo;

#define ACTUALIZAR_SUBTOTAL @"ActualizarSubtotal"
#define ACTUALIZAR_SUBTOTAL_REMOVE @"ActualizarSubtotalDisminuir"

@interface IATProductoAcuerdoCellCV : UICollectionViewCell<UIGestureRecognizerDelegate>
@property NSCache *cache;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;
@property (retain, nonatomic) UILongPressGestureRecognizer *pressLong;
@property (retain, nonatomic) UITapGestureRecognizer *tap;
@property (strong, nonatomic) AVAudioPlayer* soundAdd;
@property (strong, nonatomic) AVAudioPlayer* soundRemove;

@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidad;
@property (strong, nonatomic) IATPreItemAcuerdo *preItemAcuerdo;
@property (strong, nonatomic) NSDecimalNumber *subTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;



- (IBAction)addOne:(id)sender;
- (IBAction)removeOne:(id)sender;
-(void) observePreItemAcuerdo:(IATPreItemAcuerdo *) preItemAcuerdo;
+(NSString *)cellId;
@end

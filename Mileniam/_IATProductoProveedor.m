// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATProductoProveedor.m instead.

#import "_IATProductoProveedor.h"

@implementation IATProductoProveedorID
@end

@implementation _IATProductoProveedor

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ProductoProveedor" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ProductoProveedor";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ProductoProveedor" inManagedObjectContext:moc_];
}

- (IATProductoProveedorID*)objectID {
	return (IATProductoProveedorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic precio;

@dynamic producto;

@dynamic provedor;

@dynamic subcategoria;

@dynamic variedad;

@end

@implementation IATProductoProveedorAttributes 
+ (NSString *)precio {
	return @"precio";
}
@end

@implementation IATProductoProveedorRelationships 
+ (NSString *)producto {
	return @"producto";
}
+ (NSString *)provedor {
	return @"provedor";
}
+ (NSString *)subcategoria {
	return @"subcategoria";
}
+ (NSString *)variedad {
	return @"variedad";
}
@end


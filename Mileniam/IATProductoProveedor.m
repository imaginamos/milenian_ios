#import "IATProductoProveedor.h"
#import "IATProducto.h"
#import "IATProveedor.h"

@interface IATProductoProveedor ()

// Private interface goes here.

@end

@implementation IATProductoProveedor

+(instancetype) productoProveedorWithPrecio:(NSDecimalNumber *)precio
                                   producto:(IATProducto *)producto
                                  proveedor:(IATProveedor *)proveedor
                                   variedad:(IATVariedad *)variedad
                               subcategoria:(IATSubcategoria *)subcategoria
                                    context:(NSManagedObjectContext *)context{
    
    IATProductoProveedor *productoProveedor = [NSEntityDescription insertNewObjectForEntityForName:[IATProductoProveedor entityName]
                                                                      inManagedObjectContext:context];
    
    productoProveedor.precio = precio;
    productoProveedor.producto = producto;
    productoProveedor.provedor = proveedor;
    productoProveedor.variedad = variedad;
    productoProveedor.subcategoria = subcategoria;
    
    return productoProveedor;
    
}

+(instancetype) obtenerProductoProvedorWithProducto:(IATProducto *)producto
                                          proveedor:(IATProveedor *)proveedor
                                  context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProductoProveedor entityName]];
    
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"producto == %@ AND provedor == %@",producto, proveedor];
    
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

+(instancetype) obtenerPrecioMasBajo:(IATProducto *)producto
                             context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProductoProveedor entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto = %@", producto];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"precio" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array.count > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(instancetype) obtenerPrecioPorProveedor:(IATProveedor *)proveedor
                                 producto:(IATProducto *)producto
                                  context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProductoProveedor entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto = %@ && provedor = %@", producto, proveedor];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array.count > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) filtrarSubcategoriasWithProveedor:(IATProveedor *)proveedor{
    
    NSArray *productosProvedor = [proveedor.preciosProductos allObjects];
    NSMutableArray *subcategorias = [NSMutableArray array];
    for (id object in productosProvedor) {
        IATProductoProveedor *new = object;
        
        if (![subcategorias containsObject:new.subcategoria]) {
            [subcategorias addObject:new.subcategoria];
        }
    }
    
    return subcategorias;
    
}

+ (NSArray *)filtrarVariedadesWithProveedor:(IATProveedor *)proveedor subcategoria:(IATSubcategoria *)subcategoria{
    
    NSArray *productosProvedor = [proveedor.preciosProductos allObjects];
    NSMutableArray *variedades = [NSMutableArray array];
    for (id object in productosProvedor) {
        IATProductoProveedor *new = object;
        if (new.subcategoria == subcategoria) {
            if (![variedades containsObject:new.variedad]){
                [variedades addObject:new.variedad];
            }
        }
        
    }
    
    return variedades;
    
}

+ (NSArray *)filtrarVariedadesWithProveedor:(IATProveedor *)proveedor subcategoria:(IATSubcategoria *)subcategoria variedad:(IATVariedad *)variedad{
    
    NSArray *productosProvedor = [proveedor.preciosProductos allObjects];
    NSMutableArray *productos = [NSMutableArray array];
    for (id object in productosProvedor) {
        IATProductoProveedor *new = object;
        if (new.subcategoria == subcategoria) {
            if (new.variedad == variedad) {
                if (![productos containsObject:new.producto]){
                    [productos addObject:new.producto];
                }
            }
        }
    }
    
    return productos;
    
}

+(NSArray *) obtenerPreciosOrdenadoPorMenor:(IATProducto *)producto
                             context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProductoProveedor entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto = %@", producto];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"precio" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array.count > 0)
    {
        return array;
    }
    
    return nil;
    
}



@end

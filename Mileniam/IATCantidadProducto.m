#import "IATCantidadProducto.h"
#import "IATProducto.h"
#import "IATProductoProveedor.h"

@interface IATCantidadProducto ()

// Private interface goes here.

@end

@implementation IATCantidadProducto

+(instancetype) productoWithCantidad:(NSNumber *)cantidad
                            producto:(IATProducto *)producto
                            variedad:(IATVariedad *)variedad
                             context:(NSManagedObjectContext *)context{
    
    IATCantidadProducto *cantProducto = [NSEntityDescription insertNewObjectForEntityForName:[IATCantidadProducto entityName]
                                                                      inManagedObjectContext:context];
    
    cantProducto.cantidad = cantidad;
    cantProducto.producto = producto;
    cantProducto.variedad = variedad;
    
    IATProductoProveedor *productoProveedor = [IATProductoProveedor obtenerPrecioMasBajo:producto context:context];
    
    if (productoProveedor != nil) {
        cantProducto.precio = productoProveedor.precio;
    }
    
    
    return cantProducto;
    
}



+(void) deleteAllWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATCantidadProducto entityName]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array > 0)
    {
        for (id object in array) {
            IATCantidadProducto *cantProducto = object;
            NSLog(@"Registro de IATCantidadProducto eliminado:%@", cantProducto);
            [context deleteObject:cantProducto];
        }
    }
}


@end

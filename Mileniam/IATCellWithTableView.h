//
//  IATCellWithTableView.h
//  Milenian
//
//  Created by Desarrollador IOS on 27/07/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ACTUALIZAR_TABLA_PADRE @"Actualizar Tabla Padre"

@interface IATCellWithTableView : UITableViewCell<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSCache *cache;
@property (strong, nonatomic) NSString *mayorista;
@property (strong, nonatomic) NSMutableArray *productos;
@property (strong, nonatomic) UIStoryboard *storyboard;
@property (strong, nonatomic) UINavigationController *navController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

+(NSString *) cellId;
+(CGFloat)cellHeight;
-(void)asignarDelegado;
@end

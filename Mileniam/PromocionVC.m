//
//  PromocionVC.m
//  Milenian
//
//  Created by Carlos Obregón on 18/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "PromocionVC.h"
#import "IATPromocion.h"
#import "IATPromoProveedor.h"
#import "IATProductoProveedor.h"
#import "IATProveedor.h"
#import "IATPedido.h"
#import "IATItemPedido.h"
#import "IATProductoProveedor.h"
#import "IATCantidadProducto.h"
#import "IATProveedorProductoCellView.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "CacheImgs.h"

@interface PromocionVC ()
@property (strong, nonatomic) NSArray *proveedores;
@property (strong, nonatomic) IATCantidadProducto *preItem;
@property (strong, nonatomic) IATPromoProveedor *promoProveedor;
@property (strong, nonatomic) IATProveedor *proveedorSeleccionado;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSDecimalNumber *precioTotal;
@property (nonatomic) NSUInteger cantidad;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation PromocionVC

#pragma mark - Ciclo de vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Promoción";
    self.cache = [CacheImgs sharedInstance];
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.tfCantidad.delegate = self;
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    NSString* fileSoundRemove = [[NSBundle mainBundle] pathForResource:@"Restar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundRemove = [[NSURL alloc]initFileURLWithPath:fileSoundRemove];
    self.soundRemove = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundRemove error:nil];
    
    self.soundRemove.volume = 0.4;
    self.soundRemove.numberOfLoops = 1;
    self.soundRemove.pan = 0;
    self.soundRemove.enableRate = YES;
    self.soundRemove.rate = 1;
    
    //Sincronizar vista
    //self.proveedores = [self.promocionSeleccionada.promociones allObjects];
    self.proveedores = [IATPromoProveedor obtenerPreciosOrdenadoPorMenor:self.promocionSeleccionada context:self.context];
    self.promoProveedor = [self.proveedores objectAtIndex:0];
    self.proveedorSeleccionado = self.promoProveedor.proveedor;
    
    if (![self.promocionSeleccionada.nombre isEqualToString:@""] && self.promocionSeleccionada.nombre != nil) {
        self.lblNombre.text = self.promocionSeleccionada.nombre;
    }
    
    if (![self.promocionSeleccionada.unidad isEqualToString:@""] && self.promocionSeleccionada.unidad != nil) {
        self.lblUnidad.text = self.promocionSeleccionada.unidad;
    }
    if (self.promocionSeleccionada.fechaInicio != nil) {
        
        NSString *fechaString = [NSString stringWithFormat:@"%@", self.promocionSeleccionada.fechaInicio];
        NSArray *arrayFecha = [fechaString componentsSeparatedByString:@" "];
        self.lblFechaInicio.text = [NSString stringWithFormat:@"%@", arrayFecha[0]];
    }
    if (self.promocionSeleccionada.fechaFinal != nil) {
        NSString *fechaString = [NSString stringWithFormat:@"%@", self.promocionSeleccionada.fechaFinal];
        NSArray *arrayFecha = [fechaString componentsSeparatedByString:@" "];
        self.lblFechaFinal.text = [NSString stringWithFormat:@"%@",arrayFecha[0]];
    }
    
    UIImage *imgPromo = [self.cache objectForKey:[NSURL URLWithString:self.promocionSeleccionada.urlImagen]];
    if (imgPromo) {
        self.imgPromo.image = imgPromo;
    }
    else{
        [self.activityLoad startAnimating];
        [self imageWithUrl:[NSURL URLWithString:self.promocionSeleccionada.urlImagen] block:^(UIImage *image) {
            if (image != nil) {
                self.imgPromo.image = image;
            }
            [self.activityLoad stopAnimating];
        }];
    }
    
    self.cantidad = 1;
    self.tfCantidad.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.cantidad];
    
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.promoProveedor.precio doubleValue]]];
    self.precioTotal = self.promoProveedor.precio;
    
    self.listaProveedores.delegate = self;
    self.listaProveedores.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATProveedorProductoCellView" bundle:nil];
    [self.listaProveedores registerNib:cellNib forCellReuseIdentifier:[IATProveedorProductoCellView cellId]];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.proveedores.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATPromoProveedor *precioProveedor = [self.proveedores objectAtIndex:indexPath.row];
    IATProveedorProductoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedorProductoCellView cellId]];
    
    cell.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[precioProveedor.precio doubleValue]]];
    cell.lblNombre.text = [NSString stringWithFormat:@"%@", precioProveedor.proveedor.nombre];
    
    if (precioProveedor.proveedor.urlLogo != nil) {
        
        [cell.activityLoad startAnimating];
        
        [self imageWithUrl:[NSURL URLWithString:precioProveedor.proveedor.urlLogo] block:^(UIImage *image) {
            if (image != nil) {
                cell.logoProveedor.image = image;
            }
            [cell.activityLoad stopAnimating];
        }];
        
    }
    
    return cell;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Cual proveedor y precio escogio
    self.promoProveedor = [self.proveedores objectAtIndex:indexPath.row];
    
    //Actualizar el precio y proveedor del item
    NSDecimalNumber *cantidad =
    [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
    self.precioTotal = [self.promoProveedor.precio decimalNumberByMultiplyingBy:cantidad];
    self.proveedorSeleccionado = self.promoProveedor.proveedor;
    
    //Mostrar el nuevo precio en vista
    
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
    
    self.preItem.cantidad = [NSNumber numberWithUnsignedInteger:self.cantidad];
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

#pragma mark - Target/Actions
-(void)animationProducto{
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.imgPromo.transform = CGAffineTransformScale(self.imgPromo.transform, 1.2, 1.2);
                         
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.imgPromo.transform = CGAffineTransformScale(self.imgPromo.transform, 0.8335, 0.8335);
                                              
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}
- (IBAction)sumarUnaPromo:(id)sender {
    
    [self.soundAdd play];
    self.cantidad++;
    
    NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
    
    self.precioTotal = [self.promoProveedor.precio decimalNumberByMultiplyingBy:cant];
    
    //Actualizar vista
    self.tfCantidad.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad];
    
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
    
    [self animationProducto];
}
- (IBAction)restarUnaPromo:(id)sender {
    
    if (self.cantidad > 0) {
        
        [self.soundRemove play];
        
        self.cantidad--;
        
        NSDecimalNumber *cant =
        [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
        
        self.precioTotal = [self.promoProveedor.precio decimalNumberByMultiplyingBy:cant];
        
        //Actualizar vista
        self.tfCantidad.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad];
        self.lblPrecioTotal.text =
        [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
        
        [self animationProducto];
        
    }
}
- (IBAction)agregarAlPedido:(id)sender {
    
    //Preguntar si existe algun pedido en proceso
    IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
    if (pedido == nil) {
        pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
    }
    
    if( self.cantidad > 0){
        
        NSDecimalNumber *precioItemPedido = self.precioTotal;
        
        IATItemPedido *itemPedido =
        [IATItemPedido obtenerItemPedidoWithPromocion:self.promocionSeleccionada context:self.context];
        if (itemPedido == nil) {
            
            itemPedido = [IATItemPedido itemPedidoWithCantidad:[NSNumber numberWithUnsignedInteger:self.cantidad] promocion:self.promocionSeleccionada pedido:pedido precio:precioItemPedido context:self.context];
            
        }
        else{
            itemPedido.cantidad = [NSNumber numberWithUnsignedInteger:self.cantidad];
            itemPedido.precioTotal = precioItemPedido;
        }
        
        itemPedido.pedido = pedido;
        itemPedido.proveedor = self.proveedorSeleccionado;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Producto Agregado"
                                                                       message:@"Producto agregado a pedido"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion =
        [UIAlertAction actionWithTitle:@"Aceptar"
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
        
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}



#pragma mark - Utility
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

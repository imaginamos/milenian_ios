//
//  IATNotificacionCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 16/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATNotificacionCellView.h"

@implementation IATNotificacionCellView

+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 100.0f;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

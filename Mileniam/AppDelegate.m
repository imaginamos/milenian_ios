//
//  AppDelegate.m
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"


@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    self.model = [AGTSimpleCoreDataStack coreDataStackWithModelName:@"Model"];
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    
    
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                      categories:nil];
    
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    return YES;
}
- (void)applicationWillResignActive:(UIApplication *)application{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL secondTime = [defaults boolForKey:@"kSecondTime"];
    if (secondTime) {
        [self.model saveWithErrorBlock:^(NSError *error) {
            if (error != nil){
                NSLog(@"No se guardaron los datos - Error:%@", [error description]);
            }
        }];
    }
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
}
- (void)applicationWillTerminate:(UIApplication *)application {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL secondTime = [defaults boolForKey:@"kSecondTime"];
    if (secondTime) {
        [self.model saveWithErrorBlock:^(NSError *error) {
            if (error != nil){
                NSLog(@"No se guardaron los datos - Error:%@", [error description]);
            }
        }];
    }
}


// Notificaciones
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // storage uuid
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:dt forKey:@"uuid"];
    [defaults synchronize];
    
    NSLog(@"My token is: %@", dt);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"Receive ush %@", userInfo);
    
    id msgBody = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
    NSString *message = [NSString stringWithFormat:@"%@", msgBody];
    
    if(application.applicationState == UIApplicationStateInactive){
        NSLog(@"Inactive - the user has tapped in the notification when app was closed or in background");
    }
    else if (application.applicationState == UIApplicationStateBackground) {
        NSLog(@"application Background - notification has arrived when app was in background");
    }
    else {
        NSLog(@"application Active - notication has arrived while app was opened");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notificación"
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        
        
        UIViewController *presentedVC = [self.window rootViewController];
        while (presentedVC.presentedViewController != nil){
            presentedVC = presentedVC.presentedViewController;
        }
        
        [presentedVC presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    NSLog(@"Failed to get token, error: %@", error);
#if TARGET_IPHONE_SIMULATOR
    // show the cover for testing
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"4e7b54db144ef812428c4111c5f6c7bcedbbea75875f690dc900c1d49cb36a57" forKey:@"uuid"];
    [defaults synchronize];
#endif
}

@end

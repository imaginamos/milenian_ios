//
//  IATProductoAcuerdoCellCV.m
//  Milenian
//
//  Created by Carlos Obregón on 2/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATProductoAcuerdoCellCV.h"
#import "IATPreItemAcuerdo.h"
#import "VariedadVC.h"
#import "IATProducto.h"
#import "KLCPopup/KLCPopup.h"
#import <QuartzCore/QuartzCore.h>

@implementation IATProductoAcuerdoCellCV

+(NSString *)cellId{
    return NSStringFromClass(self);
}

-(void) observePreItemAcuerdo:(IATPreItemAcuerdo *) preItemAcuerdo{
    
    self.preItemAcuerdo = preItemAcuerdo;
    
    
    self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.contentView addGestureRecognizer:self.tap];
    [self.imgProducto addGestureRecognizer:self.tap];
    
    self.pressLong =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    self.pressLong.minimumPressDuration = 0.2;
    self.pressLong.delegate = self;
    [self.contentView addGestureRecognizer:self.pressLong];
    self.imgProducto.userInteractionEnabled = YES;
    [self.imgProducto addGestureRecognizer:self.pressLong];
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    
    NSString* fileSoundRemove = [[NSBundle mainBundle] pathForResource:@"Restar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundRemove = [[NSURL alloc]initFileURLWithPath:fileSoundRemove];
    self.soundRemove = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundRemove error:nil];
    
    self.soundRemove.volume = 0.3;
    self.soundRemove.numberOfLoops = 1;
    self.soundRemove.pan = 0;
    self.soundRemove.enableRate = YES;
    self.soundRemove.rate = 1;
    
    
    [self.preItemAcuerdo addObserver:self
                          forKeyPath:@"cantidad"
                             options:NSKeyValueObservingOptionNew
                             context:NULL];
    
    
    [self syncWithCantidadProducto];
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context{
    
    [self syncWithCantidadProducto];
    
    
    
}

-(void) syncWithCantidadProducto{
    
    
    if (self.imgProducto.image == nil) {
        self.imgProducto.image = [UIImage imageNamed:@"page"];
        [self.imgProducto setContentMode:UIViewContentModeScaleAspectFill];
        
        IATProducto *producto = self.preItemAcuerdo.producto;
        NSString *url = producto.urlImagen;
        NSURL *urlImg = [NSURL URLWithString:url];
        
        UIImage *imgProduct = [self.cache objectForKey:urlImg];
        if (imgProduct) {
            self.imgProducto.image = imgProduct;
            [self.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
            [self.activityLoad startAnimating];
            [self imageWithUrl:urlImg producto:self.preItemAcuerdo.producto block:^(UIImage *image) {
                if (image != nil) {
                    self.imgProducto.image = image;
                    [self.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
                }
                [self.activityLoad stopAnimating];
            }];
        }
    }
    
    
    self.lblCantidad.text = [NSString stringWithFormat:@"%@",self.preItemAcuerdo.cantidad];
    
    self.lblPrecio.text = [NSString stringWithFormat:@"%@",[self formatCurrencyValue:[self.preItemAcuerdo.precio doubleValue]]];
   
    
}

- (void)handleTap:(UITapGestureRecognizer *)sender{
    
    [self.soundAdd play];
    
    int value = [self.preItemAcuerdo.cantidad intValue];
    self.preItemAcuerdo.cantidad = [NSNumber numberWithInt:value + 1];
    
    self.lblCantidad.text = [NSString stringWithFormat:@"%@", self.preItemAcuerdo.cantidad ];
    
    NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_SUBTOTAL
                                                      object:self
                                                    userInfo:@{@"precio":self.preItemAcuerdo.precio}];
    
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
    [self animationProducto];
    
}

- (IBAction)addOne:(id)sender {
    [self.soundAdd play];
    int cantidad = [self.preItemAcuerdo.cantidad intValue];
    cantidad++;
    self.lblCantidad.text = [NSString stringWithFormat:@"%d", cantidad];
    [self.preItemAcuerdo setCantidad:[NSNumber numberWithInt:cantidad]];
    
    NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_SUBTOTAL
                                                      object:self
                                                    userInfo:@{@"precio":self.preItemAcuerdo.precio}];
    
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
    [self animationProducto];
    
    NSLog(@"%@", self.preItemAcuerdo.cantidad);
}

- (IBAction)removeOne:(id)sender {
    int cantidad = [self.preItemAcuerdo.cantidad intValue];
    if (cantidad > 0) {
        [self.soundRemove play];
        cantidad--;
        self.lblCantidad.text = [NSString stringWithFormat:@"%d", cantidad];
        self.preItemAcuerdo.cantidad = [NSNumber numberWithInt:cantidad];
        
        NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_SUBTOTAL_REMOVE
                                                          object:self
                                                        userInfo:@{@"precio":self.preItemAcuerdo.precio}];
        
        [[NSNotificationCenter defaultCenter] postNotification:n];
        
        [self animationProducto];
        
        NSLog(@"%@", self.preItemAcuerdo.cantidad);
    }
}

-(void)animationProducto{
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 1.2, 1.2);
                         
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 0.8335, 0.8335);
                                              
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}

-(void) prepareForReuse{
    
    [self.preItemAcuerdo removeObserver:self forKeyPath:@"cantidad"];
    self.preItemAcuerdo = nil;
    self.imgProducto.image = nil;
    
}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:imageData];
            completionBlock(image);
        });
    });
}

-(void)imageWithUrl:(NSURL *)urlImage producto:(IATProducto *)producto block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        
        if (self.preItemAcuerdo.producto == producto){
            NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
            UIImage *image = [UIImage imageWithData:imageData];
            
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.cache setObject:image forKey:urlImage];
                    completionBlock(image);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil);
            });
        }
    });
}

- (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
        
        ////////////////////////////////////////Pop Up////////////////////////////////////////
        UIView* contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 340)];
        contentView.backgroundColor = [UIColor whiteColor];
        contentView.layer.cornerRadius = 8.0;
        
        //imagen
        UIImageView *imgProducto = [[UIImageView alloc] initWithFrame:CGRectMake(contentView.bounds.size.width/6, 30, 200, 200)];
        imgProducto.backgroundColor = [UIColor clearColor];
        [imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        
        
        
        //Modo online
        NSString *urlProducto = self.preItemAcuerdo.producto.urlImagen;
        NSURL *url = [NSURL URLWithString:urlProducto];
        UIImage *imgP = [self.cache objectForKey:url];
        if (imgProducto) {
            imgProducto.image = imgP;
        }
        else{
            //Activity
            UIActivityIndicatorView *actView =
            [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imgProducto.bounds.size.width/2 - 25, imgProducto.bounds.size.height/2 - 25, 50.0f, 50.0f)];
            actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [actView startAnimating];
            
            [imgProducto addSubview:actView];
            [self imageWithUrl:url block:^(UIImage *image) {
                if (image != nil) {
                    imgProducto.image = image;
                }
                [actView stopAnimating];
            }];
        }
        
        
        UILabel* dismissLabel = [[UILabel alloc] initWithFrame:CGRectMake(contentView.bounds.size.width/10, 230, 240, 70)];
        dismissLabel.backgroundColor = [UIColor clearColor];
        dismissLabel.textColor = [UIColor blackColor];
        dismissLabel.font = [UIFont boldSystemFontOfSize:12.0];
        dismissLabel.text = self.preItemAcuerdo.producto.nombre;
        dismissLabel.textAlignment = NSTextAlignmentCenter;
        dismissLabel.numberOfLines = 0;
        
        UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
        dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
        //dismissButton.backgroundColor = [UIColor klcGreenColor];
        [dismissButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
        [dismissButton setTitle:@"Cerrar" forState:UIControlStateNormal];
        dismissButton.layer.cornerRadius = 6.0;
        [dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [contentView addSubview:imgProducto];
        [contentView addSubview:dismissLabel];
        [contentView addSubview:dismissButton];
        
        NSDictionary* views = NSDictionaryOfVariableBindings(contentView, dismissButton, dismissLabel, imgProducto);
        
        [contentView addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(16)-[dismissLabel]-(10)-[dismissButton]-(24)-|"
                                                 options:NSLayoutFormatAlignAllCenterX
                                                 metrics:nil
                                                   views:views]];
        
        [contentView addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(36)-[dismissLabel]-(36)-|"
                                                 options:0
                                                 metrics:nil
                                                   views:views]];
        
        
        KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                                showType:KLCPopupShowTypeBounceInFromTop
                                             dismissType:KLCPopupDismissTypeFadeOut
                                                maskType:KLCPopupMaskTypeDimmed
                                dismissOnBackgroundTouch:NO
                                   dismissOnContentTouch:YES];
        
        [popup show];
    }
    //    else if (sender.state == UIGestureRecognizerStateBegan){
    //        NSLog(@"UIGestureRecognizerStateBegan.");
    //        //Do Whatever You want on Began of Gesture
    //    }
    
    
}
- (void)dismissButtonPressed:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
}

-(void)dealloc {
    
    [self.preItemAcuerdo removeObserver:self forKeyPath:@"cantidad"];
    self.preItemAcuerdo = nil;
    self.imgProducto.image = nil;
}

@end

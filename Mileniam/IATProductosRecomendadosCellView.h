//
//  IATProductosRecomendadosCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 11/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATProductosRecomendadosCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;
@property (weak, nonatomic) IBOutlet UILabel *nombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *unidadProducto;
@property (weak, nonatomic) IBOutlet UILabel *marcaProducto;
@property (weak, nonatomic) IBOutlet UILabel *precioProducto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;


+(NSString *) cellId;
+(CGFloat)cellHeight;
@end

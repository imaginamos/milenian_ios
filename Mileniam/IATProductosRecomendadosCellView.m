//
//  IATProductosRecomendadosCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 11/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATProductosRecomendadosCellView.h"

@implementation IATProductosRecomendadosCellView


#pragma mark - Metodos de clase
+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 70.0f;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)prepareForReuse{
    
    self.imgProducto.image = nil;
    [self.activityLoad stopAnimating];
    
}


@end

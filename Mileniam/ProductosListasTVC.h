//
//  ProductosListasTVC.h
//  Milenian
//
//  Created by Carlos Obregón on 31/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IATLista;

@interface ProductosListasTVC : UITableViewController<UISearchResultsUpdating>

@property (strong, nonatomic) IATLista *lista;
@property (strong, nonatomic) UISearchController *searchController;

@end

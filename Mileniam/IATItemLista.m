#import "IATItemLista.h"
#import "IATProductoProveedor.h"

@interface IATItemLista ()

// Private interface goes here.

@end

@implementation IATItemLista

+(instancetype) itemListaWithCantidad:(NSNumber *)cantidad
                             producto:(IATProducto *)producto
                                lista:(IATLista *)lista
                               precio:(NSDecimalNumber *)precio
                              context:(NSManagedObjectContext *)context{
    
    IATItemLista *nItemLista = [NSEntityDescription insertNewObjectForEntityForName:[IATItemLista entityName]
                                                               inManagedObjectContext:context];
    
    nItemLista.cantidad = cantidad;
    nItemLista.producto = producto;
    nItemLista.lista = lista;
    nItemLista.precio = precio;
    
    return nItemLista;
    
}

+(instancetype) itemListaWithCantidad:(NSNumber *)cantidad
                             producto:(IATProducto *)producto
                                lista:(IATLista *)lista
                            proveedor:(IATProveedor *)proveedor
                              context:(NSManagedObjectContext *)context{
    
    IATItemLista *nItemLista = [NSEntityDescription insertNewObjectForEntityForName:[IATItemLista entityName]
                                                             inManagedObjectContext:context];
    
    nItemLista.cantidad = cantidad;
    nItemLista.producto = producto;
    nItemLista.lista = lista;
    nItemLista.proveedor = proveedor;
    nItemLista.precio = [NSDecimalNumber decimalNumberWithString:@"0"];
    
    if (proveedor != nil) {
        IATProductoProveedor *productoProveedor = [IATProductoProveedor obtenerPrecioPorProveedor:proveedor producto:producto context:context];
        
        
        if (productoProveedor != nil) {
            NSDecimalNumber *precio = productoProveedor.precio;
            nItemLista.precio = [precio decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithDecimal:[cantidad decimalValue]]];
        }
    }
    
    return nItemLista;
    
}

+(instancetype) obtenerItemListaWithProducto:(IATProducto *)producto
                                       lista:(IATLista *)lista
                                     context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemLista entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto == %@ && lista == %@",producto, lista];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(BOOL) existeItemListaWithProducto:(IATProducto *)producto
                                        lista:(IATLista *)lista
                                      context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemLista entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto == %@ && lista == %@",producto, lista];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return YES;
    }
    
    return NO;
    
}


+(instancetype) obtenerItemListaWithProducto:(IATProducto *)producto
                                     context:(NSManagedObjectContext *)context{
    
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemLista entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto == %@",producto];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}



@end

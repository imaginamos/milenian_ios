#import "IATPreItemAcuerdo.h"
#import "IATProducto.h"
#import "IATProductoProveedor.h"

@interface IATPreItemAcuerdo ()

// Private interface goes here.

@end

@implementation IATPreItemAcuerdo

+(instancetype) preItemAcuerdoWithCantidad:(NSNumber *)cantidad
                                  producto:(IATProducto *)producto proveedor:(IATProveedor *)proveedor
                                  variedad:(IATVariedad *)variedad
                                    precio:(NSDecimalNumber *)precio
                                   context:(NSManagedObjectContext *)context{
    
    IATPreItemAcuerdo *preItem =
    [NSEntityDescription insertNewObjectForEntityForName:[IATPreItemAcuerdo entityName]
                                  inManagedObjectContext:context];
    
    preItem.cantidad = cantidad;
    preItem.producto = producto;
    preItem.variedad = variedad;
    
    IATProductoProveedor *productoProveedor = [IATProductoProveedor obtenerPrecioPorProveedor:proveedor producto:producto context:context];
    
    
    if (productoProveedor != nil) {
        preItem.precio = productoProveedor.precio;
    }
    
    return preItem;
}

+(NSArray *) obtenerPreItemsAcuerdoWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPreItemAcuerdo entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cantidad > 0"];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"producto.nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array > 0)
    {
        return array;
    }
    
    return nil;
}

+(void) deleteAllWithContext:(NSManagedObjectContext *)context{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPreItemAcuerdo entityName]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array > 0)
    {
        for (id object in array) {
            IATPreItemAcuerdo *preItem = object;
            [context deleteObject:preItem];
            NSLog(@"Registro de IATCantidadProducto eliminado:%@", object);
        }
    }
}

@end

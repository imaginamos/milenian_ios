#import "IATItemAcuerdo.h"

@interface IATItemAcuerdo ()

// Private interface goes here.

@end

@implementation IATItemAcuerdo

+(instancetype) itemAcuerdoWithCantidad:(NSNumber *)cantidad
                                context:(NSManagedObjectContext *)context{
    
    IATItemAcuerdo *nItemAcuerdo =
    [NSEntityDescription insertNewObjectForEntityForName:[IATItemAcuerdo entityName]
                                  inManagedObjectContext:context];
    
    nItemAcuerdo.cantidad = cantidad;
    
    return nItemAcuerdo;
    
}

+(instancetype) itemAcuerdoWithCantidad:(NSNumber *)cantidad
                                acuerdo:(IATAcuerdo *)acuerdo
                                context:(NSManagedObjectContext *)context{
    
    IATItemAcuerdo *nItemAcuerdo =
    [NSEntityDescription insertNewObjectForEntityForName:[IATItemAcuerdo entityName]
                                  inManagedObjectContext:context];
    
    nItemAcuerdo.cantidad = cantidad;
    nItemAcuerdo.acuerdo = acuerdo;
    
    return nItemAcuerdo;
    
}

+(instancetype) obtenerItemAcuerdoWithAcuerdo:(IATAcuerdo *)acuerdo producto:(IATProducto *)producto context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemAcuerdo entityName]];
    
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"producto == %@ && acuerdo == %@",producto, acuerdo];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(BOOL) itemAcuerdoExisteWithProducto:(IATProducto *)producto
                              context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATItemAcuerdo entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"producto == %@",producto];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return YES;
    }
    
    return NO;
}


@end

//
//  PedidoRealizadoVC.m
//  Milenian
//
//  Created by Desarrollador IOS on 25/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.

#import "PedidoRealizadoVC.h"
#import "IATPedido.h"
#import "IATProducto.h"
#import "IATPromocion.h"
#import "IATProveedor.h"
#import "IATItemPedido.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATPedidoCellView.h"
#import "CacheImgs.h"

@interface PedidoRealizadoVC ()
@property (nonatomic, strong) NSArray *productos;
@property (nonatomic, strong) NSArray *itemsPedido;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation PedidoRealizadoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.pedido.nombre;
    self.cache = [CacheImgs sharedInstance];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    
    NSLog(@"-> %@ <-",self.pedido.precioTotal);
    self.lblPrecioPedido.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.pedido.precioTotal doubleValue]]];

    self.itemsPedido = [self.pedido.itemsPedido allObjects];
    self.productos = @[@"cell1", @"cell2", @"cell3", @"cell4", @"cell5", @"cell6"];
    
    self.listProducts.delegate = self;
    self.listProducts.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATPedidoCellView" bundle:nil];
    [self.listProducts registerNib:cellNib forCellReuseIdentifier:[IATPedidoCellView cellId]];
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return self.productos.count;
    return self.itemsPedido.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATItemPedido *item = [self.itemsPedido objectAtIndex:indexPath.row];
    IATPedidoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATPedidoCellView cellId]];
    
    if (item.producto != nil) {
        cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.producto.nombre];
        cell.lblMarca.text = item.producto.marca;
    }
    else{
        cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.promocion.nombre];
        
        //cell.lblMarca.text = item.producto.marca;
    }
    
    cell.lblUnidad.text = [NSString stringWithFormat:@"Cantidad: %@", item.cantidad];
    cell.lblProveedor.text = [NSString stringWithFormat:@"%@", item.proveedor.nombre];
    cell.lblPrecioItem.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[item.precioTotal doubleValue]]];
    
    
    
    if (cell.imgProducto.image == nil) {
        cell.imgProducto.image = [UIImage imageNamed:@"page"];
        [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFill];
  
        
        NSURL *urlImg;
        if (item.producto != nil) {
            IATProducto *producto = item.producto;
            urlImg = [NSURL URLWithString:producto.urlImagen];
        }
        else{
            IATPromocion *promocion = item.promocion;
            urlImg = [NSURL URLWithString:promocion.urlImagen];
        }
        
        UIImage *imgProducto = [self.cache objectForKey:urlImg];
        if (imgProducto) {
            cell.imgProducto.image = imgProducto;
        }
        else{
            [cell.activityLoad startAnimating];
            [self imageWithUrl:urlImg block:^(UIImage *image){
                if (image != nil) {
                    cell.imgProducto.image = image;
                    [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
                }
                [cell.activityLoad stopAnimating];
            }];
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATPedidoCellView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (IBAction)agregarListaAlPedidoActual:(id)sender {
    
    
    IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
    if (pedido == nil) {
        pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
    }
    
    for (IATItemPedido *item in self.itemsPedido) {
        
        IATItemPedido *newItem;
        if (item.producto != nil) {
            newItem = [IATItemPedido itemPedidoWithCantidad:item.cantidad producto:item.producto pedido:pedido precio:item.precioTotal context:self.context];
        }
        else{
            newItem = [IATItemPedido itemPedidoWithCantidad:item.cantidad promocion:item.promocion pedido:pedido precio:item.precioTotal context:self.context];
        }
        newItem.proveedor = item.proveedor;
        
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Items Agregados"
                                                                   message:@"Items agregados al pedido"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *accion =
    [UIAlertAction actionWithTitle:@"Aceptar"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               [alert dismissViewControllerAnimated:YES completion:nil];
                               [self.navigationController popViewControllerAnimated:YES];
                           }];
    
    [alert addAction:accion];
    [self presentViewController:alert animated:YES completion:nil];

}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

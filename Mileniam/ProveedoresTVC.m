//
//  ProveedoresTVC.m
//  Milenian
//
//  Created by Desarrollador IOS on 15/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "ProveedoresTVC.h"
#import "SWRevealViewController.h"
#import "IATProveedoresCellView.h"
#import "IATProveedor.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "CacheImgs.h"

@interface ProveedoresTVC ()
@property (strong, nonatomic) NSArray *proveedores;
@property (strong, nonatomic) NSCache *cache;
@end

@implementation ProveedoresTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cache = [CacheImgs sharedInstance];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.proveedores = [IATProveedor obtenerProveedoresWithContext:localVar.model.context];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATProveedoresCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATProveedoresCellView cellId]];
    
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.proveedores.count;
    //return self.productosRecomendados.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATProveedoresCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedoresCellView cellId] forIndexPath:indexPath];
    
    IATProveedor *proveedor = [self.proveedores objectAtIndex:indexPath.row];
    
    cell.lblNombre.text = proveedor.nombre;
    cell.descripcion.text = proveedor.descripcion;
    //cell.logoProveedor.image = proveedor.urlLogo;
    
    
    UIImage *imgProducto = [self.cache objectForKey:[NSURL URLWithString:proveedor.urlLogo]];
    if (imgProducto) {
        cell.logoProveedor.image = imgProducto;
    }
    else{
        [cell.activityLoad startAnimating];
        
        [self imageWithUrl:[NSURL URLWithString:proveedor.urlLogo] block:^(UIImage *image) {
            if (image != nil) {
                cell.logoProveedor.image = image;
            }
            [cell.activityLoad stopAnimating];
        }];
    }
    
    
    return cell;
}



#pragma mark - TableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [IATProveedoresCellView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATLista.m instead.

#import "_IATLista.h"

@implementation IATListaID
@end

@implementation _IATLista

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Lista" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Lista";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Lista" inManagedObjectContext:moc_];
}

- (IATListaID*)objectID {
	return (IATListaID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic nombre;

@dynamic precioTotal;

@dynamic items;

- (NSMutableSet<IATItemLista*>*)itemsSet {
	[self willAccessValueForKey:@"items"];

	NSMutableSet<IATItemLista*> *result = (NSMutableSet<IATItemLista*>*)[self mutableSetValueForKey:@"items"];

	[self didAccessValueForKey:@"items"];
	return result;
}

@end

@implementation IATListaAttributes 
+ (NSString *)nombre {
	return @"nombre";
}
+ (NSString *)precioTotal {
	return @"precioTotal";
}
@end

@implementation IATListaRelationships 
+ (NSString *)items {
	return @"items";
}
@end


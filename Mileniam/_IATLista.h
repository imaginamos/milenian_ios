// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATLista.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATItemLista;

@interface IATListaID : NSManagedObjectID {}
@end

@interface _IATLista : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATListaID*objectID;

@property (nonatomic, strong, nullable) NSString* nombre;

@property (nonatomic, strong, nullable) NSDecimalNumber* precioTotal;

@property (nonatomic, strong, nullable) NSSet<IATItemLista*> *items;
- (nullable NSMutableSet<IATItemLista*>*)itemsSet;

@end

@interface _IATLista (ItemsCoreDataGeneratedAccessors)
- (void)addItems:(NSSet<IATItemLista*>*)value_;
- (void)removeItems:(NSSet<IATItemLista*>*)value_;
- (void)addItemsObject:(IATItemLista*)value_;
- (void)removeItemsObject:(IATItemLista*)value_;

@end

@interface _IATLista (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveNombre;
- (void)setPrimitiveNombre:(NSString*)value;

- (NSDecimalNumber*)primitivePrecioTotal;
- (void)setPrimitivePrecioTotal:(NSDecimalNumber*)value;

- (NSMutableSet<IATItemLista*>*)primitiveItems;
- (void)setPrimitiveItems:(NSMutableSet<IATItemLista*>*)value;

@end

@interface IATListaAttributes: NSObject 
+ (NSString *)nombre;
+ (NSString *)precioTotal;
@end

@interface IATListaRelationships: NSObject
+ (NSString *)items;
@end

NS_ASSUME_NONNULL_END

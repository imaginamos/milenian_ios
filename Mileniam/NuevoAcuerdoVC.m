//
//  NuevoAcuerdoVC.m
//  Milenian
//
//  Created by Carlos Obregón on 25/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "NuevoAcuerdoVC.h"
#import "AcuerdoProveedorTVC.h"
#import "AcuerdoSubcategoriasVC.h"
#import "IATProveedor.h"
#import "IATProducto.h"
#import "IATPreItemAcuerdo.h"
#import "IATItemAcuerdo.h"
#import "IATAcuerdo.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATItemAcuerdoCellVC.h"
#import "IATProductoProveedor.h"
#import "MBProgressHUD.h"
#import "Api.h"
#import "CacheImgs.h"

@interface NuevoAcuerdoVC ()
@property (strong, nonatomic) NSMutableArray *listaProductosAcuerdo;
@property(nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) MBProgressHUD *hud;
@property(nonatomic) int moveView;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation NuevoAcuerdoVC

#pragma mark - Ciclo de vida
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tvComentarios.delegate = self;
    self.cache = [CacheImgs sharedInstance];
    
    datePickerDS = [[UIDatePicker alloc] init];
    datePickerDS.datePickerMode = UIDatePickerModeDate;
    [self.tfFechaInicial setInputView:datePickerDS];
    
    datePickerDF = [[UIDatePicker alloc] init];
    datePickerDF.datePickerMode = UIDatePickerModeDate;
    [self.tfFechaFinal setInputView:datePickerDF];
    
    //Toolbar DS
    UIToolbar *toolBarDS = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 34)];
    [toolBarDS setBackgroundColor:[UIColor grayColor]];
    [toolBarDS setTintColor:[UIColor colorWithRed:64/255.0f green:0/255.0f blue:128/255.0f alpha:1]];
    UIBarButtonItem *btnSeleccionarDS =
    [[UIBarButtonItem alloc] initWithTitle:@"Aceptar" style:UIBarButtonItemStylePlain target:self action:@selector(selectDateStar:)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBarDS setItems:[NSArray arrayWithObjects:space, btnSeleccionarDS,nil]];
    [self.tfFechaInicial setInputAccessoryView:toolBarDS];
    
    
    UIToolbar *toolBarDF = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 34)];
    [toolBarDF setBackgroundColor:[UIColor grayColor]];
    [toolBarDF setTintColor:[UIColor colorWithRed:64/255.0f green:0/255.0f blue:128/255.0f alpha:1]];
    UIBarButtonItem *btnSeleccionarDF =
    [[UIBarButtonItem alloc] initWithTitle:@"Aceptar" style:UIBarButtonItemStylePlain target:self action:@selector(selectDateEnd:)];
    [toolBarDF setItems:[NSArray arrayWithObjects:space, btnSeleccionarDF, nil]];
    [self.tfFechaFinal setInputAccessoryView:toolBarDF];
    
    //Creación de gesto Tap: Para ocultar teclado.
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ocultaTeclado:)];
    [tapGesture setNumberOfTouchesRequired:1];
    [[self view] addGestureRecognizer:tapGesture];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.title  = @"Acuerdo nuevo";
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    UIImage *bntAcept = [UIImage imageNamed:@"chulo-ok"];
    
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithImage:bntAcept
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self action:@selector(acept)];
    
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnAcept.imageInsets = UIEdgeInsetsMake(12, 10, 6, 10);
    
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
    self.tfProveedorSeleccionado.leftView = paddingView;
    self.tfProveedorSeleccionado.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingViewFI = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
    self.tfFechaInicial.leftView = paddingViewFI;
    self.tfFechaInicial.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingViewFF = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
    self.tfFechaFinal.leftView = paddingViewFF;
    self.tfFechaFinal.leftViewMode = UITextFieldViewModeAlways;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATItemAcuerdoCellVC" bundle:nil];
    [self.listProductosAcuerdo registerNib:cellNib forCellReuseIdentifier:[IATItemAcuerdoCellVC cellId]];
    
    self.listProductosAcuerdo.dataSource = self;
    self.listProductosAcuerdo.delegate = self;
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self ocultaTeclado:nil];
    if (self.proveedor != nil) {
        self.tfProveedorSeleccionado.text = self.proveedor.nombre;
    }
    
    self.listaProductosAcuerdo = [NSMutableArray arrayWithArray:[IATPreItemAcuerdo obtenerPreItemsAcuerdoWithContext:self.context]];
    
    if ([self.listaProductosAcuerdo count] > 0) {
        self.productosAcuerdo.hidden = NO;
        [self.productosAcuerdo reloadData];
        
    }
    else{
        self.productosAcuerdo.hidden = YES;
    }
    
    [self.listProductosAcuerdo reloadData];
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [IATPreItemAcuerdo deleteAllWithContext:self.context];
        self.listaProductosAcuerdo = nil;
        self.proveedor = nil;
    }
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
}



#pragma mark - Target/Actions
- (void)acept{
    
    if ([self.listaProductosAcuerdo count] > 0) {
        
        [self enableControls:NO];
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        Api *api =[Api sharedInstance];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *keySesion = [defaults stringForKey:@"kKey"];
        NSString *idCustomer = [defaults stringForKey:@"kIdCustomer"];
        NSDate* now = [NSDate date];
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateStar = [dateFormat dateFromString:self.tfFechaInicial.text];
        NSDate *dateFinish = [dateFormat dateFromString:self.tfFechaFinal.text];
        
        
        if(self.tvComentarios.text == nil ||  [self.tvComentarios.text isEqualToString:@""]){
            self.tvComentarios.text = @"";
        }
        
        //Acuerdo
        IATAcuerdo *newAcuerdo =
        [IATAcuerdo acuerdoWithId:@"1"
                      comentarios:self.tvComentarios.text
                      fechaInicio:dateStar
                       fechaFinal:dateFinish
                          context:self.context];
        
        newAcuerdo.provedor = self.proveedor;
        
        //Items del acuerdo
        for (IATPreItemAcuerdo *object in self.listaProductosAcuerdo) {
            
            IATItemAcuerdo *itemAcuerdo =
            [IATItemAcuerdo itemAcuerdoWithCantidad:object.cantidad context:self.context];
            itemAcuerdo.acuerdo = newAcuerdo;
            itemAcuerdo.producto = object.producto;
            
        }
        
        NSMutableArray *listProducts = [NSMutableArray array];
        
        for (int i = 0; i < self.listaProductosAcuerdo.count; i++) {
            IATPreItemAcuerdo *item = [self.listaProductosAcuerdo objectAtIndex:i];
            
            NSDictionary *itemPedido = @{@"idProduct": item.producto.idEntity, @"quantity": item.cantidad};
            
            [listProducts addObject:itemPedido];
        }
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:listProducts options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"jsonData as string:\n%@", jsonString);
        
        NSDictionary *parameters = @{@"dateRequest":now, @"keys":keySesion, @"idCustomer":idCustomer, @"dataAgreementsItems":jsonString, @"idShop":self.proveedor.idEntity, @"dateStart":newAcuerdo.fechaInicio, @"dateEnd":newAcuerdo.fechaFinal, @"comment":self.tvComentarios.text};
        
        [api enviarAcuerdo:parameters success:^(BOOL success, id response){
            if(success){
                
                newAcuerdo.idAcuerdo = [response objectForKey:@"IdAgreements"];
                newAcuerdo.estado = [response objectForKey:@"Status"];
                
                //Limpiar Cantidades por producto
                [IATPreItemAcuerdo deleteAllWithContext:self.context];
                
                UIAlertController *alert =
                [UIAlertController alertControllerWithTitle:@"Acuerdo realizado"
                                                    message:@"Su acuerdo ha sido realizado con exito" //[response description]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   [self.navigationController popViewControllerAnimated:YES];
                                                               }];
                [alert addAction:accion];
                self.hud.hidden = YES;
                [self presentViewController:alert animated:YES completion:nil];
                [self enableControls:YES];
                
            }
            else{
                [self.context deleteObject:newAcuerdo];
                if([newAcuerdo isDeleted]) {
                    
                    NSError *errorInfo = nil;
                    
                    if([self.context save:&errorInfo]){
                        
                        NSLog(@"Actualizado el contexto;");
                    }
                    else{
                        NSLog(@"Error al eliminar: %@", errorInfo);
                    }
                }
                
                //NSError *error = response;
                NSLog(@"%@", response);
                UIAlertController *alert =
                [UIAlertController alertControllerWithTitle:@"Acuerdo fallido"
                                                    message:@"Su acuerdo no se ha podido realizar con exito"//error.description
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                               }];
                [alert addAction:accion];
                self.hud.hidden = YES;
                [self presentViewController:alert animated:YES completion:nil];
                [self enableControls:YES];
                
            }
        }];
    }
    else{
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@"Acuerdo"
                                            message:@"Debe seleccionar por lo menos un producto"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        self.hud.hidden = YES;
        [self presentViewController:alert animated:YES completion:nil];
        [self enableControls:YES];
    }
    
    
    
}
- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)seleccionarProveedor:(id)sender{
    
    [self ocultaTeclado:nil];
    if (self.proveedor != nil && self.listaProductosAcuerdo.count > 0) {
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@"Advertencia"
                                            message:@"Si cambia de proveedor se eliminaran los productos seleccionados actualmente. Desea continuar?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           
                                                           //Eliminar productos seleccionados
                                                           [IATPreItemAcuerdo deleteAllWithContext:self.context];
                                                           self.listaProductosAcuerdo = nil;
                                                           
                                                           AcuerdoProveedorTVC *selProveedor = [self.storyboard instantiateViewControllerWithIdentifier:@"acuerdoProveedorTVC"];
                                                           
                                                           selProveedor.nuevoAcuerdoVC = self;
                                                           [self.navigationController pushViewController:selProveedor animated:YES];
                                                       }];
        UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        [alert addAction:accion];
        [alert addAction:cancelar];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        AcuerdoProveedorTVC *selProveedor = [self.storyboard instantiateViewControllerWithIdentifier:@"acuerdoProveedorTVC"];
        
        selProveedor.nuevoAcuerdoVC = self;
        [self.navigationController pushViewController:selProveedor animated:YES];
    }
    
    
}
- (IBAction)seleccionarProductos:(id)sender{
    
    [self ocultaTeclado:nil];
    if (self.proveedor != nil) {
        if ([[IATProductoProveedor filtrarSubcategoriasWithProveedor:self.proveedor] count] > 0) {
            AcuerdoSubcategoriasVC *subcategoriasAcuerdo = [self.storyboard instantiateViewControllerWithIdentifier:@"acuerdoSubcategoriasVC"];
            
            subcategoriasAcuerdo.proveedor = self.proveedor;
            [self.navigationController pushViewController:subcategoriasAcuerdo animated:YES];
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                           message:@"Este mayorista, no tiene productos registrados aun, por favor seleccione otro."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            [self presentViewController:alert animated:YES completion:nil];
        }
        

    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Debe primero seleccionar un proveedor"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (IBAction)cargarDatePicker:(id)sender {
    
    if ([sender isEqual:self.tfFechaInicial])
    {
        [datePickerDS addTarget:self action:@selector(datePickerValueChangedStar:) forControlEvents:UIControlEventValueChanged];
    }
    else{
        if (![self.tfFechaInicial.text isEqualToString:@""]) {
            [datePickerDF addTarget:self action:@selector(datePickerValueChangedFinish:) forControlEvents:UIControlEventValueChanged];
        }
        else{
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Alerta"
                                                message:@"Primero debe seleccionar una fecha de inicio"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
    
}
- (IBAction)ocultarDatePicker:(id)sender {
    [self ocultaTeclado:nil];
}

- (void)selectDateStar:(id)sender{
    
    if ([self esMayorIgual:[datePickerDS date]]) {
        NSDateFormatter *dateFormatter = [self darFormatoFecha];
        self.tfFechaInicial.text = [dateFormatter stringFromDate:[datePickerDS date]];
        
        if (![self esMenor:[datePickerDS date]]) {
            self.tfFechaFinal.text = @"";
        }
        
    }
    else{
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@"Alerta"
                                            message:@"La fecha debe ser mayor o igual a la de hoy"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [self.tfFechaInicial resignFirstResponder];
    
}
- (void)selectDateEnd:(id)sender{
    NSDateFormatter *dateFormatter = [self darFormatoFecha];
    if([self esMayor:[datePickerDF date]]){
        self.tfFechaFinal.text = [dateFormatter stringFromDate:[datePickerDF date]];
    }
    else{
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@"Alerta"
                                            message:@"La fecha debe ser mayor a la fecha de inicio"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [self.tfFechaFinal resignFirstResponder];
    
}

#pragma mark - Date picker
- (void)datePickerValueChangedStar:(id)sender{
    if([self esMayorIgual:[sender date]]){
        
        NSDateFormatter *dateFormatter = [self darFormatoFecha];
        self.tfFechaInicial.text = [dateFormatter stringFromDate:[sender date]];
        
        if (![self esMenor:[sender date]]) {
            self.tfFechaFinal.text = @"";
        }
    }
    else{
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@"Alerta"
                                            message:@"La fecha debe ser mayor o igual a la de hoy"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }

}
- (void)datePickerValueChangedFinish:(id)sender{
    
    NSDateFormatter *dateFormatter = [self darFormatoFecha];
    if([self esMayor:[sender date]]){
        self.tfFechaFinal.text = [dateFormatter stringFromDate:[sender date]];
    }
    else{
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@"Alerta"
                                            message:@"La fecha debe ser mayor a la fecha de inicio"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listaProductosAcuerdo.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IATPreItemAcuerdo *preItem = [self.listaProductosAcuerdo objectAtIndex:indexPath.row];
    IATItemAcuerdoCellVC *cellItem =
    [tableView dequeueReusableCellWithIdentifier:[IATItemAcuerdoCellVC cellId]];
    
    cellItem.lblNombre.text = preItem.producto.nombre;
    cellItem.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[preItem.precio doubleValue]]];
    cellItem.lblCantidad.text = [NSString stringWithFormat:@"Cantidad: %@", preItem.cantidad];
    
    if (![preItem.producto.urlImagen isEqualToString:@""] && preItem.producto.urlImagen != nil) {
        
        UIImage *imgProducto = [self.cache objectForKey:[NSURL URLWithString:preItem.producto.urlImagen]];
        if (imgProducto) {
            cellItem.imgProducto.image = imgProducto;
        }
        else{
            [cellItem.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:preItem.producto.urlImagen] block:^(UIImage *image) {
                if (image != nil) {
                    cellItem.imgProducto.image = image;
                }
                [cellItem.activityLoad stopAnimating];
            }];
        }
    }
    
    return cellItem;
}

#pragma mark - Table view delegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //Tomo el elemento a elimar antes de que sea quitado
        IATPreItemAcuerdo *deletePreItem = [self.listaProductosAcuerdo objectAtIndex:indexPath.row];
        
        //Lo remuevo de la tabla
        [self.listaProductosAcuerdo removeObjectAtIndex:indexPath.row];
        
        //Lo elimino de base de datos
        [self.context deleteObject:deletePreItem];
        if([deletePreItem isDeleted]) {
            
            NSError *errorInfo = nil;
            
            if([self.context save:&errorInfo]){
                
                NSLog(@"Actualizado el contexto;");
            }
            else{
                NSLog(@"Error al eliminar: %@", errorInfo);
            }
        }
        
        //Refresco la tabla
        [tableView reloadData];
        
        if (self.listaProductosAcuerdo.count == 0) {
            self.listProductosAcuerdo.hidden = YES;
        }
        
        
    }
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [IATItemAcuerdoCellVC cellHeight];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *sectionName = @"Productos seleccionados";
    return sectionName;
    
}




#pragma mark - Utilidades
//Validaciones
- (BOOL)esMayor:(NSDate *)fechaFinal{
    
    NSComparisonResult result;
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateInicio = [dateFormat dateFromString:self.tfFechaInicial.text];
    
    NSDateFormatter *dateFormatter = [self darFormatoFecha];
    NSString *fechafin = [dateFormatter stringFromDate:fechaFinal];
    
    result = [dateInicio compare:[dateFormat dateFromString:fechafin]]; // comparing two dates
    
    if(result==NSOrderedAscending){
        NSLog(@"La fecha de inicio es menor");
        return YES;
    }
    
    return NO;
}

- (BOOL)esMenor:(NSDate *)fechaInicial{
    
    NSComparisonResult result;
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSDate *fechaFinal = [dateFormat dateFromString:self.tfFechaFinal.text];
    
    NSDateFormatter *dateFormatter = [self darFormatoFecha];
    NSString *fechaInicio = [dateFormatter stringFromDate:fechaInicial];
    
    result = [[dateFormat dateFromString:fechaInicio] compare:fechaFinal];
    
    if(result==NSOrderedAscending){
        NSLog(@"La fecha de inicio es menor");
        return YES;
    }
    
    
    return NO;
}
//Validaciones
- (BOOL)esMayorIgual:(NSDate *)fechaInicial{
    
    NSComparisonResult result;
    NSDateFormatter *dateFormatter = [self darFormatoFecha];
    NSString *dateInicio = [dateFormatter stringFromDate:[NSDate date]];
    
    result = [[dateFormatter dateFromString:dateInicio] compare:fechaInicial];
    
    if(result==NSOrderedAscending){
        NSLog(@"La fecha de hoy es mayor");
        return YES;
    }
    else if (result == NSOrderedSame){
        NSLog(@"La fecha de hoy es igual");
        return YES;
    }
    
    return NO;
}

//Dar formato al precio
- (NSString *)formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}
//Dar formato al fecha
- (NSDateFormatter *)darFormatoFecha{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    return dateFormatter;
    
}
//Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}
//Ocultar teclado
- (void)ocultaTeclado:(UITapGestureRecognizer *)sender{
    
    [self.tvComentarios resignFirstResponder];
    [self.tfProveedorSeleccionado resignFirstResponder];
    [self.tfFechaInicial resignFirstResponder];
    [self.tfFechaFinal resignFirstResponder];
    
}

#pragma mark - Habilitar y deshabilitar controles
-(void)enableControls:(BOOL)enable{
    self.tfFechaFinal.enabled = enable;
    self.tfFechaInicial.enabled = enable;
    self.tfProveedorSeleccionado.enabled = enable;
    self.btnSeleccionarProductos.enabled = enable;
    self.btnSeleccionarProveedor.enabled = enable;
    
    self.tvComentarios.editable = enable;
    self.listProductosAcuerdo.scrollEnabled = enable;
}

#pragma mark - Movimiento de la vista
- (void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= self.moveView;
        rect.size.height += self.moveView;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += self.moveView;
        rect.size.height -= self.moveView;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if  (self.view.frame.origin.y >= 0)
    {
        self.moveView = 100;
        [self setViewMovedUp:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.moveView = 100;
    [self setViewMovedUp:NO];
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

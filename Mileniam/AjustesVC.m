//
//  AjustesVC.m
//  Mileniam
//
//  Created by Desarrollador IOS on 27/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "AjustesVC.h"
#import "SWRevealViewController.h"
#import "NSString+MD5.h"
#import "Api.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATSectorEconomico.h"

@interface AjustesVC ()
@property(nonatomic, strong) UITapGestureRecognizer *scrollGestureRecognizer;
@property(nonatomic, strong) UIImagePickerController *controladorImagen;
@property(nonatomic, strong) NSManagedObjectContext *context;
@property(nonatomic, strong) NSArray *tipologias;
@property(nonatomic, strong) UIImage *logo;
@property(nonatomic) CGRect rectOrig;
@property(nonatomic) int moveView;
@property(nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) IATSectorEconomico *sectorEconomyc;
@end

@implementation AjustesVC
#pragma mark - Ciclo de vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Perfil";
    self.tfSector.delegate = self;
    
    //Capturando frame de la vista
    self.rectOrig = self.view.frame;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //Creando, captura de gesto
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ocultaTeclado:)];
    [tapGesture setNumberOfTouchesRequired:1];
    [[self view] addGestureRecognizer:tapGesture];
    
    self.pickerTipologia.delegate = self;
    self.pickerTipologia.dataSource = self;
    
    self.tipologias = [IATSectorEconomico obtenerTipologiaWithContext:self.context];
    if (self.tipologias.count == 0) {
        [self loadTipologias];
    }
    else{
        NSString *idSector = [defaults stringForKey:@"kSector"];
        IATSectorEconomico *sector = [IATSectorEconomico obtenerTipologiaWithId:idSector context:self.context];
        if (sector != nil) {
            self.tfSector.text = sector.nombre;
            self.sectorEconomyc = sector;
        }
    }
    
    NSString *email = [defaults stringForKey:@"kEmail"];
    self.tfEmail.text = email;
    
    NSString *cif = [defaults stringForKey:@"kCif"];
    self.tfCif.text = cif;
    
    NSString *phone = [defaults stringForKey:@"kPhone"];
    self.tfTelefono.text = phone;
    
    NSString *company = [defaults stringForKey:@"kCompany"];
    self.tfNombre.text = company;

    NSString *address = [defaults stringForKey:@"kAddress"];
    self.tfDireccion.text = address;
    
    //self.tfSector.text = address;

    [defaults synchronize];
    
    NSString  *imagePath =
    [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/someImageName.png"];
    NSFileManager *gestorArchivos = [NSFileManager defaultManager];
    
    if ([gestorArchivos fileExistsAtPath:imagePath] == YES) {
        
        if (imagePath != nil) {
            UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
            self.imgUser.image = image;
        }
    }
    else{
        NSLog(@"Aun no se ha guardado imagen de perfil");
    }

}

#pragma mark - Cargar tipologias
- (void)loadTipologias{
    
    NSString *keys = COOKIE_KEY;
    
    NSDictionary *params = @{@"keys":keys, @"firstTime":@"1"};
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.label.text = @"Descargando Tipologia";
    [self enableControls:NO];
    [self hideControls:YES];
    
    Api *api =[Api sharedInstance];
    
    [api obtenerTipologia:params success:^(BOOL success, id response) {
        if(success) {
            [self saveTipologias:response];
            self.tipologias = [IATSectorEconomico obtenerTipologiaWithContext:self.context];
            self.hud.hidden = YES;
            //[self enableControls:YES];
            [self hideControls:NO];
            [self.pickerTipologia reloadAllComponents];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *idSector = [defaults stringForKey:@"kSector"];
            IATSectorEconomico *sector = [IATSectorEconomico obtenerTipologiaWithId:idSector context:self.context];
            if (sector != nil) {
                self.tfSector.text = sector.nombre;
                self.sectorEconomyc = sector;
            }
        }
        else{
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Error"
                                                message:@"No se cargaron las tipologias de negocio"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *accion =
            [UIAlertAction actionWithTitle:@"Aceptar"
                                     style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       self.hud.hidden = YES;
                                       [self enableControls:YES];
                                       [self hideControls:NO];
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
            
            [alert addAction:accion];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }];
    
    
}
- (void)saveTipologias:(NSDictionary *)data{
    
    NSArray *tipologias = [data objectForKey:@"SectorEconomic"];
    
    if ([tipologias isKindOfClass:[NSArray class]]){
        if (tipologias != nil && tipologias.count > 0) {
            for (id objeto in tipologias) {
                
                NSString *idTipologia, *nombre;
                
                idTipologia = [self changeNullToString:[objeto objectForKey:@"IdSector"]];
                nombre = [self changeNullToString:[objeto objectForKey:@"Name"]];
                
                if ([IATSectorEconomico obtenerTipologiaWithId:idTipologia context:self.context] == nil) {
                    [IATSectorEconomico tipologiaWithId:idTipologia nombre:nombre context:self.context];
                }
            }
        }
    }
}

#pragma mark - Delegado de navigation bar
- (void)acept{
    
    if (self.tfCif.text.length > 0 && self.tfTelefono.text.length > 0  && self.tfNombre.text.length > 0  && self.tfSector.text.length > 0 && self.tfDireccion.text.length > 0) {
        
        //Validar Campos
        if ([self validateForm]) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            //[defaults setObject:self.tfSector.text forKey:@"kTipologia"];
            //[defaults setObject:self.tfEmail.text forKey:@"kEmail"];
            [defaults setObject:self.tfTelefono.text forKey:@"kPhone"];
            [defaults setObject:self.tfCif.text forKey:@"kCif"];
            [defaults setObject:self.tfNombre.text forKey:@"kCompany"];
            [defaults setObject:self.tfDireccion.text forKey:@"kAddress"];
            [defaults setObject:self.sectorEconomyc.idSector forKey:@"kSector"];
            
            [defaults synchronize];
            
            NSString  *imagePath =
            [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/someImageName.png"];
            
            BOOL isSave = [UIImagePNGRepresentation(self.logo) writeToFile:imagePath atomically:YES];
            NSLog(isSave ? @"Foto almacenada localmente" : @"La foto no se almaceno localmente");
            
            NSString *idCustomer = [defaults stringForKey:@"kIdCustomer"];
            NSString *keys = [defaults stringForKey:@"kKey"];
            
            NSDictionary *params = @{@"cif":self.tfCif.text, @"nameCompany":self.tfNombre.text, @"phoneCompany": self.tfTelefono.text, @"addressCompany": self.tfDireccion.text , @"economySector":self.sectorEconomyc.idSector, @"keys":keys, @"idCustomer":idCustomer};
            
            //Profile
            Api *api =[Api sharedInstance];
            
            [api enviarAjustesPerfil:params success:^(BOOL success, id response) {
                if(success) {
                    NSDictionary *dictionary = (NSDictionary *)response;
                    NSLog(@"%@", dictionary);
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Datos completos"
                                                                                   message:@"Datos actualizados exitosamente"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Cerrar"
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction *action) {
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   }];
                    [alert addAction:accion];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }];
            
            UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithTitle:@"Editar" style:UIBarButtonItemStylePlain target:self action:@selector(editarPerfil:)];
            self.navigationItem.rightBarButtonItem = barBtnAcept;
            [self enableControls:NO];
        }
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Datos incompletos"
                                                                       message:@"Debe completar cada campo requerido"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Cerrar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    
}
- (BOOL)validateForm{
    if (![self NSStringIsValidCIF:self.tfCif.text] && ![self NSStringIsValidNIF:self.tfCif.text]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Ingrese un CIF o NIF valido, CIF: 1letra + 8números, NIF:8números + 1letra."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (IBAction)editarPerfil:(id)sender {
    
    
    [self enableControls:YES];
    
    UIImage *bntAcept = [UIImage imageNamed:@"chulo-ok"];
    
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithImage:bntAcept style:UIBarButtonItemStylePlain target:self action:@selector(acept)];
    
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnAcept.imageInsets = UIEdgeInsetsMake(12, 10, 6, 10);
    
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    
}
- (IBAction)seleccionarImgUser:(id)sender {
    
    NSLog(@"Boton añadir logo presionado");
    
    self.controladorImagen =  [[UIImagePickerController alloc] init];
    
    self.controladorImagen.delegate = self;
    
    [self.controladorImagen setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:self.controladorImagen animated:YES completion:^{
        NSLog(@"Pasando a carrete");
    }];
    
}

#pragma mark - Movimiento de la vista
- (void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= self.moveView;
        rect.size.height += self.moveView;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += self.moveView;
        rect.size.height -= self.moveView;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
- (IBAction)editingDidBeginTfNombre:(id)sender {
    if ([sender isEqual:self.tfNombre])
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
        self.tfNombre.leftView = paddingView;
        self.tfNombre.leftViewMode = UITextFieldViewModeAlways;
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 300;
            [self setViewMovedUp:NO];
        }
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 130;
            [self setViewMovedUp:YES];
        }
    }
}
- (IBAction)editingDidEndTfNombre:(id)sender {
    if ([sender isEqual:self.tfNombre])
    {
        if (!(self.tfNombre.text.length > 0)) {
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            self.tfNombre.leftView = paddingView;
            self.tfNombre.leftViewMode = UITextFieldViewModeAlways;
            [self.tfNombre.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
        }
        if  (self.view.frame.origin.y <= 0){
            self.moveView = 130;
            [self setViewMovedUp:NO];
        }
        
    }
}

- (IBAction)editingDidBeginTfCif:(id)sender {
    if ([sender isEqual:self.tfCif])
    {
        
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 300;
            [self setViewMovedUp:NO];
        }
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 190;
            [self setViewMovedUp:YES];
        }
    }
}
- (IBAction)editingDidEndTfCif:(id)sender {
    if ([sender isEqual:self.tfCif])
    {
        if  (self.view.frame.origin.y <= 0){
            self.moveView = 190;
            [self setViewMovedUp:NO];
            
        }
    }
}

- (IBAction)editingDidBeginTfTelefono:(id)sender {
    if ([sender isEqual:self.tfTelefono])
    {
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 300;
            [self setViewMovedUp:NO];
        }
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 250;
            [self setViewMovedUp:YES];
        }
    }
}
- (IBAction)editingDidEndTfTelefono:(id)sender {
    if ([sender isEqual:self.tfTelefono])
    {
        if  (self.view.frame.origin.y <= 0){
            self.moveView = 250;
            [self setViewMovedUp:NO];
        }
    }
}

- (IBAction)editingDidBeginTfDireccion:(id)sender {
    if ([sender isEqual:self.tfDireccion])
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
        self.tfDireccion.leftView = paddingView;
        self.tfDireccion.leftViewMode = UITextFieldViewModeAlways;
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 300;
            [self setViewMovedUp:NO];
        }
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 310;
            [self setViewMovedUp:YES];
        }
        else{
            
        }
    }
}
- (IBAction)editingDidEndTfDireccion:(id)sender {
    if ([sender isEqual:self.tfDireccion])
    {
        if (!(self.tfDireccion.text.length > 0)) {
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            self.tfDireccion.leftView = paddingView;
            self.tfDireccion.leftViewMode = UITextFieldViewModeAlways;
            [self.tfDireccion.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
        }
        if  (self.view.frame.origin.y <= 0){
            self.moveView = 310;
            [self setViewMovedUp:NO];
        }
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if ([textField isEqual:self.tfSector])
    {
        
        if (self.pickerTipologia.hidden) {
            [self ocultaTeclado:nil];
            self.moveView = 300;
            [self setViewMovedUp:YES];
            
            self.pickerTipologia.hidden = NO;
        }
        
    }
    else{
        return YES;
    }
    
    return NO;
}


#pragma mark - Deleagado de UIImagePickerController
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editInfo {
    
    
    self.logo = img;
    self.imgUser.image = img;
    
    [self.imgUser setContentMode:UIViewContentModeScaleAspectFill];
    [self.imgUser setClipsToBounds:YES];
    self.imgUser.layer.masksToBounds = YES;
    self.imgUser.layer.cornerRadius = self.imgUser.bounds.size.width/2;
    self.imgUser.layer.borderWidth = 1.0;
    self.imgUser.layer.borderColor = (__bridge CGColorRef)([UIColor lightGrayColor]);
    
    self.imgUser.hidden = NO;
    //self.btnAddImg.hidden = YES;
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Volviendo a vista");
    }];
    
}

#pragma mark - UIPickerView Delegado
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.tipologias.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component{
    
    IATSectorEconomico *sector = [self.tipologias objectAtIndex:row];
    return (NSString*)[sector nombre];
}
-(void)pickerView:(UIPickerView *)pickerView
     didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component{
    IATSectorEconomico *sector = [self.tipologias objectAtIndex:row];
    self.sectorEconomyc = sector;
    self.tfSector.text = sector.nombre;
}

#pragma mark - Ocultar teclado
-(void)ocultaTeclado:(UITapGestureRecognizer *)sender{
    
    //Aquí hay que declarar todos los UITextField de nuestra escena
    [self.tfCif resignFirstResponder];
    [self.tfEmail resignFirstResponder];
    [self.tfNombre resignFirstResponder];
    [self.tfSector resignFirstResponder];
    [self.tfDireccion resignFirstResponder];
    [self.tfTelefono resignFirstResponder];
    
    self.view.frame = self.rectOrig;
    
}

#pragma mark - Habilitar y deshabilitar controles
-(void)enableControls:(BOOL)enable{
    //self.tfEmail.enabled = enable;
    self.tfCif.enabled = enable;
    self.tfNombre.enabled = enable;
    self.tfTelefono.enabled = enable;
    self.tfSector.enabled = enable;
    self.tfDireccion.enabled = enable;
    self.btnPickerImage.enabled = enable;
}
-(void)hideControls:(BOOL)enable{
    self.imgUser.hidden = enable;
    self.tfCif.hidden = enable;
    self.tfNombre.hidden = enable;
    self.tfDireccion.hidden = enable;
    self.tfTelefono.hidden = enable;
    self.tfSector.hidden = enable;
    self.tfEmail.hidden = enable;
    self.tfDireccion.hidden = enable;
    self.btnPickerImage.hidden = enable;
}
-(NSString *)changeNullToString:(NSString *)string{
    
    if ( string == (NSString *)[NSNull null] )
    {
        string = @"";
    }
    
    return string;
}

//Validaciones
- (BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL) NSStringIsValidCIF:(NSString *)checkString{
    NSString *cifFilterString = @"^[A-Za-z]{1}+[0-9]{9}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cifFilterString];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL) NSStringIsValidNIF:(NSString *)checkString{
    NSString *nifFilterString = @"^[0-9]{9}+[A-Za-z]{1}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nifFilterString];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.tfCif]) {
        if ([[self.tfCif text] length] + [string length] - range.length > 9) {
            return NO;
        }
        else{
            return YES;
        }
    }
    else if ([textField isEqual:self.tfTelefono]){
        if ([[textField text] length] + [string length] - range.length > 20) {
            return NO;
        }
        else{
            return YES;
        }
    }
    else {
        if ([[textField text] length] + [string length] - range.length > 100) {
            return NO;
        }
        else{
            return YES;
        }
    }
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

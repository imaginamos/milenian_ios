#import "_IATUsuario.h"

@interface IATUsuario : _IATUsuario

+(instancetype) usuarioWithId:(NSString *)idUsuario
                       nombre:(NSString *)nombre
                  descripcion:(NSString *)descripcion
                     telefono:(NSString *)telefono
                  tipoUsuario:(NSString *)tipoUsuario
                     urlImage:(NSString *)urlImage
                      context:(NSManagedObjectContext *)context;

@end

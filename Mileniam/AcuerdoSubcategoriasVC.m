//
//  AcuerdoSubcategoriasVC.m
//  Milenian
//
//  Created by Carlos Obregón on 26/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "AcuerdoSubcategoriasVC.h"
#import "SWRevealViewController.h"
#import "VariedadVC.h"
#import "AcuerdosProductosVC.h"
#import "IATVariedad.h"
#import "IATCategoria.h"
#import "IATSubcategoria.h"
#import "IATVariedadCellView.h"
#import "AppDelegate.h"
#import "IATPedido.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATProductoProveedor.h"
#import "CacheImgs.h"

@interface AcuerdoSubcategoriasVC ()
@property (nonatomic, strong) NSArray *subcategorias;
@property (nonatomic, strong) NSArray *subitems;
@property (nonatomic, strong) NSArray *variedades;
@property (nonatomic, strong) IATSubcategoria *subcategoria;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation AcuerdoSubcategoriasVC

#pragma mark - Ciclo de vida
-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Subcategorias";
    self.cache = [CacheImgs sharedInstance];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    //self.subcategorias = [IATSubcategoria obtenerSubcategoriasWithContext:self.context];
    self.subcategorias = [IATProductoProveedor filtrarSubcategoriasWithProveedor:self.proveedor];
    //Traer solo subcategorias del proveedor seleccionado
    self.subcategoria = [self.subcategorias objectAtIndex:0];
    //self.variedades = [subCatIni.variedades allObjects];
    self.variedades = [IATProductoProveedor filtrarVariedadesWithProveedor:self.proveedor subcategoria:self.subcategoria];
    NSLog(@"Cargando subcategorias");
    
    _carousel.delegate = self;
    _carousel.dataSource = self;
    _carousel.type = iCarouselTypeLinear;
    
    self.listSubcategories.delegate = self;
    self.listSubcategories.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATVariedadCellView" bundle:nil];
    [self.listSubcategories registerNib:cellNib forCellReuseIdentifier:[IATVariedadCellView cellId]];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.title = @"Subcategorias";
    self.carousel.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.carousel.hidden = YES;
}
-(void)searchSubCategoria{
    

}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.variedades.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATVariedad *variedad = [self.variedades objectAtIndex:indexPath.row];
    IATVariedadCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATVariedadCellView cellId]];
    cell.nombreVariedad.text = variedad.nombre;
    cell.cantidadProductos.text =  [NSString stringWithFormat:@"%u productos", [[IATProductoProveedor filtrarVariedadesWithProveedor:self.proveedor subcategoria:self.subcategoria variedad:variedad] count]];
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AcuerdosProductosVC *variedad = [self.storyboard instantiateViewControllerWithIdentifier:@"acuerdosProductosVC"];
    variedad.variedad = [self.variedades objectAtIndex:indexPath.row];
    variedad.proveedor = self.proveedor;
    variedad.productos = [IATProductoProveedor filtrarVariedadesWithProveedor:self.proveedor subcategoria:self.subcategoria variedad:[self.variedades objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:variedad animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [IATVariedadCellView cellHeight];
}


#pragma mark iCarousel methods
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.subcategorias.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    //Vista principal
    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 248.0f, 266.0f)];
    view.backgroundColor = [UIColor clearColor];
    
    
    //Activity
    UIActivityIndicatorView *actView =
    [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(100, 100, 50.0f, 50.0f)];
    actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    
    
    //Placeholder
    UIImageView *placeholder = [[UIImageView alloc] initWithFrame:view.bounds];
    placeholder.image = [UIImage imageNamed:@"cuadro-blanco-sombriado"];
    [placeholder setContentMode:UIViewContentModeScaleToFill];
    [view addSubview:placeholder];
    
    
    
//    UIImageView *imgSubCategoria = [[UIImageView alloc] initWithFrame:CGRectMake(4.35f, 14, 238.92f, 238.92f)];
//    [imgSubCategoria setContentMode:UIViewContentModeScaleAspectFit];
    
    //Modo online
    IATSubcategoria *subcategoria = [self.subcategorias objectAtIndex:index];
    NSString *urlSubCat = subcategoria.urlImagen;
    NSURL *url = [NSURL URLWithString:urlSubCat];
    
    UIImage *imgSubCat = [self.cache objectForKey:url];
    if (imgSubCat) {
        placeholder.image = imgSubCat;
        [placeholder setContentMode:UIViewContentModeScaleAspectFit];
    }
    else{
        [placeholder addSubview:actView];
        [actView startAnimating];
        [self imageWithUrl:url block:^(UIImage *image) {
            if (image != nil) {
                placeholder.image = image;
                [placeholder setContentMode:UIViewContentModeScaleAspectFit];
            }
            [actView stopAnimating];
        }];
    }
    
    
    //[placeholder addSubview:imgSubCategoria];
    view.contentMode = UIViewContentModeCenter;
    
    return view;
    
}
- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}
- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    
    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 250.0f, 250.0f)];
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 240.0f, 240.0f)];
    [imgV setContentMode:UIViewContentModeScaleAspectFit];
    //UIImage *img = [self.items objectAtIndex:index];
    //imgV.image = img;
    
    //UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 180, 180, 21)];
    //label.font = [UIFont fontWithName:UIFontTextStyleBody size:10];
    //label.text = [self.subcategorias objectAtIndex:index];
    //label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:imgV];
    //[view addSubview:label];
    //((UIImageView *)view).image = img;
    view.contentMode = UIViewContentModeCenter;
    
    return view;
}
- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
}
- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return NO;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}
- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel{
    
    NSInteger index = [self.carousel currentItemIndex];
    
    //Online
    self.subcategoria = [self.subcategorias objectAtIndex:index];
    self.variedades = [IATProductoProveedor filtrarVariedadesWithProveedor:self.proveedor subcategoria:self.subcategoria];
    
    
    [self.listSubcategories reloadData];
    
}


#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}

#pragma mark - Manejo de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)dealloc{
    self.carousel = nil;
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
    
}

@end

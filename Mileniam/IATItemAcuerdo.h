#import "_IATItemAcuerdo.h"

@interface IATItemAcuerdo : _IATItemAcuerdo

+(instancetype) itemAcuerdoWithCantidad:(NSNumber *)cantidad
                                context:(NSManagedObjectContext *)context;

+(instancetype) itemAcuerdoWithCantidad:(NSNumber *)cantidad
                                acuerdo:(IATAcuerdo *)acuerdo
                                context:(NSManagedObjectContext *)context;

+(instancetype) obtenerItemAcuerdoWithAcuerdo:(IATAcuerdo *)acuerdo
                                     producto:(IATProducto *)producto
                                      context:(NSManagedObjectContext *)context;

+(BOOL) itemAcuerdoExisteWithProducto:(IATProducto *)producto
                              context:(NSManagedObjectContext *)context;

@end

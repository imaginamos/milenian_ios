#import "_IATPedido.h"

@interface IATPedido : _IATPedido

+(instancetype) pedidoWithId:(NSString *)idPedido
                      estado:(NSString *)estado
                       fecha:(NSDate *)fecha
                     context:(NSManagedObjectContext *)context;

+(instancetype) obtenerPedidoWithId:(NSString *)idPedido
                            context:(NSManagedObjectContext *)context;

+(instancetype) obtenerpedidoWithEstado:(NSString *)estado
                                context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerPedidosWithContext:(NSManagedObjectContext *)context;
+(NSArray *) obtenerPedidosEnProcesoWithContext:(NSManagedObjectContext *)context;
+(NSArray *) obtenerPedidosAprobadosWithContext:(NSManagedObjectContext *)context;

@end

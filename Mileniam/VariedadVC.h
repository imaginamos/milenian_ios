//
//  VariedadVC.h
//  Milenian
//
//  Created by Desarrollador IOS on 25/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@import AVFoundation;
#import "IATVariedad.h"

#define ACTUALIZAR_CELDA_CANTIDAD @"ActualizarCeldaCantidad"

@interface VariedadVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) AVAudioPlayer* soundAdd;

@property (weak, nonatomic) IBOutlet UICollectionView *variedadCV;
@property (weak, nonatomic) IBOutlet UILabel *subTotal;

@property (strong, nonatomic) IATVariedad *variedad;




@end

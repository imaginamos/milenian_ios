//
//  ViewController.h
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;

#define ACCESO_KEY @"Acceso"
#define COOKIE_KEY @"sVVP2U7FwaoyjFj5IYQapn0O10oDWDYpNad12fVCb5wsud2Ac35QuVj4"
//#define COOKIE_KEY @"hWnOpFgi8oCdSKELrySox7OGYUvsMuygshqSNEZK1OtVYKK0a8FCMDhi"

#define USER_CLIENT 1
#define USER_MILENIAN 2
#define USER_PROVIDER 3

@interface LoginVC : UIViewController<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
}

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgetPass;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@end


//
//  SubCategoriasVC.m
//  Milenian
//
//  Created by MacBook Pro on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "SubCategoriasVC.h"
#import "SWRevealViewController.h"
#import "VariedadVC.h"
#import "IATPedido.h"
#import "ProductoVC.h"
#import "IATVariedad.h"
#import "IATCategoria.h"
#import "IATSubcategoria.h"
#import "IATVariedadCellView.h"
#import "AppDelegate.h"
#import "SolicitudActualTVC.h"
#import "AGTSimpleCoreDataStack.h"
#import "KLCPopup/KLCPopup.h"
#import <QuartzCore/QuartzCore.h>
#import "CacheImgs.h"



@interface SubCategoriasVC ()
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSArray *subcategorias;
@property (nonatomic, strong) NSArray *subitems;
@property (nonatomic, strong) NSArray *variedades;
@property (nonatomic, strong) NSArray *elementos;
@property (nonatomic, strong) NSString *elementoSeleccionado;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property NSCache *cache;
@end

@implementation SubCategoriasVC

#pragma mark - Ciclo de vida
-(void)viewDidLoad {
    [super viewDidLoad];
    
    //self.cache = [[NSCache alloc] init];
    self.cache = [CacheImgs sharedInstance];
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UIImage *imgBntCarrito = [UIImage imageNamed:@"carrito-de-compra"];
    
    UIBarButtonItem *barBtnCarrito =
    [[UIBarButtonItem alloc] initWithImage:imgBntCarrito
                                     style:UIBarButtonItemStylePlain
                                    target:self action:@selector(searchSubCategoria)];
    
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnCarrito.imageInsets = UIEdgeInsetsMake(35, 60, 35, 12);
    
    self.navigationItem.rightBarButtonItem = barBtnCarrito;
    
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    self.subcategorias = [_categoriaSeleccionada.subcategorias allObjects];
    IATSubcategoria *subCatIni = [self.subcategorias objectAtIndex:0];
    self.variedades = [subCatIni.variedades allObjects];
    NSLog(@"Cargando subcategorias");
    
    _carousel.delegate = self;
    _carousel.dataSource = self;
    _carousel.type = iCarouselTypeLinear;
    
    self.listSubcategories.delegate = self;
    self.listSubcategories.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATVariedadCellView" bundle:nil];
    [self.listSubcategories registerNib:cellNib forCellReuseIdentifier:[IATVariedadCellView cellId]];

}
- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.title = @"Subcategorias";
    self.carousel.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.carousel.hidden = YES;
}
-(void)searchSubCategoria{
    NSLog(@"Implementación busqueda de subcategorias");
    NSLog(@"Implementación busqueda de subcategorias");
    IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
    if (pedido == nil) {
        pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
    }
    
    
    if (pedido.itemsPedido.count > 0) {
//        ProductoVC *producto = [self.storyboard instantiateViewControllerWithIdentifier:@"productoVC"];
//        producto.pedido = pedido;
//        [self.navigationController pushViewController:producto animated:YES];
        
        SolicitudActualTVC *solicitud = [self.storyboard instantiateViewControllerWithIdentifier:@"solicitudActualTVC"];
        solicitud.pedido = pedido;
        [self.navigationController pushViewController:solicitud animated:YES];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pedido vacio"
                                                                       message:@"No existen productos agregados al pedido, debe agregar por lo menos un producto"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.variedades.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    IATVariedad *variedad = [self.variedades objectAtIndex:indexPath.row];
    IATVariedadCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATVariedadCellView cellId]];
    cell.nombreVariedad.text = variedad.nombre;
    cell.cantidadProductos.text =  [NSString stringWithFormat:@"%u productos", variedad.productos.count];
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VariedadVC *variedad = [self.storyboard instantiateViewControllerWithIdentifier:@"variedadVC"];
    variedad.variedad = [self.variedades objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:variedad animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATVariedadCellView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


#pragma mark - iCarousel methods
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.subcategorias.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    //Vista principal
    //view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 248.0f, 266.0f)];
    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 248.0f, 248.0f)];
    view.backgroundColor = [UIColor clearColor];
    
    
    //Activity
    UIActivityIndicatorView *actView =
    [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(100, 105, 50.0f, 50.0f)];
    actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [actView startAnimating];
    
    //Placeholder
    UIImageView *placeholder = [[UIImageView alloc] initWithFrame:view.bounds];
    placeholder.image = [UIImage imageNamed:@"cuadro-blanco-sombriado"];
    [placeholder setContentMode:UIViewContentModeScaleToFill];
    [view addSubview:placeholder];
    
    [placeholder addSubview:actView];
    
    //UIImageView *imgSubCategoria = [[UIImageView alloc] initWithFrame:CGRectMake(4.35f, 14, 238.92f, 238.92f)];
    //[imgSubCategoria setContentMode:UIViewContentModeScaleAspectFit];
    
    //Modo online
    IATSubcategoria *subcategoria = [self.subcategorias objectAtIndex:index];
    NSString *urlSubCat = subcategoria.urlImagen;
    NSURL *url = [NSURL URLWithString:urlSubCat];
    
    UIImage *imgSubcategoria = [self.cache objectForKey:url];
    if (imgSubcategoria) {
        placeholder.image = imgSubcategoria;
        [placeholder setContentMode:UIViewContentModeScaleAspectFit];
        [actView stopAnimating];
    }
    else{
        [self imageWithUrl:url block:^(UIImage *image) {
            if (image != nil) {
                placeholder.image = image;
                [placeholder setContentMode:UIViewContentModeScaleAspectFit];
            }
            [actView stopAnimating];
        }];
    }
    
    
    //[placeholder addSubview:imgSubCategoria];
    view.contentMode = UIViewContentModeCenter;
    
    return view;
    
}


- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
}
- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return NO;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}
- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel{
   
    NSInteger index = [self.carousel currentItemIndex];
    
    //Demo
    //self.elementoSeleccionado = [self.subcategorias objectAtIndex:index];
    
    //Online
    IATSubcategoria *subCatIni = [self.subcategorias objectAtIndex:index];
    self.variedades = [subCatIni.variedades allObjects];
    
    
    [self.listSubcategories reloadData];
    
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    
    
    
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
    
    
}




#pragma mark - Manejo de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)dealloc{
    self.carousel = nil;
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
    
}

@end

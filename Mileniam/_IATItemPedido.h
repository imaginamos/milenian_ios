// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATItemPedido.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATPedido;
@class IATProducto;
@class IATPromocion;
@class IATProveedor;

@interface IATItemPedidoID : NSManagedObjectID {}
@end

@interface _IATItemPedido : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATItemPedidoID*objectID;

@property (nonatomic, strong, nullable) NSNumber* cantidad;

@property (atomic) int32_t cantidadValue;
- (int32_t)cantidadValue;
- (void)setCantidadValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSDecimalNumber* precioTotal;

@property (nonatomic, strong, nullable) IATPedido *pedido;

@property (nonatomic, strong, nullable) IATProducto *producto;

@property (nonatomic, strong, nullable) IATPromocion *promocion;

@property (nonatomic, strong, nullable) IATProveedor *proveedor;

@end

@interface _IATItemPedido (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCantidad;
- (void)setPrimitiveCantidad:(NSNumber*)value;

- (int32_t)primitiveCantidadValue;
- (void)setPrimitiveCantidadValue:(int32_t)value_;

- (NSDecimalNumber*)primitivePrecioTotal;
- (void)setPrimitivePrecioTotal:(NSDecimalNumber*)value;

- (IATPedido*)primitivePedido;
- (void)setPrimitivePedido:(IATPedido*)value;

- (IATProducto*)primitiveProducto;
- (void)setPrimitiveProducto:(IATProducto*)value;

- (IATPromocion*)primitivePromocion;
- (void)setPrimitivePromocion:(IATPromocion*)value;

- (IATProveedor*)primitiveProveedor;
- (void)setPrimitiveProveedor:(IATProveedor*)value;

@end

@interface IATItemPedidoAttributes: NSObject 
+ (NSString *)cantidad;
+ (NSString *)precioTotal;
@end

@interface IATItemPedidoRelationships: NSObject
+ (NSString *)pedido;
+ (NSString *)producto;
+ (NSString *)promocion;
+ (NSString *)proveedor;
@end

NS_ASSUME_NONNULL_END

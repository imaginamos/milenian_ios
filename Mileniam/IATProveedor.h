#import "_IATProveedor.h"

@interface IATProveedor : _IATProveedor

+(instancetype) proveedorWithId:(NSString *)idProveedor
                        nombre:(NSString *)nombre
                   descripcion:(NSString *)descripcion
                         direccion:(NSString *)direccion
                       telefono:(NSString *)telefono
                        urlLogo:(NSString *)urlLogo
                       context:(NSManagedObjectContext *)context;

+(instancetype) obtenerProveedorWithId:(NSString *)idProveedor
                            context:(NSManagedObjectContext *)context;
+(NSArray *) obtenerProveedoresWithContext:(NSManagedObjectContext *)context;

@end

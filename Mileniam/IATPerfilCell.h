//
//  IATPerfilCell.h
//  Milenian
//
//  Created by Carlos Obregón on 24/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATPerfilCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPerfil;
@property (weak, nonatomic) IBOutlet UILabel *lblCompania;
@property (weak, nonatomic) IBOutlet UILabel *lblUbicacion;


+(NSString *) cellId;
+(CGFloat)cellHeight;
@end

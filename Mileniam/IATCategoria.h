#import "_IATCategoria.h"

@interface IATCategoria : _IATCategoria

+(instancetype) categoriaWithId:(NSString *)idCategoria
                         nombre:(NSString *)nombre
                    descripcion:(NSString *)descripcion
                       urlImage:(NSString *)urlImage
                        context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerCategoriasWithContext:(NSManagedObjectContext *)context;
+(instancetype) obtenerCategoriaWithId:(NSString *)idCategoria
                               context:(NSManagedObjectContext *)context;

@end

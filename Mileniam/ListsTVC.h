//
//  ListsTVC.h
//  Milenian
//
//  Created by Leonardo Rodriguez on 9/3/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListsTVC : UITableViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

- (IBAction)AgregarNuevaLista:(id)sender;

@end

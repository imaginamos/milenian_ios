#import "_IATAcuerdo.h"

@interface IATAcuerdo : _IATAcuerdo
// Custom logic goes here.

+(instancetype) acuerdoWithId:(NSString *)idAcuerdo
                  comentarios:(NSString *)comentarios
                  fechaInicio:(NSDate *)fechaInicio
                   fechaFinal:(NSDate *)fechaFinal
                      context:(NSManagedObjectContext *)context;

+(instancetype) obtenerAcuerdoWithId:(NSString *)idAcuerdo
                             context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerAcuerdoWithContext:(NSManagedObjectContext *)context;
+(NSArray *) obtenerAcuerdosWithEstado:(NSString *)estado
                               context:(NSManagedObjectContext *)context;

@end

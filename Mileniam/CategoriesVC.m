//
//  CategoriesVC.m
//  Mileniam
//
//  Created by Carlos Obregon on 26/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "CategoriesVC.h"
#import "SWRevealViewController.h"
#import "SubCategoriasVC.h"
#import "ProductoRecomendadoVC.h"
#import "ProductoVC.h"
#import "IATProductoProveedor.h"
#import "PromocionVC.h"
#import "IATCategoria.h"
#import "IATProducto.h"
#import "AppDelegate.h"
#import "IATProductosRecomendadosCellView.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATPedido.h"
#import "SolicitudActualTVC.h"
#import "CacheImgs.h"
#import "LoginVC.h"


@interface CategoriesVC ()
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSArray *productos;
@property (nonatomic, strong) NSArray *categorias;
@property (nonatomic, strong) NSArray *productosRecomendados;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property NSCache *cache;

@end

@implementation CategoriesVC

#pragma mark - Ciclo de vida
-(void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults boolForKey:@"kSuccessfulLoad"]) {
        NSLog(@"Carga exitosa");
        
        AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
        self.context = localVar.model.context;
        
        self.categorias = [IATCategoria obtenerCategoriasWithContext:self.context];
        self.productosRecomendados = [IATProducto obtenerProductosRecomendadosWithContext:localVar.model.context];
        
        //self.cache = [[NSCache alloc] init];
        self.cache = [CacheImgs sharedInstance];
        
        //Asignaciones y registro celda - Lista de Producto (TableView)
        self.listProducts.delegate = self;
        self.listProducts.dataSource = self;
        
        UINib *cellNib = [UINib nibWithNibName:@"IATProductosRecomendadosCellView" bundle:nil];
        [self.listProducts registerNib:cellNib forCellReuseIdentifier:[IATProductosRecomendadosCellView cellId]];
        
        //Asignacion de iCarrusel
        _carousel.delegate = self;
        _carousel.dataSource = self;
        _carousel.type = iCarouselTypeLinear;
    }
    else{
        NSLog(@"Almacenamiento fallido");
        [defaults setBool:NO forKey:@"kSecondTime"];
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [self.navigationController presentViewController:login animated:YES completion:^{
            NSLog(@"Volviendo a la vista del login");
        }];
    }
    [defaults synchronize];
    

    

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.carousel.hidden = NO;
    //self.navigationController.navigationBar.topItem.title = @"Categorias";
    //self.title = @"Categorias";
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.carousel.hidden = YES;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.productosRecomendados.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IATProducto *productoCell = [self.productosRecomendados objectAtIndex:indexPath.row];
    IATProductosRecomendadosCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProductosRecomendadosCellView cellId]];
    
    if (![productoCell.nombre isEqualToString:@""] && productoCell.nombre != nil) {
        cell.nombreProducto.text = productoCell.nombre;
    }
    
    if (![productoCell.unidad isEqualToString:@""] && productoCell.unidad != nil) {
        cell.unidadProducto.text = productoCell.unidad;
    }
    
    if (![productoCell.marca isEqualToString:@""] && productoCell.marca != nil) {
        cell.marcaProducto.text = productoCell.marca;
    }
    
    if (![productoCell.urlImagen isEqualToString:@""] && productoCell.urlImagen != nil) {
        
        UIImage *imgProductoRecomendado = [self.cache objectForKey:[NSURL URLWithString:productoCell.urlImagen]];
        cell.imgProducto.image = imgProductoRecomendado;
        
        if(!imgProductoRecomendado){
            
            [cell.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:productoCell.urlImagen] block:^(UIImage *image) {
                NSIndexPath *indexPath_ = [tableView indexPathForCell:cell];
                if([indexPath isEqual:indexPath_]){
                    cell.imgProducto.image = image;
                }
                [cell.activityLoad stopAnimating];
            }];
            /*[self downloadPhotoFromURL:[NSURL URLWithString:productoCell.urlImagen] completion:^(__unused NSURL *URL, UIImage *image) {
                NSIndexPath *indexPath_ = [tableView indexPathForCell:cell];
                if([indexPath isEqual:indexPath_]){
                    cell.imgProducto.image = image;
                }
                [cell.activityLoad stopAnimating];
            }];*/
        }
 
    }
    
    IATProductoProveedor *productoProveedor =
    [IATProductoProveedor obtenerPrecioMasBajo:productoCell context:self.context];
    
    if (productoProveedor.precio != nil) {
        cell.precioProducto.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[productoProveedor.precio doubleValue]]];
    }
    
    return cell;
    
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}
- (void)downloadPhotoFromURL:(NSURL*)URL completion:(void(^)(NSURL *URL, UIImage *image))completion {
    static dispatch_queue_t downloadQueue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        downloadQueue = dispatch_queue_create("ru.codeispoetry.downloadQueue", DISPATCH_QUEUE_CONCURRENT);
    });
    
    dispatch_async(downloadQueue, ^{
        NSData *data = [NSData dataWithContentsOfURL:URL];
        UIImage *image = [UIImage imageWithData:data];
        
        if(image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:URL];
                if(completion) {
                    completion(URL, image);
                }
            });
        }
    });
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ProductoRecomendadoVC *productoRecomendadoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"productoRecomendadoVC"];
    productoRecomendadoVC.producto = [self.productosRecomendados objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:productoRecomendadoVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATProductosRecomendadosCellView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


#pragma mark - iCarousel data source
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return [self.categorias count];
}
- (UIView *)carousel:(iCarousel *)carousel
  viewForItemAtIndex:(NSInteger)index
         reusingView:(UIView *)view{
    
    //Vista principal
    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 248.0f, 266.0f)];
    view.backgroundColor = [UIColor clearColor];
    
    //Activity
    UIActivityIndicatorView *actView =
    [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(100, 110, 50.0f, 50.0f)];
    actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [actView startAnimating];
    
    //Placeholder
    UIImageView *placeholder = [[UIImageView alloc] initWithFrame:view.bounds];
    placeholder.image = [UIImage imageNamed:@"cuadro-blanco-sombriado"];
    [placeholder setContentMode:UIViewContentModeScaleToFill];
    [view addSubview:placeholder];
    
    [placeholder addSubview:actView];
    
    //UIImageView *imgCategoria = [[UIImageView alloc] initWithFrame:CGRectMake(4.28f, 14, 239.03f, 239.03f)];
    //[imgCategoria setContentMode:UIViewContentModeScaleAspectFit];
    
    IATCategoria *categoria = [self.categorias objectAtIndex:index];
    NSString *urlCat = categoria.urlImagen;
    NSURL *url = [NSURL URLWithString:urlCat];
    
    UIImage *imgCategoria = [self.cache objectForKey:url];
    if (imgCategoria) {
        placeholder.image = imgCategoria;
        [placeholder setContentMode:UIViewContentModeScaleAspectFit];
        [actView stopAnimating];
    }
    else{
        [self imageWithUrl:url block:^(UIImage *image) {
            if (image != nil) {
                //imgCategoria.image = image;
                placeholder.image = image;
                [placeholder setContentMode:UIViewContentModeScaleAspectFit];
            }
            else{
                UILabel *lblNombre = [[UILabel alloc] initWithFrame:CGRectMake(5, 120, 240.0f, 30.0f)];
                [placeholder addSubview:lblNombre];
                lblNombre.text = categoria.nombre;
                [lblNombre setTextAlignment:NSTextAlignmentCenter];
            }
            [actView stopAnimating];
        }];
    }
    
    
    //[placeholder addSubview:imgCategoria];
    view.contentMode = UIViewContentModeCenter;
    
    return view;
}
- (CATransform3D)carousel:(__unused iCarousel *)carousel
   itemTransformForOffset:(CGFloat)offset
            baseTransform:(CATransform3D)transform{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
}


#pragma mark - iCarousel delegate
- (CGFloat)carousel:(__unused iCarousel *)carousel
     valueForOption:(iCarouselOption)option
        withDefault:(CGFloat)value{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    
    IATCategoria *categoriaSelec = [self.categorias objectAtIndex:index];
    
    SubCategoriasVC *subcategoriasVC = [self.storyboard instantiateViewControllerWithIdentifier:@"subcategoriesVC"];
    subcategoriasVC.categoriaSeleccionada = categoriaSelec;
    
    [self.navigationController pushViewController:subcategoriasVC animated:YES];
    
}


#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

- (IBAction)irAlPedido:(id)sender {
    
    
    IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
    if (pedido == nil) {
        pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
    }
    
    
    if (pedido.itemsPedido.count > 0) {
        
        //SolicitudActualTVC *solicitud = [[SolicitudActualTVC alloc] init];
        SolicitudActualTVC *solicitud = [self.storyboard instantiateViewControllerWithIdentifier:@"solicitudActualTVC"];
        solicitud.pedido = pedido;
        [self.navigationController pushViewController:solicitud animated:YES];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pedido vacio"
                                                                       message:@"No existen productos agregados al pedido, debe agregar por lo menos un producto"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Manejo de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)dealloc{
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
}


@end

#import "_IATNotificacion.h"

@interface IATNotificacion : _IATNotificacion

+(instancetype) notificacionWithId:(NSString *)idNotificacion
                            nombre:(NSString *)nombre
                           mensaje:(NSString *)mensaje
                             fecha:(NSString *)fecha
                           context:(NSManagedObjectContext *)context;

+(instancetype) obtenerNotificacionWithId:(NSString *)idNoti
                                  context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerNotificacionWithContext:(NSManagedObjectContext *)context;


@end

//
//  PedidosVC.m
//  Mileniam
//
//  Created by Desarrollador IOS on 27/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "PedidosTVC.h"
#import "SWRevealViewController.h"
#import "PedidoRealizadoVC.h"
#import "IATPedidosCellView.h"
#import "IATPedido.h"
#import "IATItemPedido.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"

@interface PedidosTVC ()
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSArray *pedidos;
@property (strong, nonatomic) NSArray *pedidosEnviados;
@property (strong, nonatomic) NSArray *pedidosAprobados;
@end

@implementation PedidosTVC{
    //NSArray *pedidos;
    NSArray *historicos;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //pedidos = @[@"procesouno", @"procesodos"];
    
    historicos = @[@"historial1", @"historial2", @"historial3", @"historial4"];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    self.pedidos = [IATPedido obtenerPedidosWithContext:self.context];
    self.pedidosEnviados = [IATPedido obtenerPedidosEnProcesoWithContext:self.context];
    self.pedidosAprobados = [IATPedido obtenerPedidosAprobadosWithContext:self.context];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    UINib *cellNib = [UINib nibWithNibName:@"IATPedidosCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATPedidosCellView cellId]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sections = 0;
    if (self.pedidosEnviados.count > 0) {
        sections++;
    }
    if (self.pedidosAprobados.count > 0){
        sections++;
    }
    return sections;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (section == SECTION_1) {
        if (self.pedidosEnviados.count > 0) {
            return self.pedidosEnviados.count;
        }
        else{
            return self.pedidosAprobados.count;
        }
        
    }
    else if (section == SECTION_2){
        return self.pedidosAprobados.count;
    }
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATPedido *pedido;
    if (indexPath.section == SECTION_1) {
        if (self.pedidosEnviados.count > 0) {
            pedido = [self.pedidosEnviados objectAtIndex:indexPath.row];
        }
        else{
            pedido = [self.pedidosAprobados objectAtIndex:indexPath.row];
        }
        
    }
    else if (indexPath.section == SECTION_2){
        pedido = [self.pedidosAprobados objectAtIndex:indexPath.row];
    }
    
    IATPedidosCellView *cell =
    [tableView dequeueReusableCellWithIdentifier:[IATPedidosCellView cellId]];
    
    if (![pedido.nombre isEqualToString:@""] && pedido.nombre != nil) {
        cell.lblIdPedido.text = pedido.nombre;
    }
    
    if (pedido.precioTotal != nil) {
        cell.lblPrecioPedido.text =
        [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[pedido.precioTotal  doubleValue]]];
    }
    
    if (pedido.fecha != nil) {
        
        NSString *fechaString = [NSString stringWithFormat:@"%@", pedido.fecha];
        NSArray *arrayFecha = [fechaString componentsSeparatedByString:@" "];
        
        cell.lblFechaPedido.text = [NSString stringWithFormat:@"%@ - %@", arrayFecha[0], arrayFecha[1]];
        
    }
    
    
    if (pedido.itemsPedido.count > 0  && pedido.itemsPedido != nil) {
        NSUInteger cantidad = pedido.itemsPedido.count;
        cell.lblCantidad.text = [NSString stringWithFormat:@"%lu productos", (unsigned long)cantidad];
    }
    
    return cell;
    
}

-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATPedidosCellView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *sectionName = nil;
    
    if (section == SECTION_1) {
        if (self.pedidosEnviados.count > 0) {
            sectionName = @"En proceso";
        }
        else{
            sectionName = @"Historicos";
        }
        
    }
    else if (section == SECTION_2){
        sectionName = @"Historicos";
    }
    
    return sectionName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATPedido *pedido;
    if (indexPath.section == SECTION_1) {
        if (self.pedidosEnviados.count > 0){
            pedido = [self.pedidosEnviados objectAtIndex:indexPath.row];
        }
        else{
            pedido = [self.pedidosAprobados objectAtIndex:indexPath.row];
        }
        
    }
    else if (indexPath.section == SECTION_2){
        pedido = [self.pedidosAprobados objectAtIndex:indexPath.row];
    }
    
    PedidoRealizadoVC *pedidoVC =
    [self.storyboard instantiateViewControllerWithIdentifier:@"pedidoRealizadoVC"];
    pedidoVC.pedido = pedido;
    
    [self.navigationController pushViewController:pedidoVC animated:YES];
}





@end

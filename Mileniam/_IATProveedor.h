// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATProveedor.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@class IATAcuerdo;
@class IATItemLista;
@class IATItemPedido;
@class IATProductoProveedor;
@class IATPromoProveedor;

@interface IATProveedorID : IATEntidadBaseID {}
@end

@interface _IATProveedor : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATProveedorID*objectID;

@property (nonatomic, strong, nullable) NSString* direccion;

@property (nonatomic, strong, nullable) NSString* telefono;

@property (nonatomic, strong, nullable) NSString* urlLogo;

@property (nonatomic, strong, nullable) NSSet<IATAcuerdo*> *acuerdos;
- (nullable NSMutableSet<IATAcuerdo*>*)acuerdosSet;

@property (nonatomic, strong, nullable) NSSet<IATItemLista*> *itemsLista;
- (nullable NSMutableSet<IATItemLista*>*)itemsListaSet;

@property (nonatomic, strong, nullable) NSSet<IATItemPedido*> *itemsPedido;
- (nullable NSMutableSet<IATItemPedido*>*)itemsPedidoSet;

@property (nonatomic, strong, nullable) NSSet<IATProductoProveedor*> *preciosProductos;
- (nullable NSMutableSet<IATProductoProveedor*>*)preciosProductosSet;

@property (nonatomic, strong, nullable) NSSet<IATPromoProveedor*> *promosProveedor;
- (nullable NSMutableSet<IATPromoProveedor*>*)promosProveedorSet;

@end

@interface _IATProveedor (AcuerdosCoreDataGeneratedAccessors)
- (void)addAcuerdos:(NSSet<IATAcuerdo*>*)value_;
- (void)removeAcuerdos:(NSSet<IATAcuerdo*>*)value_;
- (void)addAcuerdosObject:(IATAcuerdo*)value_;
- (void)removeAcuerdosObject:(IATAcuerdo*)value_;

@end

@interface _IATProveedor (ItemsListaCoreDataGeneratedAccessors)
- (void)addItemsLista:(NSSet<IATItemLista*>*)value_;
- (void)removeItemsLista:(NSSet<IATItemLista*>*)value_;
- (void)addItemsListaObject:(IATItemLista*)value_;
- (void)removeItemsListaObject:(IATItemLista*)value_;

@end

@interface _IATProveedor (ItemsPedidoCoreDataGeneratedAccessors)
- (void)addItemsPedido:(NSSet<IATItemPedido*>*)value_;
- (void)removeItemsPedido:(NSSet<IATItemPedido*>*)value_;
- (void)addItemsPedidoObject:(IATItemPedido*)value_;
- (void)removeItemsPedidoObject:(IATItemPedido*)value_;

@end

@interface _IATProveedor (PreciosProductosCoreDataGeneratedAccessors)
- (void)addPreciosProductos:(NSSet<IATProductoProveedor*>*)value_;
- (void)removePreciosProductos:(NSSet<IATProductoProveedor*>*)value_;
- (void)addPreciosProductosObject:(IATProductoProveedor*)value_;
- (void)removePreciosProductosObject:(IATProductoProveedor*)value_;

@end

@interface _IATProveedor (PromosProveedorCoreDataGeneratedAccessors)
- (void)addPromosProveedor:(NSSet<IATPromoProveedor*>*)value_;
- (void)removePromosProveedor:(NSSet<IATPromoProveedor*>*)value_;
- (void)addPromosProveedorObject:(IATPromoProveedor*)value_;
- (void)removePromosProveedorObject:(IATPromoProveedor*)value_;

@end

@interface _IATProveedor (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDireccion;
- (void)setPrimitiveDireccion:(NSString*)value;

- (NSString*)primitiveTelefono;
- (void)setPrimitiveTelefono:(NSString*)value;

- (NSString*)primitiveUrlLogo;
- (void)setPrimitiveUrlLogo:(NSString*)value;

- (NSMutableSet<IATAcuerdo*>*)primitiveAcuerdos;
- (void)setPrimitiveAcuerdos:(NSMutableSet<IATAcuerdo*>*)value;

- (NSMutableSet<IATItemLista*>*)primitiveItemsLista;
- (void)setPrimitiveItemsLista:(NSMutableSet<IATItemLista*>*)value;

- (NSMutableSet<IATItemPedido*>*)primitiveItemsPedido;
- (void)setPrimitiveItemsPedido:(NSMutableSet<IATItemPedido*>*)value;

- (NSMutableSet<IATProductoProveedor*>*)primitivePreciosProductos;
- (void)setPrimitivePreciosProductos:(NSMutableSet<IATProductoProveedor*>*)value;

- (NSMutableSet<IATPromoProveedor*>*)primitivePromosProveedor;
- (void)setPrimitivePromosProveedor:(NSMutableSet<IATPromoProveedor*>*)value;

@end

@interface IATProveedorAttributes: NSObject 
+ (NSString *)direccion;
+ (NSString *)telefono;
+ (NSString *)urlLogo;
@end

@interface IATProveedorRelationships: NSObject
+ (NSString *)acuerdos;
+ (NSString *)itemsLista;
+ (NSString *)itemsPedido;
+ (NSString *)preciosProductos;
+ (NSString *)promosProveedor;
@end

NS_ASSUME_NONNULL_END

//
//  DetalleItemListaVC.m
//  Milenian
//
//  Created by Carlos Obregón on 22/06/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "DetalleItemListaVC.h"
#import "IATProducto.h"
#import "IATProveedor.h"
#import "IATPedido.h"
#import "IATItemPedido.h"
#import "IATItemLista.h"
#import "IATProductoProveedor.h"
#import "IATCantidadProducto.h"
#import "IATProveedorProductoCellView.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "KLCPopup/KLCPopup.h"
#import <QuartzCore/QuartzCore.h>
#import "CacheImgs.h"

@interface DetalleItemListaVC ()
@property (strong, nonatomic) NSArray *proveedores;
@property (strong, nonatomic) IATCantidadProducto *preItem;
@property (strong, nonatomic) IATProductoProveedor *productoProveedor;
@property (strong, nonatomic) IATProveedor *proveedorSeleccionado;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSDecimalNumber *precioTotal;
@property (nonatomic) NSUInteger cantidad;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation DetalleItemListaVC

#pragma mark - Ciclo de Vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    self.cache = [CacheImgs sharedInstance];
    
    self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePress:)];
    self.imgProducto.userInteractionEnabled = YES;
    [self.imgProducto addGestureRecognizer:self.tap];
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.tfCantidad.delegate = self;
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    NSString* fileSoundRemove = [[NSBundle mainBundle] pathForResource:@"Restar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundRemove = [[NSURL alloc]initFileURLWithPath:fileSoundRemove];
    self.soundRemove = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundRemove error:nil];
    
    self.soundRemove.volume = 0.4;
    self.soundRemove.numberOfLoops = 1;
    self.soundRemove.pan = 0;
    self.soundRemove.enableRate = YES;
    self.soundRemove.rate = 1;
    
    //Sincronizar vista
    self.proveedores =
    [IATProductoProveedor obtenerPreciosOrdenadoPorMenor:self.itemLista.producto context:self.context];
    self.productoProveedor = [IATProductoProveedor obtenerPrecioPorProveedor:self.itemLista.proveedor producto:self.itemLista.producto context:self.context];
    
    self.proveedorSeleccionado = self.itemLista.proveedor;
    
    if (![self.itemLista.producto.nombre isEqualToString:@""] && self.itemLista.producto.nombre != nil) {
        self.lblNombre.text = self.itemLista.producto.nombre;
    }
    
    if (![self.itemLista.producto.unidad isEqualToString:@""] && self.itemLista.producto.unidad != nil) {
        self.lblUnidad.text = self.itemLista.producto.unidad;
    }
    
    if (![self.itemLista.producto.marca isEqualToString:@""] && self.itemLista.producto.marca != nil) {
        self.lblMarca.text = self.itemLista.producto.marca;
    }
    
    UIImage *imgProducto = [self.cache objectForKey:[NSURL URLWithString:self.itemLista.producto.urlImagen]];
    if (imgProducto) {
        self.imgProducto.image = imgProducto;
    }
    else{
        [self.activityLoad startAnimating];
        [self imageWithUrl:[NSURL URLWithString:self.itemLista.producto.urlImagen] block:^(UIImage *image) {
            if (image != nil) {
                self.imgProducto.image = image;
            }
            [self.activityLoad stopAnimating];
        }];
    }
    
    self.cantidad = [self.itemLista.cantidad integerValue];
    self.tfCantidad.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.cantidad];
    
    
    self.lblPrecioTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.itemLista.precio doubleValue]]];
    self.precioTotal = self.itemLista.precio;
    
    
    self.listProveedores.delegate = self;
    self.listProveedores.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATProveedorProductoCellView" bundle:nil];
    [self.listProveedores registerNib:cellNib forCellReuseIdentifier:[IATProveedorProductoCellView cellId]];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.listProveedores reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.proveedores.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATProductoProveedor *precioProveedor = [self.proveedores objectAtIndex:indexPath.row];
    IATProveedorProductoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedorProductoCellView cellId]];
    
    if (precioProveedor == self.productoProveedor) {
        cell.backgroundColor = [UIColor colorWithRed:232/255.0f green:232/255.0f blue:232/255.0f alpha:1];
    }
    else{
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[precioProveedor.precio doubleValue]]];
    cell.lblNombre.text = [NSString stringWithFormat:@"%@", precioProveedor.provedor.nombre];
    
    if (precioProveedor.provedor.urlLogo != nil) {
        
        UIImage *imgProveedor = [self.cache objectForKey:[NSURL URLWithString:precioProveedor.provedor.urlLogo]];
        if (imgProveedor) {
            cell.logoProveedor.image = imgProveedor;
        }
        else{
            [cell.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:precioProveedor.provedor.urlLogo] block:^(UIImage *image) {
                if (image != nil) {
                    cell.logoProveedor.image = image;
                }
                [cell.activityLoad stopAnimating];
            }];
        }
    }
    
    return cell;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Cual proveedor y precio escogio
    self.productoProveedor = [self.proveedores objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //Actualizar el precio y proveedor del item
    NSDecimalNumber *cantidad =
    [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
    self.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cantidad];
    self.proveedorSeleccionado = self.productoProveedor.provedor;
    
    self.lblPrecioTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
    
    self.preItem.cantidad =
    [NSNumber numberWithUnsignedInteger:self.cantidad];
    
    [self.listProveedores reloadData];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

#pragma mark - Targets/Actions
- (IBAction)agregarAlPedido:(id)sender {
    
    self.itemLista.proveedor = self.proveedorSeleccionado;
    self.itemLista.cantidad = [NSNumber numberWithInteger:self.cantidad];
    self.itemLista.precio = self.precioTotal;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)animationProducto{
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 1.2, 1.2);
                         
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 0.8335, 0.8335);
                                              
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}

- (IBAction)restarUno:(id)sender {
    //Actualizar modelo
    if (self.cantidad > 0) {
        [self.soundRemove play];
        self.cantidad--;
        
        NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
        
        self.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cant];
        
        //Actualizar vista
        self.tfCantidad.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad];
        self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
        
        [self animationProducto];
        
    }
    
}
- (IBAction)sumarUno:(id)sender {
    
    [self.soundAdd play];
    //Actualizar modelo
    self.cantidad++;
    
    NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
    
    self.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cant];
    
    //Actualizar vista
    self.tfCantidad.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad];
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
    
    [self animationProducto];
}



- (void)handlePress:(UITapGestureRecognizer *)gesto {
    
    NSLog(@"UIGestureRecognizerStateEnded");
    //Do Whatever You want on End of Gesture
    
    ////////////////////////////////////////Pop Up////////////////////////////////////////
    UIView* contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 340)];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 8.0;
    
    //imagen
    UIImageView *imgProducto = [[UIImageView alloc] initWithFrame:CGRectMake(contentView.bounds.size.width/6, 30, 200, 200)];
    imgProducto.backgroundColor = [UIColor clearColor];
    [imgProducto setContentMode:UIViewContentModeScaleAspectFit];
    
    //Activity
    UIActivityIndicatorView *actView =
    [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imgProducto.bounds.size.width/2 - 25, imgProducto.bounds.size.height/2 - 25, 50.0f, 50.0f)];
    actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [actView startAnimating];
    
    [imgProducto addSubview:actView];
    
    //Modo online
    NSString *urlProducto = self.itemLista.producto.urlImagen;
    NSURL *url = [NSURL URLWithString:urlProducto];
    
    [self imageWithUrl:url block:^(UIImage *image) {
        if (image != nil) {
            imgProducto.image = image;
        }
        [actView stopAnimating];
    }];
    
    UILabel* dismissLabel = [[UILabel alloc] initWithFrame:CGRectMake(contentView.bounds.size.width/10, 230, 240, 70)];
    dismissLabel.backgroundColor = [UIColor clearColor];
    dismissLabel.textColor = [UIColor blackColor];
    dismissLabel.font = [UIFont boldSystemFontOfSize:12.0];
    dismissLabel.text = self.itemLista.producto.nombre;
    dismissLabel.textAlignment = NSTextAlignmentCenter;
    dismissLabel.numberOfLines = 0;
    
    UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    //dismissButton.backgroundColor = [UIColor klcGreenColor];
    [dismissButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [dismissButton setTitle:@"Cerrar" forState:UIControlStateNormal];
    dismissButton.layer.cornerRadius = 6.0;
    [dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView addSubview:imgProducto];
    [contentView addSubview:dismissLabel];
    [contentView addSubview:dismissButton];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(contentView, dismissButton, dismissLabel, imgProducto);
    
    [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(16)-[dismissLabel]-(10)-[dismissButton]-(24)-|"
                                             options:NSLayoutFormatAlignAllCenterX
                                             metrics:nil
                                               views:views]];
    
    [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(36)-[dismissLabel]-(36)-|"
                                             options:0
                                             metrics:nil
                                               views:views]];
    
    
    KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                            showType:KLCPopupShowTypeBounceInFromTop
                                         dismissType:KLCPopupDismissTypeFadeOut
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:NO
                               dismissOnContentTouch:YES];
    
    [popup show];
    
    
    
}

- (void)dismissButtonPressed:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
}


#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
    });
}

#pragma mark - Utilidades
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

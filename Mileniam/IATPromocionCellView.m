//
//  IATPromocionCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 12/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATPromocionCellView.h"

@implementation IATPromocionCellView
#pragma mark - Metodos de clase
+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 260.0f;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  IATProductoCellCollectionView.m
//  Milenian
//
//  Created by Carlos Obregón on 11/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATProductoCellCollectionView.h"
#import "IATCantidadProducto.h"
#import "IATItemAcuerdo.h"
#import "IATItemPedido.h"
#import "IATLista.h"
#import "IATItemLista.h"
#import "VariedadVC.h"
#import "IATProducto.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "KLCPopup/KLCPopup.h"
#import <QuartzCore/QuartzCore.h>

@interface IATProductoCellCollectionView()
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (retain, nonatomic) UIPickerView *picker;
@property (strong, nonatomic) IATLista *listaSeleccionada;
@property (strong, nonatomic) NSArray *listasCreadas;
@end

@implementation IATProductoCellCollectionView

+(NSString *)cellId{
    return NSStringFromClass(self);
}

- (void)observeCantidadProducto:(IATCantidadProducto *) cantProducto{
    
    self.cantProducto = cantProducto;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    [self.cantProducto addObserver:self
                        forKeyPath:@"cantidad"
                           options:NSKeyValueObservingOptionNew
                           context:NULL];
    
    self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.contentView addGestureRecognizer:self.tap];
    [self.imgProducto addGestureRecognizer:self.tap];
    
    self.pressLong =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    self.pressLong.minimumPressDuration = 0.2;
    self.pressLong.delegate = self;
    [self.contentView addGestureRecognizer:self.pressLong];
    [self.imgProducto addGestureRecognizer:self.pressLong];
    
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    
    NSString* fileSoundRemove = [[NSBundle mainBundle] pathForResource:@"Restar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundRemove = [[NSURL alloc]initFileURLWithPath:fileSoundRemove];
    self.soundRemove = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundRemove error:nil];
    
    self.soundRemove.volume = 0.4;
    self.soundRemove.numberOfLoops = 1;
    self.soundRemove.pan = 0;
    self.soundRemove.enableRate = YES;
    self.soundRemove.rate = 1;
    
    
    [self syncWithCantidadProducto];
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context{
    
    [self syncWithCantidadProducto];
    
}
- (void)syncWithCantidadProducto{
    
    if (self.imgProducto.image == nil) {
        self.imgProducto.image = [UIImage imageNamed:@"page"];
        [self.imgProducto setContentMode:UIViewContentModeScaleAspectFill];
        
        
        
        IATProducto *producto = self.cantProducto.producto;
        NSString *url = producto.urlImagen;
        NSURL *urlImg = [NSURL URLWithString:url];
        
        UIImage *imgProduct = [self.cache objectForKey:urlImg];
        if (imgProduct) {
            self.imgProducto.image = imgProduct;
            [self.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
            [self.activityLoad startAnimating];
            [self imageWithUrl:urlImg producto:self.cantProducto.producto block:^(UIImage *image) {
                if (image != nil) {
                    self.imgProducto.image = image;
                    [self.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
                }
                [self.activityLoad stopAnimating];
            }];
        }
    }
    
    if(![self.cantProducto.producto.esFavorito isEqual:@NO]){
        UIImage *fav = [UIImage imageNamed:@"estrella-amarilla"];
        [self.btnFavorito setImage:fav forState:UIControlStateNormal];
        
    }
    else{
        UIImage *noFav = [UIImage imageNamed:@"estrella-gris"];
        [self.btnFavorito setImage:noFav forState:UIControlStateNormal];
    }
    
    if ([IATItemAcuerdo itemAcuerdoExisteWithProducto:self.cantProducto.producto context:self.context]) {
        self.imgAcuerdo.hidden = NO;
    }
    else{
        self.imgAcuerdo.hidden = YES;
    }
    
    if ([IATItemPedido existeItemPedidoWithProducto:self.cantProducto.producto context:self.context]) {
        self.imgCarrito.hidden = NO;
    }
    else{
        self.imgCarrito.hidden = YES;
    }
    
    
    self.lblCantidad.text = [NSString stringWithFormat:@"%@",self.cantProducto.cantidad];
    
    self.lblPrecio.text =
    [NSString stringWithFormat:@"%@",[self formatCurrencyValue:[self.cantProducto.precio doubleValue]]];
    
}

- (void)handleTap:(UITapGestureRecognizer *)sender{
    
    [self.soundAdd play];
    
    int value = [self.cantProducto.cantidad intValue];
    self.cantProducto.cantidad = [NSNumber numberWithInt:value + 1];
    
    self.lblCantidad.text = [NSString stringWithFormat:@"%@", self.cantProducto.cantidad ];
    
    NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_SUBTOTAL
                                                      object:self
                                                    userInfo:@{@"precio":self.cantProducto.precio}];
    
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
    [self animationProducto];
    
}
- (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded){
        NSLog(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
        
        ////////////////////////////////////////Pop Up////////////////////////////////////////
        UIView* contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 340)];
        contentView.backgroundColor = [UIColor whiteColor];
        contentView.layer.cornerRadius = 8.0;
        
        //imagen
        UIImageView *imgProducto = [[UIImageView alloc] initWithFrame:CGRectMake(contentView.bounds.size.width/6, 20, 200, 200)];
        imgProducto.backgroundColor = [UIColor clearColor];
        [imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        
        //Activity
        UIActivityIndicatorView *actView =
        [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imgProducto.bounds.size.width/2 - 25, imgProducto.bounds.size.height/2 - 25, 50.0f, 50.0f)];
        [actView setColor:[UIColor lightGrayColor]];
        actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [actView startAnimating];
        
        [imgProducto addSubview:actView];
        
        //Modo online
        NSString *urlProducto = self.cantProducto.producto.urlImagen;
        NSURL *url = [NSURL URLWithString:urlProducto];
        
        UIImage *imgProduct = [self.cache objectForKey:url];
        if (imgProduct) {
            imgProducto.image = imgProduct;
            [imgProducto setContentMode:UIViewContentModeScaleAspectFit];
        }
        [self imageWithUrl:url producto:self.cantProducto.producto block:^(UIImage *image) {
            if (image != nil) {
                imgProducto.image = image;
            }
            [actView stopAnimating];
        }];
        
        UILabel* dismissLabel = [[UILabel alloc] initWithFrame:CGRectMake(contentView.bounds.size.width/10, 230, 240, 70)];
        dismissLabel.backgroundColor = [UIColor clearColor];
        dismissLabel.textColor = [UIColor blackColor];
        dismissLabel.font = [UIFont boldSystemFontOfSize:12.0];
        dismissLabel.text = self.cantProducto.producto.nombre;
        dismissLabel.textAlignment = NSTextAlignmentCenter;
        dismissLabel.numberOfLines = 0;
        
        UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
        dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
        //dismissButton.backgroundColor = [UIColor klcGreenColor];
        [dismissButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
        [dismissButton setTitle:@"Cerrar" forState:UIControlStateNormal];
        dismissButton.layer.cornerRadius = 6.0;
        [dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [contentView addSubview:imgProducto];
        [contentView addSubview:dismissLabel];
        [contentView addSubview:dismissButton];
        
        NSDictionary* views = NSDictionaryOfVariableBindings(contentView, dismissButton, dismissLabel, imgProducto);
        
        [contentView addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(16)-[dismissLabel]-(10)-[dismissButton]-(24)-|"
                                                 options:NSLayoutFormatAlignAllCenterX
                                                 metrics:nil
                                                   views:views]];
        
        [contentView addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(36)-[dismissLabel]-(36)-|"
                                                 options:0
                                                 metrics:nil
                                                   views:views]];
        
        
        KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                                showType:KLCPopupShowTypeBounceInFromTop
                                             dismissType:KLCPopupDismissTypeFadeOut
                                                maskType:KLCPopupMaskTypeDimmed
                                dismissOnBackgroundTouch:NO
                                   dismissOnContentTouch:YES];
        
        [popup show];
    }
//    else if (sender.state == UIGestureRecognizerStateBegan){
//        NSLog(@"UIGestureRecognizerStateBegan.");
//        //Do Whatever You want on Began of Gesture
//    }
    
    
}
- (void)dismissButtonPressed:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}


-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

- (IBAction)addOne:(id)sender {
    
    [self.soundAdd play];
    
    int cantidad = [self.cantProducto.cantidad intValue];
    cantidad++;
    self.lblCantidad.text = [NSString stringWithFormat:@"%d", cantidad];
    [self.cantProducto setCantidad:[NSNumber numberWithInt:cantidad]];
    
    NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_SUBTOTAL
                                                      object:self
                                                    userInfo:@{@"precio":self.cantProducto.precio}];
    
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
    [self animationProducto];

    NSLog(@"%@", self.cantProducto.cantidad);
    
}
- (IBAction)removeOne:(id)sender {
    int cantidad = [self.cantProducto.cantidad intValue];
    if (cantidad > 0) {
        [self.soundRemove play];
        cantidad--;
        self.lblCantidad.text = [NSString stringWithFormat:@"%d", cantidad];
        self.cantProducto.cantidad = [NSNumber numberWithInt:cantidad];
        
        NSNotification *n = [NSNotification notificationWithName:ACTUALIZAR_SUBTOTAL_REMOVE
                                                          object:self
                                                        userInfo:@{@"precio":self.cantProducto.precio}];
        
        [[NSNotificationCenter defaultCenter] postNotification:n];
        
        [self animationProducto];
        
        NSLog(@"%@", self.cantProducto.cantidad);
    }
}

-(void)animationProducto{
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 1.2, 1.2);
                         
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 0.8335, 0.8335);
                                              
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}

- (IBAction)marcarFavorito:(id)sender{
    
    
    UIView* contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 250)];
    self.listasCreadas = [IATLista obtenerListasWithContext:self.context];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 8.0;
    
    UILabel *tituloPopUp = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 290, 25)];
    tituloPopUp.backgroundColor = [UIColor clearColor];
    tituloPopUp.textColor = [UIColor blackColor];
    tituloPopUp.font = [UIFont boldSystemFontOfSize:18.0];
    tituloPopUp.text = @"Seleccionar lista";
    tituloPopUp.textAlignment = NSTextAlignmentCenter;
    tituloPopUp.numberOfLines = 0;
    
    self.picker = [[UIPickerView alloc] initWithFrame:CGRectMake(5, 30, 290, 120)];
    self.picker.delegate = self;
    self.picker.dataSource = self;
    
    UIButton* asignarLista = [UIButton buttonWithType:UIButtonTypeCustom];
    asignarLista.frame = CGRectMake(5, 162, 290, 40);
    asignarLista.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    asignarLista.backgroundColor = [UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1.0f];
    //[UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1.0f]
    [asignarLista setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [asignarLista setTitleColor:[[asignarLista titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    asignarLista.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [asignarLista setTitle:@"Asignar" forState:UIControlStateNormal];
    asignarLista.layer.cornerRadius = 6.0;
    //crearLista.hidden = YES;
    [asignarLista addTarget:self action:@selector(asignarLista:) forControlEvents:UIControlEventTouchUpInside];
    
    self.listaSeleccionada = [self.listasCreadas objectAtIndex:0];
    if (self.listaSeleccionada == nil) {
        UILabel *lblMensaje = [[UILabel alloc] initWithFrame:CGRectMake(5, 145, 290, 20)];
        lblMensaje.backgroundColor = [UIColor clearColor];
        lblMensaje.textColor = [UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1.0f];
        lblMensaje.font = [UIFont systemFontOfSize:12.0];
        lblMensaje.text = @"No hay listas creadas";
        lblMensaje.textAlignment = NSTextAlignmentCenter;
        lblMensaje.numberOfLines = 0;
        [contentView addSubview:lblMensaje];
        
        asignarLista.enabled = YES;
    }
    
    
    
    UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.frame = CGRectMake(5, 205, 290, 40);
    //dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    dismissButton.backgroundColor = [UIColor colorWithRed:81/255.0f green:49/255.0f blue:136/255.0f alpha:1.0f];
    [dismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [dismissButton setTitle:@"Cerrar" forState:UIControlStateNormal];
    dismissButton.layer.cornerRadius = 6.0;
    [dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView addSubview:tituloPopUp];
    [contentView addSubview:self.picker];
    //[contentView addSubview:tfNameList];
    //[contentView addSubview:self.nombreNuevaLista];
    [contentView addSubview:asignarLista];
    [contentView addSubview:dismissButton];
    
    
    
    KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                            showType:KLCPopupShowTypeBounceInFromTop
                                         dismissType:KLCPopupDismissTypeFadeOut
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:NO];
    
    [popup show];
    
    
}
- (void)asignarLista:(id)sender{
    
    
    if (self.listaSeleccionada != nil) {
        //Politicas
        //Un producto puede estar en varias listas
        if (![IATItemLista existeItemListaWithProducto:self.cantProducto.producto lista:self.listaSeleccionada context:self.context]) {
            [IATItemLista itemListaWithCantidad:[NSNumber numberWithInt:0] producto:self.cantProducto.producto lista:self.listaSeleccionada proveedor:nil context:self.context];
            
            //icono-estrella-favorito
            if([self.cantProducto.producto.esFavorito isEqual:@NO]){
                self.cantProducto.producto.esFavorito = @YES;
                UIImage *fav = [UIImage imageNamed:@"estrella-amarilla"];
                [self.btnFavorito setImage:fav forState:UIControlStateNormal];
            }
            //    else{
            //        self.cantProducto.producto.esFavorito = @NO;
            //        UIImage *fav = [UIImage imageNamed:@"estrella-gris"];
            //        [self.btnFavorito setImage:fav forState:UIControlStateNormal];
            //    }
            
            if ([sender isKindOfClass:[UIView class]]) {
                [(UIView*)sender dismissPresentingPopup];
            }
        }
        
        
        UILabel *lblMensaje = [[UILabel alloc] initWithFrame:CGRectMake(5, -18, 290, 20)];
        lblMensaje.backgroundColor = [UIColor clearColor];
        lblMensaje.textColor = [UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1.0f];
        lblMensaje.font = [UIFont systemFontOfSize:12.0];
        lblMensaje.text = @"Este producto ya existe en esta lista";
        lblMensaje.textAlignment = NSTextAlignmentCenter;
        lblMensaje.numberOfLines = 0;
        [(UIView*)sender addSubview:lblMensaje];
    }
    
}

#pragma mark - UIPickerView Delegado
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.listasCreadas.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component{
    
    IATLista *lista = [self.listasCreadas objectAtIndex:row];
    return lista.nombre;
    
}
-(void)pickerView:(UIPickerView *)pickerView
     didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component{
    
    self.listaSeleccionada = [self.listasCreadas objectAtIndex:row];
    
}


#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage producto:(IATProducto *)producto block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        
        if (self.cantProducto.producto == producto){
            NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
            UIImage *image = [UIImage imageWithData:imageData];
            
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.cache setObject:image forKey:urlImage];
                    completionBlock(image);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil);
            });
        }
    });
}

#pragma mark - Ciclo de Vida
-(void)prepareForReuse{
    
    [self.cantProducto removeObserver:self forKeyPath:@"cantidad"];
    self.cantProducto = nil;
    self.imgProducto.image = nil;
    
}
-(void)dealloc {
    
    [self.cantProducto removeObserver:self forKeyPath:@"cantidad"];
    self.cantProducto = nil;
    self.imgProducto.image = nil;
    
}

@end

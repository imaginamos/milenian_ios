#import "IATPromocion.h"

@interface IATPromocion ()

// Private interface goes here.

@end

@implementation IATPromocion

+(instancetype) promocionWithId:(NSString *)idPromocion
                         nombre:(NSString *)nombre
                    descripcion:(NSString *)descripcion
                   fechaInicial:(NSDate *)fechaInicial
                     fechaFinal:(NSDate *)fechaFinal
                         unidad:(NSString *)unidad
                       urlImage:(NSString *)urlImage
                        context:(NSManagedObjectContext *)context{
    
    IATPromocion *nPromocion = [NSEntityDescription insertNewObjectForEntityForName:[IATPromocion entityName]
                                                           inManagedObjectContext:context];
    
    nPromocion.idEntity = idPromocion;
    nPromocion.nombre = nombre;
    nPromocion.descripcion = descripcion;
    nPromocion.fechaInicio = fechaInicial;
    nPromocion.fechaFinal = fechaFinal;
    nPromocion.unidad = unidad;
    nPromocion.urlImagen = urlImage;
    
    return nPromocion;
}

+(instancetype) obtenerPromocionWithId:(NSString *)idPromocion
                               context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPromocion entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idEntity == %@", idPromocion];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) obtenerPromocionesWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPromocion entityName]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
    
}

@end

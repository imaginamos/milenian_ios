//
//  InfoCompanyVC.h
//  Milenian
//
//  Created by MacBook Pro on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCompanyVC : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITextView *tvAcercaDeNosotros;
@end

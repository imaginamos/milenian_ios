//
//  ProductoVC.h
//  Milenian
//
//  Created by Carlos Obregón on 17/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@import AVFoundation;
@class IATPedido;
#import "iCarousel.h"


@interface ProductoVC : UIViewController<iCarouselDataSource, iCarouselDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) AVAudioPlayer* soundDelete;
@property (strong, nonatomic) AVAudioPlayer* soundAdd;
@property (strong, nonatomic) AVAudioPlayer* soundRemove;

//Descripción del producto
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UILabel *lblnameProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblnameMarca;
@property (weak, nonatomic) IBOutlet UILabel *lblnameUnidades;

//Barra y Lista de proveedores
@property (weak, nonatomic) IBOutlet UITableView *listProvedores;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidad;

@property (strong, nonatomic) IATPedido *pedido;

- (IBAction)agregarUnProducto:(id)sender;
- (IBAction)quitarUnProducto:(id)sender;
- (IBAction)ConsolidarPedido:(id)sender;
- (IBAction)eliminarProductoDelCarrito:(id)sender;


@end

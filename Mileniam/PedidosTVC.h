//
//  PedidosVC.h
//  Mileniam
//
//  Created by Desarrollador IOS on 27/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SECTION_1 0
#define SECTION_2 1
#define SECTION_3 2

@interface PedidosTVC : UITableViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@end

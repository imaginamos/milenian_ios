// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPreItemAcuerdo.m instead.

#import "_IATPreItemAcuerdo.h"

@implementation IATPreItemAcuerdoID
@end

@implementation _IATPreItemAcuerdo

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PreItemAcuerdo" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PreItemAcuerdo";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PreItemAcuerdo" inManagedObjectContext:moc_];
}

- (IATPreItemAcuerdoID*)objectID {
	return (IATPreItemAcuerdoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cantidadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cantidad"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cantidad;

- (int32_t)cantidadValue {
	NSNumber *result = [self cantidad];
	return [result intValue];
}

- (void)setCantidadValue:(int32_t)value_ {
	[self setCantidad:@(value_)];
}

- (int32_t)primitiveCantidadValue {
	NSNumber *result = [self primitiveCantidad];
	return [result intValue];
}

- (void)setPrimitiveCantidadValue:(int32_t)value_ {
	[self setPrimitiveCantidad:@(value_)];
}

@dynamic precio;

@dynamic producto;

@dynamic variedad;

@end

@implementation IATPreItemAcuerdoAttributes 
+ (NSString *)cantidad {
	return @"cantidad";
}
+ (NSString *)precio {
	return @"precio";
}
@end

@implementation IATPreItemAcuerdoRelationships 
+ (NSString *)producto {
	return @"producto";
}
+ (NSString *)variedad {
	return @"variedad";
}
@end


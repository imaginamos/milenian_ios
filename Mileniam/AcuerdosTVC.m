//
//  AcuerdosVC.m
//  Mileniam
//
//  Created by Desarrollador IOS on 27/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "AcuerdosTVC.h"
#import "SWRevealViewController.h"
#import "DetalleAcuerdoVC.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATAcuerdosCellView.h"
#import "IATAcuerdo.h"
#import "IATItemAcuerdo.h"
#import "IATProveedor.h"
#import "KNCirclePercentView.h"
#import "CacheImgs.h"

@interface AcuerdosTVC ()
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSArray *acuerdos;
@property (strong, nonatomic) NSArray *acuerdosAprobados;
@property (strong, nonatomic) NSArray *acuerdosNOAprobados;
@property (strong, nonatomic) NSCache *cache;
@end

@implementation AcuerdosTVC

#pragma mark - Ciclo de vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cache = [CacheImgs sharedInstance];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    UINib *cellNib = [UINib nibWithNibName:@"IATAcuerdosCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATAcuerdosCellView cellId]];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = @"Acuerdos";
    
    self.acuerdos = [IATAcuerdo obtenerAcuerdoWithContext:self.context];
    self.acuerdosAprobados = [IATAcuerdo obtenerAcuerdosWithEstado:@"1" context:self.context];
    self.acuerdosNOAprobados = [IATAcuerdo obtenerAcuerdosWithEstado:@"2" context:self.context];
    [self.tableView reloadData];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return 1;
    NSInteger sections = 0;
    if (self.acuerdosNOAprobados.count > 0) {
        sections++;
    }
    if (self.acuerdosAprobados.count > 0){
        sections++;
    }
    return sections;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    //return self.acuerdos.count;
    if (section == 0) {
        if (self.acuerdosNOAprobados.count > 0) {
            return self.acuerdosNOAprobados.count;
        }
        else{
            return self.acuerdosAprobados.count;
        }
        
    }
    else if (section == 1){
        return self.acuerdosAprobados.count;
    }
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    IATAcuerdo *acuerdo;
    if (indexPath.section == 0) {
        if (self.acuerdosNOAprobados.count > 0) {
            acuerdo = [self.acuerdosNOAprobados objectAtIndex:indexPath.row];
        }
        else{
            acuerdo = [self.acuerdosAprobados objectAtIndex:indexPath.row];
        }
        
    }
    else if (indexPath.section == 1){
        acuerdo = [self.acuerdosAprobados objectAtIndex:indexPath.row];
    }
    
    IATAcuerdosCellView *cell =
    [tableView dequeueReusableCellWithIdentifier:[IATAcuerdosCellView cellId]];
    
    NSString *fechaStringI = [NSString stringWithFormat:@"%@", acuerdo.fechaInicio];
    NSArray *arrayFechaI = [fechaStringI componentsSeparatedByString:@" "];
    
    NSString *fechaString = [NSString stringWithFormat:@"%@", acuerdo.fechaFinal];
    NSArray *arrayFecha = [fechaString componentsSeparatedByString:@" "];
    cell.lblPorcentaje.text = [NSString stringWithFormat:@"%@ / %@ ", arrayFechaI[0],arrayFecha[0]];
    int porcent = 0;
    
    if (![acuerdo.porcentaje isEqualToString:@""] && acuerdo.porcentaje != nil) {
        porcent = [acuerdo.porcentaje intValue];
    }
    
    // Auto calculate radius
    [cell.porcentajeView drawCircleWithPercent:porcent
                                      duration:2
                                     lineWidth:3
                                     clockwise:YES
                                       lineCap:kCALineCapRound
                                     fillColor:[UIColor clearColor]
                                   strokeColor:[UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1]//[UIColor blueColor]//
                                animatedColors:nil];
    cell.porcentajeView.percentLabel.font = [UIFont systemFontOfSize:15];
    
    
    [cell.porcentajeView startAnimation];
    
    
    
    //cell.lblPorcentaje.text = [NSString stringWithFormat:@"%@%%", acuerdo.porcentaje];
    
    
    
    if (acuerdo.provedor != nil) {
        
        cell.lblProveedor.text = acuerdo.provedor.nombre;
        
        if (![acuerdo.provedor.urlLogo isEqualToString:@""] && acuerdo.provedor.urlLogo != nil) {
            UIImage *logoProvedor = [self.cache objectForKey:[NSURL URLWithString:acuerdo.provedor.urlLogo]];
            if (logoProvedor) {
                cell.imgProveedor.image = logoProvedor;
            }
            else{
                [cell.activityLoad startAnimating];
                [self imageWithUrl:[NSURL URLWithString:acuerdo.provedor.urlLogo] block:^(UIImage *image) {
                    if (image != nil) {
                        cell.imgProveedor.image = image;
                    }
                    [cell.activityLoad stopAnimating];
                }];
            }
            
        }
    }
    
    if (acuerdo.itemsAcuerdo != nil) {
        cell.lblCantidadProductos.text =
        [NSString stringWithFormat:@"%u productos", acuerdo.itemsAcuerdo.count];
    }
    
    return cell;
    
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATAcuerdosCellView cellHeight];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IATAcuerdo *acuerdo;
    if (indexPath.section == 0) {
        if (self.acuerdosNOAprobados.count > 0){
            acuerdo = [self.acuerdosNOAprobados objectAtIndex:indexPath.row];
        }
        else{
            acuerdo = [self.acuerdosAprobados objectAtIndex:indexPath.row];
        }
        
    }
    else if (indexPath.section == 1){
        acuerdo = [self.acuerdosAprobados objectAtIndex:indexPath.row];
    }
    
    //IATAcuerdo *acuerdo = [self.acuerdos objectAtIndex:indexPath.row];
    
    DetalleAcuerdoVC *detalleAcuerdo = [self.storyboard instantiateViewControllerWithIdentifier:@"detalleAcuerdoVC"];
    
    detalleAcuerdo.acuerdo = acuerdo;
    
    [self.navigationController pushViewController:detalleAcuerdo animated:YES];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *sectionName = nil;
    
    if (section == 0) {
        if (self.acuerdosNOAprobados.count > 0) {
            sectionName = @"Por aprobar";
        }
        else{
            sectionName = @"Aprobados";
        }
        
    }
    else if (section == 1){
        sectionName = @"Aprobados";
    }
    
    return sectionName;
}

#pragma mark - Utility format price
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Utility Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end

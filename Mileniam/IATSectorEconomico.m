#import "IATSectorEconomico.h"

@interface IATSectorEconomico ()

// Private interface goes here.

@end

@implementation IATSectorEconomico

+(instancetype) tipologiaWithId:(NSString *)idTipologia
                         nombre:(NSString *)nombre
                        context:(NSManagedObjectContext *)context{
    
    IATSectorEconomico *tipologia =
    [NSEntityDescription insertNewObjectForEntityForName:[IATSectorEconomico entityName]
                                  inManagedObjectContext:context];
    
    
    tipologia.idSector = idTipologia;
    tipologia.nombre = nombre;
   
    
    return tipologia;
    
}

+(instancetype) obtenerTipologiaWithId:(NSString *)idTipologia
                               context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATSectorEconomico entityName]];
    
    NSPredicate *filtrarPorId = [NSPredicate predicateWithFormat:@"idSector == %@",idTipologia];
    [fetch setPredicate:filtrarPorId];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) obtenerTipologiaWithContext:(NSManagedObjectContext *)context{
    
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATSectorEconomico entityName]];

    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}

@end

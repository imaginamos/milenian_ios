//
//  SolicitudActualTVC.h
//  Milenian
//
//  Created by Carlos Obregón on 18/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IATPedido;

@interface SolicitudActualTVC : UITableViewController

@property (nonatomic, strong) IATPedido *pedido;

@end

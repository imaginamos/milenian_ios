// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATUsuario.m instead.

#import "_IATUsuario.h"

@implementation IATUsuarioID
@end

@implementation _IATUsuario

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Usuario" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Usuario";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Usuario" inManagedObjectContext:moc_];
}

- (IATUsuarioID*)objectID {
	return (IATUsuarioID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic direccion;

@dynamic telefono;

@dynamic tipoUsuario;

@dynamic urlLogo;

@end

@implementation IATUsuarioAttributes 
+ (NSString *)direccion {
	return @"direccion";
}
+ (NSString *)telefono {
	return @"telefono";
}
+ (NSString *)tipoUsuario {
	return @"tipoUsuario";
}
+ (NSString *)urlLogo {
	return @"urlLogo";
}
@end


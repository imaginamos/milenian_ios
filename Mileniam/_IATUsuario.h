// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATUsuario.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@interface IATUsuarioID : IATEntidadBaseID {}
@end

@interface _IATUsuario : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATUsuarioID*objectID;

@property (nonatomic, strong, nullable) NSString* direccion;

@property (nonatomic, strong, nullable) NSString* telefono;

@property (nonatomic, strong, nullable) NSString* tipoUsuario;

@property (nonatomic, strong, nullable) NSString* urlLogo;

@end

@interface _IATUsuario (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDireccion;
- (void)setPrimitiveDireccion:(NSString*)value;

- (NSString*)primitiveTelefono;
- (void)setPrimitiveTelefono:(NSString*)value;

- (NSString*)primitiveTipoUsuario;
- (void)setPrimitiveTipoUsuario:(NSString*)value;

- (NSString*)primitiveUrlLogo;
- (void)setPrimitiveUrlLogo:(NSString*)value;

@end

@interface IATUsuarioAttributes: NSObject 
+ (NSString *)direccion;
+ (NSString *)telefono;
+ (NSString *)tipoUsuario;
+ (NSString *)urlLogo;
@end

NS_ASSUME_NONNULL_END

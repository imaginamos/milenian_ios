//
//  IATProveedoresCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 13/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATProveedoresCellView.h"

@implementation IATProveedoresCellView
#pragma mark - Metodos de clase
+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 80.0f;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

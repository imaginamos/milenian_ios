#import "_IATVariedad.h"

@interface IATVariedad : _IATVariedad


+(instancetype) variedadWithId:(NSString *)idVariedad
                        nombre:(NSString *)nombre
                   descripcion:(NSString *)descripcion
                  subcategoria:(IATSubcategoria *)subcategoria
                       context:(NSManagedObjectContext *)context;

+(instancetype) obtenerVariedadWithId:(NSString *)idVariedad
                              context:(NSManagedObjectContext *)context;

@end

#import "_IATProductoProveedor.h"
@class IATProveedor;
@class IATProducto;

@interface IATProductoProveedor : _IATProductoProveedor

+(instancetype) productoProveedorWithPrecio:(NSDecimalNumber *)precio
                                   producto:(IATProducto *)producto
                                  proveedor:(IATProveedor *)proveedor
                                   variedad:(IATVariedad *)variedad
                               subcategoria:(IATSubcategoria *)subcategoria
                                    context:(NSManagedObjectContext *)context;

+ (instancetype) obtenerProductoProvedorWithProducto:(IATProducto *)producto
                                          proveedor:(IATProveedor *)proveedor
                                            context:(NSManagedObjectContext *)context;
+ (instancetype) obtenerPrecioMasBajo:(IATProducto *)producto
                             context:(NSManagedObjectContext *)context;
+ (instancetype) obtenerPrecioPorProveedor:(IATProveedor *)proveedor
                                 producto:(IATProducto *)producto
                                  context:(NSManagedObjectContext *)context;

+ (NSArray *) obtenerPreciosOrdenadoPorMenor:(IATProducto *)producto
                                    context:(NSManagedObjectContext *)context;
+ (NSArray *) filtrarSubcategoriasWithProveedor:(IATProveedor *)proveedor;
+ (NSArray *)filtrarVariedadesWithProveedor:(IATProveedor *)proveedor
                               subcategoria:(IATSubcategoria *)subcategoria;
+ (NSArray *)filtrarVariedadesWithProveedor:(IATProveedor *)proveedor
                               subcategoria:(IATSubcategoria *)subcategoria
                                   variedad:(IATVariedad *)variedad;

@end

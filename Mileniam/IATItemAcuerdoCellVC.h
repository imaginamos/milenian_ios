//
//  IATItemAcuerdoCellVC.h
//  Milenian
//
//  Created by Carlos Obregón on 11/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNCirclePercentView.h"

@interface IATItemAcuerdoCellVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;
@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidad;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

@property (weak, nonatomic) IBOutlet KNCirclePercentView *porcentajePedido;

+(NSString *) cellId;
+(CGFloat) cellHeight;
@end

//
//  MisListasTVC.h
//  Milenian
//
//  Created by Carlos Obregón on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;

@interface MisListasTVC : UITableViewController<UITextFieldDelegate>

- (IBAction)AgregarNuevaLista:(id)sender;

@end

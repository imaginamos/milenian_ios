// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATCategoria.m instead.

#import "_IATCategoria.h"

@implementation IATCategoriaID
@end

@implementation _IATCategoria

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Categoria" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Categoria";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Categoria" inManagedObjectContext:moc_];
}

- (IATCategoriaID*)objectID {
	return (IATCategoriaID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic urlImagen;

@dynamic promociones;

- (NSMutableSet<IATPromocion*>*)promocionesSet {
	[self willAccessValueForKey:@"promociones"];

	NSMutableSet<IATPromocion*> *result = (NSMutableSet<IATPromocion*>*)[self mutableSetValueForKey:@"promociones"];

	[self didAccessValueForKey:@"promociones"];
	return result;
}

@dynamic subcategorias;

- (NSMutableSet<IATSubcategoria*>*)subcategoriasSet {
	[self willAccessValueForKey:@"subcategorias"];

	NSMutableSet<IATSubcategoria*> *result = (NSMutableSet<IATSubcategoria*>*)[self mutableSetValueForKey:@"subcategorias"];

	[self didAccessValueForKey:@"subcategorias"];
	return result;
}

@end

@implementation IATCategoriaAttributes 
+ (NSString *)urlImagen {
	return @"urlImagen";
}
@end

@implementation IATCategoriaRelationships 
+ (NSString *)promociones {
	return @"promociones";
}
+ (NSString *)subcategorias {
	return @"subcategorias";
}
@end


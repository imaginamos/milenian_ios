//
//  NuevoAcuerdoVC.h
//  Milenian
//
//  Created by Carlos Obregón on 25/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@class IATProveedor;
#import "iCarousel.h"

@interface NuevoAcuerdoVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>{
    UIDatePicker *datePickerDS, *datePickerDF;
}

@property (nonatomic, strong) IATProveedor *proveedor;
@property (weak, nonatomic) IBOutlet UITextView *tvComentarios;
@property (weak, nonatomic) IBOutlet UITextField *tfFechaInicial;
@property (weak, nonatomic) IBOutlet UITextField *tfFechaFinal;
@property (weak, nonatomic) IBOutlet UITextField *tfProveedorSeleccionado;
@property (weak, nonatomic) IBOutlet iCarousel *productosAcuerdo;
@property (weak, nonatomic) IBOutlet UITableView *listProductosAcuerdo;
@property (weak, nonatomic) IBOutlet UIButton *btnSeleccionarProveedor;
@property (weak, nonatomic) IBOutlet UIButton *btnSeleccionarProductos;

- (IBAction)seleccionarProveedor:(id)sender;
- (IBAction)seleccionarProductos:(id)sender;
- (IBAction)cargarDatePicker:(id)sender;

@end

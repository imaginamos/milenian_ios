//
//  Api.m
//  Created by Leonardo Rodriguez on 1/5/16.

#import "Api.h"

NSString *const urlBase = @"http://45.33.116.10/";
//NSString *const urlBase = @"http://www.milenian.com/";

@implementation Api

+(instancetype)sharedInstance {
    static dispatch_once_t onceQueue;
    static Api *__sharedInstance = nil;
    dispatch_once(&onceQueue, ^{
        __sharedInstance = [[self alloc] init];
    });
    
    return __sharedInstance;
    
}

-(void)userLogin:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/loginUsers.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)loadCategories:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/categories.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)loadProductos:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/products.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)registerUser:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"registerCustomer.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)enviarAjustesPerfil:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"milenian/modules/profile/profile.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)loadProveedores:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/provider.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)loadPromociones:(NSDictionary *)parameters success:(ResponseBlock)success{
    
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/promotionsProducts.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)enviarPedido:(NSDictionary *)parameters success:(ResponseBlock)success {
    //NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"prestashop/modules/orders/orders.php"];
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"milenian/modules/orders/orders.php"];
    [self postRequest:endpoint parameters:parameters success:success];
}

-(void)obtenerPedidos:(NSDictionary *)parameters success:(ResponseBlock)success {
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/orders.php"];
    
    [self postRequest:endpoint parameters:parameters success:success];
}

-(void)obtenerClientes:(NSDictionary *)parameters success:(ResponseBlock)success {
    NSString *endpoint = [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/customerList.php"];
    
    [self postRequest:endpoint parameters:parameters success:success];
}

-(void)obtenerContent:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint =
    [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/cmsContent.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)enviarAcuerdo:(NSDictionary *)parameters success:(ResponseBlock)success {
    //NSString *endpoint =
    //[NSString stringWithFormat:@"%@%@",urlBase,@"prestashop/modules/agreements/agreementsPost.php"];
    
    NSString *endpoint =
    [NSString stringWithFormat:@"%@%@",urlBase,@"milenian/modules/agreements/agreementsPost.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)obtenerAcuerdos:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint =
    [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/agreements.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)obtenerNotificaciones:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint =
    [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/notifications.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)obtenerTipologia:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    NSString *endpoint =
    [NSString stringWithFormat:@"%@%@",urlBase,@"modules/JSONapps/sectorEconomic.php"];
    [self postRequest:endpoint parameters:parameters success:success];
    
}

-(void)postRequest:(NSString *)endpoint parameters:(NSDictionary *)parameters success:(ResponseBlock)success {
    
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [self POST:endpoint parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]){
            success(YES,responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        success(NO,error);
    }];
    
}



@end

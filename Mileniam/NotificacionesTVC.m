//
//  NotificacionesTVC.m
//  Milenian
//
//  Created by Desarrollador IOS on 15/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "NotificacionesTVC.h"
#import "SWRevealViewController.h"
#import "IATNotificacion.h"
#import "IATNotificacionCellView.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "MBProgressHUD.h"
#import "Api.h"

@interface NotificacionesTVC ()
@property (nonatomic, strong) NSMutableArray *notificaciones;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property(nonatomic, strong) MBProgressHUD *hud;
@end

@implementation NotificacionesTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    self.notificaciones = [NSMutableArray arrayWithArray:[IATNotificacion obtenerNotificacionWithContext:self.context]];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATNotificacionCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATNotificacionCellView cellId]];
    
    [self sincronizarNotificaciones];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}

- (void)sincronizarNotificaciones{
    
    Api *api =[Api sharedInstance];
    
    //Enviando petición de Categorias
    NSDate* now = [NSDate date];
    
    //Variable secondTime: Conocer si es primera vez que se ejecuta la app
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *keySesion = [defaults stringForKey:@"kKey"];
    NSString *idCustomer = [defaults stringForKey:@"kIdCustomer"];
    
    //NSDictionary *parametersConten = @{@"keys":keySesion, @"idCustomer":idCustomer};
    
    //Opcional
    NSDictionary *parameters =
    @{@"firstTime":@"1", @"dateRequest":now ,@"keys":keySesion , @"idCustomer":idCustomer};

    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.label.text = @"Actualizando...";
    
    NSLog(@"Descargando notificaciones");
    [api obtenerNotificaciones:parameters success:^(BOOL success, id response) {
        if(success){
            NSLog(@"Guardando notificaciones");
            [self saveNotificaciones:response];
        }
        else{
            NSLog(@"Fallo descarga JSON de Notificaciones");
        }
    }];
    
}

-(void)saveNotificaciones:(NSDictionary *)data{
    
    NSDictionary *notificaciones = [data objectForKey:@"Notifications"];
    
    if (![notificaciones isEqual:@""]){
        NSString *idNoti, *nombre, *mensaje, *fecha, *hora, *fechaHora;
        
        for (id objeto in notificaciones){
            
            idNoti = [self changeNullToString:[objeto objectForKey:@"IdNotification"]];
            mensaje = [self changeNullToString:[objeto objectForKey:@"Message"]];
            nombre = [self changeNullToString:[objeto objectForKey:@"Name"]];
            fecha = [self changeNullToString:[objeto objectForKey:@"Date"]];
            hora = [self changeNullToString:[objeto objectForKey:@"Hour"]];
            
            fechaHora = [NSString stringWithFormat:@"%@ - %@", fecha, hora];
            
            nombre = [self decodeWithString:nombre];
            mensaje = [self decodeWithString:mensaje];
            
            if ([IATNotificacion obtenerNotificacionWithId:idNoti context:self.context] == nil) {
                [IATNotificacion notificacionWithId:idNoti nombre:nombre mensaje:mensaje fecha:fechaHora context:self.context];
            }
            
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString *dateRequestNotification =[NSString stringWithFormat:@"%@", [NSDate date]];
        [defaults setObject:dateRequestNotification forKey:@"kLastDateRequestNotification"];
        
        [defaults synchronize];
        self.notificaciones = [NSMutableArray arrayWithArray:[IATNotificacion obtenerNotificacionWithContext:self.context]];
        [self.tableView reloadData];
    
    }
    else{
        NSLog(@"JSON de Notificaciones vacio");
    }
    
    [self.hud hideAnimated:YES];
    
}

#pragma mark - Utilidades
-(NSString *)decodeWithString:(NSString *)string{
    
    NSString *newString;
    
    if (string != nil) {
        const char *c = [string cStringUsingEncoding:NSISOLatin1StringEncoding];
        newString = [[NSString alloc]initWithCString:c encoding:NSUTF8StringEncoding];
    }
    else{
        newString = @"";
    }
    
    return newString;
}
-(NSString *)changeNullToString:(NSString *)string{
    
    if ( string == (NSString *)[NSNull null] )
    {
        string = @"";
    }
    
    return string;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.notificaciones.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATNotificacion *noti = [self.notificaciones objectAtIndex:indexPath.row];
    IATNotificacionCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATNotificacionCellView cellId]];
    
    if (noti.nombre != nil && ![noti.nombre isEqualToString:@""]) {
        cell.lblNombreNoti.text = noti.nombre;
    }
    
    if (noti.date != nil && ![noti.date isEqualToString:@""]) {
        cell.lblFechaNoti.text = noti.date;
    }
    if (noti.message != nil && ![noti.message isEqualToString:@""]) {
        cell.lblMensajeNoti.text = noti.message;
    }
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATNotificacionCellView cellHeight];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //Tomo el elemento a elimar antes de que sea quitado
        IATNotificacion *deleteItem = [self.notificaciones objectAtIndex:indexPath.row];
        
        //Lo remuevo de la tabla
        [self.notificaciones removeObjectAtIndex:indexPath.row];
        
        //Lo elimino de base de datos
        [self.context deleteObject:deleteItem];
        if([deleteItem isDeleted]) {
            
            NSError *errorInfo = nil;
            
            if([self.context save:&errorInfo]){
                
                NSLog(@"Actualizado el contexto;");
            }
            else{
                NSLog(@"Error al eliminar: %@", errorInfo);
            }
        }
        
        if (self.notificaciones.count == 0) {
            //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        //Refresco la tabla
        [tableView reloadData];
    }
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

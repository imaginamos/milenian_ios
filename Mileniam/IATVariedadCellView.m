//
//  IATVariedadCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 11/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATVariedadCellView.h"

@implementation IATVariedadCellView

#pragma mark - Metodos de clase
+(NSString *) cellId{
    return NSStringFromClass(self);
}
+(CGFloat)cellHeight{
    return 44.0f;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

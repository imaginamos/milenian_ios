//
//  AjustesVC.h
//  Mileniam
//
//  Created by Desarrollador IOS on 27/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
#define COOKIE_KEY @"sVVP2U7FwaoyjFj5IYQapn0O10oDWDYpNad12fVCb5wsud2Ac35QuVj4"

@interface AjustesVC : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (weak, nonatomic) IBOutlet UITextField *tfCif;
@property (weak, nonatomic) IBOutlet UITextField *tfNombre;
@property (weak, nonatomic) IBOutlet UITextField *tfTelefono;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfDireccion;
@property (weak, nonatomic) IBOutlet UITextField *tfSector;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerTipologia;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerImage;



- (IBAction)seleccionarImgUser:(id)sender;
@end

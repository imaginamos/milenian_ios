// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPedido.m instead.

#import "_IATPedido.h"

@implementation IATPedidoID
@end

@implementation _IATPedido

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Pedido" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Pedido";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Pedido" inManagedObjectContext:moc_];
}

- (IATPedidoID*)objectID {
	return (IATPedidoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic estado;

@dynamic fecha;

@dynamic idCliente;

@dynamic idPedido;

@dynamic nombre;

@dynamic precioTotal;

@dynamic itemsPedido;

- (NSMutableSet<IATItemPedido*>*)itemsPedidoSet {
	[self willAccessValueForKey:@"itemsPedido"];

	NSMutableSet<IATItemPedido*> *result = (NSMutableSet<IATItemPedido*>*)[self mutableSetValueForKey:@"itemsPedido"];

	[self didAccessValueForKey:@"itemsPedido"];
	return result;
}

@end

@implementation IATPedidoAttributes 
+ (NSString *)estado {
	return @"estado";
}
+ (NSString *)fecha {
	return @"fecha";
}
+ (NSString *)idCliente {
	return @"idCliente";
}
+ (NSString *)idPedido {
	return @"idPedido";
}
+ (NSString *)nombre {
	return @"nombre";
}
+ (NSString *)precioTotal {
	return @"precioTotal";
}
@end

@implementation IATPedidoRelationships 
+ (NSString *)itemsPedido {
	return @"itemsPedido";
}
@end


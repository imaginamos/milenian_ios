//
//  VariedadVC.m
//  Milenian
//
//  Created by Desarrollador IOS on 25/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "VariedadVC.h"
#import "ProductoVC.h"
#import "IATVariedad.h"
#import "IATProducto.h"
#import "IATPedido.h"
#import "IATItemPedido.h"
#import "IATCantidadProducto.h"
#import "IATProductoProveedor.h"
#import "IATProductoCellCollectionView.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "KLCPopup/KLCPopup.h"
#import <QuartzCore/QuartzCore.h>
#import "CacheImgs.h"

@interface VariedadVC()
@property (strong, nonatomic) NSArray *productos, *cantidadXproducto;
@property (nonatomic) NSDecimalNumber *subtotal;
@property (nonatomic) int lbl1, lbl2, lbl3, lbl4, lbl5, lbl6;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property NSCache *cache;
@end

@implementation VariedadVC

#pragma mark - Ciclo de vida
-(void)viewDidLoad{
    [super viewDidLoad];
    
    //self.cache = [[NSCache alloc] init];
    self.cache = [CacheImgs sharedInstance];
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    self.title = @"Productos";
    self.subtotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    self.subTotal.text = [NSString stringWithFormat:@"€ %@", self.subtotal];
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    
    UIImage *bntAcept = [UIImage imageNamed:@"chulo-ok"];
    
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithImage:bntAcept
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self action:@selector(acept)];
    
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnAcept.imageInsets = UIEdgeInsetsMake(12, 10, 6, 10);
    
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    
    
    UINib *nib = [UINib nibWithNibName:@"IATProductoCellCollectionView" bundle:nil];
    [self.variedadCV registerNib:nib forCellWithReuseIdentifier:[IATProductoCellCollectionView cellId]];
    
    self.productos = [self.variedad.productos allObjects];
    
    self.variedadCV.delegate = self;
    self.variedadCV.dataSource = self;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self.variedadCV reloadData];
    
    NSNotificationCenter *n = [NSNotificationCenter defaultCenter];
    [n addObserver:self selector:@selector(actualizarSubTotal:) name:ACTUALIZAR_SUBTOTAL object:nil];
    [n addObserver:self selector:@selector(actualizarSubTotalDisminuir:) name:ACTUALIZAR_SUBTOTAL_REMOVE object:nil];
    
    [self calcularSubTotal];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Notificaciones Cambio de precio
-(void)actualizarSubTotal:(NSNotification *)notificacion{
    NSDictionary *dic = [notificacion userInfo];
    
    NSDecimalNumber *newProduct = [dic objectForKey:@"precio"];
    
    self.subtotal = [self.subtotal decimalNumberByAdding:newProduct];
    self.subTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
}
-(void)actualizarSubTotalDisminuir:(NSNotification *)notificacion{
    NSDictionary *dic = [notificacion userInfo];
    
    NSDecimalNumber *newProduct = [dic objectForKey:@"precio"];
    self.subtotal = [self.subtotal decimalNumberBySubtracting:newProduct];
    self.subTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
}
-(void)calcularSubTotal{
    
    self.subtotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    
    for (IATProducto *preItem in self.productos) {
        if(preItem.cantidadPedido.cantidad > 0){
            NSDecimalNumber *cantidad =
            [NSDecimalNumber decimalNumberWithDecimal:[preItem.cantidadPedido.cantidad decimalValue]];
            
            NSDecimalNumber *precioPreItem =
            [preItem.cantidadPedido.precio decimalNumberByMultiplyingBy:cantidad];
            
            self.subtotal = [self.subtotal decimalNumberByAdding:precioPreItem];
        }
        
    }
    
    self.subTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
    
}

-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.productos.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IATProductoCellCollectionView *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:[IATProductoCellCollectionView cellId]
                                              forIndexPath:indexPath];
    
    cell.cache = self.cache;
    
    IATProducto *producto = [self.productos objectAtIndex:indexPath.row];
    
    //NSLog(@"producto:%@ row:%ld", producto, (long)indexPath.row);
    if(producto.cantidadPedido.cantidad == nil){
        IATCantidadProducto *cantProducto = [IATCantidadProducto productoWithCantidad:[NSNumber numberWithInt:0] producto:producto variedad:self.variedad context:self.context];
        [cell observeCantidadProducto:cantProducto];
    }
    else{
        IATCantidadProducto *cantProducto = producto.cantidadPedido;
        
        [cell observeCantidadProducto:cantProducto];
    }
    
    return cell;
    
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    IATProducto *producto = [self.productos objectAtIndex:indexPath.row];
//    
//    [self.soundAdd play];
//    
//    //Agregar una unidad
//    int cantidad = [producto.cantidadPedido.cantidad intValue];
//    cantidad++;
//    producto.cantidadPedido.cantidad = [NSNumber numberWithInt:cantidad];
//    
//    self.subtotal = [self.subtotal decimalNumberByAdding:producto.cantidadPedido.precio];
//    
//    self.subTotal.text =
//    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
    
}

- (void) addSubviewWithZoomInAnimation:(UIView*)view duration:(float)secs option:(UIViewAnimationOptions)option
{
    // first reduce the view to 1/100th of its original dimension
    CGAffineTransform trans = CGAffineTransformScale(view.transform, 0.01, 0.01);
    view.transform = trans;	// do it instantly, no animation
    [self.view addSubview:view];
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:secs delay:0.0 options:option
                     animations:^{
                         view.transform = CGAffineTransformScale(view.transform, 100.0, 100.0);
                     }
                     completion:nil];
}

//- (void) removeWithZoomOutAnimation:(float)secs option:(UIViewAnimationOptions)option
//{
//    [UIView animateWithDuration:secs delay:0.0 options:option
//                     animations:^{
//                         self.transform = CGAffineTransformScale(self.transform, 0.01, 0.01);
//                     }
//                     completion:^(BOOL finished) {
//                         [self removeFromSuperview]; 
//                     }];
//}



#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:imageData];
            completionBlock(image);
        });
    });
    
    
}

#pragma mark- Pedido preliminar
- (void)acept{
    
    //Preguntar si existe algun pedido en proceso
    IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
    if (pedido == nil) {
        pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
    }
    
    
    //Ingresar items de pedido
    for (id objeto in [self.variedad.cantidadesProductos allObjects]){
        
        IATCantidadProducto *cantProducto = objeto;
        if([cantProducto.cantidad intValue] > 0){
            
            NSDecimalNumber *cantidad = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",cantProducto.cantidad]];
            
            NSDecimalNumber *precioItemPedido = [cantProducto.precio decimalNumberByMultiplyingBy:cantidad];
            
            IATItemPedido *itemPedido =
            [IATItemPedido obtenerItemPedidoWithProducto:cantProducto.producto
                                                 context:self.context];
            if (itemPedido == nil) {
                [IATItemPedido itemPedidoWithCantidad:cantProducto.cantidad producto:cantProducto.producto pedido:pedido precio:precioItemPedido context:self.context];
            }
            else{
                itemPedido.cantidad = cantidad;
                itemPedido.precioTotal = precioItemPedido;
                itemPedido.pedido = pedido;
            }
            
        }
    }
    
    if (pedido.itemsPedido.count > 0) {
        ProductoVC *producto = [self.storyboard instantiateViewControllerWithIdentifier:@"productoVC"];
        producto.pedido = pedido;
        [self.navigationController pushViewController:producto animated:YES];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Debe seleccionar por lo menos un producto"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}



#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

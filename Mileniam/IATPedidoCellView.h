//
//  IATPedidoCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 18/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATPedidoCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;
@property (weak, nonatomic) IBOutlet UILabel *lblNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *lblUnidad;
@property (weak, nonatomic) IBOutlet UILabel *lblMarca;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecioItem;
@property (weak, nonatomic) IBOutlet UILabel *lblProveedor;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

+(NSString *) cellId;
+(CGFloat) cellHeight;

@end

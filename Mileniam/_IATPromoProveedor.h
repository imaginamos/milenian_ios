// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPromoProveedor.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATPromocion;
@class IATProveedor;

@interface IATPromoProveedorID : NSManagedObjectID {}
@end

@interface _IATPromoProveedor : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATPromoProveedorID*objectID;

@property (nonatomic, strong, nullable) NSDecimalNumber* precio;

@property (nonatomic, strong) IATPromocion *promocion;

@property (nonatomic, strong) IATProveedor *proveedor;

@end

@interface _IATPromoProveedor (CoreDataGeneratedPrimitiveAccessors)

- (NSDecimalNumber*)primitivePrecio;
- (void)setPrimitivePrecio:(NSDecimalNumber*)value;

- (IATPromocion*)primitivePromocion;
- (void)setPrimitivePromocion:(IATPromocion*)value;

- (IATProveedor*)primitiveProveedor;
- (void)setPrimitiveProveedor:(IATProveedor*)value;

@end

@interface IATPromoProveedorAttributes: NSObject 
+ (NSString *)precio;
@end

@interface IATPromoProveedorRelationships: NSObject
+ (NSString *)promocion;
+ (NSString *)proveedor;
@end

NS_ASSUME_NONNULL_END

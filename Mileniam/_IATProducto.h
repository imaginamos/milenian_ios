// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATProducto.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@class IATCantidadProducto;
@class IATItemAcuerdo;
@class IATItemLista;
@class IATItemPedido;
@class IATPreItemAcuerdo;
@class IATProductoProveedor;
@class IATVariedad;

@interface IATProductoID : IATEntidadBaseID {}
@end

@interface _IATProducto : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATProductoID*objectID;

@property (nonatomic, strong, nullable) NSNumber* esFavorito;

@property (atomic) BOOL esFavoritoValue;
- (BOOL)esFavoritoValue;
- (void)setEsFavoritoValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* esRecomendado;

@property (atomic) BOOL esRecomendadoValue;
- (BOOL)esRecomendadoValue;
- (void)setEsRecomendadoValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* marca;

@property (nonatomic, strong, nullable) NSString* unidad;

@property (nonatomic, strong, nullable) NSString* urlImagen;

@property (nonatomic, strong, nullable) IATCantidadProducto *cantidadPedido;

@property (nonatomic, strong, nullable) NSSet<IATItemAcuerdo*> *itemsAcuerdo;
- (nullable NSMutableSet<IATItemAcuerdo*>*)itemsAcuerdoSet;

@property (nonatomic, strong, nullable) NSSet<IATItemLista*> *itemsLista;
- (nullable NSMutableSet<IATItemLista*>*)itemsListaSet;

@property (nonatomic, strong, nullable) NSSet<IATItemPedido*> *itemsPedido;
- (nullable NSMutableSet<IATItemPedido*>*)itemsPedidoSet;

@property (nonatomic, strong, nullable) IATPreItemAcuerdo *preItemsAcuerdo;

@property (nonatomic, strong, nullable) NSSet<IATProductoProveedor*> *precioProvedor;
- (nullable NSMutableSet<IATProductoProveedor*>*)precioProvedorSet;

@property (nonatomic, strong) IATVariedad *variedad;

@end

@interface _IATProducto (ItemsAcuerdoCoreDataGeneratedAccessors)
- (void)addItemsAcuerdo:(NSSet<IATItemAcuerdo*>*)value_;
- (void)removeItemsAcuerdo:(NSSet<IATItemAcuerdo*>*)value_;
- (void)addItemsAcuerdoObject:(IATItemAcuerdo*)value_;
- (void)removeItemsAcuerdoObject:(IATItemAcuerdo*)value_;

@end

@interface _IATProducto (ItemsListaCoreDataGeneratedAccessors)
- (void)addItemsLista:(NSSet<IATItemLista*>*)value_;
- (void)removeItemsLista:(NSSet<IATItemLista*>*)value_;
- (void)addItemsListaObject:(IATItemLista*)value_;
- (void)removeItemsListaObject:(IATItemLista*)value_;

@end

@interface _IATProducto (ItemsPedidoCoreDataGeneratedAccessors)
- (void)addItemsPedido:(NSSet<IATItemPedido*>*)value_;
- (void)removeItemsPedido:(NSSet<IATItemPedido*>*)value_;
- (void)addItemsPedidoObject:(IATItemPedido*)value_;
- (void)removeItemsPedidoObject:(IATItemPedido*)value_;

@end

@interface _IATProducto (PrecioProvedorCoreDataGeneratedAccessors)
- (void)addPrecioProvedor:(NSSet<IATProductoProveedor*>*)value_;
- (void)removePrecioProvedor:(NSSet<IATProductoProveedor*>*)value_;
- (void)addPrecioProvedorObject:(IATProductoProveedor*)value_;
- (void)removePrecioProvedorObject:(IATProductoProveedor*)value_;

@end

@interface _IATProducto (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveEsFavorito;
- (void)setPrimitiveEsFavorito:(NSNumber*)value;

- (BOOL)primitiveEsFavoritoValue;
- (void)setPrimitiveEsFavoritoValue:(BOOL)value_;

- (NSNumber*)primitiveEsRecomendado;
- (void)setPrimitiveEsRecomendado:(NSNumber*)value;

- (BOOL)primitiveEsRecomendadoValue;
- (void)setPrimitiveEsRecomendadoValue:(BOOL)value_;

- (NSString*)primitiveMarca;
- (void)setPrimitiveMarca:(NSString*)value;

- (NSString*)primitiveUnidad;
- (void)setPrimitiveUnidad:(NSString*)value;

- (NSString*)primitiveUrlImagen;
- (void)setPrimitiveUrlImagen:(NSString*)value;

- (IATCantidadProducto*)primitiveCantidadPedido;
- (void)setPrimitiveCantidadPedido:(IATCantidadProducto*)value;

- (NSMutableSet<IATItemAcuerdo*>*)primitiveItemsAcuerdo;
- (void)setPrimitiveItemsAcuerdo:(NSMutableSet<IATItemAcuerdo*>*)value;

- (NSMutableSet<IATItemLista*>*)primitiveItemsLista;
- (void)setPrimitiveItemsLista:(NSMutableSet<IATItemLista*>*)value;

- (NSMutableSet<IATItemPedido*>*)primitiveItemsPedido;
- (void)setPrimitiveItemsPedido:(NSMutableSet<IATItemPedido*>*)value;

- (IATPreItemAcuerdo*)primitivePreItemsAcuerdo;
- (void)setPrimitivePreItemsAcuerdo:(IATPreItemAcuerdo*)value;

- (NSMutableSet<IATProductoProveedor*>*)primitivePrecioProvedor;
- (void)setPrimitivePrecioProvedor:(NSMutableSet<IATProductoProveedor*>*)value;

- (IATVariedad*)primitiveVariedad;
- (void)setPrimitiveVariedad:(IATVariedad*)value;

@end

@interface IATProductoAttributes: NSObject 
+ (NSString *)esFavorito;
+ (NSString *)esRecomendado;
+ (NSString *)marca;
+ (NSString *)unidad;
+ (NSString *)urlImagen;
@end

@interface IATProductoRelationships: NSObject
+ (NSString *)cantidadPedido;
+ (NSString *)itemsAcuerdo;
+ (NSString *)itemsLista;
+ (NSString *)itemsPedido;
+ (NSString *)preItemsAcuerdo;
+ (NSString *)precioProvedor;
+ (NSString *)variedad;
@end

NS_ASSUME_NONNULL_END

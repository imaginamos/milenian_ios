// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPromoProveedor.m instead.

#import "_IATPromoProveedor.h"

@implementation IATPromoProveedorID
@end

@implementation _IATPromoProveedor

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PromoProveedor" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PromoProveedor";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PromoProveedor" inManagedObjectContext:moc_];
}

- (IATPromoProveedorID*)objectID {
	return (IATPromoProveedorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic precio;

@dynamic promocion;

@dynamic proveedor;

@end

@implementation IATPromoProveedorAttributes 
+ (NSString *)precio {
	return @"precio";
}
@end

@implementation IATPromoProveedorRelationships 
+ (NSString *)promocion {
	return @"promocion";
}
+ (NSString *)proveedor {
	return @"proveedor";
}
@end


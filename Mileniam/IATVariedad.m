#import "IATVariedad.h"

@interface IATVariedad ()

// Private interface goes here.

@end

@implementation IATVariedad

+(instancetype) variedadWithId:(NSString *)idVariedad
                        nombre:(NSString *)nombre
                   descripcion:(NSString *)descripcion
                  subcategoria:(IATSubcategoria *)subcategoria
                       context:(NSManagedObjectContext *)context{
    
    IATVariedad *nVariedad = [NSEntityDescription insertNewObjectForEntityForName:[IATVariedad entityName]
                                                                   inManagedObjectContext:context];
    
    nVariedad.idEntity = idVariedad;
    nVariedad.nombre = nombre;
    nVariedad.descripcion = descripcion;
    nVariedad.subcategoria = subcategoria;
    
    return nVariedad;
    
}


+(instancetype) obtenerVariedadWithId:(NSString *)idVariedad
                              context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATVariedad entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idEntity == %@",idVariedad];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

@end

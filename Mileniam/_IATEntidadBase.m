// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATEntidadBase.m instead.

#import "_IATEntidadBase.h"

@implementation IATEntidadBaseID
@end

@implementation _IATEntidadBase

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"EntidadBase" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"EntidadBase";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"EntidadBase" inManagedObjectContext:moc_];
}

- (IATEntidadBaseID*)objectID {
	return (IATEntidadBaseID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic descripcion;

@dynamic idEntity;

@dynamic nombre;

@end

@implementation IATEntidadBaseAttributes 
+ (NSString *)descripcion {
	return @"descripcion";
}
+ (NSString *)idEntity {
	return @"idEntity";
}
+ (NSString *)nombre {
	return @"nombre";
}
@end


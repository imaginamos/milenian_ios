//
//  MenuVC.m
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "MenuVC.h"
#import "SWRevealViewController.h"
#import "LoginVC.h"
#import "IATPerfilCell.h"
#import "IATPedido.h"
#import "SolicitudActualTVC.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"

@interface MenuVC ()
@property (nonatomic, strong) NSManagedObjectContext *context;
@end

@implementation MenuVC{
    NSArray *menuItems;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    self.title = @"Menu";
    
    menuItems = @[@"idcliente", @"inicio",@"carrito", @"pedidos",@"listas" ,@"acuerdos", @"promociones", @"busqueda", @"proveedores", @"notificaciones", @"ajustes",@"tickets", @"info", @"terminos&condiciones", @"cat"];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATPerfilCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATPerfilCell cellId]];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        IATPerfilCell *cell =
        [tableView dequeueReusableCellWithIdentifier:[IATPerfilCell cellId] forIndexPath:indexPath];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString *company = [defaults stringForKey:@"kCompany"];
        cell.lblCompania.text = company;
        
        NSString *address = [defaults stringForKey:@"kAddress"];
        cell.lblUbicacion.text = address;
        
        [defaults synchronize];
        
        
        NSString  *imagePath =
        [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/someImageName.png"];
        NSFileManager *gestorArchivos = [NSFileManager defaultManager];
        if ([gestorArchivos fileExistsAtPath:imagePath] == YES) {
            
            UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
            cell.imgPerfil.image = image;
            
            [cell.imgPerfil setContentMode:UIViewContentModeScaleAspectFill];
            [cell.imgPerfil setClipsToBounds:YES];
            cell.imgPerfil.layer.masksToBounds = YES;
            cell.imgPerfil.layer.cornerRadius = cell.imgPerfil.bounds.size.width/2;
            cell.imgPerfil.layer.borderWidth = 1.0;
            cell.imgPerfil.layer.borderColor = (__bridge CGColorRef)([UIColor lightGrayColor]);
            
            
        }
        else{
            NSLog(@"Aun no se ha guardado imagen de perfil");
        }

        
        return cell;
    }
    else{
        NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return [IATPerfilCell cellHeight];
    }
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        NSLog(@"carrito selected");
        
        IATPedido *pedido = [IATPedido obtenerpedidoWithEstado:@"enproceso" context:self.context];
        if (pedido == nil) {
            pedido = [IATPedido pedidoWithId:@"1" estado:@"enproceso" fecha:nil context:self.context];
        }
        
        
        if (pedido.itemsPedido.count > 0) {
            
            //SolicitudActualTVC *solicitud = [[SolicitudActualTVC alloc] init];
            SolicitudActualTVC *solicitud = [self.storyboard instantiateViewControllerWithIdentifier:@"solicitudActualTVC"];
            solicitud.pedido = pedido;
          //  [self.navigationController pushViewController:solicitud animated:YES];
            [self.navigationController presentViewController:solicitud animated:YES completion:^{
                NSLog(@"Volviendo a la vista del login");
            }];
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pedido vacio"
                                                                           message:@"No existen productos agregados al pedido, debe agregar por lo menos un producto"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    if (indexPath.row == 11) { // 9
        NSString *textURL = @"https://milenian.freshdesk.com/support/tickets/new";
        NSURL *cleanURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", textURL]];
        [[UIApplication sharedApplication] openURL:cleanURL];
    }
    else if (indexPath.row == 14) { // 12
        [self removeUserPrefs];
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [self.navigationController presentViewController:login animated:YES completion:^{
            NSLog(@"Volviendo a la vista del login");
        }];
    }
}


-(void)removeUserPrefs {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"kEmail"];
    [defaults removeObjectForKey:@"kPass"];
    [defaults removeObjectForKey:@"kIdCustomer"];
    [defaults removeObjectForKey:@"kTypeUser"];
    [defaults removeObjectForKey:@"kPhone"];
    [defaults removeObjectForKey:@"kCif"];
    [defaults removeObjectForKey:@"kCompany"];
    [defaults removeObjectForKey:@"kAddress"];
    [defaults removeObjectForKey:@"kSector"];
    [defaults removeObjectForKey:@"kKey"];
    [defaults removeObjectForKey:@"kLastEmail"];
    [defaults removeObjectForKey:@"kLastPass"];
    [defaults synchronize];
    
}



@end

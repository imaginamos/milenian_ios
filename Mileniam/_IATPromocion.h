// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPromocion.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@class IATCategoria;
@class IATItemPedido;
@class IATPromoProveedor;

@interface IATPromocionID : IATEntidadBaseID {}
@end

@interface _IATPromocion : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATPromocionID*objectID;

@property (nonatomic, strong, nullable) NSDate* fechaFinal;

@property (nonatomic, strong, nullable) NSDate* fechaInicio;

@property (nonatomic, strong, nullable) NSString* idProducto;

@property (nonatomic, strong, nullable) NSString* unidad;

@property (nonatomic, strong, nullable) NSString* urlImagen;

@property (nonatomic, strong, nullable) IATCategoria *categoria;

@property (nonatomic, strong, nullable) NSSet<IATItemPedido*> *itemPedido;
- (nullable NSMutableSet<IATItemPedido*>*)itemPedidoSet;

@property (nonatomic, strong, nullable) NSSet<IATPromoProveedor*> *promociones;
- (nullable NSMutableSet<IATPromoProveedor*>*)promocionesSet;

@end

@interface _IATPromocion (ItemPedidoCoreDataGeneratedAccessors)
- (void)addItemPedido:(NSSet<IATItemPedido*>*)value_;
- (void)removeItemPedido:(NSSet<IATItemPedido*>*)value_;
- (void)addItemPedidoObject:(IATItemPedido*)value_;
- (void)removeItemPedidoObject:(IATItemPedido*)value_;

@end

@interface _IATPromocion (PromocionesCoreDataGeneratedAccessors)
- (void)addPromociones:(NSSet<IATPromoProveedor*>*)value_;
- (void)removePromociones:(NSSet<IATPromoProveedor*>*)value_;
- (void)addPromocionesObject:(IATPromoProveedor*)value_;
- (void)removePromocionesObject:(IATPromoProveedor*)value_;

@end

@interface _IATPromocion (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveFechaFinal;
- (void)setPrimitiveFechaFinal:(NSDate*)value;

- (NSDate*)primitiveFechaInicio;
- (void)setPrimitiveFechaInicio:(NSDate*)value;

- (NSString*)primitiveIdProducto;
- (void)setPrimitiveIdProducto:(NSString*)value;

- (NSString*)primitiveUnidad;
- (void)setPrimitiveUnidad:(NSString*)value;

- (NSString*)primitiveUrlImagen;
- (void)setPrimitiveUrlImagen:(NSString*)value;

- (IATCategoria*)primitiveCategoria;
- (void)setPrimitiveCategoria:(IATCategoria*)value;

- (NSMutableSet<IATItemPedido*>*)primitiveItemPedido;
- (void)setPrimitiveItemPedido:(NSMutableSet<IATItemPedido*>*)value;

- (NSMutableSet<IATPromoProveedor*>*)primitivePromociones;
- (void)setPrimitivePromociones:(NSMutableSet<IATPromoProveedor*>*)value;

@end

@interface IATPromocionAttributes: NSObject 
+ (NSString *)fechaFinal;
+ (NSString *)fechaInicio;
+ (NSString *)idProducto;
+ (NSString *)unidad;
+ (NSString *)urlImagen;
@end

@interface IATPromocionRelationships: NSObject
+ (NSString *)categoria;
+ (NSString *)itemPedido;
+ (NSString *)promociones;
@end

NS_ASSUME_NONNULL_END

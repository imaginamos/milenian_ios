#import "IATAcuerdo.h"

@interface IATAcuerdo ()

// Private interface goes here.

@end

@implementation IATAcuerdo

+(instancetype) acuerdoWithId:(NSString *)idAcuerdo
                  comentarios:(NSString *)comentarios
                  fechaInicio:(NSDate *)fechaInicio
                   fechaFinal:(NSDate *)fechaFinal
                      context:(NSManagedObjectContext *)context{
    
    IATAcuerdo *nAcuerdo = [NSEntityDescription insertNewObjectForEntityForName:[IATAcuerdo entityName]
                                                         inManagedObjectContext:context];
    
    nAcuerdo.idAcuerdo = idAcuerdo;
    nAcuerdo.comentarios = comentarios;
    nAcuerdo.fechaInicio = fechaInicio;
    nAcuerdo.fechaFinal = fechaFinal;
    nAcuerdo.estado = @"2";
    
    return nAcuerdo;
    
    
}

+(instancetype) obtenerAcuerdoWithId:(NSString *)idAcuerdo
                             context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATAcuerdo entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idAcuerdo == %@",idAcuerdo];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

+(NSArray *) obtenerAcuerdoWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATAcuerdo entityName]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}

+(NSArray *) obtenerAcuerdosWithEstado:(NSString *)estado
                               context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATAcuerdo entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"estado == %@",estado];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}

@end

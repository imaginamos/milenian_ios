#import "IATProveedor.h"

@interface IATProveedor ()

// Private interface goes here.

@end

@implementation IATProveedor

+(instancetype) proveedorWithId:(NSString *)idProveedor
                         nombre:(NSString *)nombre
                    descripcion:(NSString *)descripcion
                      direccion:(NSString *)direccion
                       telefono:(NSString *)telefono
                        urlLogo:(NSString *)urlLogo
                        context:(NSManagedObjectContext *)context{
    
    IATProveedor *nProveedor = [NSEntityDescription insertNewObjectForEntityForName:[IATProveedor entityName]
                                                           inManagedObjectContext:context];
    
    nProveedor.idEntity = idProveedor;
    nProveedor.nombre = nombre;
    nProveedor.descripcion = descripcion;
    nProveedor.direccion = direccion;
    nProveedor.telefono = telefono;
    nProveedor.urlLogo = urlLogo;
    
    return nProveedor;
    
}

+(NSArray *) obtenerProveedoresWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProveedor entityName]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
    
}

+(instancetype) obtenerProveedorWithId:(NSString *)idProveedor
                            context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProveedor entityName]];
    
    NSPredicate *filtrarPorId = [NSPredicate predicateWithFormat:@"idEntity == %@",idProveedor];
    [fetch setPredicate:filtrarPorId];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

@end

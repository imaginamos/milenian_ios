//
//  ProductoVC.m
//  Milenian
//
//  Created by Carlos Obregón on 17/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "ProductoVC.h"
#import "SolicitudActualTVC.h"
#import "IATItemPedido.h"
#import "IATPedido.h"
#import "IATProducto.h"
#import "IATPromocion.h"
#import "IATPromoProveedor.h"
#import "IATProveedor.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATProductoProveedor.h"
#import "IATProveedorProductoCellView.h"
#import "IATCantidadProducto.h"
#import "CacheImgs.h"


@interface ProductoVC ()
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) UIBarButtonItem *barBtnFavorito;
@property (nonatomic, strong) NSMutableArray *itemsPedido;
@property (nonatomic, strong) NSArray *promociones;
@property (nonatomic, strong) NSMutableArray *promosYproductos;
@property (nonatomic, strong) NSArray *subitems, *proveedores;
@property (nonatomic, strong) IATItemPedido *itemActual;
@property (nonatomic, strong) IATProductoProveedor *productoProveedor;
@property (nonatomic, strong) IATPromoProveedor *promoProveedor;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) UIView *celdaCarousel;
@property (nonatomic, strong) NSMutableArray *imgViews;
@property (nonatomic) NSInteger indexActual;
@property NSCache *cache;
@end

@implementation ProductoVC

#pragma mark - Ciclo de vida
-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.imgViews = [NSMutableArray array];
    //self.cache = [[NSCache alloc] init];
    self.cache = [CacheImgs sharedInstance];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    NSString* fileSoundRemove = [[NSBundle mainBundle] pathForResource:@"Restar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundRemove = [[NSURL alloc]initFileURLWithPath:fileSoundRemove];
    self.soundRemove = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundRemove error:nil];
    
    self.soundRemove.volume = 0.4;
    self.soundRemove.numberOfLoops = 1;
    self.soundRemove.pan = 0;
    self.soundRemove.enableRate = YES;
    self.soundRemove.rate = 1;
    
    
    
    NSString* fileSoundDelete = [[NSBundle mainBundle] pathForResource:@"EliminarCarrito" ofType:@"m4a"];
    NSURL* rutaFileSoundDelete = [[NSURL alloc]initFileURLWithPath:fileSoundDelete];
    self.soundDelete = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundDelete error:nil];
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATProveedorProductoCellView" bundle:nil];
    [self.listProvedores registerNib:cellNib
              forCellReuseIdentifier:[IATProveedorProductoCellView cellId]];
    
    _carousel.delegate = self;
    _carousel.dataSource = self;
    _carousel.type = iCarouselTypeLinear;
    
    self.listProvedores.delegate = self;
    self.listProvedores.dataSource = self;
    
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.carousel.hidden = NO;
    
    //self.itemsPedido = [self.pedido.itemsPedido allObjects];
    NSArray *itemsPedido = [self.pedido.itemsPedido allObjects];
    self.itemsPedido = [NSMutableArray arrayWithArray:itemsPedido];
    
    self.itemActual = [self.itemsPedido objectAtIndex:0];
    self.indexActual = 0;
    
    if (self.itemActual.producto != nil) {
        IATProducto *producto = self.itemActual.producto;
        self.lblnameProduct.text = producto.nombre;
        self.lblnameUnidades.text = producto.unidad;
        self.lblnameMarca.text = producto.marca;
        
        self.lblPrecio.text = [NSString stringWithFormat:@"%@",[self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
        self.lblCantidad.text = [NSString stringWithFormat:@"%@",self.itemActual.cantidad];
        

        self.proveedores = [producto.precioProvedor allObjects];
        self.proveedores = [IATProductoProveedor obtenerPreciosOrdenadoPorMenor:producto context:self.context];
        self.productoProveedor = [self.proveedores objectAtIndex:0];
        
        
        UIImage *btnFavorito;
        
        if (![self.itemActual.producto.esFavorito isEqual:@NO]) {
            btnFavorito = [UIImage imageNamed:@"estrella-amarilla"];
        }
        else{
            btnFavorito = [UIImage imageNamed:@"estrella-gris"];
        }
        
        @try{
            btnFavorito = [btnFavorito imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        @catch (NSException *exception){
        }
        
        self.barBtnFavorito = [[UIBarButtonItem alloc] initWithImage:btnFavorito
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(acept)];
        
        self.barBtnFavorito.enabled = NO;
        
        //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
        self.barBtnFavorito.imageInsets = UIEdgeInsetsMake(12, 12, 12, 10);
        
        self.navigationItem.rightBarButtonItem = self.barBtnFavorito;
        
    }
    else{
        IATPromocion *promocion = self.itemActual.promocion;
        self.lblnameProduct.text = promocion.nombre;
        self.lblnameUnidades.text = promocion.unidad;
        //self.lblnameMarca.text = promocion.marca;
        
        self.lblPrecio.text = [NSString stringWithFormat:@"%@",[self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
        self.lblCantidad.text = [NSString stringWithFormat:@"%@",self.itemActual.cantidad];
        
        //self.proveedores = [promocion.promociones allObjects];
        self.proveedores = [IATPromoProveedor obtenerPreciosOrdenadoPorMenor:promocion context:self.context];
        self.productoProveedor = [self.proveedores objectAtIndex:0];
    }
    
    
    [self.carousel reloadData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.carousel.hidden = YES;
}
-(void)onTap:(id)sender{
    NSLog(@"Tocando foto de Eliminar producto");
}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Ir a pedido
-(void)acept{
    
//    UIImage *btnFavorito;
//    if ([self.itemActual.producto.esFavorito isEqual:@NO]) {
//        btnFavorito = [UIImage imageNamed:@"icono-estrella-favorito"];
//        self.itemActual.producto.esFavorito = @YES;
//    }
//    else{
//        btnFavorito = [UIImage imageNamed:@"estrella-gris"];
//        self.itemActual.producto.esFavorito = @NO;
//    }
//    
//    @try{
//        btnFavorito = [btnFavorito imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    }
//    @catch (NSException *exception){
//    }
//    
//    [self.barBtnFavorito setImage:btnFavorito];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.proveedores.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.itemActual.producto != nil) {
        IATProductoProveedor *precioProveedor = [self.proveedores objectAtIndex:indexPath.row];
        IATProveedorProductoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedorProductoCellView cellId]];
        
        cell.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[precioProveedor.precio doubleValue]]];
        cell.lblNombre.text = [NSString stringWithFormat:@"%@", precioProveedor.provedor.nombre];
        
        if (precioProveedor.provedor.urlLogo != nil) {
            
            UIImage *imgLogo = [self.cache objectForKey:[NSURL URLWithString:precioProveedor.provedor.urlLogo]];
            if (imgLogo) {
                cell.logoProveedor.image = imgLogo;
            }
            else{
                UIActivityIndicatorView *actView = [[UIActivityIndicatorView alloc]
                                                    initWithFrame:CGRectMake(cell.logoProveedor.bounds.size.width/2 - 25, cell.logoProveedor.bounds.size.height/2 - 25, 50.0f, 50.0f)];
                actView.activityIndicatorViewStyle =UIActivityIndicatorViewStyleGray;
                [actView startAnimating];
                
                [cell.logoProveedor addSubview:actView];
                
                
                [self imageWithUrl:[NSURL URLWithString:precioProveedor.provedor.urlLogo] block:^(UIImage *image) {
                    if (image != nil) {
                        cell.logoProveedor.image = image;
                    }
                    [actView stopAnimating];
                }];
            }
            
            
        }
        
        return cell;
    }
    else{
        IATPromoProveedor *precioProveedor = [self.proveedores objectAtIndex:indexPath.row];
        IATProveedorProductoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedorProductoCellView cellId]];
        
        cell.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[precioProveedor.precio doubleValue]]];
        cell.lblNombre.text = [NSString stringWithFormat:@"%@", precioProveedor.proveedor.nombre];
        
        if (precioProveedor.proveedor.urlLogo != nil) {
            
            
            
            UIImage *imgLogo = [self.cache objectForKey:[NSURL URLWithString:precioProveedor.proveedor.urlLogo]];
            if (imgLogo) {
                 cell.logoProveedor.image = imgLogo;
            }
            else{
                UIActivityIndicatorView *actView = [[UIActivityIndicatorView alloc]
                                                    initWithFrame:CGRectMake(cell.logoProveedor.bounds.size.width/2 - 25, cell.logoProveedor.bounds.size.height/2 - 25, 50.0f, 50.0f)];
                actView.activityIndicatorViewStyle =UIActivityIndicatorViewStyleGray;
                [cell.logoProveedor addSubview:actView];
                [actView startAnimating];
                [self imageWithUrl:[NSURL URLWithString:precioProveedor.proveedor.urlLogo] block:^(UIImage *image) {
                    if (image != nil) {
                        cell.logoProveedor.image = image;
                    }
                    [actView stopAnimating];
                }];
            }
            
            
        }
        
        return cell;
    }
    
    
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Cual proveedor y precio escogio
    if (self.itemActual.producto != nil) {
        self.productoProveedor = [self.proveedores objectAtIndex:indexPath.row];
        
        //Actualizar el precio y proveedor del item
        NSDecimalNumber *cantidad = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@", self.itemActual.cantidad]];
        self.itemActual.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cantidad];
        self.itemActual.proveedor = self.productoProveedor.provedor;
        
        //Mostrar el nuevo precio en vista
        //self.lblPrecio.text = [NSString stringWithFormat:@"€ %@", self.itemActual.precioTotal];
        self.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
    }
    
    else{
        self.promoProveedor = [self.proveedores objectAtIndex:indexPath.row];
        
        //Actualizar el precio y proveedor del item
        NSDecimalNumber *cantidad = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@", self.itemActual.cantidad]];
        self.itemActual.precioTotal = [self.promoProveedor.precio decimalNumberByMultiplyingBy:cantidad];
        self.itemActual.proveedor = self.promoProveedor.proveedor;
        
        //Mostrar el nuevo precio en vista
        //self.lblPrecio.text = [NSString stringWithFormat:@"€ %@", self.itemActual.precioTotal];
        self.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
    }
    

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATProveedorProductoCellView cellHeight];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


#pragma mark - iCarousel methods
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.itemsPedido.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{

    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 220.0f, 220.0f)];
    
    
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:view.bounds];
    //self.imgProducto = [[UIImageView alloc] initWithFrame:view.bounds];
    
    UIActivityIndicatorView *actView =
    [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imgV.bounds.size.width/2 - 25, imgV.bounds.size.height/2 - 25, 50.0f, 50.0f)];
    
    actView.activityIndicatorViewStyle =UIActivityIndicatorViewStyleGray;
    [actView startAnimating];
    
    imgV.image = [UIImage imageNamed:@"page"];
    [imgV setContentMode:UIViewContentModeScaleAspectFill];
    
    [imgV addSubview:actView];

    NSURL *url;
    if (self.itemActual.pedido != nil) {
        IATItemPedido *item = [self.itemsPedido objectAtIndex:index];
        IATProducto *producto = item.producto;
        NSString *urlProducto = producto.urlImagen;
        url = [NSURL URLWithString:urlProducto];
    }
    else{
        IATItemPedido *item = [self.itemsPedido objectAtIndex:index];
        IATPromocion *promocion = item.promocion;
        NSString *urlPromo = promocion.urlImagen;
        url = [NSURL URLWithString:urlPromo];
    }

    UIImage *imgProducto = [self.cache objectForKey:url];
    if (imgProducto) {
        imgV.image = imgProducto;
        [imgV setContentMode:UIViewContentModeScaleAspectFit];
        [actView stopAnimating];
    }
    else{
        [self imageWithUrl:url block:^(UIImage *image) {
            if (image != nil) {
                imgV.image = image;
                imgV.contentMode = UIViewContentModeScaleAspectFit;
            }
            [actView stopAnimating];
        }];
    }
    
    
    [view addSubview:imgV];
    
    if (index == 0){
        self.celdaCarousel = view;
    }
    
    return view;
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.1;
    }
    if (option == iCarouselOptionWrap)
    {
        return NO;
    }
    return value;
}
- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel{
    
    NSInteger index = [self.carousel currentItemIndex];
    self.indexActual = index;
    
    NSLog(@"%ld", (long)self.indexActual);
    self.celdaCarousel = [carousel itemViewAtIndex:self.indexActual];
    
    //Online
    self.itemActual = [self.itemsPedido objectAtIndex:index];
    if (self.itemActual.producto != nil) {
        IATProducto *producto = self.itemActual.producto;
        self.lblnameProduct.text = producto.nombre;
        self.lblnameUnidades.text = producto.unidad;
        self.lblnameMarca.text = producto.marca;
        
        //self.proveedores = [producto.precioProvedor allObjects];
        self.proveedores = [IATProductoProveedor obtenerPreciosOrdenadoPorMenor:producto context:self.context];
        self.productoProveedor = [self.proveedores objectAtIndex:0];
        
        UIImage *btnFavorito;
        
        if (![self.itemActual.producto.esFavorito isEqual:@NO]) {
            btnFavorito = [UIImage imageNamed:@"estrella-amarilla"];
        }
        else{
            btnFavorito = [UIImage imageNamed:@"estrella-gris"];
        }
        
        @try{
            btnFavorito = [btnFavorito imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        @catch (NSException *exception){
        }
        
        self.barBtnFavorito = [[UIBarButtonItem alloc] initWithImage:btnFavorito
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(acept)];
        
        //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
        self.barBtnFavorito.imageInsets = UIEdgeInsetsMake(12, 12, 12, 10);
        
        self.navigationItem.rightBarButtonItem = self.barBtnFavorito;
    }
    else{
        IATPromocion *promocion = self.itemActual.promocion;
        self.lblnameProduct.text = promocion.nombre;
        self.lblnameUnidades.text = promocion.unidad;
        //self.lblnameMarca.text = producto.marca;
        
        //self.proveedores = [promocion.promociones allObjects];
        self.proveedores =
        [IATPromoProveedor obtenerPreciosOrdenadoPorMenor:promocion context:self.context];
        self.productoProveedor = [self.proveedores objectAtIndex:0];
        
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    self.lblPrecio.text = [NSString stringWithFormat:@"%@",[self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
    self.lblCantidad.text = [NSString stringWithFormat:@"%@",self.itemActual.cantidad];
    
    [self.listProvedores reloadData];
    
}


#pragma mark - Actions methods
- (IBAction)ConsolidarPedido:(id)sender {
    
    //Asignar provedores a todos los Items del Pedido
    NSDecimalNumber *precioTotalPedido = [[NSDecimalNumber alloc] initWithString:@"0"];
    for (int i=0; i < self.itemsPedido.count; i++) {
        
        IATItemPedido *item = [self.itemsPedido objectAtIndex:i];
        
        
        if(item.proveedor == nil){
            
            if (item.producto != nil) {
                IATProducto *producto = item.producto;
                IATProductoProveedor *prodProvee = [[IATProductoProveedor obtenerPreciosOrdenadoPorMenor:producto context:self.context] objectAtIndex:0];
                item.proveedor = prodProvee.provedor;
                NSDecimalNumber *cantidad = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@", item.cantidad]];
                item.precioTotal = [cantidad decimalNumberByMultiplyingBy:prodProvee.precio];
            }
            else{
                IATPromocion *promocion = item.promocion;
                IATPromoProveedor *promoProveedor = [[IATPromoProveedor obtenerPreciosOrdenadoPorMenor:promocion context:self.context] objectAtIndex:0];
                item.proveedor = promoProveedor.proveedor;
                NSDecimalNumber *cantidad = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@", item.cantidad]];
                item.precioTotal = [cantidad decimalNumberByMultiplyingBy:promoProveedor.precio];
            }
            
            
        }
        
        precioTotalPedido = [precioTotalPedido decimalNumberByAdding:item.precioTotal];
    }
    
    
    //Completar pedido con datos faltantes
    self.pedido.fecha = [NSDate date];
    self.pedido.precioTotal = precioTotalPedido;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *idUsuario = [defaults stringForKey:@"kIdCustomer"];
    
    self.pedido.idCliente = idUsuario;
    
    //Enviar a la siguiente vista
    SolicitudActualTVC *listPedido = [self.storyboard instantiateViewControllerWithIdentifier:@"solicitudActualTVC"];
    listPedido.pedido = self.pedido;
    
    
    [self.navigationController pushViewController:listPedido animated:YES];
}

- (IBAction)eliminarProductoDelCarrito:(id)sender {
    
    if (self.carousel.numberOfItems > 0)
    {
        [self.soundDelete play];
        
        NSInteger index = self.carousel.currentItemIndex;
        
        IATItemPedido *item = [self.itemsPedido objectAtIndex:index];
        item.producto.cantidadPedido.cantidad = 0;
        
        [self.itemsPedido removeObjectAtIndex:(NSUInteger)index];
        
        if (self.itemsPedido.count == 0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            [self.carousel removeItemAtIndex:index animated:YES];
            [self carouselCurrentItemIndexDidChange:self.carousel];
        }
        [self.context deleteObject:item];
        
    }
    
   
    
}
- (IBAction)agregarUnProducto:(id)sender {
    
    [self.soundAdd play];
    //Actualizar modelo
    int cantidad = [self.itemActual.cantidad intValue];
    cantidad++;
    self.itemActual.cantidad = [NSNumber numberWithInt:cantidad];
    
    NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d", cantidad]];
    
    self.itemActual.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cant];
    
    //Actualizar vista
    self.lblCantidad.text = [NSString stringWithFormat:@"%d", cantidad];
    self.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
    
    self.itemActual.producto.cantidadPedido.cantidad = cant;
    
    [self animationProducto];
}

- (IBAction)quitarUnProducto:(id)sender {
    
    //Actualizar modelo
    int cantidad = [self.itemActual.cantidad intValue];
    if (cantidad > 1) {
        [self.soundRemove play];
        cantidad--;
        self.itemActual.cantidad = [NSNumber numberWithInt:cantidad];
        
        NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d", cantidad]];
        self.itemActual.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cant];
        self.itemActual.producto.cantidadPedido.cantidad = cant;

    }
    
    //Actualizar vista
    self.lblCantidad.text = [NSString stringWithFormat:@"%d", cantidad];
    self.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.itemActual.precioTotal doubleValue]]];
    
    [self animationProducto];
    
}
-(void)animationProducto{
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.celdaCarousel.transform = CGAffineTransformScale(self.celdaCarousel.transform, 1.2, 1.2);
                         
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.celdaCarousel.transform = CGAffineTransformScale(self.celdaCarousel.transform, 0.8335, 0.8335);
                                              
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}
- (void)borrarItem:(id)sender{
    
    //self.context dele
    [self.context deleteObject:self.itemActual];

    BOOL rta = YES;
    if([self.itemActual isDeleted]) {
        
        NSError *errorInfo = nil;
        
        if([self.context save:&errorInfo]){
            
            NSLog(@"Actualizado el contexto");
            rta = YES;
            //TODO: mirar
            //[self.itemsPedido delete:self.itemActual];
            [self.itemsPedido removeObject:self.itemActual];
            
        }
    }else{
        rta = NO;
    }
    
}


#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}


#pragma mark - Manejo de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

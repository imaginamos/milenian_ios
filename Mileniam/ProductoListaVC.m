//
//  ProductoListaVC.m
//  Milenian
//
//  Created by Carlos Obregón on 31/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "ProductoListaVC.h"
#import "IATProducto.h"
#import "IATProveedor.h"
#import "IATLista.h"
#import "IATItemLista.h"
#import "IATProductoProveedor.h"
#import "IATCantidadProducto.h"
#import "IATProveedorProductoCellView.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "CacheImgs.h"

@interface ProductoListaVC ()
@property (strong, nonatomic) NSArray *proveedores;
@property (strong, nonatomic) IATCantidadProducto *preItem;
@property (strong, nonatomic) IATProductoProveedor *productoProveedor;
@property (strong, nonatomic) IATProveedor *proveedorSeleccionado;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSDecimalNumber *precioTotal;
@property (nonatomic) NSUInteger cantidad;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation ProductoListaVC

#pragma mark - Ciclo de Vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    self.cache = [CacheImgs sharedInstance];
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.tfCantidad.delegate = self;
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    NSString* fileSoundRemove = [[NSBundle mainBundle] pathForResource:@"Restar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundRemove = [[NSURL alloc]initFileURLWithPath:fileSoundRemove];
    self.soundRemove = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundRemove error:nil];
    
    self.soundRemove.volume = 0.4;
    self.soundRemove.numberOfLoops = 1;
    self.soundRemove.pan = 0;
    self.soundRemove.enableRate = YES;
    self.soundRemove.rate = 1;
    
    //Creo un pre-Item Pedido
    if(self.producto.cantidadPedido.cantidad == nil){
        self.preItem = [IATCantidadProducto productoWithCantidad:[NSNumber numberWithInt:0]
                                                        producto:self.producto
                                                        variedad:self.producto.variedad
                                                         context:self.context];
    }
    else{
        self.preItem = self.producto.cantidadPedido;
    }
    
    
    //Sincronizar vista
    //self.proveedores = [self.producto.precioProvedor allObjects];
    self.proveedores = [IATProductoProveedor obtenerPreciosOrdenadoPorMenor:self.producto context:self.context];
    self.productoProveedor = [self.proveedores objectAtIndex:0];
    
    if (![self.producto.nombre isEqualToString:@""] && self.producto.nombre != nil) {
        self.lblNombre.text = self.producto.nombre;
    }
    
    if (![self.producto.unidad isEqualToString:@""] && self.producto.unidad != nil) {
        self.lblUnidad.text = self.producto.unidad;
    }
    
    if (![self.producto.marca isEqualToString:@""] && self.producto.marca != nil) {
        self.lblMarca.text = self.producto.marca;
    }
    
    
    UIImage *imgP = [self.cache objectForKey:[NSURL URLWithString:self.producto.urlImagen]];
    if (imgP) {
        self.imgProducto.image = imgP;
    }
    else{
        [self.activityLoad startAnimating];
        
        [self imageWithUrl:[NSURL URLWithString:self.producto.urlImagen] block:^(UIImage *image) {
            if (image != nil) {
                self.imgProducto.image = image;
            }
            [self.activityLoad stopAnimating];
        }];
    }
       
    self.cantidad = 1;
    self.tfCantidad.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.cantidad];
    
    
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.productoProveedor.precio doubleValue]]];
    self.precioTotal = self.productoProveedor.precio;
    
    
    self.listProveedores.delegate = self;
    self.listProveedores.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATProveedorProductoCellView" bundle:nil];
    [self.listProveedores registerNib:cellNib forCellReuseIdentifier:[IATProveedorProductoCellView cellId]];
    
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.proveedores.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATProductoProveedor *precioProveedor = [self.proveedores objectAtIndex:indexPath.row];
    IATProveedorProductoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedorProductoCellView cellId]];
    
    cell.lblPrecio.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[precioProveedor.precio doubleValue]]];
    cell.lblNombre.text = [NSString stringWithFormat:@"%@", precioProveedor.provedor.nombre];
    
    if (precioProveedor.provedor.urlLogo != nil) {
       
        UIImage *imgProducto = [self.cache objectForKey:[NSURL URLWithString:precioProveedor.provedor.urlLogo]];
        if (imgProducto) {
            cell.logoProveedor.image = imgProducto;
        }
        else{
            [cell.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:precioProveedor.provedor.urlLogo] block:^(UIImage *image) {
                if (image != nil) {
                    cell.logoProveedor.image = image;
                }
                [cell.activityLoad stopAnimating];
            }];
        }
    }
    
    return cell;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Cual proveedor y precio escogio
    self.productoProveedor = [self.proveedores objectAtIndex:indexPath.row];
    
    //Actualizar el precio y proveedor del item
    NSDecimalNumber *cantidad =
    [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
    self.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cantidad];
    self.proveedorSeleccionado = self.productoProveedor.provedor;
    
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
    
    self.preItem.cantidad = [NSNumber numberWithUnsignedInteger:self.cantidad];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


#pragma mark - Targets/Actions
- (IBAction)agregarALista:(id)sender {
    //Ingresar item a la lista
    if( self.cantidad > 0){
        
        NSDecimalNumber *precioItemPedido = self.precioTotal;
        
        IATItemLista *itemLista =
        [IATItemLista obtenerItemListaWithProducto:self.producto lista:self.lista context:self.context];
        if (itemLista == nil) {
            itemLista = [IATItemLista itemListaWithCantidad:[NSNumber numberWithInteger:self.cantidad] producto:self.producto lista:self.lista precio:self.precioTotal context:self.context];
        }
        else{
            itemLista.cantidad = [NSNumber numberWithUnsignedInteger:self.cantidad];
            itemLista.precio = precioItemPedido;
            itemLista.lista = self.lista;
        }
        
        itemLista.proveedor = self.productoProveedor.provedor;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Producto Agregado"
                                                                       message:@"Producto agregado a Lista"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion =
        [UIAlertAction actionWithTitle:@"Aceptar"
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
        
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Debe agregar una unidad a la lista"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion =
        [UIAlertAction actionWithTitle:@"Aceptar"
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
        
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }

}

-(void)animationProducto{
    // now return the view to normal dimension, animating this tranformation
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 1.2, 1.2);
                         
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.imgProducto.transform = CGAffineTransformScale(self.imgProducto.transform, 0.8335, 0.8335);
                                              
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}

- (IBAction)restarUno:(id)sender {
    //Actualizar modelo
    if (self.cantidad > 0) {
        [self.soundRemove play];
        self.cantidad--;
        
        NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
        
        self.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cant];
        
        //Actualizar vista
        self.tfCantidad.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad];
        self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
        [self animationProducto];
    }
    
}
- (IBAction)sumarUno:(id)sender {
    
    [self.soundAdd play];
    //Actualizar modelo
    self.cantidad++;
    
    NSDecimalNumber *cant = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad]];
    
    self.precioTotal = [self.productoProveedor.precio decimalNumberByMultiplyingBy:cant];
    
    //Actualizar vista
    self.tfCantidad.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.cantidad];
    self.lblPrecioTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.precioTotal doubleValue]]];
    
    [self animationProducto];
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
    });
}

#pragma mark - Utilidades
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

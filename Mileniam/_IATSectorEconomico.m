// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATSectorEconomico.m instead.

#import "_IATSectorEconomico.h"

@implementation IATSectorEconomicoID
@end

@implementation _IATSectorEconomico

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SectorEconomico" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SectorEconomico";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SectorEconomico" inManagedObjectContext:moc_];
}

- (IATSectorEconomicoID*)objectID {
	return (IATSectorEconomicoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic idSector;

@dynamic nombre;

@end

@implementation IATSectorEconomicoAttributes 
+ (NSString *)idSector {
	return @"idSector";
}
+ (NSString *)nombre {
	return @"nombre";
}
@end


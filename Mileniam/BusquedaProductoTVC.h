//
//  BusquedaProductoTVC.h
//  Milenian
//
//  Created by Carlos Obregón on 13/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BusquedaProductoTVC : UITableViewController<UISearchResultsUpdating>



@property (strong, nonatomic) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;



@end

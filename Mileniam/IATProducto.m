#import "IATProducto.h"
@class IATSubcategoria;

@interface IATProducto ()

// Private interface goes here.

@end

@implementation IATProducto

+(instancetype) productoWithId:(NSString *)idProducto
                        nombre:(NSString *)nombre
                   descripcion:(NSString *)descripcion
                        unidad:(NSString *)unidad
                         marca:(NSString *)marca
                      urlImage:(NSString *)urlImage
                      variedad:(IATVariedad *)variedad
                 esRecomendado:(NSNumber *)esRecomendado
                       context:(NSManagedObjectContext *)context{
    
    
    
    IATProducto *nProducto = [NSEntityDescription insertNewObjectForEntityForName:[IATProducto entityName]
                                                       inManagedObjectContext:context];
    
    nProducto.idEntity = idProducto;
    nProducto.nombre = nombre;
    nProducto.descripcion = descripcion;
    nProducto.marca = marca;
    nProducto.urlImagen = urlImage;
    nProducto.esRecomendado = esRecomendado;
    nProducto.unidad = unidad;
    nProducto.variedad = variedad;
    
    
    return nProducto;
}

+(instancetype) obtenerProductoWithId:(NSString *)idProducto
                              context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProducto entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idEntity == %@", idProducto];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
    
}

+(NSArray *) obtenerProductosRecomendadosWithContext:(NSManagedObjectContext *)context{
    
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProducto entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"esRecomendado == YES"];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
    
}

+(NSArray *) obtenerProductosWithContext:(NSManagedObjectContext *)context{
    
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProducto entityName]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
    
}

+(NSArray *) obtenerProductoWithPrename:(NSString *)prename
                                context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATProducto entityName]];
    
    //NSString *nombre = [NSString stringWithFormat:@"%@*", prename];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nombre LIKE[cd] %@", nombre];
//    [fetch setPredicate:predicate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nombre CONTAINS[cd] %@", prename];
    [fetch setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
    
}

@end

#import "_IATPromoProveedor.h"

@interface IATPromoProveedor : _IATPromoProveedor

+(instancetype) promoProveedorWithPrecio:(NSDecimalNumber *)precio
                               promocion:(IATPromocion *)promocion
                               proveedor:(IATProveedor *)proveedor
                                 context:(NSManagedObjectContext *)context;

+(instancetype) obtenerPromoProvedorWithPromo:(IATPromocion *)promocion
                                    proveedor:(IATProveedor *)proveedor
                                      context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerPreciosOrdenadoPorMenor:(IATPromocion *)promocion
                                    context:(NSManagedObjectContext *)context;

@end

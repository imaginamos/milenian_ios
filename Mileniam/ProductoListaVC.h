//
//  ProductoListaVC.h
//  Milenian
//
//  Created by Carlos Obregón on 31/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@import AVFoundation;
@class IATProducto;
@class IATLista;

@interface ProductoListaVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IATLista *lista;
@property (strong, nonatomic) IATProducto *producto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;

@property (strong, nonatomic) AVAudioPlayer* soundAdd;
@property (strong, nonatomic) AVAudioPlayer* soundRemove;

@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblUnidad;
@property (weak, nonatomic) IBOutlet UILabel *lblMarca;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecioTotal;
@property (weak, nonatomic) IBOutlet UITextField *tfCantidad;

@property (weak, nonatomic) IBOutlet UITableView *listProveedores;

- (IBAction)agregarALista:(id)sender;

- (IBAction)restarUno:(id)sender;
- (IBAction)sumarUno:(id)sender;

@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATSubcategoria.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@class IATCategoria;
@class IATProductoProveedor;
@class IATVariedad;

@interface IATSubcategoriaID : IATEntidadBaseID {}
@end

@interface _IATSubcategoria : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATSubcategoriaID*objectID;

@property (nonatomic, strong, nullable) NSString* urlImagen;

@property (nonatomic, strong) IATCategoria *categoria;

@property (nonatomic, strong, nullable) NSSet<IATProductoProveedor*> *productosProvedores;
- (nullable NSMutableSet<IATProductoProveedor*>*)productosProvedoresSet;

@property (nonatomic, strong, nullable) NSSet<IATVariedad*> *variedades;
- (nullable NSMutableSet<IATVariedad*>*)variedadesSet;

@end

@interface _IATSubcategoria (ProductosProvedoresCoreDataGeneratedAccessors)
- (void)addProductosProvedores:(NSSet<IATProductoProveedor*>*)value_;
- (void)removeProductosProvedores:(NSSet<IATProductoProveedor*>*)value_;
- (void)addProductosProvedoresObject:(IATProductoProveedor*)value_;
- (void)removeProductosProvedoresObject:(IATProductoProveedor*)value_;

@end

@interface _IATSubcategoria (VariedadesCoreDataGeneratedAccessors)
- (void)addVariedades:(NSSet<IATVariedad*>*)value_;
- (void)removeVariedades:(NSSet<IATVariedad*>*)value_;
- (void)addVariedadesObject:(IATVariedad*)value_;
- (void)removeVariedadesObject:(IATVariedad*)value_;

@end

@interface _IATSubcategoria (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveUrlImagen;
- (void)setPrimitiveUrlImagen:(NSString*)value;

- (IATCategoria*)primitiveCategoria;
- (void)setPrimitiveCategoria:(IATCategoria*)value;

- (NSMutableSet<IATProductoProveedor*>*)primitiveProductosProvedores;
- (void)setPrimitiveProductosProvedores:(NSMutableSet<IATProductoProveedor*>*)value;

- (NSMutableSet<IATVariedad*>*)primitiveVariedades;
- (void)setPrimitiveVariedades:(NSMutableSet<IATVariedad*>*)value;

@end

@interface IATSubcategoriaAttributes: NSObject 
+ (NSString *)urlImagen;
@end

@interface IATSubcategoriaRelationships: NSObject
+ (NSString *)categoria;
+ (NSString *)productosProvedores;
+ (NSString *)variedades;
@end

NS_ASSUME_NONNULL_END

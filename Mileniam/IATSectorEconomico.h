#import "_IATSectorEconomico.h"

@interface IATSectorEconomico : _IATSectorEconomico

+(instancetype) tipologiaWithId:(NSString *)idTipologia
                         nombre:(NSString *)nombre
                        context:(NSManagedObjectContext *)context;

+(instancetype) obtenerTipologiaWithId:(NSString *)idTipologia
                               context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerTipologiaWithContext:(NSManagedObjectContext *)context;

@end

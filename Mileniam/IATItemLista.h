#import "_IATItemLista.h"

@interface IATItemLista : _IATItemLista


+(instancetype) itemListaWithCantidad:(NSNumber *)cantidad
                             producto:(IATProducto *)producto
                                lista:(IATLista *)lista
                               precio:(NSDecimalNumber *)precio
                              context:(NSManagedObjectContext *)context;

+(instancetype) itemListaWithCantidad:(NSNumber *)cantidad
                             producto:(IATProducto *)producto
                                lista:(IATLista *)lista
                            proveedor:(IATProveedor *)proveedor
                              context:(NSManagedObjectContext *)context;

+(instancetype) obtenerItemListaWithProducto:(IATProducto *)producto
                                       lista:(IATLista *)lista
                                     context:(NSManagedObjectContext *)context;

+(BOOL) existeItemListaWithProducto:(IATProducto *)producto
                              lista:(IATLista *)lista
                            context:(NSManagedObjectContext *)context;

+(instancetype) obtenerItemListaWithProducto:(IATProducto *)producto
                                     context:(NSManagedObjectContext *)context;


@end

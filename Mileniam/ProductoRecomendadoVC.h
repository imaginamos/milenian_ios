//
//  ProductoRecomendadoVC.h
//  Milenian
//
//  Created by Carlos Obregón on 4/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@import AVFoundation;
@class IATProducto;

@interface ProductoRecomendadoVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IATProducto *producto;
@property (retain, nonatomic) UITapGestureRecognizer *tap;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

@property (weak, nonatomic) IBOutlet UIImageView *imgProducto;

@property (strong, nonatomic) AVAudioPlayer* soundAdd;
@property (strong, nonatomic) AVAudioPlayer* soundRemove;

@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblUnidad;
@property (weak, nonatomic) IBOutlet UILabel *lblMarca;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecioTotal;
@property (weak, nonatomic) IBOutlet UITextField *tfCantidad;

@property (weak, nonatomic) IBOutlet UITableView *listProveedores;

- (IBAction)agregarAlPedido:(id)sender;
- (IBAction)restarUno:(id)sender;
- (IBAction)sumarUno:(id)sender;

@end

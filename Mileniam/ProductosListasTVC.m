//
//  ProductosListasTVC.m
//  Milenian
//
//  Created by Carlos Obregón on 31/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "ProductosListasTVC.h"
#import "BusquedaProductoTVC.h"
#import "IATProducto.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "ProductoListaVC.h"
#import "IATProveedorProductoCellView.h"
#import "CacheImgs.h"

@interface ProductosListasTVC ()
@property (strong, nonatomic) NSArray *filteredProductos;
@property (strong, nonatomic) NSArray *productos;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation ProductosListasTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Buscar Productos";
    self.cache = [CacheImgs sharedInstance];
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = true;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.hidden = NO;
    self.searchController.searchBar.enablesReturnKeyAutomatically = YES;
    [self.tableView setContentOffset:CGPointMake(0, self.searchController.searchBar.frame.size.height) animated:YES];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    self.productos = [IATProducto obtenerProductosWithContext:self.context];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATProveedorProductoCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATProveedorProductoCellView cellId]];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {
        return self.filteredProductos.count;
    }
    return self.productos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IATProducto *producto;
    
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]){
        producto = [self.filteredProductos objectAtIndex:indexPath.row];
    }
    else{
        producto = [self.productos objectAtIndex:indexPath.row];
    }
    
    
    IATProveedorProductoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATProveedorProductoCellView cellId]];
    
    cell.lblPrecio.text = @"";
    cell.lblDescription.text = producto.marca;
    cell.lblNombre.text = [NSString stringWithFormat:@"%@", producto.nombre];
    
    if (producto.urlImagen != nil) {
        
        
        UIImage *img = [self.cache objectForKey:[NSURL URLWithString:producto.urlImagen]];
        if (img) {
            cell.logoProveedor.image = img;
        }
        else{
            [cell.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:producto.urlImagen] block:^(UIImage *image) {
                if (image != nil) {
                    cell.logoProveedor.image = image;
                }
                [cell.activityLoad stopAnimating];
            }];
        }
    }
   
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ProductoListaVC *productoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"productoListaVC"];
    
    IATProducto *producto;
    
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]){
        producto = [self.filteredProductos objectAtIndex:indexPath.row];
    }
    else{
        producto = [self.productos objectAtIndex:indexPath.row];
    }
    
    productoVC.producto = producto;
    productoVC.lista = self.lista;
    
    [self.navigationController pushViewController:productoVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return [IATProveedorProductoCellView cellHeight];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

#pragma mark - Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
    });
}

-(void)filterContentForSearchText:(NSString *)searchText{
    
    self.filteredProductos = [IATProducto obtenerProductoWithPrename:searchText context:self.context];
    
    [self.tableView reloadData];
    
    
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    if (![self.searchController.searchBar.text isEqualToString:@""] && self.searchController.searchBar.text != nil) {
        [self filterContentForSearchText:self.searchController.searchBar.text ];
    }
    else{
        [self.tableView reloadData];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

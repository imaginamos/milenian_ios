//
//  AcuerdosProductosVC.h
//  Milenian
//
//  Created by Carlos Obregón on 26/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVFoundation;
@class IATVariedad;
@class IATProveedor;

@interface AcuerdosProductosVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) NSArray *productos;
@property (strong, nonatomic) IATProveedor *proveedor;
@property (strong, nonatomic) IATVariedad *variedad;
@property (strong, nonatomic) AVAudioPlayer* soundAdd;

@property (weak, nonatomic) IBOutlet UICollectionView *variedadCV;
@property (weak, nonatomic) IBOutlet UILabel *subTotal;

@end

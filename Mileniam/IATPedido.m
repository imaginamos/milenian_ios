#import "IATPedido.h"

@interface IATPedido ()

// Private interface goes here.

@end

@implementation IATPedido

+(instancetype) pedidoWithId:(NSString *)idPedido
                      estado:(NSString *)estado
                       fecha:(NSDate *)fecha
                     context:(NSManagedObjectContext *)context{
    
    
    IATPedido *nPedido = [NSEntityDescription insertNewObjectForEntityForName:[IATPedido entityName]
                                                               inManagedObjectContext:context];
    
    nPedido.idPedido = idPedido;
    nPedido.estado = estado;
    nPedido.fecha = fecha;
    
    return nPedido;
    
}

+(NSArray *) obtenerPedidosWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPedido entityName]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

+(instancetype) obtenerPedidoWithId:(NSString *)idPedido
                            context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idPedido == %@",idPedido];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

/* ////Estado de pedidos////
 1 Preparacion en Proceso
 2 Aceptado Proveedor
 4 Enviado Cliente
 5 Recibido Cliente
 6 Cancelado
 8 Rechazado Proveedor
 9 Pedido en Reparto
 12 Aceptado /Modificado Proveedor
*/ ////Estado de pedidos////

+(instancetype) obtenerpedidoWithEstado:(NSString *)estado
                                context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPedido entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"estado == %@",estado];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

+(NSArray *) obtenerPedidosEnProcesoWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPedido entityName]];

    NSSortDescriptor *sortDate = [[NSSortDescriptor alloc] initWithKey:@"fecha" ascending:NO];
    NSSortDescriptor *sortId = [[NSSortDescriptor alloc] initWithKey:@"idPedido" ascending:NO];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"estado != %@ AND estado != %@ AND estado != %@", @"6", @"8", @"5"];
    [fetch setPredicate:predicate];
    [fetch setSortDescriptors:@[sortDate]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}

+(NSArray *) obtenerPedidosAprobadosWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATPedido entityName]];
    NSSortDescriptor *sortDate = [[NSSortDescriptor alloc] initWithKey:@"fecha" ascending:NO];
    NSSortDescriptor *sortId = [[NSSortDescriptor alloc] initWithKey:@"idPedido" ascending:NO];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"estado == %@ AND estado = %@ AND estado = %@",@"6", @"8", @"5"];
    [fetch setPredicate:predicate];
    [fetch setSortDescriptors:@[sortDate]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array;
    }
    
    return nil;
}


@end

//
//  ForgetPasswordVC.m
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "ForgetPasswordVC.h"
#import "LoginVC.h"
#import "MBProgressHUD.h"

@interface ForgetPasswordVC ()
@property(nonatomic) int moveView;
@property(nonatomic, strong) MBProgressHUD *hud;
@property(nonatomic) CGRect rectOrig;
@end

@implementation ForgetPasswordVC

#pragma mark - Movimiento de la Vista
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Recuperar contraseña";
    
    //Capturando frame de la vista
    self.rectOrig = self.view.frame;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ocultaTeclado:)];
    [tapGesture setNumberOfTouchesRequired:1];
    [[self view] addGestureRecognizer:tapGesture];
  
    UIImage *imgbtnBack = [UIImage imageNamed:@"icono-atras"];
    @try{
        imgbtnBack = [imgbtnBack imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    @catch (NSException *exception){
    }
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:imgbtnBack
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
}


#pragma mark - Ocultar teclado
-(void)ocultaTeclado:(UITapGestureRecognizer *)sender{
    
    [self.txtRegisterEmail resignFirstResponder];
    
}


#pragma mark - Movimiento de la Vista
-(void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= self.moveView;
        rect.size.height += self.moveView;
    }
    else
    {
        rect.origin.y += self.moveView;
        rect.size.height -= self.moveView;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}
- (IBAction)txtEmailSend:(id)sender {
    
    if ([sender isEqual:self.txtRegisterEmail])
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
        self.txtRegisterEmail.leftView = paddingView;
        self.txtRegisterEmail.leftViewMode = UITextFieldViewModeAlways;
        
        if  (self.view.frame.origin.y == self.rectOrig.origin.y)
        {
            self.moveView = 230;
            [self setViewMovedUp:YES];
            
        }
    }
}
- (IBAction)txtDidEndEmailSend:(id)sender {
    
    if ((!self.txtRegisterEmail.text.length) > 0) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.txtRegisterEmail.leftView = paddingView;
        self.txtRegisterEmail.leftViewMode = UITextFieldViewModeAlways;
        [self.txtRegisterEmail.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
    }
    if  (self.view.frame.origin.y != self.rectOrig.origin.y)
    {
        self.moveView = 230;
        [self setViewMovedUp:NO];
    }
}


#pragma mark - Actions
- (void)goBack:(id)sender {
    //Redirigir a vista de ingreso
    LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    [self.navigationController presentViewController:login animated:YES completion:^{
        NSLog(@"Volviendo a la vista del login");
    }];
}
- (IBAction)sendEmail:(id)sender {

    if (self.txtRegisterEmail.text.length > 0) {
        
        self.txtRegisterEmail.enabled = NO;
        self.btnSendMail.enabled = NO;
        
        // Create the request.
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://45.33.116.10/forgetPassword.php"]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
       
        NSString *stringData = [NSString stringWithFormat:@"email=%@", self.txtRegisterEmail.text];
        NSData *myRequestData = [NSData dataWithBytes:[stringData UTF8String] length:[stringData length]];
        
        [request setHTTPBody:myRequestData];
        
        request.timeoutInterval = 20.0;
        
        self.hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        // Create url connection and fire request
        [NSURLConnection connectionWithRequest:request delegate:self];
        //NSURLConnection *con = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Debe ingresar su correo electronico"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}


#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _responseData = [[NSMutableData alloc] init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    self.hud.hidden = YES;
    
    NSError *error = nil;
    if (_responseData != nil) {
        
        id object = [NSJSONSerialization
                     JSONObjectWithData:_responseData
                     options:0
                     error:&error];
        
        NSDictionary *response;
        if([object isKindOfClass:[NSDictionary class]])
        {
            
            response = object;
            NSString *rtaFound = [response objectForKey:FOUND_KEY];
            NSString *rtaMjs = [response objectForKey:MSJ_KEY];
            
            if ([rtaFound isEqualToString:@"1"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Envío exitoso"
                                                                               message:rtaMjs
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   [self goBack:nil];
                                                               }];
                [alert addAction:accion];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Envío fallido"
                                                                               message:rtaMjs
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction *action) {
                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                               }];
                [alert addAction:accion];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Envío fallido"
                                                                       message:@"Imposible conectar"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
//    self.txtRegisterEmail.enabled = YES;
//    self.btnSendMail.enabled = YES;

}
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

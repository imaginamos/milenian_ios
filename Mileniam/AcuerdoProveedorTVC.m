//
//  AcuerdoProveedorTVC.m
//  Milenian
//
//  Created by Carlos Obregón on 25/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "AcuerdoProveedorTVC.h"
#import "IATproveedorCheckCellView.h"
#import "AppDelegate.h"
#import "IATProveedor.h"
#import "AGTSimpleCoreDataStack.h"
#import "NuevoAcuerdoVC.h"
#import "IATProductoProveedor.h"
#import "CacheImgs.h"

@interface AcuerdoProveedorTVC ()
@property (strong, nonatomic) NSArray *proveedores;
@property (strong, nonatomic) NSMutableArray *listCheckProvider;
@property (strong, nonatomic) IATProveedor *proveedorSeleccionado;
@property (strong, nonatomic) IATproveedorCheckCellView *cellCheck;
@property (strong, nonatomic) NSCache *cache;
@end

@implementation AcuerdoProveedorTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Seleccione proveedor";
    self.cache = [CacheImgs sharedInstance];
    UIImage *bntAcept = [UIImage imageNamed:@"chulo-ok"];
    
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithImage:bntAcept
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self action:@selector(acept)];
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnAcept.imageInsets = UIEdgeInsetsMake(12, 10, 6, 10);
    
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    self.navigationItem.hidesBackButton = YES;
    
    
    UIBarButtonItem *barBtnSalir = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(backView)];
    self.navigationItem.leftBarButtonItem = barBtnSalir;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.proveedores = [IATProveedor obtenerProveedoresWithContext:localVar.model.context];
    
    UINib *cellNib = [UINib nibWithNibName:@"IATproveedorCheckCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATproveedorCheckCellView cellId]];
    
}
                                    
- (void)backView{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)acept{
    
    NSLog(@"Retornando proveedor seleccionado");
    if (self.proveedorSeleccionado != nil) {
        self.nuevoAcuerdoVC.proveedor = self.proveedorSeleccionado;
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Debe seleccionar un proveedor"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.proveedores.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATproveedorCheckCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATproveedorCheckCellView cellId] forIndexPath:indexPath];
    
    IATProveedor *proveedor = [self.proveedores objectAtIndex:indexPath.row];
    
    cell.lblNombreProveedor.text = proveedor.nombre;
    
    CGRect checkbox_frame = CGRectMake(0, 0, 20, 18);
    UIImageView *ivAccessory = [[UIImageView alloc]initWithFrame:checkbox_frame];
    [ivAccessory setImage:[UIImage imageNamed:@"check-box-vacio-ListaProveedor.png"]];
    cell.accessoryView = ivAccessory;
    
    if (proveedor.urlLogo != nil && ![proveedor.urlLogo isEqualToString:@""]) {
        UIImage *imgLogo = [self.cache objectForKey:[NSURL URLWithString:proveedor.urlLogo]];
        if (imgLogo) {
            cell.logoProveedor.image = imgLogo;
        }
        else{
            [cell.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:proveedor.urlLogo] block:^(UIImage *image) {
                if (image != nil) {
                    cell.logoProveedor.image = image;
                }
                [cell.activityLoad stopAnimating];
            }];
        }
    }
    
    return cell;
}

//Img download
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}
#pragma mark - TableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [IATproveedorCheckCellView cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([[IATProductoProveedor filtrarSubcategoriasWithProveedor:[self.proveedores objectAtIndex:indexPath.row]] count] > 0) {
        
        IATproveedorCheckCellView *cell = [tableView cellForRowAtIndexPath:indexPath];

        
        CGRect checkbox_frame = CGRectMake(0, 0, 20, 18);
        UIImageView *ivAccessory = [[UIImageView alloc]initWithFrame:checkbox_frame];
        [ivAccessory setImage:[UIImage imageNamed:@"check-box-vacio-ListaProveedor.png"]];
        self.cellCheck.accessoryView = ivAccessory;
        
        if (self.proveedorSeleccionado == nil) {
            self.proveedorSeleccionado = [self.proveedores objectAtIndex:indexPath.row];
            CGRect checkbox_frame = CGRectMake(0, 0, 20, 18);
            UIImageView *ivAccessory = [[UIImageView alloc]initWithFrame:checkbox_frame];
            [ivAccessory setImage:[UIImage imageNamed:@"check-box-ListaProvedor.png"]];
            cell.accessoryView = ivAccessory;
            self.cellCheck = cell;
        }
        else{

            if (self.proveedorSeleccionado == [self.proveedores objectAtIndex:indexPath.row]) {
                CGRect checkbox_frame = CGRectMake(0, 0, 20, 18);
                UIImageView *ivAccessory = [[UIImageView alloc]initWithFrame:checkbox_frame];
                [ivAccessory setImage:[UIImage imageNamed:@"check-box-vacio-ListaProveedor.png"]];
                cell.accessoryView = ivAccessory;
                self.proveedorSeleccionado = nil;
                self.cellCheck = nil;
            }
            else{
                self.proveedorSeleccionado = [self.proveedores objectAtIndex:indexPath.row];
                CGRect checkbox_frame = CGRectMake(0, 0, 20, 18);
                UIImageView *ivAccessory = [[UIImageView alloc]initWithFrame:checkbox_frame];
                [ivAccessory setImage:[UIImage imageNamed:@"check-box-ListaProvedor.png"]];
                cell.accessoryView = ivAccessory;
                self.cellCheck = cell;
            }
        }
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Este mayorista, no tiene productos registrados aun, por favor seleccione otro."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

//
//  IATproveedorCheckCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 25/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATproveedorCheckCellView.h"

@implementation IATproveedorCheckCellView

+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 60.0f;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  RegisterVC.m
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "RegisterVC.h"
#import "LoginVC.h"
#import "NSString+MD5.h"
#import "Api.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "IATSectorEconomico.h"


@interface RegisterVC ()
@property(nonatomic, strong) UITapGestureRecognizer *scrollGestureRecognizer;
@property(nonatomic, strong) UIImagePickerController *controladorImagen;
@property(nonatomic, strong) NSManagedObjectContext *context;
@property(nonatomic, strong) NSArray *tipologias;
@property(nonatomic, strong) UIImage *logo;
@property(nonatomic, weak) IBOutlet UIImageView *logoClient;
@property(nonatomic) CGRect rectOrig;
@property(nonatomic) int moveView;
@property(nonatomic, strong) NSString *tipologia;
@property(nonatomic, strong) MBProgressHUD *hud;
@end

@implementation RegisterVC


#pragma mark - Ciclo de vida
- (void)viewDidLoad {
    [super viewDidLoad];
        
    //Titulo de la vista
    self.title = @"Registro";
    
    //Capturando frame de la vista
    self.rectOrig = self.view.frame;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    //Botón atrás - Navigation Bar
    UIImage *imgbtnBack = [UIImage imageNamed:@"icono-atras"];
    UIImage *imgbtnCheck = [UIImage imageNamed:@"chulo-ok"];
    @try{
        imgbtnBack = [imgbtnBack imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imgbtnCheck = [imgbtnCheck imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    @catch (NSException *exception){
    }
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:imgbtnBack
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    //Botón Aceptar - Navigation Bar
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithImage:imgbtnCheck
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(addUser)];
    
     //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnAcept.imageInsets = UIEdgeInsetsMake(10, 3, 2, 12);
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    //Creando, captura de gesto
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ocultaTeclado:)];
    [tapGesture setNumberOfTouchesRequired:1];
    [[self view] addGestureRecognizer:tapGesture];
    
    //self.txtSectorEconomico.inputView = self.pickerTipologia;
    self.txtSectorEconomico.delegate = self;
    self.txtCif.delegate = self;
    self.txtPasswd.delegate = self;
    self.txtCompanyEmail.delegate = self;
    self.txtNumberPhone.delegate = self;
    self.txtComercialName.delegate = self;
    self.txtAddress.delegate = self;
    self.pickerTipologia.delegate = self;
    self.pickerTipologia.dataSource = self;
    
    [self loadTipologias];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self ocultaTeclado:nil];
}


#pragma mark - Cargar tipologias
- (void)loadTipologias{
    
    NSString *keys = COOKIE_KEY;
    
    NSDictionary *params = @{@"keys":keys, @"firstTime":@"1"};
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.label.text = @"Descargando Tipologia";
    [self enableControls:NO];
    [self hideControls:YES];
    
    Api *api =[Api sharedInstance];
    
    [api obtenerTipologia:params success:^(BOOL success, id response) {
        if(success) {
            [self saveTipologias:response];
            self.tipologias = [IATSectorEconomico obtenerTipologiaWithContext:self.context];
            self.hud.hidden = YES;
            [self enableControls:YES];
            [self hideControls:NO];
            
            [self.pickerTipologia reloadAllComponents];
        }
        else{
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Error"
                                                message:@"No se cargaron las tipologias de negocio"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *accion =
            [UIAlertAction actionWithTitle:@"Aceptar"
                                     style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       self.hud.hidden = YES;
                                       [self enableControls:YES];
                                       [self hideControls:NO];
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
            
            [alert addAction:accion];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }];
    
    
}
- (void)saveTipologias:(NSDictionary *)data{
    
    NSArray *tipologias = [data objectForKey:@"SectorEconomic"];
    
    if ([tipologias isKindOfClass:[NSArray class]]){
        if (tipologias != nil && tipologias.count > 0) {
            for (id objeto in tipologias) {
                
                NSString *idTipologia, *nombre;
                
                idTipologia = [self changeNullToString:[objeto objectForKey:@"IdSector"]];
                nombre = [self changeNullToString:[objeto objectForKey:@"Name"]];
                
                if ([IATSectorEconomico obtenerTipologiaWithId:idTipologia context:self.context] == nil) {
                    [IATSectorEconomico tipologiaWithId:idTipologia nombre:nombre context:self.context];
                }
            }
        }
    }

    
    
}


#pragma mark - Movimiento de la vista
-(void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= self.moveView;
        rect.size.height += self.moveView;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += self.moveView;
        rect.size.height -= self.moveView;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
//Text Field CIF
- (IBAction)tfCIFEditingDidEnd:(id)sender {
    if (![self NSStringIsValidCIF:self.txtCif.text] && ![self NSStringIsValidNIF:self.txtCif.text]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Ingrese un CIF o NIF valido, CIF: 1letra + 8números, NIF:8números + 1letra."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (IBAction)tfComercialName:(id)sender {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
    self.txtComercialName.leftView = paddingView;
    self.txtComercialName.leftViewMode = UITextFieldViewModeAlways;
}
- (IBAction)tfComercialNameEDE:(id)sender {
    
    if (!(self.txtComercialName.text.length > 0)) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.txtComercialName.leftView = paddingView;
        self.txtComercialName.leftViewMode = UITextFieldViewModeAlways;
        [self.txtComercialName.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
    }
}

//Text Field Phone
-(IBAction)txtPhoneDidBeginEditing:(id)sender {
    if ([sender isEqual:self.txtNumberPhone])
    {
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 150;
            [self setViewMovedUp:NO];
        }
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 100;
            [self setViewMovedUp:YES];
        }
    }
}
-(IBAction)txtPhoneDidEndEditing:(id)sender {
    if ([sender isEqual:self.txtNumberPhone])
    {
        
        if  (self.view.frame.origin.y <= 0){
            self.moveView = 100;
            [self setViewMovedUp:NO];
        }
       
    }
}
//Text Field Dirección
-(IBAction)txtAddressDidBeginEditing:(id)sender {
    if ([sender isEqual:self.txtAddress])
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
        self.txtAddress.leftView = paddingView;
        self.txtAddress.leftViewMode = UITextFieldViewModeAlways;
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 150;
            [self setViewMovedUp:NO];
        }
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 150;
            [self setViewMovedUp:YES];
            
        }
    }
}
-(IBAction)txtAddressDidEndEditing:(id)sender {
    if ([sender isEqual:self.txtAddress])
    {
        if (!(self.txtCompanyEmail.text.length > 0)) {
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            self.txtAddress.leftView = paddingView;
            self.txtAddress.leftViewMode = UITextFieldViewModeAlways;
            [self.txtAddress.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
        }
        self.moveView = 150;
        [self setViewMovedUp:NO];
        
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if ([textField isEqual:self.txtSectorEconomico])
    {
        
        if (self.pickerTipologia.hidden) {
            [self ocultaTeclado:nil];
            self.moveView = 150;
            [self setViewMovedUp:YES];
            
            self.pickerTipologia.hidden = NO;
        }
        
    }
    else{
        return YES;
    }
    
    return NO;
}
-(IBAction)txtSectorEconomicoDidBeginEditing:(id)sender {
    
    
}
-(IBAction)txtSectorEconomicoDidEndEditing:(id)sender {

}
//Text Field Company Mail
-(IBAction)txtMailDidBeginEditing:(id)sender {
    if ([sender isEqual:self.txtCompanyEmail])
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 40)];
        self.txtCompanyEmail.leftView = paddingView;
        self.txtCompanyEmail.leftViewMode = UITextFieldViewModeAlways;
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 150;
            [self setViewMovedUp:NO];
        }
        
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 100;
            [self setViewMovedUp:YES];
        }
    }
}
-(IBAction)txtMailDidEndEditing:(id)sender {
    if ([sender isEqual:self.txtCompanyEmail])
    {
        if (!(self.txtCompanyEmail.text.length > 0)) {
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            self.txtCompanyEmail.leftView = paddingView;
            self.txtCompanyEmail.leftViewMode = UITextFieldViewModeAlways;
            [self.txtCompanyEmail.placeholder setAccessibilityFrame:CGRectMake(0, 0, 20, 20)];
        }
        if (![self NSStringIsValidEmail:self.txtCompanyEmail.text]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                           message:@"Ingrese un correo valido."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            [alert addAction:accion];
            [self presentViewController:alert animated:YES completion:nil];
        }
        self.moveView = 100;
        [self setViewMovedUp:NO];
        
    }
}
//Text Field Passwd
-(IBAction)txtPasswdDidBeginEditing:(id)sender {
    if ([sender isEqual:self.txtPasswd])
    {
        if (!self.pickerTipologia.hidden) {
            self.pickerTipologia.hidden = YES;
            
            self.moveView = 150;
            [self setViewMovedUp:NO];
        }
        if  (self.view.frame.origin.y >= 0)
        {
            self.moveView = 200;
            [self setViewMovedUp:YES];
            
        }
    }
}
-(IBAction)txtPasswdDidEndEditing:(id)sender {
    if ([sender isEqual:self.txtPasswd])
    {
        self.moveView = 200;
        [self setViewMovedUp:NO];
        
    }
}


#pragma mark - Acciones navigation bar
-(void)goBack:(id)sender {
    LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    [self.navigationController presentViewController:login animated:YES completion:^{
        NSLog(@"Volviendo a la vista del login");
    }];
}
-(BOOL)validateForm{
    if (![self NSStringIsValidCIF:self.txtCif.text] && ![self NSStringIsValidNIF:self.txtCif.text]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Ingrese un CIF o NIF valido, CIF: 1letra + 8números, NIF:8números + 1letra."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else if (![self NSStringIsValidEmail:self.txtCompanyEmail.text]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"Ingrese un Correo valido"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else if (self.txtPasswd.text.length < 8){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Advertencia"
                                                                       message:@"La contraseña debe tener mínimo 8 caracteres"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}
-(void)addUser{
    NSLog(@"Registrar usuario");
    
    if (self.txtPasswd.text.length > 0 && self.txtAddress.text.length > 0  && self.txtCif.text.length > 0  && self.txtComercialName.text.length > 0 && self.txtCompanyEmail.text.length > 0 && self.txtNumberPhone.text.length > 0 && self.txtSectorEconomico.text.length > 0) {
        
        //Validar Campos
        if ([self validateForm]) {
            NSString *passwithcookie = [NSString stringWithFormat:@"%@%@", COOKIE_KEY, self.txtPasswd.text];
            NSString *hash = [[NSString alloc] generateMD5:passwithcookie];
            
            //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //NSString *idCustomer = [defaults stringForKey:@"kIdCustomer"];
            NSString *keys = COOKIE_KEY;//[defaults stringForKey:@"kKey"];
            
            NSDictionary *params = @{@"cif":self.txtCif.text, @"nameCompany":self.txtComercialName.text, @"phoneCompany": self.txtNumberPhone.text, @"emailCompany": self.txtCompanyEmail.text, @"addressCompany": self.txtAddress.text , @"economySector": self.tipologia, @"passwd": hash, @"keys":keys};
            
            
            Api *api =[Api sharedInstance];
            
            [api registerUser:params success:^(BOOL success, id response) {
                if(success) {
                    
                    NSDictionary *dictionary = (NSDictionary *)response;
                    NSLog(@"%@", dictionary);
                    if ([[dictionary objectForKey:@"Registro Exitoso"] intValue] == 1) {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                                       message:@"Registro Exitoso"
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction *action) {
                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                                           [self goBack:nil];
                                                                       }];
                        [alert addAction:accion];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else{
                        NSString *error = [dictionary objectForKey:@"Error"];
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Registro Fallido"
                                                                                       message:error
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction *action) {
                                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       }];
                        [alert addAction:accion];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    
                }
                else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Registro Fallido"
                                                                                   message:@"Algunos datos ya existen"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction *action) {
                                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                                   }];
                    [alert addAction:accion];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }];
        }
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Datos incompletos"
                                                                       message:@"Debe completar cada campo requerido"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Cerrar"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:accion];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}
- (IBAction)mostrarTipologias:(id)sender {
    if (self.tipologias.count > 0){
        self.pickerTipologia.hidden = NO;
    }
    else{
        self.pickerTipologia.hidden = YES;
    }
}


#pragma mark - UIPickerView Delegado
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.tipologias.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component{
    
    IATSectorEconomico *sector = [self.tipologias objectAtIndex:row];
    return (NSString*)[sector nombre];
}
-(void)pickerView:(UIPickerView *)pickerView
     didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component{
    IATSectorEconomico *sector = [self.tipologias objectAtIndex:row];
    self.txtSectorEconomico.text = sector.nombre;
    self.tipologia = sector.idSector;
}


#pragma mark - Agregar logo
-(IBAction)addLogo:(id)sender {
    
    NSLog(@"Boton añadir logo presionado");
    self.controladorImagen =  [[UIImagePickerController alloc] init];
    self.controladorImagen.delegate = self;
    [self.controladorImagen setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:self.controladorImagen animated:YES completion:^{
        NSLog(@"Pasando a carrete");
    }];
    
}

#pragma mark - Deleagado de UIImagePickerController
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)img
                 editingInfo:(NSDictionary *)editInfo {
    
    self.logo = img;
    self.logoClient.image = img;
    
    [self.logoClient setContentMode:UIViewContentModeScaleAspectFill];
    [self.logoClient setClipsToBounds:YES];
    self.logoClient.layer.masksToBounds = YES;
    self.logoClient.layer.cornerRadius = self.logoClient.bounds.size.width/2;
    self.logoClient.layer.borderWidth = 1.0;
    self.logoClient.layer.borderColor = (__bridge CGColorRef)([UIColor lightGrayColor]);
    self.logoClient.hidden = NO;
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Volviendo a vista");
    }];
    
}



#pragma mark - Utilidades
//Validaciones
-(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL) NSStringIsValidCIF:(NSString *)checkString{
    NSString *cifFilterString = @"^[A-Za-z]{1}+[0-9]{8}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cifFilterString];
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL) NSStringIsValidNIF:(NSString *)checkString{
    NSString *nifFilterString = @"^[0-9]{8}+[A-Za-z]{1}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nifFilterString];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.txtCif]) {
        if ([[self.txtCif text] length] + [string length] - range.length > 9) {
            return NO;
        }
        else{
            return YES;
        }
    }
    else if ([textField isEqual:self.txtNumberPhone] || [textField isEqual:self.txtPasswd]){
        if ([[textField text] length] + [string length] - range.length > 20) {
            return NO;
        }
        else{
            return YES;
        }
    }
    else {
        if ([[textField text] length] + [string length] - range.length > 100) {
            return NO;
        }
        else{
            return YES;
        }
    }
}
//Formatear datos nulos
-(NSString *)changeNullToString:(NSString *)string{
    
    if ( string == (NSString *)[NSNull null] )
    {
        string = @"";
    }
    
    return string;
}
//Ocultar teclado
-(void)ocultaTeclado:(UITapGestureRecognizer *)sender{
    
    self.pickerTipologia.hidden = YES;
    
    [self.txtCif resignFirstResponder];
    [self.txtComercialName resignFirstResponder];
    [self.txtNumberPhone resignFirstResponder];
    [self.txtSectorEconomico resignFirstResponder];
    [self.txtCompanyEmail resignFirstResponder];
    [self.txtAddress resignFirstResponder];
    [self.txtPasswd resignFirstResponder];
    
    self.view.frame = self.rectOrig;
    
}
//Habilitar y deshabilitar controles
-(void)enableControls:(BOOL)enable{
    self.txtCif.enabled = enable;
    self.txtPasswd.enabled = enable;
    self.txtAddress.enabled = enable;
    self.txtNumberPhone.enabled = enable;
    self.txtCompanyEmail.enabled = enable;
    self.txtComercialName.enabled = enable;
    self.txtSectorEconomico.enabled = enable;
}
//Mostrar o ocultar controles
-(void)hideControls:(BOOL)enable{
    self.txtCif.hidden = enable;
    self.txtPasswd.hidden = enable;
    self.txtAddress.hidden = enable;
    self.txtNumberPhone.hidden = enable;
    self.txtCompanyEmail.hidden = enable;
    self.txtComercialName.hidden = enable;
    self.txtSectorEconomico.hidden = enable;
}
//Advertencia de memoria
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

//
//  IATVariedadCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 11/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATVariedadCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nombreVariedad;
@property (weak, nonatomic) IBOutlet UILabel *cantidadProductos;

+(NSString *) cellId;
+(CGFloat)cellHeight;
@end

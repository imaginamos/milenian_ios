// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATCategoria.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@class IATPromocion;
@class IATSubcategoria;

@interface IATCategoriaID : IATEntidadBaseID {}
@end

@interface _IATCategoria : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATCategoriaID*objectID;

@property (nonatomic, strong, nullable) NSString* urlImagen;

@property (nonatomic, strong, nullable) NSSet<IATPromocion*> *promociones;
- (nullable NSMutableSet<IATPromocion*>*)promocionesSet;

@property (nonatomic, strong, nullable) NSSet<IATSubcategoria*> *subcategorias;
- (nullable NSMutableSet<IATSubcategoria*>*)subcategoriasSet;

@end

@interface _IATCategoria (PromocionesCoreDataGeneratedAccessors)
- (void)addPromociones:(NSSet<IATPromocion*>*)value_;
- (void)removePromociones:(NSSet<IATPromocion*>*)value_;
- (void)addPromocionesObject:(IATPromocion*)value_;
- (void)removePromocionesObject:(IATPromocion*)value_;

@end

@interface _IATCategoria (SubcategoriasCoreDataGeneratedAccessors)
- (void)addSubcategorias:(NSSet<IATSubcategoria*>*)value_;
- (void)removeSubcategorias:(NSSet<IATSubcategoria*>*)value_;
- (void)addSubcategoriasObject:(IATSubcategoria*)value_;
- (void)removeSubcategoriasObject:(IATSubcategoria*)value_;

@end

@interface _IATCategoria (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveUrlImagen;
- (void)setPrimitiveUrlImagen:(NSString*)value;

- (NSMutableSet<IATPromocion*>*)primitivePromociones;
- (void)setPrimitivePromociones:(NSMutableSet<IATPromocion*>*)value;

- (NSMutableSet<IATSubcategoria*>*)primitiveSubcategorias;
- (void)setPrimitiveSubcategorias:(NSMutableSet<IATSubcategoria*>*)value;

@end

@interface IATCategoriaAttributes: NSObject 
+ (NSString *)urlImagen;
@end

@interface IATCategoriaRelationships: NSObject
+ (NSString *)promociones;
+ (NSString *)subcategorias;
@end

NS_ASSUME_NONNULL_END

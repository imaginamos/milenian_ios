//
//  Api.h
//  Neptune
//  Created by Leonardo Rodriguez on 1/5/16.
//

#import <AFNetworking/AFNetworking.h>

typedef void(^ResponseBlock)(BOOL success,id response);

@interface Api: AFHTTPSessionManager

+(instancetype)sharedInstance;
-(void)userLogin:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)loadCategories:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)loadProductos:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)registerUser:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)enviarAjustesPerfil:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)loadProveedores:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)loadPromociones:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)enviarPedido:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)obtenerClientes:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)obtenerContent:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)obtenerPedidos:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)enviarAcuerdo:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)obtenerAcuerdos:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)obtenerNotificaciones:(NSDictionary *)parameters success:(ResponseBlock)success;
-(void)obtenerTipologia:(NSDictionary *)parameters success:(ResponseBlock)success;

@end

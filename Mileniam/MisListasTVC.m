//
//  MisListasTVC.m
//  Milenian
//
//  Created by Carlos Obregón on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "MisListasTVC.h"
#import "PedidoRealizadoVC.h"
#import "DetalleListaVC.h"
#import "IATPedidosCellView.h"
#import "IATLista.h"
#import "IATItemLista.h"
#import "DetalleListaVC.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "KLCPopup/KLCPopup.h"
#import <QuartzCore/QuartzCore.h>

@interface MisListasTVC ()
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (nonatomic, strong) NSMutableArray *listas;
@property (nonatomic, retain) UITextField *nombreNuevaLista;
@end

@implementation MisListasTVC

#pragma mark - Ciclo de Vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Mis Listas";
    
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    //self.listas = [NSMutableArray array];
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATPedidosCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATPedidosCellView cellId]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.listas = [NSMutableArray arrayWithArray:[IATLista obtenerListasWithContext:self.context]];
    [self.tableView reloadData];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.listas.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IATLista *lista = [self.listas objectAtIndex:indexPath.row];
    
    IATPedidosCellView *cell =
    [tableView dequeueReusableCellWithIdentifier:[IATPedidosCellView cellId]];
    
    if (![lista.nombre isEqualToString:@""] && lista.nombre != nil) {
        cell.lblIdPedido.text = lista.nombre;
    }
    
    cell.lblCantidad.text = @"0";
    if (lista.precioTotal != nil) {
        cell.lblPrecioPedido.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[lista.precioTotal  doubleValue]]];
    }
    
    cell.lblFechaPedido.text = @"";
    
    cell.lblCantidad.text = @"0 productos";
    if (lista.items.count > 0  && lista.items != nil) {
        NSUInteger cantidad = lista.items.count;
        cell.lblCantidad.text = [NSString stringWithFormat:@"%lu productos", (unsigned long)cantidad];
    }
    
    return cell;
    
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IATLista *lista = [self.listas objectAtIndex:indexPath.row];
    
    DetalleListaVC *listaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detalleListaVC"];
    listaVC.lista = lista;
    
    [self.navigationController pushViewController:listaVC animated:YES];
    
    //PedidoRealizadoVC
    //PedidoRealizadoVC *pedido = [self.storyboard instantiateViewControllerWithIdentifier:@"pedidoRealizadoVC"];
    //[self.navigationController pushViewController:pedido animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)AgregarNuevaLista:(id)sender {
    
    UIView* contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 8.0;
    
    
    UILabel *tituloPopUp = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 290, 25)];
    tituloPopUp.backgroundColor = [UIColor clearColor];
    tituloPopUp.textColor = [UIColor blackColor];
    tituloPopUp.font = [UIFont boldSystemFontOfSize:16.0];
    tituloPopUp.text = @"Crear lista";
    tituloPopUp.textAlignment = NSTextAlignmentCenter;
    tituloPopUp.numberOfLines = 0;
    
    //UITextField *tfNameList = [[UITextField alloc] initWithFrame:CGRectMake(5, 45, 290, 50)];
    self.nombreNuevaLista = [[UITextField alloc] initWithFrame:CGRectMake(5, 45, 290, 50)];
//    tfNameList.placeholder = @"Ingrese el nombre de la lista";
//    [tfNameList setFont:[UIFont systemFontOfSize:15.0]];
//    [tfNameList setTextAlignment:NSTextAlignmentCenter];
    self.nombreNuevaLista.placeholder = @"Ingrese el nombre de la lista";
    [self.nombreNuevaLista setFont:[UIFont systemFontOfSize:15.0]];
    [self.nombreNuevaLista setTextAlignment:NSTextAlignmentCenter];
    [self.nombreNuevaLista setKeyboardType:UIKeyboardTypeDefault];
    [self.nombreNuevaLista setReturnKeyType:UIReturnKeyDone];
    [self.nombreNuevaLista setDelegate:self];
    
    UIButton* crearLista = [UIButton buttonWithType:UIButtonTypeCustom];
    crearLista.frame = CGRectMake(5, 105, 290, 40);
    //crearLista.translatesAutoresizingMaskIntoConstraints = NO;
    crearLista.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    crearLista.backgroundColor = [UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1.0f];
    [crearLista setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [crearLista setTitleColor:[[crearLista titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    crearLista.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [crearLista setTitle:@"Crear" forState:UIControlStateNormal];
    crearLista.layer.cornerRadius = 6.0;
    [crearLista addTarget:self action:@selector(crearLista:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.frame = CGRectMake(5, 150, 290, 40);
    //dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    dismissButton.backgroundColor = [UIColor colorWithRed:81/255.0f green:49/255.0f blue:136/255.0f alpha:1.0f];
    [dismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [dismissButton setTitle:@"Cerrar" forState:UIControlStateNormal];
    dismissButton.layer.cornerRadius = 6.0;
    [dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView addSubview:tituloPopUp];
    //[contentView addSubview:tfNameList];
    [contentView addSubview:self.nombreNuevaLista];
    [contentView addSubview:crearLista];
    [contentView addSubview:dismissButton];
    
    
    KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                            showType:KLCPopupShowTypeBounceInFromTop
                                         dismissType:KLCPopupDismissTypeFadeOut
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:NO
                               dismissOnContentTouch:NO];
    
    [popup show];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO; //Retorna YES si se permiten saltos de linea
}

- (void)dismissButtonPressed:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
}

- (void)crearLista:(id)sender {
    if (![self.nombreNuevaLista.text isEqualToString:@""]) {
        
        IATLista *nuevaLista = [IATLista ListaWithNombre:self.nombreNuevaLista.text context:self.context];
        self.nombreNuevaLista.text = @"";
        [(UIView*)sender dismissPresentingPopup];
        
        DetalleListaVC *listaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detalleListaVC"];
        listaVC.lista = nuevaLista;
        
        [self.navigationController pushViewController:listaVC animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //Tomo el elemento a elimar antes de que sea quitado
        IATLista *deleteLista = [self.listas objectAtIndex:indexPath.row];
        
        //Lo remuevo de la tabla
        [self.listas removeObjectAtIndex:indexPath.row];
        
        //Lo elimino de base de datos
        [self.context deleteObject:deleteLista];
        if([deleteLista isDeleted]) {
            
            NSError *errorInfo = nil;
            
            if([self.context save:&errorInfo]){
                
                NSLog(@"Actualizado el contexto;");
            }
            else{
                NSLog(@"Error al eliminar: %@", errorInfo);
            }
        }
        
        if (self.listas.count == 0) {
            //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        //Refresco la tabla
        [tableView reloadData];
    }
    
    
    
}
@end

//
//  ForgetPasswordVC.h
//  Mileniam
//
//  Created by Desarrollador IOS on 25/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;

#define FOUND_KEY @"Found"
#define MSJ_KEY @"Message"

@interface ForgetPasswordVC : UIViewController<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
}
@property (weak, nonatomic) IBOutlet UITextField *txtRegisterEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMail;

@end

//
//  IATPedidosCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 16/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATPedidosCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblIdPedido;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidad;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecioPedido;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaPedido;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorito;


+(NSString *) cellId;
+(CGFloat) cellHeight;
- (IBAction)MarcarPedidoComoFavorito:(id)sender;

@end

//
//  IATProveedoresCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 13/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATProveedoresCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *descripcion;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

+(NSString *) cellId;
+(CGFloat) cellHeight;

@end

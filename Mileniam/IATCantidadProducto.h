#import "_IATCantidadProducto.h"
@class IATProducto;

@interface IATCantidadProducto : _IATCantidadProducto

+(instancetype) productoWithCantidad:(NSNumber *)cantidad
                            producto:(IATProducto *)producto
                            variedad:(IATVariedad *)variedad
                       context:(NSManagedObjectContext *)context;

+(void) deleteAllWithContext:(NSManagedObjectContext *)context;

@end

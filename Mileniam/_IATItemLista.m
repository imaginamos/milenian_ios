// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATItemLista.m instead.

#import "_IATItemLista.h"

@implementation IATItemListaID
@end

@implementation _IATItemLista

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ItemLista" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ItemLista";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ItemLista" inManagedObjectContext:moc_];
}

- (IATItemListaID*)objectID {
	return (IATItemListaID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"cantidadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cantidad"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cantidad;

- (int32_t)cantidadValue {
	NSNumber *result = [self cantidad];
	return [result intValue];
}

- (void)setCantidadValue:(int32_t)value_ {
	[self setCantidad:@(value_)];
}

- (int32_t)primitiveCantidadValue {
	NSNumber *result = [self primitiveCantidad];
	return [result intValue];
}

- (void)setPrimitiveCantidadValue:(int32_t)value_ {
	[self setPrimitiveCantidad:@(value_)];
}

@dynamic precio;

@dynamic lista;

@dynamic producto;

@dynamic proveedor;

@end

@implementation IATItemListaAttributes 
+ (NSString *)cantidad {
	return @"cantidad";
}
+ (NSString *)precio {
	return @"precio";
}
@end

@implementation IATItemListaRelationships 
+ (NSString *)lista {
	return @"lista";
}
+ (NSString *)producto {
	return @"producto";
}
+ (NSString *)proveedor {
	return @"proveedor";
}
@end


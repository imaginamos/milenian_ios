//
//  SolicitudActualTVC.m
//  Milenian
//
//  Created by Carlos Obregón on 18/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "SolicitudActualTVC.h"
#import "IATVariedad.h"
#import "IATCategoria.h"
#import "IATSubcategoria.h"
#import "IATPedidoCellView.h"
#import "IATItemPedido.h"
#import "IATPedido.h"
#import "IATProducto.h"
#import "IATPromocion.h"
#import "IATCantidadProducto.h"
#import "IATProveedor.h"
#import "ItemSolicitudVC.h"
#import "ItemSolicitudPromoVC.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "Api.h"
#import "MBProgressHUD.h"
#import "CacheImgs.h"
#import "IATCellWithTableView.h"

@interface SolicitudActualTVC ()
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSMutableArray *itemsPedido;
@property (nonatomic, strong) NSArray *categorias, *mayoristas;
@property (nonatomic, strong) NSDecimalNumber *subtotal;
@property (nonatomic, strong) MBProgressHUD *hud;
@property NSCache *cache;
@end

@implementation SolicitudActualTVC

#pragma mark - Ciclo de vida
-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"Solicitud";
    self.cache = [CacheImgs sharedInstance];
    
    //Botón Atras
    UIBarButtonItem *myBackButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icono-atras"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(goBack:)];
    
    myBackButton.imageInsets = UIEdgeInsetsMake(11, -12, 11, 33);
    self.navigationItem.leftBarButtonItem = myBackButton;
    
    //Botón Aceptar
    UIImage *bntAcept = [UIImage imageNamed:@"chulo-ok"];
    UIBarButtonItem *barBtnAcept =
    [[UIBarButtonItem alloc] initWithImage:bntAcept style:UIBarButtonItemStylePlain target:self action:@selector(acept)];
    barBtnAcept.imageInsets = UIEdgeInsetsMake(12, 10, 6, 10);
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    //Obteniendo items del pedido
    self.itemsPedido = [[NSMutableArray alloc] initWithArray:[self.pedido.itemsPedido allObjects]];
    
    //Registrando celda personalizada
    UINib *cellNib = [UINib nibWithNibName:@"IATPedidoCellView" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:[IATPedidoCellView cellId]];
    
    //IATCellWithTableView
    UINib *nibCell = [UINib nibWithNibName:@"IATCellWithTableView" bundle:nil];
    [self.tableView registerNib:nibCell forCellReuseIdentifier:[IATCellWithTableView cellId]];
    
    //Obtener contexto
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
    
    //Agregando nueva funcionalidad clasificacion por Categoria/Mayorista
    if (self.itemsPedido.count > 0) {
        //[self seleccionarCategorias];//CATEGORIAS
        [self seleccionarMayoristas];//MAYORISTAS//Falta cuando es una promoción
        NSLog(@"%@", self.mayoristas);
    }//COMENTAR
    
    NSNotificationCenter *n = [NSNotificationCenter defaultCenter];
    [n addObserver:self selector:@selector(actualizarTabla:) name:ACTUALIZAR_TABLA_PADRE object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.tableView reloadData];
}


#pragma mark - NUEVO:Funcionalidad para filtrado por categorias
- (void)seleccionarCategorias{
    
    NSMutableArray *categorias = [NSMutableArray array];
    for (IATItemPedido *item in self.pedido.itemsPedido) {
        
        if(![categorias containsObject:item.producto.variedad.subcategoria.categoria]){
            [categorias addObject:item.producto.variedad.subcategoria.categoria];
        }
        
    }
    self.categorias = categorias;
}//Obtener Categorias del Pedido
- (void)seleccionarMayoristas{
    
    NSMutableArray *mayoristas = [NSMutableArray array];
    for (IATItemPedido *item in self.pedido.itemsPedido) {
        
        if(![mayoristas containsObject:item.proveedor]){
            [mayoristas addObject:item.proveedor];
        }
        
    }
    self.mayoristas = mayoristas;
}//Obtener Mayoristas del Pedido
- (void)actualizarTabla:(NSNotification *)notificacion{
    
    self.itemsPedido = [[NSMutableArray alloc] initWithArray:[self.pedido.itemsPedido allObjects]];
    if (self.itemsPedido.count > 0) {
        //[self seleccionarCategorias];//CATEGORIAS
        //NSLog(@"%@", self.categorias);
        [self seleccionarMayoristas];//MAYORISTAS
        NSLog(@"%@", self.mayoristas);
        [self.tableView reloadData];
    }
    else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}//Actualizar al editar un item

#pragma mark - Target/Actions
- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}//Ir atras
- (void)acept{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pedido"
                                                                   message:@"¿Esta seguro de realizar este pedido?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *accion =
    [UIAlertAction actionWithTitle:@"Aceptar"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                               
                               Api *api =[Api sharedInstance];
                               
                               NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                               NSString *keySesion = [defaults stringForKey:@"kKey"];
                               NSString *idCustomer = [defaults stringForKey:@"kIdCustomer"];
                               NSDate* now = [NSDate date];
                               
                               NSMutableArray *listProducts = [NSMutableArray array];
                               
                               for (int i = 0; i < self.itemsPedido.count; i++) {
                                   
                                   IATItemPedido *item = [self.itemsPedido objectAtIndex:i];
                                   
                                   NSDictionary *itemPedido;
                                   if (item.producto != nil){
                                       itemPedido = @{@"idProduct": item.producto.idEntity,
                                                      @"idProvider": item.proveedor.idEntity,
                                                      @"idVariety": item.producto.variedad.idEntity,
                                                      @"quantity": item.cantidad};
                                   }
                                   else{
                                       itemPedido = @{@"idProduct": item.promocion.idProducto,
                                                      @"idProvider": item.proveedor.idEntity,
                                                      @"idVariety": item.producto.variedad.idEntity,
                                                      @"quantity": item.cantidad};
                                   }
                                   NSLog(@"itemProducto %@",itemPedido);
                                   [listProducts addObject:itemPedido];
                               }
                               
                               NSError *error;
                               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:listProducts options:NSJSONWritingPrettyPrinted error:&error];
                               NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                               NSLog(@"jsonData as string:\n%@", jsonString);
                               NSString *numItems = [NSString stringWithFormat:@"%lu", (unsigned long)[listProducts count]];
                               
                               NSDictionary *parameters = @{@"dateRequest":now, @"keys":keySesion , @"idCustomer":idCustomer, @"listProducts":jsonString, @"numProducts":numItems};
                               
                               [api enviarPedido:parameters success:^(BOOL success, id response){
                                   if(success){
                                       
                                       self.pedido.fecha = now;
                                       
                                       NSArray *order = [response objectForKey:@"Orders"];
                                       if (order.count > 0) {
                                           NSString *idOrder = [[order objectAtIndex:0] objectForKey:@"OrderPrimary"];
                                           if (idOrder != nil && ![idOrder isEqualToString:@""]) {
                                               self.pedido.idPedido = idOrder;
                                               self.pedido.estado = @"Recibido";
                                           }
                                           else{
                                               self.pedido.idPedido = @"00";
                                               self.pedido.estado = @"NoRecibido";
                                           }
                                           
                                           
                                           NSString *referencia = [[order objectAtIndex:1] objectForKey:@"OrderReference"];
                                           if (referencia != nil && ![referencia isEqualToString:@""]){
                                               self.pedido.nombre = referencia;
                                           }
                                           else{
                                               self.pedido.nombre = @"Pedido no enviado";
                                           }
                                           
                                       }
                                       else{
                                           self.pedido.idPedido = @"00";
                                           self.pedido.estado = @"NoRecibido";
                                           self.pedido.nombre = @"Pedido no enviado";
                                       }
                                       
                                       
                                       for (IATItemPedido *item in self.itemsPedido) {
                                           item.producto.cantidadPedido.cantidad = 0;
                                           item.pedido = self.pedido;
                                       }
                                       
                                       //Limpiar Cantidades por producto
                                       //[IATCantidadProducto deleteAllWithContext:self.context];
                                       
                                       self.hud.hidden = YES;
                                       
                                       UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pedido realizado"
                                                                                                      message:@"Su pedido ha sido realizado con exito"
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                       UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                                        style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction *action) {
                                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                          self.navigationController.navigationBar.topItem.title = @"Categorias";
                                                                                          [self.navigationController popToRootViewControllerAnimated:YES];
                                                                                      }];
                                       [alert addAction:accion];
                                       [self presentViewController:alert animated:YES completion:nil];
                                       
                                   }
                                   else{
                                       self.hud.hidden = YES;
                                       //self.pedido.idPedido = @"00";
                                       //self.pedido.estado = @"NoRecibido";
                                       UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pedido fallido"
                                                                                                      message:@"Su pedido no se ha podido realizar con exito"
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                       UIAlertAction *accion = [UIAlertAction actionWithTitle:@"Aceptar"
                                                                                        style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction *action) {
                                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                      }];
                                       [alert addAction:accion];
                                       [self presentViewController:alert animated:YES completion:nil];
                                   }
                               }];
                           }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle:@"Cancelar"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:accion];
    [alert addAction:cancelar];
    [self presentViewController:alert animated:YES completion:nil];
    
}//Enviar pedido

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return 1;//DESCOMENTAR
    //return self.categorias.count + 1;//CATEGORIAS//COMENTAR
    return self.mayoristas.count + 1;//MAYORISTAS//COMENTAR
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return self.itemsPedido.count;//DESCOMENTAR
    
    if (section == 0) {
        return 0;
    }
    else{
        //Averiguar mayorista por categoria
        //IATCategoria *categoria = [self.categorias objectAtIndex:section-1];//CATEGORIAS//COMENTAR
        IATProveedor *proveedor = [self.mayoristas objectAtIndex:section-1];//COMENTAR
        NSArray *categorias = [IATItemPedido obtenerCategoriaPorMayorista:proveedor conPedido:self.pedido context:self.context];
        return categorias.count;
        
        //if (![categoria isEqual:@"Promociones"]){
            //NSArray *mayoristas = [IATItemPedido obtenerMayoristasPorCategoria:categoria conPedido:self.pedido context:self.context];//COMENTAR
            //return mayoristas.count;//COMENTAR
        //}
        //else{
        //    return 0;
        //}
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //COMENTAR
    //IATCategoria *categoria = [self.categorias objectAtIndex:indexPath.section-1];//COMENTAR
    IATProveedor *mayorista = [self.mayoristas objectAtIndex:indexPath.section-1];
    
    //NSArray *mayoristas = [IATItemPedido obtenerMayoristasPorCategoria:categoria conPedido:self.pedido context:self.context];//COMENTAR
    //IATProveedor *mayorista = [mayoristas objectAtIndex:indexPath.row];//COMENTAR
    
    NSArray *categorias = [IATItemPedido obtenerCategoriaPorMayorista:mayorista conPedido:self.pedido context:self.context];
    IATCategoria *categoria = [categorias objectAtIndex:indexPath.row];
    
    NSArray *items =
    [IATItemPedido obtenerProductosPorCategoria:categoria yMayorista:mayorista conPedido:self.pedido context:self.context];//COMENTAR
    
    //NSArray *items =
    
    IATCellWithTableView *cellTable = [tableView dequeueReusableCellWithIdentifier:[IATCellWithTableView cellId]];
    cellTable.cache = self.cache;
    cellTable.context = self.context;
    cellTable.mayorista = categoria.nombre;
    cellTable.productos = [NSMutableArray arrayWithArray:items];
    cellTable.navController = self.navigationController;
    cellTable.storyboard = self.storyboard;
    [cellTable asignarDelegado];
    
    
    IATPedidoCellView *cell = [tableView dequeueReusableCellWithIdentifier:[IATPedidoCellView cellId]];
    cell.lblNombreProducto.text = mayorista.nombre;//COMENTAR
    
    //DESCOMENTAR
    /*IATItemPedido *item = [self.itemsPedido objectAtIndex:indexPath.row];
     if (item.producto != nil) {
     cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.producto.nombre];
     cell.lblMarca.text = item.producto.marca;
     }
     else{
     cell.lblNombreProducto.text = [NSString stringWithFormat:@"%@", item.promocion.nombre];
     //cell.lblMarca.text = item.producto.marca;
     }
     
     cell.lblUnidad.text = [NSString stringWithFormat:@"Cantidad: %@", item.cantidad];
     cell.lblProveedor.text = [NSString stringWithFormat:@"%@", item.proveedor.nombre];
     cell.lblPrecioItem.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[item.precioTotal doubleValue]]];
     
     if (cell.imgProducto.image == nil) {
     
     NSURL *urlImg;
     if (item.producto != nil) {
     IATProducto *producto = item.producto;
     urlImg = [NSURL URLWithString:producto.urlImagen];
     }
     else{
     IATPromocion *promocion = item.promocion;
     urlImg = [NSURL URLWithString:promocion.urlImagen];
     }
     
     UIImage *imgProducto = [self.cache objectForKey:urlImg];
     if (imgProducto) {
     cell.imgProducto.image = imgProducto;
     [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
     }
     else{
     [cell.activityLoad startAnimating];
     
     cell.imgProducto.image = [UIImage imageNamed:@"page"];
     [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFill];
     [self imageWithUrl:urlImg block:^(UIImage *image) {
     if (image != nil) {
     cell.imgProducto.image = image;
     [cell.imgProducto setContentMode:UIViewContentModeScaleAspectFit];
     }
     [cell.activityLoad stopAnimating];
     }];
     }
     
     }*/
    return cellTable;//COMENTAR
    //return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//    IATItemPedido *itemPedido = [self.itemsPedido objectAtIndex:indexPath.row];
//    if (itemPedido.promocion == nil) {
//        ItemSolicitudVC *itemPedidoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"itemSolicitudVC"];
//        
//        itemPedidoVC.itemPedido = itemPedido;
//        [self.navigationController pushViewController:itemPedidoVC animated:YES];
//        //[self.navigationController presentViewController:itemPedidoVC animated:YES completion:nil];
//    }
//    else{
//        ItemSolicitudPromoVC *itemPedidoPromoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"itemSolicitudPromoVC"];
//        
//        itemPedidoPromoVC.itemPedido = itemPedido;
//        [self.navigationController pushViewController:itemPedidoPromoVC animated:YES];
//        //[self.navigationController presentViewController:itemPedidoPromoVC animated:YES completion:nil];
//        
//    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 100;
    }
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 100)];
        headerView.backgroundColor = [UIColor colorWithRed:81/255.0f green:49/255.0f blue:136/255.0f alpha:1.0f];
        
        UILabel *cantidadItems =
        [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.tableView.frame.size.width-20, 30)];
        [cantidadItems setTextColor:[UIColor whiteColor]];
        [cantidadItems setText:[NSString stringWithFormat:@"TOTAL %lu PRODUCTOS", (unsigned long)self.itemsPedido.count]];//DESCOMENTAR
        [cantidadItems setFont:[UIFont fontWithName:@"Arial" size:20]];
        [cantidadItems setTextAlignment:NSTextAlignmentCenter];
        
        UILabel *precioTotal =
        [[UILabel alloc] initWithFrame:CGRectMake(10, 40, self.tableView.frame.size.width-20, 58)];
        [precioTotal setTextColor:[UIColor whiteColor]];
        
        [precioTotal setText:[NSString stringWithFormat:@"%@",[self formatCurrencyValue:[[self calcularSubTotal] doubleValue]]]];
        [precioTotal setFont:[UIFont fontWithName:@"Arial" size:40]];
        [precioTotal setTextAlignment:NSTextAlignmentRight];
        
        [headerView addSubview:cantidadItems];
        [headerView addSubview:precioTotal];
        
        return headerView;
    }
    else{
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 50)];
        headerView.backgroundColor = [UIColor colorWithRed:0/255.0f green:173/255.0f blue:237/255.0f alpha:1.0f];
        
        //COMENTAR
        //IATCategoria *categoria = [self.categorias objectAtIndex:section-1];
        IATProveedor *mayorista = [self.mayoristas objectAtIndex:section-1];
        
        NSDecimalNumber *subtotal = [self calcularSubTotal:mayorista];
        
        UILabel *nombreCategoria =
        [[UILabel alloc] initWithFrame:CGRectMake(10, 3, self.tableView.frame.size.width-20, 25)];
        [nombreCategoria setTextColor:[UIColor whiteColor]];
        //[nombreCategoria setText:[NSString stringWithFormat:@"%@", categoria.nombre]];
        [nombreCategoria setText:[NSString stringWithFormat:@"%@", mayorista.nombre]];
        [nombreCategoria setFont:[UIFont fontWithName:@"Arial" size:18]];
        [nombreCategoria setTextAlignment:NSTextAlignmentCenter];
        
        UILabel *precioTotal =
        [[UILabel alloc] initWithFrame:CGRectMake(5, 25, self.tableView.frame.size.width-20, 25)];
        [precioTotal setTextColor:[UIColor whiteColor]];
        [precioTotal setText:[NSString stringWithFormat:@"%@",[self formatCurrencyValue:[subtotal doubleValue]]]];
        [precioTotal setFont:[UIFont fontWithName:@"Arial" size:16]];
        [precioTotal setTextAlignment:NSTextAlignmentRight];
        
        [headerView addSubview:nombreCategoria];
        [headerView addSubview:precioTotal];
        
        return headerView;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    //COMENTAR
    //IATCategoria *categoria = [self.categorias objectAtIndex:indexPath.section - 1];//COMENTAR
    IATProveedor *mayorista = [self.mayoristas objectAtIndex:indexPath.section - 1];
    
    NSArray *categorias = [IATItemPedido obtenerCategoriaPorMayorista:mayorista conPedido:self.pedido context:self.context];
    IATCategoria *categoria = [categorias objectAtIndex:indexPath.row];
    
    //NSArray *mayoristas = [IATItemPedido obtenerMayoristasPorCategoria:categoria conPedido:self.pedido context:self.context];//COMENTAR
    //IATProveedor *mayorista = [mayoristas objectAtIndex:indexPath.row];//COMENTAR
    
    NSArray *items = [IATItemPedido obtenerProductosPorCategoria:categoria yMayorista:mayorista conPedido:self.pedido context:self.context];//COMENTAR
    float numItems = 80.0f * items.count + 25;
    
    return numItems;
    //return [IATPedidoCellView cellHeight];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //Tomo el elemento a elimar antes de que sea quitado
        IATItemPedido *deleteItem = [self.itemsPedido objectAtIndex:indexPath.row];
        
        //Lo remuevo de la tabla
        [self.itemsPedido removeObjectAtIndex:indexPath.row];
        
        //Reseteo el item de Cantidad por producto
        deleteItem.producto.cantidadPedido.cantidad = 0;
        
        //Lo elimino de base de datos
        [self.context deleteObject:deleteItem];
        if([deleteItem isDeleted]) {
            
            NSError *errorInfo = nil;
            
            if([self.context save:&errorInfo]){
                
                NSLog(@"Actualizado el contexto;");
            }
            else{
                NSLog(@"Error al eliminar: %@", errorInfo);
            }
        }
        
        if (self.itemsPedido.count == 0) {
            //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        //Refresco la tabla
        [tableView reloadData];
    }
    
}


#pragma mark - Utilidades
-(NSString*) formatCurrencyValue:(double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}//Dar formato al precio
-(void)imageWithUrl:(NSURL *)urlImage block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}//Download Images
-(NSDecimalNumber *)calcularSubTotal{
    
    self.subtotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    
    for (IATItemPedido *preItem in self.itemsPedido) {
        
        self.subtotal = [self.subtotal decimalNumberByAdding:preItem.precioTotal];
        
    }
    
    return self.subtotal;
}//Calcular subtotal

-(NSDecimalNumber *)calcularSubTotal:(IATProveedor *)mayorista{
    
    NSDecimalNumber *subtotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    
    for (IATItemPedido *preItem in self.itemsPedido) {
        if (preItem.proveedor == mayorista) {
            subtotal = [subtotal decimalNumberByAdding:preItem.precioTotal];
        }
    }
    
    return subtotal;
}//Calcular subtotal

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

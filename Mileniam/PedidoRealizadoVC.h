//
//  PedidoRealizadoVC.h
//  Milenian
//
//  Created by Desarrollador IOS on 25/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IATPedido;

@interface PedidoRealizadoVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IATPedido *pedido;
@property (weak, nonatomic) IBOutlet UITableView *listProducts;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecioPedido;

- (IBAction)agregarListaAlPedidoActual:(id)sender;
@end

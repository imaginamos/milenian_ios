// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATPedido.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATItemPedido;

@interface IATPedidoID : NSManagedObjectID {}
@end

@interface _IATPedido : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATPedidoID*objectID;

@property (nonatomic, strong, nullable) NSString* estado;

@property (nonatomic, strong, nullable) NSDate* fecha;

@property (nonatomic, strong, nullable) NSString* idCliente;

@property (nonatomic, strong, nullable) NSString* idPedido;

@property (nonatomic, strong, nullable) NSString* nombre;

@property (nonatomic, strong, nullable) NSDecimalNumber* precioTotal;

@property (nonatomic, strong, nullable) NSSet<IATItemPedido*> *itemsPedido;
- (nullable NSMutableSet<IATItemPedido*>*)itemsPedidoSet;

@end

@interface _IATPedido (ItemsPedidoCoreDataGeneratedAccessors)
- (void)addItemsPedido:(NSSet<IATItemPedido*>*)value_;
- (void)removeItemsPedido:(NSSet<IATItemPedido*>*)value_;
- (void)addItemsPedidoObject:(IATItemPedido*)value_;
- (void)removeItemsPedidoObject:(IATItemPedido*)value_;

@end

@interface _IATPedido (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveEstado;
- (void)setPrimitiveEstado:(NSString*)value;

- (NSDate*)primitiveFecha;
- (void)setPrimitiveFecha:(NSDate*)value;

- (NSString*)primitiveIdCliente;
- (void)setPrimitiveIdCliente:(NSString*)value;

- (NSString*)primitiveIdPedido;
- (void)setPrimitiveIdPedido:(NSString*)value;

- (NSString*)primitiveNombre;
- (void)setPrimitiveNombre:(NSString*)value;

- (NSDecimalNumber*)primitivePrecioTotal;
- (void)setPrimitivePrecioTotal:(NSDecimalNumber*)value;

- (NSMutableSet<IATItemPedido*>*)primitiveItemsPedido;
- (void)setPrimitiveItemsPedido:(NSMutableSet<IATItemPedido*>*)value;

@end

@interface IATPedidoAttributes: NSObject 
+ (NSString *)estado;
+ (NSString *)fecha;
+ (NSString *)idCliente;
+ (NSString *)idPedido;
+ (NSString *)nombre;
+ (NSString *)precioTotal;
@end

@interface IATPedidoRelationships: NSObject
+ (NSString *)itemsPedido;
@end

NS_ASSUME_NONNULL_END

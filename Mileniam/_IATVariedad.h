// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATVariedad.h instead.

@import CoreData;

#import "IATEntidadBase.h"

NS_ASSUME_NONNULL_BEGIN

@class IATCantidadProducto;
@class IATPreItemAcuerdo;
@class IATProducto;
@class IATProductoProveedor;
@class IATSubcategoria;

@interface IATVariedadID : IATEntidadBaseID {}
@end

@interface _IATVariedad : IATEntidadBase
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATVariedadID*objectID;

@property (nonatomic, strong, nullable) NSSet<IATCantidadProducto*> *cantidadesProductos;
- (nullable NSMutableSet<IATCantidadProducto*>*)cantidadesProductosSet;

@property (nonatomic, strong, nullable) NSSet<IATPreItemAcuerdo*> *preItemsAcuerdo;
- (nullable NSMutableSet<IATPreItemAcuerdo*>*)preItemsAcuerdoSet;

@property (nonatomic, strong, nullable) NSSet<IATProducto*> *productos;
- (nullable NSMutableSet<IATProducto*>*)productosSet;

@property (nonatomic, strong, nullable) NSSet<IATProductoProveedor*> *productosProveedores;
- (nullable NSMutableSet<IATProductoProveedor*>*)productosProveedoresSet;

@property (nonatomic, strong) IATSubcategoria *subcategoria;

@end

@interface _IATVariedad (CantidadesProductosCoreDataGeneratedAccessors)
- (void)addCantidadesProductos:(NSSet<IATCantidadProducto*>*)value_;
- (void)removeCantidadesProductos:(NSSet<IATCantidadProducto*>*)value_;
- (void)addCantidadesProductosObject:(IATCantidadProducto*)value_;
- (void)removeCantidadesProductosObject:(IATCantidadProducto*)value_;

@end

@interface _IATVariedad (PreItemsAcuerdoCoreDataGeneratedAccessors)
- (void)addPreItemsAcuerdo:(NSSet<IATPreItemAcuerdo*>*)value_;
- (void)removePreItemsAcuerdo:(NSSet<IATPreItemAcuerdo*>*)value_;
- (void)addPreItemsAcuerdoObject:(IATPreItemAcuerdo*)value_;
- (void)removePreItemsAcuerdoObject:(IATPreItemAcuerdo*)value_;

@end

@interface _IATVariedad (ProductosCoreDataGeneratedAccessors)
- (void)addProductos:(NSSet<IATProducto*>*)value_;
- (void)removeProductos:(NSSet<IATProducto*>*)value_;
- (void)addProductosObject:(IATProducto*)value_;
- (void)removeProductosObject:(IATProducto*)value_;

@end

@interface _IATVariedad (ProductosProveedoresCoreDataGeneratedAccessors)
- (void)addProductosProveedores:(NSSet<IATProductoProveedor*>*)value_;
- (void)removeProductosProveedores:(NSSet<IATProductoProveedor*>*)value_;
- (void)addProductosProveedoresObject:(IATProductoProveedor*)value_;
- (void)removeProductosProveedoresObject:(IATProductoProveedor*)value_;

@end

@interface _IATVariedad (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableSet<IATCantidadProducto*>*)primitiveCantidadesProductos;
- (void)setPrimitiveCantidadesProductos:(NSMutableSet<IATCantidadProducto*>*)value;

- (NSMutableSet<IATPreItemAcuerdo*>*)primitivePreItemsAcuerdo;
- (void)setPrimitivePreItemsAcuerdo:(NSMutableSet<IATPreItemAcuerdo*>*)value;

- (NSMutableSet<IATProducto*>*)primitiveProductos;
- (void)setPrimitiveProductos:(NSMutableSet<IATProducto*>*)value;

- (NSMutableSet<IATProductoProveedor*>*)primitiveProductosProveedores;
- (void)setPrimitiveProductosProveedores:(NSMutableSet<IATProductoProveedor*>*)value;

- (IATSubcategoria*)primitiveSubcategoria;
- (void)setPrimitiveSubcategoria:(IATSubcategoria*)value;

@end

@interface IATVariedadRelationships: NSObject
+ (NSString *)cantidadesProductos;
+ (NSString *)preItemsAcuerdo;
+ (NSString *)productos;
+ (NSString *)productosProveedores;
+ (NSString *)subcategoria;
@end

NS_ASSUME_NONNULL_END

//
//  IATPerfilCell.m
//  Milenian
//
//  Created by Carlos Obregón on 24/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATPerfilCell.h"

@implementation IATPerfilCell
+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 70.0f;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATProveedor.m instead.

#import "_IATProveedor.h"

@implementation IATProveedorID
@end

@implementation _IATProveedor

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Proveedor" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Proveedor";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Proveedor" inManagedObjectContext:moc_];
}

- (IATProveedorID*)objectID {
	return (IATProveedorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic direccion;

@dynamic telefono;

@dynamic urlLogo;

@dynamic acuerdos;

- (NSMutableSet<IATAcuerdo*>*)acuerdosSet {
	[self willAccessValueForKey:@"acuerdos"];

	NSMutableSet<IATAcuerdo*> *result = (NSMutableSet<IATAcuerdo*>*)[self mutableSetValueForKey:@"acuerdos"];

	[self didAccessValueForKey:@"acuerdos"];
	return result;
}

@dynamic itemsLista;

- (NSMutableSet<IATItemLista*>*)itemsListaSet {
	[self willAccessValueForKey:@"itemsLista"];

	NSMutableSet<IATItemLista*> *result = (NSMutableSet<IATItemLista*>*)[self mutableSetValueForKey:@"itemsLista"];

	[self didAccessValueForKey:@"itemsLista"];
	return result;
}

@dynamic itemsPedido;

- (NSMutableSet<IATItemPedido*>*)itemsPedidoSet {
	[self willAccessValueForKey:@"itemsPedido"];

	NSMutableSet<IATItemPedido*> *result = (NSMutableSet<IATItemPedido*>*)[self mutableSetValueForKey:@"itemsPedido"];

	[self didAccessValueForKey:@"itemsPedido"];
	return result;
}

@dynamic preciosProductos;

- (NSMutableSet<IATProductoProveedor*>*)preciosProductosSet {
	[self willAccessValueForKey:@"preciosProductos"];

	NSMutableSet<IATProductoProveedor*> *result = (NSMutableSet<IATProductoProveedor*>*)[self mutableSetValueForKey:@"preciosProductos"];

	[self didAccessValueForKey:@"preciosProductos"];
	return result;
}

@dynamic promosProveedor;

- (NSMutableSet<IATPromoProveedor*>*)promosProveedorSet {
	[self willAccessValueForKey:@"promosProveedor"];

	NSMutableSet<IATPromoProveedor*> *result = (NSMutableSet<IATPromoProveedor*>*)[self mutableSetValueForKey:@"promosProveedor"];

	[self didAccessValueForKey:@"promosProveedor"];
	return result;
}

@end

@implementation IATProveedorAttributes 
+ (NSString *)direccion {
	return @"direccion";
}
+ (NSString *)telefono {
	return @"telefono";
}
+ (NSString *)urlLogo {
	return @"urlLogo";
}
@end

@implementation IATProveedorRelationships 
+ (NSString *)acuerdos {
	return @"acuerdos";
}
+ (NSString *)itemsLista {
	return @"itemsLista";
}
+ (NSString *)itemsPedido {
	return @"itemsPedido";
}
+ (NSString *)preciosProductos {
	return @"preciosProductos";
}
+ (NSString *)promosProveedor {
	return @"promosProveedor";
}
@end


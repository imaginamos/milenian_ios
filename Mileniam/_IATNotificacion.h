// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATNotificacion.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@interface IATNotificacionID : NSManagedObjectID {}
@end

@interface _IATNotificacion : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATNotificacionID*objectID;

@property (nonatomic, strong, nullable) NSString* date;

@property (nonatomic, strong) NSString* idNotificacion;

@property (nonatomic, strong, nullable) NSString* message;

@property (nonatomic, strong) NSString* nombre;

@end

@interface _IATNotificacion (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDate;
- (void)setPrimitiveDate:(NSString*)value;

- (NSString*)primitiveIdNotificacion;
- (void)setPrimitiveIdNotificacion:(NSString*)value;

- (NSString*)primitiveMessage;
- (void)setPrimitiveMessage:(NSString*)value;

- (NSString*)primitiveNombre;
- (void)setPrimitiveNombre:(NSString*)value;

@end

@interface IATNotificacionAttributes: NSObject 
+ (NSString *)date;
+ (NSString *)idNotificacion;
+ (NSString *)message;
+ (NSString *)nombre;
@end

NS_ASSUME_NONNULL_END

//
//  DetalleAcuerdoVC.m
//  Milenian
//
//  Created by Carlos Obregón on 18/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "DetalleAcuerdoVC.h"
#import "IATAcuerdo.h"
#import "IATProducto.h"
#import "IATItemAcuerdo.h"
#import "IATProveedor.h"
#import "IATItemAcuerdoCellVC.h"
#import "CacheImgs.h"

@interface DetalleAcuerdoVC ()
@property (nonatomic, strong) NSArray *productos;
@property (nonatomic, strong) NSArray *itemsAcuerdo;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation DetalleAcuerdoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Detalle de Acuerdo";
    
    self.cache = [CacheImgs sharedInstance];
    
    self.itemsAcuerdo = [self.acuerdo.itemsAcuerdo allObjects];
    
    self.lblNombreProveedor.text = self.acuerdo.provedor.nombre;
    
    self.tvComentarioAcuerdo.text = self.acuerdo.comentarios;
    
    NSString *fechaStringI = [NSString stringWithFormat:@"%@", self.acuerdo.fechaInicio];
    NSArray *arrayFechaI = [fechaStringI componentsSeparatedByString:@" "];
    
    NSString *fechaString = [NSString stringWithFormat:@"%@", self.acuerdo.fechaFinal];
    NSArray *arrayFecha = [fechaString componentsSeparatedByString:@" "];
    self.lblTiempoAcuerdo.text = [NSString stringWithFormat:@"%@ / %@", arrayFechaI[0],arrayFecha[0]];
    
    if (![self.acuerdo.provedor.urlLogo isEqualToString:@""] && self.acuerdo.provedor.urlLogo != nil) {
        
        UIImage *imgLogo = [self.cache objectForKey:[NSURL URLWithString:self.acuerdo.provedor.urlLogo]];
        if (imgLogo) {
            self.ivLogoProveedor.image = imgLogo;
        }
        else{
            [self.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:self.acuerdo.provedor.urlLogo] block:^(UIImage *image) {
                if (image != nil) {
                    self.ivLogoProveedor.image = image;
                }
                [self.activityLoad stopAnimating];
            }];
        }
        
        
    }
    
    self.lblCantidadProductos.text =
    [NSString stringWithFormat:@"%u productos", self.acuerdo.itemsAcuerdo.count];
    
    self.listAcuerdo.delegate = self;
    self.listAcuerdo.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"IATItemAcuerdoCellVC" bundle:nil];
    [self.listAcuerdo registerNib:cellNib forCellReuseIdentifier:[IATItemAcuerdoCellVC cellId]];
    
    self.productos = @[@"cell1", @"cell2", @"cell3"];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsAcuerdo.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IATItemAcuerdo *item = [self.itemsAcuerdo objectAtIndex:indexPath.row];
    
    
    IATItemAcuerdoCellVC *cellItem =
    [tableView dequeueReusableCellWithIdentifier:[IATItemAcuerdoCellVC cellId]];
    
    cellItem.lblNombre.text = item.producto.nombre;
    
    
    int porcent = 0;
    if (![item.porcentaje isEqualToString:@""] && item.porcentaje != nil) {
        
        porcent = [item.porcentaje intValue];
        
    }
    
    // Auto calculate radius
    [cellItem.porcentajePedido drawCircleWithPercent:porcent
                                            duration:2
                                           lineWidth:3
                                           clockwise:YES
                                             lineCap:kCALineCapRound
                                           fillColor:[UIColor clearColor]
                                         strokeColor:[UIColor colorWithRed:18/255.0f green:171/255.0f blue:192/255.0f alpha:1]//[UIColor blueColor]
                                      animatedColors:nil];
    cellItem.porcentajePedido.percentLabel.font = [UIFont systemFontOfSize:15];
    
    
    [cellItem.porcentajePedido startAnimation];
    
    
    
    
    cellItem.lblCantidad.text = [NSString stringWithFormat:@"Cantidad: %@/%@", item.consumido,item.cantidad];
    
    if (![item.producto.urlImagen isEqualToString:@""] && item.producto.urlImagen != nil) {
        
        UIImage *imgProducto = [self.cache objectForKey:[NSURL URLWithString:item.producto.urlImagen]];
        if (imgProducto) {
            cellItem.imgProducto.image = imgProducto;
        }
        else{
            [cellItem.activityLoad startAnimating];
            [self imageWithUrl:[NSURL URLWithString:item.producto.urlImagen] block:^(UIImage *image) {
                if (image != nil) {
                    cellItem.imgProducto.image = image;
                }
                [cellItem.activityLoad stopAnimating];
            }];

        }
        
    }
    
    
    return cellItem;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [IATItemAcuerdoCellVC cellHeight];
}

#pragma mark - Utility Download Images
-(void)imageWithUrl:(NSURL *)urlImage
              block:(void (^)(UIImage *image))completionBlock{
    
    dispatch_queue_t download = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(download, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:urlImage];
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.cache setObject:image forKey:urlImage];
                completionBlock(image);
            });
        }
        
    });
}


#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

//
//  CategoriesVC.h
//  Mileniam
//
//  Created by Desarrollador IOS on 26/01/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface CategoriesVC : UIViewController<iCarouselDataSource, iCarouselDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UITableView *listProducts;

@end

//
//  IATAcuerdosCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 16/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@class KNCirclePercentView;

@interface IATAcuerdosCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblCantidadProductos;
@property (weak, nonatomic) IBOutlet UILabel *lblPorcentaje;
@property (weak, nonatomic) IBOutlet KNCirclePercentView *porcentajeView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;


+(NSString *) cellId;
+(CGFloat) cellHeight;
@end

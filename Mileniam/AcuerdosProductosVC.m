//
//  AcuerdosProductosVC.m
//  Milenian
//
//  Created by Carlos Obregón on 26/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "AcuerdosProductosVC.h"
#import "ProductoVC.h"
#import "IATVariedad.h"
#import "IATProducto.h"
#import "IATItemAcuerdo.h"
#import "IATAcuerdo.h"
#import "IATPreItemAcuerdo.h"
#import "IATProductoAcuerdoCellCV.h"
#import "AppDelegate.h"
#import "AGTSimpleCoreDataStack.h"
#import "CacheImgs.h"

@interface AcuerdosProductosVC ()
@property (strong, nonatomic) NSArray *cantidadXproducto;
@property (nonatomic) NSDecimalNumber *subtotal;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation AcuerdosProductosVC

#pragma mark - Ciclo de vida
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.cache = [CacheImgs sharedInstance];
    NSString* fileSoundAdd = [[NSBundle mainBundle] pathForResource:@"Agregar-1" ofType:@"mp3"];
    NSURL* rutaFileSoundAdd = [[NSURL alloc]initFileURLWithPath:fileSoundAdd];
    self.soundAdd = [[AVAudioPlayer alloc]initWithContentsOfURL:rutaFileSoundAdd error:nil];
    
    self.soundAdd.volume = 0.4;
    self.soundAdd.numberOfLoops = 1;
    self.soundAdd.pan = 0;
    self.soundAdd.enableRate = YES;
    self.soundAdd.rate = 1;
    
    self.title = @"Productos";
    self.subtotal = [NSDecimalNumber decimalNumberWithString:@"0"];
    self.subTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
    
    UIImage *bntAcept = [UIImage imageNamed:@"chulo-ok"];
    
    UIBarButtonItem *barBtnAcept = [[UIBarButtonItem alloc] initWithImage:bntAcept
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self action:@selector(acept)];
    
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    barBtnAcept.imageInsets = UIEdgeInsetsMake(12, 10, 6, 10);
    
    self.navigationItem.rightBarButtonItem = barBtnAcept;
    
    
    //////////////// MODO ONLINE ////////////////
    UINib *nib = [UINib nibWithNibName:@"IATProductoAcuerdoCellCV" bundle:nil];
    [self.variedadCV registerNib:nib forCellWithReuseIdentifier:[IATProductoAcuerdoCellCV cellId]];
    
    //self.productos = [self.variedad.productos allObjects];
    
    /////////////////////////////////////////////
    
    
    self.variedadCV.delegate = self;
    self.variedadCV.dataSource = self;
    
    AppDelegate *localVar = [[UIApplication sharedApplication] delegate];
    self.context = localVar.model.context;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    NSNotificationCenter *n = [NSNotificationCenter defaultCenter];
    [n addObserver:self selector:@selector(actualizarSubTotal:) name:ACTUALIZAR_SUBTOTAL object:nil];
    [n addObserver:self selector:@selector(actualizarSubTotalDisminuir:) name:ACTUALIZAR_SUBTOTAL_REMOVE object:nil];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//Notificaciones Cambio de precio
-(void)actualizarSubTotal:(NSNotification *)notificacion{
    NSDictionary *dic = [notificacion userInfo];
    
    NSDecimalNumber *newProduct = [dic objectForKey:@"precio"];
    
    self.subtotal = [self.subtotal decimalNumberByAdding:newProduct];
    self.subTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
}
-(void)actualizarSubTotalDisminuir:(NSNotification *)notificacion{
    NSDictionary *dic = [notificacion userInfo];
    
    NSDecimalNumber *newProduct = [dic objectForKey:@"precio"];
    
    self.subtotal = [self.subtotal decimalNumberBySubtracting:newProduct];
    self.subTotal.text =
    [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.productos.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IATProductoAcuerdoCellCV *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:[IATProductoAcuerdoCellCV cellId]
                                              forIndexPath:indexPath];
    
    IATProducto *producto = [self.productos objectAtIndex:indexPath.row];
    cell.cache = self.cache;
    
    if(producto.preItemsAcuerdo.cantidad == nil){
        //IATPreItemAcuerdo *preItem = [IATPreItemAcuerdo preItemAcuerdoWithCantidad:[NSNumber numberWithInt:0] producto:producto variedad:self.variedad precio:nil context:self.context];
        
        IATPreItemAcuerdo *preItem = [IATPreItemAcuerdo preItemAcuerdoWithCantidad:[NSNumber numberWithInt:0] producto:producto proveedor:self.proveedor variedad:self.variedad precio:nil context:self.context];
        
        [cell observePreItemAcuerdo:preItem];
    }
    else{
        IATPreItemAcuerdo *preItem = producto.preItemsAcuerdo;
        [cell observePreItemAcuerdo:preItem];
    }
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    [self.soundAdd play];
//    
//    IATProducto *producto = [self.productos objectAtIndex:indexPath.row];
//    
//    int cantidad = [producto.preItemsAcuerdo.cantidad intValue];
//    cantidad++;
//    
//    producto.preItemsAcuerdo.cantidad = [NSNumber numberWithInt:cantidad];
//    
//    self.subtotal = [self.subtotal decimalNumberByAdding:producto.preItemsAcuerdo.precio];
//    self.subTotal.text = [NSString stringWithFormat:@"%@", [self formatCurrencyValue:[self.subtotal doubleValue]]];
    
}

- (void)acept{
    
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    
}

#pragma mark - Dar formato al precio
-(NSString*) formatCurrencyValue:(double)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setCurrencySymbol:@"€"];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *c = [NSNumber numberWithFloat:value];
    return [numberFormatter stringFromNumber:c];
}

#pragma mark - Advertencia de memoria
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

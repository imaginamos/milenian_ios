#import "_IATPromocion.h"

@interface IATPromocion : _IATPromocion

+(instancetype) promocionWithId:(NSString *)idPromocion
                         nombre:(NSString *)nombre
                    descripcion:(NSString *)descripcion
                   fechaInicial:(NSDate *)fechaInicial
                     fechaFinal:(NSDate *)fechaFinal
                         unidad:(NSString *)unidad
                       urlImage:(NSString *)urlImage
                        context:(NSManagedObjectContext *)context;

+(instancetype) obtenerPromocionWithId:(NSString *)idPromocion
                               context:(NSManagedObjectContext *)context;
+(NSArray *) obtenerPromocionesWithContext:(NSManagedObjectContext *)context;

@end

//
//  InfoCompanyVC.m
//  Milenian
//
//  Created by MacBook Pro on 16/02/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "InfoCompanyVC.h"
#import "SWRevealViewController.h"

@interface InfoCompanyVC ()

@end

@implementation InfoCompanyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *acercaDeNosotros = [defaults stringForKey:@"kAcerca"];
    
    [self.tvAcercaDeNosotros setText:acercaDeNosotros];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

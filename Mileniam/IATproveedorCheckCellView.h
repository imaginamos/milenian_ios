//
//  IATproveedorCheckCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 25/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATproveedorCheckCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblNombreProveedor;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

+(NSString *) cellId;
+(CGFloat) cellHeight;
@end

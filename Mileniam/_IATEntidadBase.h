// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATEntidadBase.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@interface IATEntidadBaseID : NSManagedObjectID {}
@end

@interface _IATEntidadBase : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATEntidadBaseID*objectID;

@property (nonatomic, strong, nullable) NSString* descripcion;

@property (nonatomic, strong) NSString* idEntity;

@property (nonatomic, strong) NSString* nombre;

@end

@interface _IATEntidadBase (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDescripcion;
- (void)setPrimitiveDescripcion:(NSString*)value;

- (NSString*)primitiveIdEntity;
- (void)setPrimitiveIdEntity:(NSString*)value;

- (NSString*)primitiveNombre;
- (void)setPrimitiveNombre:(NSString*)value;

@end

@interface IATEntidadBaseAttributes: NSObject 
+ (NSString *)descripcion;
+ (NSString *)idEntity;
+ (NSString *)nombre;
@end

NS_ASSUME_NONNULL_END

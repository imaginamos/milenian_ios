//
//  IATPromocionCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 12/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IATPromocionCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPromocion;
@property (weak, nonatomic) IBOutlet UILabel *nombrePromocion;
@property (weak, nonatomic) IBOutlet UILabel *nombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *marcaProducto;
@property (weak, nonatomic) IBOutlet UITextView *descripcionPromocion;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaInicial;
@property (weak, nonatomic) IBOutlet UILabel *lblFechaFinal;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

+(NSString *) cellId;
+(CGFloat) cellHeight;
@end

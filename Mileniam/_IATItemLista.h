// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATItemLista.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@class IATLista;
@class IATProducto;
@class IATProveedor;

@interface IATItemListaID : NSManagedObjectID {}
@end

@interface _IATItemLista : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATItemListaID*objectID;

@property (nonatomic, strong, nullable) NSNumber* cantidad;

@property (atomic) int32_t cantidadValue;
- (int32_t)cantidadValue;
- (void)setCantidadValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSDecimalNumber* precio;

@property (nonatomic, strong) IATLista *lista;

@property (nonatomic, strong) IATProducto *producto;

@property (nonatomic, strong) IATProveedor *proveedor;

@end

@interface _IATItemLista (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCantidad;
- (void)setPrimitiveCantidad:(NSNumber*)value;

- (int32_t)primitiveCantidadValue;
- (void)setPrimitiveCantidadValue:(int32_t)value_;

- (NSDecimalNumber*)primitivePrecio;
- (void)setPrimitivePrecio:(NSDecimalNumber*)value;

- (IATLista*)primitiveLista;
- (void)setPrimitiveLista:(IATLista*)value;

- (IATProducto*)primitiveProducto;
- (void)setPrimitiveProducto:(IATProducto*)value;

- (IATProveedor*)primitiveProveedor;
- (void)setPrimitiveProveedor:(IATProveedor*)value;

@end

@interface IATItemListaAttributes: NSObject 
+ (NSString *)cantidad;
+ (NSString *)precio;
@end

@interface IATItemListaRelationships: NSObject
+ (NSString *)lista;
+ (NSString *)producto;
+ (NSString *)proveedor;
@end

NS_ASSUME_NONNULL_END

//
//  DetalleListaVC.h
//  Milenian
//
//  Created by Carlos Obregón on 31/05/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IATLista;

@interface DetalleListaVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IATLista *lista;

@property (weak, nonatomic) IBOutlet UITableView *itemsLista;
@property (weak, nonatomic) IBOutlet UILabel *precioLista;


- (IBAction)agregarAlPedido:(id)sender;

@end

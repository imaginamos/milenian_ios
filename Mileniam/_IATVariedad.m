// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATVariedad.m instead.

#import "_IATVariedad.h"

@implementation IATVariedadID
@end

@implementation _IATVariedad

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Variedad" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Variedad";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Variedad" inManagedObjectContext:moc_];
}

- (IATVariedadID*)objectID {
	return (IATVariedadID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic cantidadesProductos;

- (NSMutableSet<IATCantidadProducto*>*)cantidadesProductosSet {
	[self willAccessValueForKey:@"cantidadesProductos"];

	NSMutableSet<IATCantidadProducto*> *result = (NSMutableSet<IATCantidadProducto*>*)[self mutableSetValueForKey:@"cantidadesProductos"];

	[self didAccessValueForKey:@"cantidadesProductos"];
	return result;
}

@dynamic preItemsAcuerdo;

- (NSMutableSet<IATPreItemAcuerdo*>*)preItemsAcuerdoSet {
	[self willAccessValueForKey:@"preItemsAcuerdo"];

	NSMutableSet<IATPreItemAcuerdo*> *result = (NSMutableSet<IATPreItemAcuerdo*>*)[self mutableSetValueForKey:@"preItemsAcuerdo"];

	[self didAccessValueForKey:@"preItemsAcuerdo"];
	return result;
}

@dynamic productos;

- (NSMutableSet<IATProducto*>*)productosSet {
	[self willAccessValueForKey:@"productos"];

	NSMutableSet<IATProducto*> *result = (NSMutableSet<IATProducto*>*)[self mutableSetValueForKey:@"productos"];

	[self didAccessValueForKey:@"productos"];
	return result;
}

@dynamic productosProveedores;

- (NSMutableSet<IATProductoProveedor*>*)productosProveedoresSet {
	[self willAccessValueForKey:@"productosProveedores"];

	NSMutableSet<IATProductoProveedor*> *result = (NSMutableSet<IATProductoProveedor*>*)[self mutableSetValueForKey:@"productosProveedores"];

	[self didAccessValueForKey:@"productosProveedores"];
	return result;
}

@dynamic subcategoria;

@end

@implementation IATVariedadRelationships 
+ (NSString *)cantidadesProductos {
	return @"cantidadesProductos";
}
+ (NSString *)preItemsAcuerdo {
	return @"preItemsAcuerdo";
}
+ (NSString *)productos {
	return @"productos";
}
+ (NSString *)productosProveedores {
	return @"productosProveedores";
}
+ (NSString *)subcategoria {
	return @"subcategoria";
}
@end


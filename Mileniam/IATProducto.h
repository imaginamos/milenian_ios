#import "_IATProducto.h"

@interface IATProducto : _IATProducto


+(instancetype) productoWithId:(NSString *)idProducto
                        nombre:(NSString *)nombre
                   descripcion:(NSString *)descripcion
                        unidad:(NSString *)unidad
                         marca:(NSString *)marca
                      urlImage:(NSString *)urlImage
                      variedad:(IATVariedad *)variedad
                 esRecomendado:(NSNumber *)esRecomendado
                       context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerProductosWithContext:(NSManagedObjectContext *)context;
+(instancetype) obtenerProductoWithId:(NSString *)idProducto
                              context:(NSManagedObjectContext *)context;
+(NSArray *) obtenerProductosRecomendadosWithContext:(NSManagedObjectContext *)context;

+(NSArray *) obtenerProductoWithPrename:(NSString *)prename
                                context:(NSManagedObjectContext *)context;

@end

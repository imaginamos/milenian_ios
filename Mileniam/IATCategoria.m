#import "IATCategoria.h"

@interface IATCategoria ()

// Private interface goes here.

@end

@implementation IATCategoria

+(instancetype) categoriaWithId:(NSString *)idCategoria
                       nombre:(NSString *)nombre
                  descripcion:(NSString *)descripcion
                     urlImage:(NSString *)urlImage
                      context:(NSManagedObjectContext *)context{
    
    IATCategoria *nCategoria = [NSEntityDescription insertNewObjectForEntityForName:[IATCategoria entityName]
                                                             inManagedObjectContext:context];
    
    nCategoria.idEntity = idCategoria;
    nCategoria.nombre = nombre;
    nCategoria.descripcion = descripcion;
    nCategoria.urlImagen = urlImage;
    
    return nCategoria;
    
}

+(NSArray *) obtenerCategoriasWithContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATCategoria entityName]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nombre" ascending:YES];
    [fetch setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if (array > 0)
    {
        return array;
    }
    
    return nil;
    
}

+(instancetype) obtenerCategoriaWithId:(NSString *)idCategoria
                               context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[IATCategoria entityName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idEntity == %@",idCategoria];
    [fetch setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetch error:&error];
    if ([array count] > 0)
    {
        return array[0];
    }
    
    return nil;
}

@end

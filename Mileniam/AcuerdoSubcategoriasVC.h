//
//  AcuerdoSubcategoriasVC.h
//  Milenian
//
//  Created by Carlos Obregón on 26/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

@import UIKit;
@class IATProveedor;
#import "iCarousel.h"

@interface AcuerdoSubcategoriasVC : UIViewController<iCarouselDataSource, iCarouselDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IATProveedor *proveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblSubcategorias;
@property (weak, nonatomic) IBOutlet UITableView *listSubcategories;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IATSectorEconomico.h instead.

@import CoreData;

NS_ASSUME_NONNULL_BEGIN

@interface IATSectorEconomicoID : NSManagedObjectID {}
@end

@interface _IATSectorEconomico : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IATSectorEconomicoID*objectID;

@property (nonatomic, strong) NSString* idSector;

@property (nonatomic, strong) NSString* nombre;

@end

@interface _IATSectorEconomico (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveIdSector;
- (void)setPrimitiveIdSector:(NSString*)value;

- (NSString*)primitiveNombre;
- (void)setPrimitiveNombre:(NSString*)value;

@end

@interface IATSectorEconomicoAttributes: NSObject 
+ (NSString *)idSector;
+ (NSString *)nombre;
@end

NS_ASSUME_NONNULL_END

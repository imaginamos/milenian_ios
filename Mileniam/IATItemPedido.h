#import "_IATItemPedido.h"
@class IATCategoria;
@interface IATItemPedido : _IATItemPedido

+(instancetype) itemPedidoWithCantidad:(NSNumber *)cantidad
                              producto:(IATProducto *)producto
                                pedido:(IATPedido *)pedido
                                precio:(NSDecimalNumber *)precio
                               context:(NSManagedObjectContext *)context;


+(instancetype) obtenerItemPedidoWithProducto:(IATProducto *)producto
                                      context:(NSManagedObjectContext *)context;

+(instancetype) itemPedidoWithCantidad:(NSNumber *)cantidad
                             promocion:(IATPromocion *)promocion
                                pedido:(IATPedido *)pedido
                                precio:(NSDecimalNumber *)precio
                               context:(NSManagedObjectContext *)context;


+(instancetype) obtenerItemPedidoWithPromocion:(IATPromocion *)promocion
                                       context:(NSManagedObjectContext *)context;

+(BOOL) existeItemPedidoWithProducto:(IATProducto *)producto
                             context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerMayoristasPorCategoria:(IATCategoria *)categoria
                                 conPedido:(IATPedido *)pedido
                                   context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerCategoriaPorMayorista:(IATProveedor *)proveedor
                                conPedido:(IATPedido *)pedido
                                  context:(NSManagedObjectContext *)context;

+(NSArray *) obtenerProductosPorCategoria:(IATCategoria *)categoria
                               yMayorista:(IATProveedor *)proveedor
                                conPedido:(IATPedido *)pedido
                                  context:(NSManagedObjectContext *)context;

@end

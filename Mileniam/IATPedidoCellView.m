//
//  IATPedidoCellView.m
//  Milenian
//
//  Created by Carlos Obregón on 18/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import "IATPedidoCellView.h"

@implementation IATPedidoCellView
+(NSString *) cellId{
    return NSStringFromClass(self);
}

+(CGFloat)cellHeight{
    return 80.0f;
}

-(void) prepareForReuse{

    self.imgProducto.image = nil;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

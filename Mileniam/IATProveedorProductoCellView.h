//
//  IATProveedorProductoCellView.h
//  Milenian
//
//  Created by Carlos Obregón on 15/04/16.
//  Copyright © 2016 imaginamos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IATProducto;

@interface IATProveedorProductoCellView : UITableViewCell
@property (strong, nonatomic) IATProducto *producto;
@property (weak, nonatomic) IBOutlet UIImageView *logoProveedor;
@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoad;

+(NSString *) cellId;
+(CGFloat) cellHeight;

-(void)imageWithUrl:(NSURL *)urlImage producto:(IATProducto *)producto block:(void (^)(UIImage *image))completionBlock;
-(void)imageWithUrl:(NSURL *)urlImage producto:(IATProducto *)producto cache:(NSCache *)cache block:(void (^)(UIImage *image))completionBlock;
@end
